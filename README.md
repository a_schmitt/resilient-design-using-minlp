# Resilient design of technical systems using MINLP

Code of the paper A Generic Optimization Framework for Resilient
Systems by Marc Pfetsch and Andreas Schmitt, see also the
[preprint](http://www.optimization-online.org/DB_HTML/2021/05/8416.html).

## Required software

- [SCIP](http://scip.zib.de)
- [SCIP-SDP](http://www.opt.tu-darmstadt.de/scipsdp)
- GSL - GNU Scientific Library (Ubuntu package libgsl-dev)

Optional:
- [MOSEK](https://www.mosek.com/) to solve problems involving SDP constraints more efficiently

## Installation

To run the code on Linux (tested: Ubuntu 18/20 with SCIP 7, SCIP-SDP  3.4) do the following.

1. Download [SCIP](http://scip.zib.de). You need at least version 7.

2. Install SCIP and compile it as described in the INSTALL file of SCIP's main
   directory with your individual settings; please use make (not cmake).
```
   make OPT=opt LPS=spx
```

3. Download [SCIP-SDP](http://www.opt.tu-darmstadt.de/scipsdp). You need at least version 3.2.

4. Install SCIP-SDP and compile it as described in the INSTALL file of SCIP's main
   directory with your individual settings; please use make (not cmake).
```
   make OPT=opt LPS=spx SDPS=msk
```

5. Set the environment variables SCIPSDPDIR to the SCIP-SDP main directory.

6. Compile the kresilience project using the same makefile options as for SCIP and SCIP-SDP.

7. The binary lies in the "bin" directory. It behaves like a normal
   SCIP instance, which can additionally read and solve the .int file
   format. This format specifies resilience problems as shown below.
   An exemplary usage is
```
   ./bin/scipkresilience -f instances/gas/gaslib11_k1.int -s settings/inexact_ConsRes_NoExtended_NoRand_NoResolve_NoReopt_FixBds.set
```
   Using the interactive mode of SCIP
```
   ./bin/scipkresilience
   SCIP> read instances/gas/gaslib11_k1.int
   SCIP> set load settings/inexact_ConsRes_NoExtended_NoRand_NoResolve_NoReopt_FixBds.set
   SCIP> opt
```




### Recommendations for problems without SDP constraints

Using ```SDPS=none``` in the compilation, we override the options of
SCIP-SDP. No parts of SCIP-SDP are included and SCIP is used
directly. This has the advantage, that instead of SDP-relaxations
LP-relaxations are solved. This is recomended for all instances not
involving SDP constraints.

## .int file format

To solve and specify resilience system design problems we use the .int
file format. Each line begins with a keyword to specify the purpose of
the following data. These indicators are

- Problem: specify a filename which corresponds to a problem. This
will model the main problem including objective function of the
resilience problem. Needs to be a file format supported by SCIP, e.g.,
.cip and resided in the same folder as the .int file.

- Feasibility_Problem: specify the set of recourse actions after
failure. A possibly specified objective function is ignored in the
following. Supports all file format supported by SCIP.

- K: specify the number of considered failures.

- Linking_Variables: total number of linking variables, i.e., the
number of variables x and y as used in the paper.

- interdictableVar: specify the name of a variable which occurs in
Problem as well as Feasibility_Problem and declares it to be a
failable design choice, i.e., a variable in x.

- noninterdictableVar: specify the name of a variable which occurs in
Problem as well as Feasibility_Problem and declares it to be a
non-failable design choice, i.e., a variable in y.

The lines for Problem, Feasibility_Problem, K and Linking_Variables
are always required. If ```Linking_Variables 0``` is used, all binary
variables within Problem are assumed to be interdictableVars.


## Settings

We employ SCIP to change solution parameters. To solve the problem
with the static scenario formulation one needs to use
```reader_int/useconskresilient = FALSE```. The dynamic scenario
formulation is used with ```cons_kresilient/useextendedcuts = TRUE```
(and ```reader_int/useconskresilient = TRUE```).

The programm searches for symmetry breaking constraints in the
problem. We assume, their name starts with ```symbreak```. If
```allowSymmetryInThirdLevel=TRUE``` such symmetry in the feasibility
problem are used within our algorithm. Otherwise, the program deletes
these constrains from the problem.

The different possibilities to derive tighter cuts can be controlled
with further parameters with the ```cons_kresilient``` prefix.

## Remarks

- If SCIP-SDP is used for instances not involving trusses, the
heuristic heur_maxtruss should not be used. Therefore its recommended
to comment out its inclusion in include_plugins_sdp.c.

- All instances used in the paper to evaluate the implementation can
be found within the folder instances. The water test rig instances use
a further fileformat. The respective reader builds the resilient
problem without usage of the .int format.


## Authors

Andreas Schmitt / [@a_schmitt](https://gitlab.com/a_schmitt)

## References

A Generic Optimization Framework for Resilient Systems, Marc
E. Pfetsch and Andreas Schmitt, 2021, preprint available at
[Optimization
Online](http://www.optimization-online.org/DB_HTML/2021/05/8416.html).