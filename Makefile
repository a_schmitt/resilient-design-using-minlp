#* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#*                                                                           */
#*    This file is part of the program kresilience                           */
#*                                                                           */
#*    Copyright (C) 2020-2021 Andreas Schmitt, Marc Pfetsch                  */
#*                                                                           */
#*                                                                           */
#*    Based on SCIP --- Solving Constraint Integer Programs                  */
#*       SCIP is distributed under the terms of the SCIP Academic Licence,   */
#*       see file COPYING in the SCIP distribution.                          */
#*                                                                           */
#* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#@file    Makefile
#@brief   Makefile for kresilience project
#@author  Andreas Schmitt

QUEUETYPE=srun
QUEUE=moskito
PPN=-1
NOWAITCLUSTER=1
EXCLUSIVE=true

GIT_VERSION := "$(shell git describe --dirty --always --tags)"

#-----------------------------------------------------------------------------
# path
#-----------------------------------------------------------------------------

ifndef SCIPSDPDIR
$(error SCIPSDPDIR is not set.)
endif

#-----------------------------------------------------------------------------
# include default project Makefile from SCIPSDP
#-----------------------------------------------------------------------------

include $(SCIPSDPDIR)/make/make.scipsdpproj
FLAGS+=-I$(SCIPSDPDIR)/src

#-----------------------------------------------------------------------------
# Main Program
#-----------------------------------------------------------------------------

MAINNAME	=	scipkresilience
CMAINOBJ	=  	cmain.o \
			reader_int.o \
			cons_kresilient.o \
			cons_interdiction.o \
			message_prefix.o \
			heur_maxtruss.o \
			reader_hr.o \
			model_potnet.o \
			sepa_2dCubeIndicator.o \
			sepa_1dquadindicator.o \
			min2dcube.o

# is SDPS is not changed to a solver (i.e. equals none), we use vanilla scip, otherwise we use scip-sdp
ifeq ($(SDPS),none)
CMAINOBJ    	+= include_plugins_scip.o
else
CMAINOBJ    	+= include_plugins_sdp.o
endif


MAINSRC		=	$(addprefix $(SRCDIR)/,$(CMAINOBJ:.o=.c))
MAINDEP		=	$(SRCDIR)/depend.cmain.$(OPT)

MAIN		=	$(MAINNAME).$(BASE).$(LPS).$(SDPS)$(EXEEXTENSION)
MAINFILE	=	$(BINDIR)/$(MAIN)
MAINSHORTLINK	=	$(BINDIR)/$(MAINNAME)
MAINOBJFILES	=	$(addprefix $(OBJDIR)/,$(CMAINOBJ))
MAINOBJFILES	+=	$(addprefix $(OBJDIR)/,$(CXXMAINOBJ))

#-----------------------------------------------------------------------------
# External libraries
#-----------------------------------------------------------------------------

FLAGS		+=
LDFLAGS		+=
GSLFLAGS	= -lgsl

LINT=flint

#-----------------------------------------------------------------------------
# Rules
#-----------------------------------------------------------------------------

# always rebuild cmain suchthat githash is updated
$(shell rm -f $(OBJDIR)/cmain.o)


ifeq ($(VERBOSE),false)
.SILENT:	$(MAINFILE) $(MAINOBJFILES) $(MAINSHORTLINK)
endif

.PHONY: all
all:            $(SCIPDIR) $(MAINFILE) $(MAINSHORTLINK)

.PHONY: lint
lint:		$(SCIPLIBSRC) $(OBJSCIPLIBSRC) $(LPILIBSRC) $(TPILIBSRC) $(NLPILIBSRC) $(MAINSRC) $(SYMSRC)
		-rm -f lint.out

		@$(SHELL) -ec 'if test -e lint/co-gcc.mak ; \
			then \
				echo "-> generating gcc-include-path lint-file" ; \
				cd lint; $(MAKE) -f co-gcc.mak ; \
			else \
				echo "-> lint Makefile not found"; \
			fi'
ifeq ($(FILES),)
		$(SHELL) -ec 'for i in $^; \
			do \
				echo $$i; \
				$(LINT) lint/main-gcc.lnt +os\(lint.out\) -u -zero \
				$(USRFLAGS) $(FLAGS) -I/usr/include -UNDEBUG -USCIP_WITH_READLINE -USCIP_ROUNDING_FE -D_BSD_SOURCE $$i; \
			done'
else
		$(SHELL) -ec  'for i in $(FILES); \
			do \
				echo $$i; \
				$(LINT) lint/main-gcc.lnt +os\(lint.out\) -u -zero \
				$(USRFLAGS) $(FLAGS) -I/usr/include -UNDEBUG -USCIP_WITH_READLINE -USCIP_ROUNDING_FE -D_BSD_SOURCE $$i; \
			done'
endif

.PHONY: scip
scip:
		@$(MAKE) -C $(SCIPDIR) libs $^

$(MAINSHORTLINK):	$(MAINFILE)
		@rm -f $@
		cd $(dir $@) && ln -s $(notdir $(MAINFILE)) $(notdir $@)

$(OBJDIR):
		@-mkdir -p $(OBJDIR)

$(BINDIR):
		@-mkdir -p $(BINDIR)

.PHONY: clean
clean:		$(OBJDIR)
ifneq ($(OBJDIR),)
		@-(rm -f $(OBJDIR)/*.o && rmdir $(OBJDIR));
		@echo "-> remove main objective files"
endif
		@-rm -f $(MAINFILE) $(MAINLINK) $(MAINSHORTLINK)
		@echo "-> remove binary"

.PHONY: test
test:           $(MAINFILE)
		cd check; \
		$(SHELL) ./check.sh $(TEST) $(EXECUTABLE) $(SETTINGS) $(BINID) $(OUTPUTDIR) $(TIME) $(NODES) $(MEM) $(THREADS) $(FEASTOL) $(DISPFREQ) $(CONTINUE) $(LOCK) "example" $(LPS) $(DEBUGTOOL) $(CLIENTTMPDIR) false $(OPTCOMMAND) $(SETCUTOFF) $(MAXJOBS) $(VISUALIZE) $(PERMUTE) $(SEEDS) $(GLBSEEDSHIFT);

.PHONY: testcluster
testcluster:
		cd check; \
      $(SHELL) ./check_cluster.sh $(TEST) $(EXECUTABLE) $(SETTINGS) \
      $(BINID) $(OUTPUTDIR) $(TIME) $(NODES) $(MEM) \
      $(THREADS) $(FEASTOL) $(LPS) $(DISPFREQ) $(CONTINUE) \
      $(QUEUETYPE) $(QUEUE) $(PPN) $(CLIENTTMPDIR) \
      $(NOWAITCLUSTER) $(EXCLUSIVE) $(PERMUTE) $(SEEDS) $(GLBSEEDSHIFT) $(DEBUGTOOL) $(REOPT) $(OPTCOMMAND) $(SETCUTOFF) $(VISUALIZE) $(CLUSTERNODES) "";

.PHONY: tags
tags:
		rm -f TAGS; ctags -e src/*.c src/*.h $(SCIPDIR)/src/scip/*.c $(SCIPDIR)/src/scip/*.h;

.PHONY: depend
depend:		$(SCIPDIR)
		$(SHELL) -ec '$(DCC) $(FLAGS) $(DFLAGS) $(MAINSRC) \
		| sed '\''s|^\([0-9A-Za-z\_]\{1,\}\)\.o *: *$(SRCDIR)/\([0-9A-Za-z\_]*\).c|$$\(OBJDIR\)/\2.o: $(SRCDIR)/\2.c|g'\'' \
		>$(MAINDEP)'

-include	$(MAINDEP)

# We use C++ (LINKCXX instead of LINKCC and so on) because of ? (its not GSL)
$(MAINFILE):	$(BINDIR) $(OBJDIR) $(SCIPLIBFILE) $(LPILIBFILE) $(NLPILIBFILE) $(MAINOBJFILES)
		@echo "-> linking $@"
		$(LINKCXX) $(MAINOBJFILES) $(LINKCXXSCIPSDPALL) $(LDFLAGS) $(GSLFLAGS) $(LINKCXX_o)$@

$(OBJDIR)/%.o:	$(SRCDIR)/%.c
		@echo "-> compiling $@"
		$(CC) $(FLAGS) $(OFLAGS) $(BINOFLAGS) $(CFLAGS) -DVERSION=\"$(GIT_VERSION)\" -c $< $(CC_o)$@

$(OBJDIR)/%.o:	$(SRCDIR)/%.cpp
		@echo "-> compiling $@"
		$(CXX) $(FLAGS) $(OFLAGS) $(BINOFLAGS) $(CXXFLAGS) -c $< $(CXX_o)$@

#---- EOF --------------------------------------------------------------------
