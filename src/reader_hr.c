/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*    This file is part of the program kresilience                           */
/*                                                                           */
/*    Copyright (C) 2020-2021 Andreas Schmitt, Marc Pfetsch                  */
/*                                                                           */
/*                                                                           */
/*    Based on SCIP --- Solving Constraint Integer Programs                  */
/*       SCIP is distributed under the terms of the SCIP Academic Licence,   */
/*       see file COPYING in the SCIP distribution.                          */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**@file   reader_hr.c
 * @brief  hr file reader
 * @author Andreas Schmitt
 *
 * The hr format specifies high rise water network instances.
 */

/*---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0----+----1----+----2*/

#define _GNU_SOURCE
#include <string.h>
#include <ctype.h>

#include "scip/scip.h"

#include <scip/cons_linear.h>
#include "scip/scipdefplugins.h"

#include "scip/var.h"

#include "scip/struct_scip.h"

#include "reader_hr.h"
#include "model_potnet.h"
#define READER_NAME             "hrreader"
#define READER_DESC             "file reader for HR (High rise) format"
#define READER_EXTENSION        "hr"



/*
 * Data structures
 */

/** pump data structure */
typedef struct Pump{
   SCIP_Real             prize;              /**< investment cost of the given pump */
   char*                 name;               /**< name of the given pump */
   SCIP_Real             coefEQQQ;           /**< coefficients of the energy consumption cubic in volume flow Q and
                                                pressure difference P:
                                                E(Q, P) = EQQQ * Q^3 + EQQP * Q^2 * P + ... + EPPP * P^3 + EQQ * Q^2 +
                                                ... + EP * P + Econst */
   SCIP_Real             coefEQQP;
   SCIP_Real             coefEQPP;
   SCIP_Real             coefEPPP;
   SCIP_Real             coefEQQ;
   SCIP_Real             coefEQP;
   SCIP_Real             coefEPP;
   SCIP_Real             coefEQ;
   SCIP_Real             coefEP;
   SCIP_Real             coefEconst;
   SCIP_Real             coefDHQQ;           /**< coefficients of the pressure increase quadratic n volume flow Q and
                                                rotation speed N:
                                                DH(Q, N) = DHQQ * Q^ + DHQN * Q * N + DHNN * N^2 */
   SCIP_Real             coefDHQN;
   SCIP_Real             coefDHNN;
   SCIP_Real             lbN;                /**< lower bound on the pump rotational speed */
   SCIP_Real             lbQ;                /**< lower bound on the pump volume flow */
   SCIP_Real             ubQ;                /**< upper bound on the pump volume flow */
   SCIP_Real             lbE;                /**< lower bound on the pump energy consumption */
   SCIP_Real             ubE;                /**< upper bound on the pump energy consumption */
   SCIP_Real             lbP;                /**< lower bound on the pump pressure increase */
   SCIP_Real             ubP;                /**< upper bound on the pump pressure increase */
} Pump;

/** highrise data structure */
typedef struct HighriseData{
   int                   nrFloors;           /**< number of floors in the building */
   SCIP_Real             qFloor;             /**< volume flow consumption of each floor */
   SCIP_Real             coefEnergy;         /**< coefficent for energy in the objective, e.g., Euro per kWh */
   int                   nrPumptypes;        /**< number of different pumps */
   Pump*                 pumps;              /**< the nrPumptypes pumps */
   int                   maxNrPumps;         /**< maximal number of pumps which may be placed in the building */
   int                   nrLoadcases;        /**< number of loadcase to fullfill */
   SCIP_Real*            probLoading;        /**< vector which gives for each loadcase its probaility */
   SCIP_Real*            qFloorLoading;      /**< the percentage of qFloor which has to be provided in each loadcase */
   SCIP_Real*            hLoading;           /**< has size #scenarios * #floors and gives the minpressure in floor i at
                                                scenario s given by [v*#scenarios + s] */
   int                   kFail;              /**< number of pump failures considered */
   SCIP_Real             qFloorFail;         /**< gives the percentage of volume flow demand needed in case of pump failure */
   SCIP_Real*            hLoadingFail;       /**< the corresponding pressure demand in each floor after failure */
} HighriseData;


/*
 * Local methods for reading/parsing
 */

/** given a pump structure, solve optimization problems over the set
    q in [lbQ, ubQ], p in [lbP, ubP], e in [lbE, ubE], n in [lbN, 1]
    p = DHQQ * q * q + DHQN * q * n + DHNN * n * n
    e >= EQQQ * q^3 + EQQP * q^2 * p + ... + EPPP * p^3 + EQQ * q^2 + ... + EP * P + Econst

    to obtain tighter bounds on all involved variables
 */
static
SCIP_RETCODE tightenPumpBounds(
   Pump*                 pump                /**< pump struct */
   )
{
   SCIP* scip;
   SCIP_CONS* cons;
   SCIP_VAR* var_Q;
   SCIP_VAR* var_P;
   SCIP_VAR* var_E;
   SCIP_VAR* var_QQ;
   SCIP_VAR* var_PP;
   SCIP_VAR* var_N;
   SCIP_VAR* vars[3];
   SCIP_Real lb[3];
   SCIP_Real ub[3];

   assert( pump != NULL );

   SCIP_CALL( SCIPcreate(&scip) );
   SCIP_CALL( SCIPcreateProb(scip, "OBBT", NULL, NULL, NULL, NULL, NULL, NULL, NULL) );
   SCIP_CALL( SCIPincludeDefaultPlugins(scip) );

   /* create model */
   SCIP_CALL( SCIPcreateVarBasic(scip, &vars[0], "Q", pump->lbQ, pump->ubQ, 0.0, SCIP_VARTYPE_CONTINUOUS) );
   SCIP_CALL( SCIPcreateVarBasic(scip, &vars[1], "P", 0.0, pump->ubP, 0.0, SCIP_VARTYPE_CONTINUOUS) );
   SCIP_CALL( SCIPcreateVarBasic(scip, &vars[2], "E", 0.0, pump->ubE, 0.0, SCIP_VARTYPE_CONTINUOUS) );
   SCIP_CALL( SCIPcreateVarBasic(scip, &var_QQ, "QQ", 0.0, pow(pump->ubQ, 2.0), 0.0, SCIP_VARTYPE_CONTINUOUS) );
   SCIP_CALL( SCIPcreateVarBasic(scip, &var_PP, "PP", 0.0, SCIPinfinity(scip), 0.0, SCIP_VARTYPE_CONTINUOUS) );
   SCIP_CALL( SCIPcreateVarBasic(scip, &var_N, "N", pump->lbN, 1.0, 0.0, SCIP_VARTYPE_CONTINUOUS) );
   SCIP_CALL( SCIPaddVar(scip, vars[0]) );
   SCIP_CALL( SCIPaddVar(scip, vars[1]) );
   SCIP_CALL( SCIPaddVar(scip, vars[2]) );
   SCIP_CALL( SCIPaddVar(scip, var_QQ) );
   SCIP_CALL( SCIPaddVar(scip, var_PP) );
   SCIP_CALL( SCIPaddVar(scip, var_N) );

   var_Q = vars[0];
   var_P = vars[1];
   var_E = vars[2];

   SCIP_CALL( SCIPcreateConsBasicQuadratic(scip, &cons, "linkQuadQ", 0, NULL, NULL, 0, NULL, NULL, NULL, 0.0, 0.0) );
   SCIP_CALL( SCIPaddLinearVarQuadratic(scip, cons, var_QQ, -1.0));
   SCIP_CALL( SCIPaddQuadVarQuadratic(scip, cons, var_Q, 0.0, 1.0) );
   SCIP_CALL( SCIPaddCons(scip, cons) );
   SCIP_CALL( SCIPreleaseCons(scip, &cons) );

   SCIP_CALL( SCIPcreateConsBasicQuadratic(scip, &cons, "linkQuadP", 0, NULL, NULL, 0, NULL, NULL, NULL, 0.0, 0.0) );
   SCIP_CALL( SCIPaddLinearVarQuadratic(scip, cons, var_PP, -1.0));
   SCIP_CALL( SCIPaddQuadVarQuadratic(scip, cons, var_P, 0.0, 1.0) );
   SCIP_CALL( SCIPaddCons(scip, cons) );
   SCIP_CALL( SCIPreleaseCons(scip, &cons) );

   SCIP_CALL( SCIPcreateConsBasicQuadratic(scip, &cons, "linkE", 0, NULL, NULL, 0, NULL, NULL, NULL, -pump->coefEconst, -pump->coefEconst) );
   SCIP_CALL( SCIPaddLinearVarQuadratic(scip, cons, var_E, -1.0) );
   SCIP_CALL( SCIPaddBilinTermQuadratic(scip, cons, var_QQ, var_Q, pump->coefEQQQ) );
   SCIP_CALL( SCIPaddBilinTermQuadratic(scip, cons, var_QQ, var_P, pump->coefEQQP) );
   SCIP_CALL( SCIPaddBilinTermQuadratic(scip, cons, var_PP, var_Q, pump->coefEQPP) );
   SCIP_CALL( SCIPaddBilinTermQuadratic(scip, cons, var_PP, var_P, pump->coefEPPP) );
   SCIP_CALL( SCIPaddLinearVarQuadratic(scip, cons, var_QQ, pump->coefEQQ) );
   SCIP_CALL( SCIPaddBilinTermQuadratic(scip, cons, var_Q, var_P, pump->coefEQP) );
   SCIP_CALL( SCIPaddLinearVarQuadratic(scip, cons, var_PP, pump->coefEPP) );
   SCIP_CALL( SCIPaddLinearVarQuadratic(scip, cons, var_Q, pump->coefEQ) );
   SCIP_CALL( SCIPaddLinearVarQuadratic(scip, cons, var_P, pump->coefEP) );
   SCIP_CALL( SCIPaddCons(scip, cons) );
   SCIP_CALL( SCIPreleaseCons(scip, &cons) );

   SCIP_CALL( SCIPcreateConsBasicQuadratic(scip, &cons, "linkP", 0, NULL, NULL, 0, NULL, NULL, NULL, 0.0, 0.0) );
   SCIP_CALL( SCIPaddLinearVarQuadratic(scip, cons, var_P, -1.0) );
   SCIP_CALL( SCIPaddBilinTermQuadratic(scip, cons, var_Q, var_N, pump->coefDHQN) );
   SCIP_CALL( SCIPaddLinearVarQuadratic(scip, cons, var_QQ, pump->coefDHQQ) );
   SCIP_CALL( SCIPaddQuadVarQuadratic(scip, cons, var_N, 0.0, pump->coefDHNN) );
   SCIP_CALL( SCIPaddCons(scip, cons) );
   SCIP_CALL( SCIPreleaseCons(scip, &cons) );

   SCIP_CALL( SCIPreleaseVar(scip, &var_QQ) );
   SCIP_CALL( SCIPreleaseVar(scip, &var_PP) );
   SCIP_CALL( SCIPreleaseVar(scip, &var_N) );

   /* solve problem for each bound */
   SCIPsetMessagehdlrQuiet(scip, TRUE);
   for(int i = 0; i < 3; ++i )
   {
      SCIP_CALL( SCIPchgVarObj(scip, vars[i], 1.0) );
      SCIP_CALL( SCIPsetObjsense(scip,  SCIP_OBJSENSE_MINIMIZE) );
      SCIP_CALL( SCIPsolve(scip) );
      lb[i] = SCIPgetPrimalbound(scip);
      SCIP_CALL( SCIPfreeTransform(scip) );

      SCIP_CALL( SCIPsetObjsense(scip,  SCIP_OBJSENSE_MAXIMIZE) );
      SCIP_CALL( SCIPsolve(scip) );
      ub[i] = SCIPgetPrimalbound(scip);
      SCIP_CALL( SCIPfreeTransform(scip) );

      SCIP_CALL( SCIPchgVarObj(scip, vars[i], 0.0) );
   }

   pump->lbQ = lb[0];
   pump->lbP = lb[1];
   pump->lbE = lb[2];
   pump->ubQ = ub[0];
   pump->ubP = ub[1];
   pump->ubE = ub[2];

   SCIP_CALL( SCIPreleaseVar(scip, &vars[0]) );
   SCIP_CALL( SCIPreleaseVar(scip, &vars[1]) );
   SCIP_CALL( SCIPreleaseVar(scip, &vars[2]) );

   SCIP_CALL( SCIPfree(&scip) );
   return SCIP_OKAY;
}

/** read a highrise instance from a .hr file */
static
SCIP_RETCODE readHighrise(
   SCIP*                  scip,              /**< pointer to scip structure */
   HighriseData*          highrise,          /**< pointer to highrise data structure */
   const char*            filename           /**< file of the highrise data */
   )
{
   SCIP_FILE* file;
   char buffer[SCIP_MAXSTRLEN];
   char* saveptr;
   char* token;
   int i;

   assert(scip != NULL);
   assert(highrise != NULL);
   assert(filename != NULL);

   if( NULL == (file = SCIPfopen(filename, "r")) )
   {
      SCIPerrorMessage("cannot open file <%s> for reading\n", filename);
      SCIPprintSysError(filename);
      return SCIP_NOFILE;
   }

   highrise->nrFloors = 0;
   highrise->nrLoadcases = 0;
   highrise->nrPumptypes = 0;
   highrise->kFail = 0;
   highrise->qFloorFail = -1;
   highrise->hLoadingFail = NULL;


   while( SCIPfgets(buffer, (int) sizeof(buffer), file) )
   {
      token = SCIPstrtok(buffer, " ", &saveptr);
      assert(token != NULL);

      if( strcmp(token, "nr_floors") == 0 )
      {
         highrise->nrFloors = atoi(saveptr);
      }
      else if( strcmp(token, "k_fail") == 0 )
      {
         highrise->kFail = atoi(saveptr);
      }
      else if( strcmp(token, "coef_energy") == 0 )
      {
         highrise->coefEnergy = atof(saveptr);
      }
      else if( strcmp(token, "q_floor") == 0 )
      {
         highrise->qFloor = atof(saveptr);
      }
      else if( strcmp(token, "max_nr_pumps") == 0 )
      {
         highrise->maxNrPumps = atoi(saveptr);
      }
      else if( strcmp(token, "nr_loadcases") == 0 )
      {
         if( highrise->nrFloors <= 0 )
         {
            fprintf(stderr, "Can not parse the nr of load cases since the number of floors is not given yet.\n");
            return SCIP_INVALIDDATA;
         }
         highrise->nrLoadcases = atoi(saveptr);
         SCIP_CALL( SCIPallocBlockMemoryArray(scip, &highrise->probLoading, highrise->nrLoadcases) );
         SCIP_CALL( SCIPallocBlockMemoryArray(scip, &highrise->qFloorLoading, highrise->nrLoadcases) );
         SCIP_CALL( SCIPallocBlockMemoryArray(scip, &highrise->hLoading, highrise->nrFloors * highrise->nrLoadcases) );
      }
      else if( strcmp(token, "prob_loading") == 0 )
      {
         if( highrise->nrLoadcases <= 0 )
         {
            fprintf(stderr, "Can not parse the load case probabilities since the number of load cases is not given yet.\n");
            return SCIP_INVALIDDATA;
         }
         i = 0;
         while( i < highrise->nrLoadcases && strlen(saveptr) >= 1 && token[0] != '\n' )
         {
            if( !SCIPstrToRealValue(saveptr, &highrise->probLoading[i], &saveptr) )
               return SCIP_READERROR;
            i++;
         }
      }
      else if( strcmp(token, "q_floor_rel_loading") == 0 )
      {
         if( highrise->nrLoadcases <= 0 )
         {
            fprintf(stderr, "Can not parse the load case flow distribution since the number of load cases is not given yet.\n");
            return SCIP_INVALIDDATA;
         }
         i = 0;
         while( i < highrise->nrLoadcases && strlen(saveptr) >= 1 && token[0] != '\n' )
         {
            if( !SCIPstrToRealValue(saveptr, &highrise->qFloorLoading[i], &saveptr) )
               return SCIP_READERROR;
            i++;
         }
      }
      else if( strcmp(token, "h_loading") == 0 )
      {
         if(highrise->nrLoadcases <= 0 )
         {
            fprintf(stderr, "Can not parse the load case probabilities since the number of load cases is not given yet.\n");
            return SCIP_INVALIDDATA;
         }
         i = 0;
         while( i < highrise->nrFloors * highrise->nrLoadcases && strlen(saveptr) >= 1 && token[0] != '\n' )
         {
            if( !SCIPstrToRealValue(saveptr, &highrise->hLoading[i], &saveptr) )
               return SCIP_READERROR;
            i++;
         }
      }
      else if( strcmp(token, "nr_pumptypes") == 0 )
      {
         highrise->nrPumptypes = atoi(saveptr);
         SCIP_CALL( SCIPallocBlockMemoryArray(scip, &highrise->pumps, highrise->nrPumptypes) );
         for( i = 0; i < highrise->nrPumptypes; ++i )
         {
            highrise->pumps[i].name = NULL;
         }
      }
      else if( strcmp(token, "name_pump") == 0 )
      {
         if( highrise->nrPumptypes <= 0 )
         {
            fprintf(stderr, "Can not parse the pump names since the number of pumps is not given yet.\n");
            return SCIP_INVALIDDATA;
         }
         i = 0;
         token = SCIPstrtok(saveptr, " ", &saveptr);
         while( token && i < highrise->nrPumptypes)
         {
            if( token[strlen(token)-1] == '\n' )
               token[strlen(token)-1]  = '\0';
            highrise->pumps[i].name = strdup(token);
            i++;
            token = SCIPstrtok(saveptr, " ", &saveptr);
         }
      }
      else if( strcmp(token, "coef_EQP") == 0 )
      {
         if( highrise->nrPumptypes <= 0 )
         {
            fprintf(stderr, "Can not parse the pump coefficients since the number of pumps is not given yet.\n");
            return SCIP_INVALIDDATA;
         }
         i = 0;
         token = SCIPstrtok(saveptr, " ", &saveptr);
         while( i < highrise->nrPumptypes && !(strcmp(token, highrise->pumps[i].name) == 0) )
         {
            i++;
         }
         if( i >= highrise->nrPumptypes )
         {
            fprintf(stderr, "Can not parse the pump coefficients since the the name %s has not been seen yet.\n", token);
            return SCIP_INVALIDDATA;

         }

         if( !SCIPstrToRealValue(saveptr, &highrise->pumps[i].coefEconst, &saveptr) )
            return SCIP_READERROR;
         if( !SCIPstrToRealValue(saveptr, &highrise->pumps[i].coefEP, &saveptr) )
            return SCIP_READERROR;
         if( !SCIPstrToRealValue(saveptr, &highrise->pumps[i].coefEQ, &saveptr) )
            return SCIP_READERROR;
         if( !SCIPstrToRealValue(saveptr, &highrise->pumps[i].coefEPP, &saveptr) )
            return SCIP_READERROR;
         if( !SCIPstrToRealValue(saveptr, &highrise->pumps[i].coefEQP, &saveptr) )
            return SCIP_READERROR;
         if( !SCIPstrToRealValue(saveptr, &highrise->pumps[i].coefEQQ, &saveptr) )
            return SCIP_READERROR;
         if( !SCIPstrToRealValue(saveptr, &highrise->pumps[i].coefEPPP, &saveptr) )
            return SCIP_READERROR;
         if( !SCIPstrToRealValue(saveptr, &highrise->pumps[i].coefEQPP, &saveptr) )
            return SCIP_READERROR;
         if( !SCIPstrToRealValue(saveptr, &highrise->pumps[i].coefEQQP, &saveptr) )
            return SCIP_READERROR;
         if( !SCIPstrToRealValue(saveptr, &highrise->pumps[i].coefEQQQ, &saveptr) )
            return SCIP_READERROR;
      }
      else if( strcmp(token, "coef_dP") == 0 )
      {
         if( highrise->nrPumptypes <= 0 )
         {
            fprintf(stderr, "Can not parse the pump coefficients since the number of pumps is not given yet.\n");
            return SCIP_INVALIDDATA;
         }
         i = 0;
         token = SCIPstrtok(saveptr, " ", &saveptr);
         while( i < highrise->nrPumptypes && !(strcmp(token, highrise->pumps[i].name) == 0) )
         {
            i++;
         }
         if( i >= highrise->nrPumptypes )
         {
            fprintf(stderr, "Can not parse the pump coefficients since the the name %s has not been seen yet.\n", token);
            return SCIP_INVALIDDATA;
         }

         if( !SCIPstrToRealValue(saveptr, &highrise->pumps[i].coefDHQQ, &saveptr) )
            return SCIP_READERROR;
         if( !SCIPstrToRealValue(saveptr, &highrise->pumps[i].coefDHQN, &saveptr) )
            return SCIP_READERROR;
         if( !SCIPstrToRealValue(saveptr, &highrise->pumps[i].coefDHNN, &saveptr) )
            return SCIP_READERROR;
      }
      else if( strcmp(token, "prize_pumptypes") == 0 )
      {
         if( highrise->nrPumptypes <= 0 )
         {
            fprintf(stderr, "Can not parse the pump costs since the number of pumps is not given yet.\n");
            return SCIP_INVALIDDATA;
         }
         i = 0;
         while( i < highrise->nrPumptypes && strlen(saveptr) >= 1 && token[0] != '\n' )
         {
            if( !SCIPstrToRealValue(saveptr, &highrise->pumps[i].prize, &saveptr) )
               return SCIP_READERROR;
            i++;
         }
      }
      else if( strcmp(token, "max_P") == 0 )
      {
         if( highrise->nrPumptypes <= 0 )
         {
            fprintf(stderr, "Can not parse the max energy consumption for pumps since the number of pumps is not given yet.\n");
            return SCIP_INVALIDDATA;
         }
         i = 0;
         while( i < highrise->nrPumptypes && strlen(saveptr) >= 1 && token[0] != '\n' )
         {
            if( !SCIPstrToRealValue(saveptr, &highrise->pumps[i].ubP, &saveptr) )
               return SCIP_READERROR;
            i++;
         }
      }
      else if( strcmp(token, "max_E") == 0 )
      {
         if( highrise->nrPumptypes <= 0 )
         {
            fprintf(stderr, "Can not parse the max energy consumption for pumps since the number of pumps is not given yet.\n");
            return SCIP_INVALIDDATA;
         }
         i = 0;
         while( i < highrise->nrPumptypes && strlen(saveptr) >= 1 && token[0] != '\n' )
         {
            if( !SCIPstrToRealValue(saveptr, &highrise->pumps[i].ubE, &saveptr) )
               return SCIP_READERROR;
            i++;
         }
      }
      else if( strcmp(token, "min_N_pump") == 0 )
      {
         if( highrise->nrPumptypes <= 0 )
         {
            fprintf(stderr, "Can not parse the min N for pumps since the number of pumps is not given yet.\n");
            return SCIP_INVALIDDATA;
         }
         i = 0;
         while( i < highrise->nrPumptypes && strlen(saveptr) >= 1 && token[0] != '\n' )
         {
            if( !SCIPstrToRealValue(saveptr, &highrise->pumps[i].lbN, &saveptr) )
               return SCIP_READERROR;
            i++;
         }
      }
      else if( strcmp(token, "max_Q_pump") == 0 )
      {
         if( highrise->nrPumptypes <= 0 )
         {
            fprintf(stderr, "Can not parse the max Q for pumps since the number of pumps is not given yet.\n");
            return SCIP_INVALIDDATA;
         }
         i = 0;
         while( i < highrise->nrPumptypes && strlen(saveptr) >= 1 && token[0] != '\n' )
         {
            if( !SCIPstrToRealValue(saveptr, &highrise->pumps[i].ubQ, &saveptr) )
               return SCIP_READERROR;
            i++;
         }
      }
      else if( strcmp(token, "min_Q_pump") == 0 )
      {
         if( highrise->nrPumptypes <= 0 )
         {
            fprintf(stderr, "Can not parse the min Q for pumps since the number of pumps is not given yet.\n");
            return SCIP_INVALIDDATA;
         }
         i = 0;
         while( i < highrise->nrPumptypes && strlen(saveptr) >= 1 && token[0] != '\n' )
         {
            if( !SCIPstrToRealValue(saveptr, &highrise->pumps[i].lbQ, &saveptr) )
               return SCIP_READERROR;
            i++;
         }
      }
      else if( strcmp(token, "q_floor_res") == 0 )
      {
         highrise->qFloorFail = atof(saveptr);
      }
      else if( strcmp(token, "h_loading_res") == 0 )
      {
         SCIP_CALL( SCIPallocBlockMemoryArray(scip, &highrise->hLoadingFail, highrise->nrFloors) );
         i = 0;
         while( i < highrise->nrFloors && strlen(saveptr) >= 1 && token[0] != '\n' )
         {
            if( !SCIPstrToRealValue(saveptr, &highrise->hLoadingFail[i], &saveptr) )
               return SCIP_READERROR;
            i++;
         }
         if( i < highrise->nrFloors )
         {
            fprintf(stderr, "Did not read enough values for the minimal pressure for resilience.\n");
            return SCIP_INVALIDDATA;
         }
      }
      else if( strlen(token) >= 1 && token[0] == 'c' )
      {
      }
      else
      {
         SCIPwarningMessage(scip, "Do not know identifier %s.\n", token);
      }
   }

   SCIPfclose(file);

   for( i = 0; i < highrise->nrPumptypes; ++i )
   {
      highrise->pumps[i].lbP = 0.0;
      highrise->pumps[i].lbE = 0.0;
      SCIP_CALL( tightenPumpBounds(&highrise->pumps[i]) );
   }

   printf("Kfail is %d\n", highrise->kFail);
   return SCIP_OKAY;
}


/** frees the memory which was allocated by function readHighrise */
static
void freeHighrise(
   SCIP*                   scip,             /**< pointer to scip */
   HighriseData*           highrise          /**< pointer to highrise data structure */
   )
{
   int i;
   assert(scip != NULL);
   assert(highrise != NULL);

   if( highrise->hLoadingFail != NULL )
      SCIPfreeBlockMemoryArray(scip, &highrise->hLoadingFail, highrise->nrFloors);
   for( i = 0; i < highrise->nrPumptypes; ++i )
   {
      if( highrise->pumps[i].name != NULL )
      {
         free(highrise->pumps[i].name);
      };
   }

   SCIPfreeBlockMemoryArray(scip, &highrise->pumps, highrise->nrPumptypes);
   SCIPfreeBlockMemoryArray(scip, &highrise->probLoading, highrise->nrLoadcases);
   SCIPfreeBlockMemoryArray(scip, &highrise->qFloorLoading, highrise->nrLoadcases);
   SCIPfreeBlockMemoryArray(scip, &highrise->hLoading, highrise->nrFloors * highrise->nrLoadcases);
}

/*
 * Callback methods of reader
 */

/** copy method for reader plugins (called when SCIP copies plugins) */
static
SCIP_DECL_READERCOPY(readerCopyHR)
{  /*lint --e{715}*/
   assert(scip != NULL);
   assert(reader != NULL);
   assert(strcmp(SCIPreaderGetName(reader), READER_NAME) == 0);

   /* call inclusion method of reader */
   SCIP_CALL( SCIPincludeReaderHR(scip) );

   return SCIP_OKAY;
}


/** problem reading method of reader */
static
SCIP_DECL_READERREAD(readerReadHR)
{  /*lint --e{715}*/

   HighriseData  highrise;
   Potnet  potentialnetwork;
   int nVertices;
   int nArcs;
   int a;
   int v;
   int s;
   int* indices;
   int nIndices;

   assert(reader != NULL);
   assert(strcmp(SCIPreaderGetName(reader), READER_NAME) == 0);
   assert(result != NULL);

   *result = SCIP_DIDNOTRUN;

   SCIP_CALL( readHighrise(scip, &highrise, filename) );

   SCIP_CALL( SCIPcreateProb(scip, filename, NULL, NULL, NULL, NULL, NULL, NULL, NULL) );

   /* create the potential network from the highrise data */

   nArcs = (highrise.nrPumptypes + 2) * highrise.nrFloors + (highrise.nrFloors * (highrise.nrFloors+1))/2;
   nVertices = 1 + 2 * highrise.nrFloors;

   SCIP_CALL( potnetInit(scip, &potentialnetwork, highrise.nrLoadcases, nArcs, nVertices) );
   for( s = 0; s < highrise.nrLoadcases; ++s )
   {
      potentialnetwork.probLoading[s] = highrise.probLoading[s];
   }
   potentialnetwork.coefEnergy = highrise.coefEnergy;

   SCIP_CALL( SCIPallocBufferArray(scip, &indices, nArcs) );

   /* we have a node for the basement */
   nVertices = 0;
   for( s = 0; s < highrise.nrLoadcases; ++s )
   {
      potentialnetwork.vertices[nVertices].lbP[s] = 0.0;
      potentialnetwork.vertices[nVertices].ubP[s] = 0.0;
      potentialnetwork.vertices[nVertices].lbQ[s] = -highrise.nrFloors * highrise.qFloor * highrise.qFloorLoading[s];
      potentialnetwork.vertices[nVertices].ubQ[s] = -highrise.nrFloors * highrise.qFloor * highrise.qFloorLoading[s];
   }
   potentialnetwork.vertices[nVertices].lbPFail = 0.0;
   potentialnetwork.vertices[nVertices].ubPFail = 0.0;
   potentialnetwork.vertices[nVertices].lbQFail = -highrise.nrFloors * highrise.qFloorFail;
   potentialnetwork.vertices[nVertices].ubQFail = -highrise.nrFloors * highrise.qFloorFail;
   (void) SCIPsnprintf(potentialnetwork.vertices[nVertices].name, SCIP_MAXSTRLEN, "basement");
   nVertices++;

   /* we have a node for each floor going in and out */
   for( v = 0; v < highrise.nrFloors; ++v )
   {
      for( s = 0; s < highrise.nrLoadcases; ++s )
      {
         potentialnetwork.vertices[nVertices].lbP[s] = 0.0;
         potentialnetwork.vertices[nVertices].ubP[s] = 10.0; /* TODO adapt bound? */
         potentialnetwork.vertices[nVertices].lbQ[s] = 0.0;
         potentialnetwork.vertices[nVertices].ubQ[s] = 0.0;
      }
      potentialnetwork.vertices[nVertices].lbPFail = 0.0;
      potentialnetwork.vertices[nVertices].ubPFail = 10.0; /* TODO adapt bound? */
      potentialnetwork.vertices[nVertices].lbQFail = 0.0;
      potentialnetwork.vertices[nVertices].ubQFail = 0.0;
      (void) SCIPsnprintf(potentialnetwork.vertices[nVertices].name, SCIP_MAXSTRLEN, "floor#%d#in", v);
      nVertices++;
      for( s = 0; s < highrise.nrLoadcases; ++s )
      {
         potentialnetwork.vertices[nVertices].lbP[s] = highrise.hLoading[v * highrise.nrLoadcases + s];
         potentialnetwork.vertices[nVertices].ubP[s] = 10.0; /* TODO adapt bound? */
         potentialnetwork.vertices[nVertices].lbQ[s] = highrise.qFloor * highrise.qFloorLoading[s];
         potentialnetwork.vertices[nVertices].ubQ[s] = highrise.qFloor * highrise.qFloorLoading[s];
      }
      potentialnetwork.vertices[nVertices].lbPFail = highrise.hLoadingFail[v];
      potentialnetwork.vertices[nVertices].ubPFail = 10.0; /* TODO adapt bound? */
      potentialnetwork.vertices[nVertices].lbQFail = highrise.qFloorFail;
      potentialnetwork.vertices[nVertices].ubQFail = highrise.qFloorFail;
      (void) SCIPsnprintf(potentialnetwork.vertices[nVertices].name, SCIP_MAXSTRLEN, "floor#%d#out", v);
      nVertices++;
   }

   /* on each floors we have arcs going from in to out representing pumps */
   nArcs = 0;
   for( a = 0; a < highrise.nrPumptypes; ++a )
   {
      nIndices = 0;
      for( v = 0; v < highrise.nrFloors; ++v )
      {
         potentialnetwork.arcs[nArcs].vIn = 1 + 2*v;
         potentialnetwork.arcs[nArcs].vOut = 1 + 2*v +1;
         potentialnetwork.arcs[nArcs].passive = FALSE;
         potentialnetwork.arcs[nArcs].emergencyUseOnly = FALSE;
         potentialnetwork.arcs[nArcs].prize = highrise.pumps[a].prize;

         potentialnetwork.arcs[nArcs].coefEQQQ = highrise.pumps[a].coefEQQQ;
         potentialnetwork.arcs[nArcs].coefEQQP = highrise.pumps[a].coefEQQP;
         potentialnetwork.arcs[nArcs].coefEQPP = highrise.pumps[a].coefEQPP;
         potentialnetwork.arcs[nArcs].coefEPPP = highrise.pumps[a].coefEPPP;
         potentialnetwork.arcs[nArcs].coefEQQ = highrise.pumps[a].coefEQQ;
         potentialnetwork.arcs[nArcs].coefEQP = highrise.pumps[a].coefEQP;
         potentialnetwork.arcs[nArcs].coefEPP = highrise.pumps[a].coefEPP;
         potentialnetwork.arcs[nArcs].coefEQ = highrise.pumps[a].coefEQ;
         potentialnetwork.arcs[nArcs].coefEP = highrise.pumps[a].coefEP;
         potentialnetwork.arcs[nArcs].coefEconst = highrise.pumps[a].coefEconst;
         potentialnetwork.arcs[nArcs].coefLbDpQQ = highrise.pumps[a].coefDHQQ;
         potentialnetwork.arcs[nArcs].coefLbDpQ = highrise.pumps[a].coefDHQN * highrise.pumps[a].lbN;
         potentialnetwork.arcs[nArcs].coefLbDpconst = highrise.pumps[a].coefDHNN * pow(highrise.pumps[a].lbN, 2.0);
         potentialnetwork.arcs[nArcs].coefUbDpQQ = highrise.pumps[a].coefDHQQ;
         potentialnetwork.arcs[nArcs].coefUbDpQ = highrise.pumps[a].coefDHQN;
         potentialnetwork.arcs[nArcs].coefUbDpconst = highrise.pumps[a].coefDHNN;
         potentialnetwork.arcs[nArcs].lbQ = highrise.pumps[a].lbQ;
         potentialnetwork.arcs[nArcs].ubQ = highrise.pumps[a].ubQ;
         potentialnetwork.arcs[nArcs].lbP = highrise.pumps[a].lbP;
         potentialnetwork.arcs[nArcs].ubP = highrise.pumps[a].ubP;
         potentialnetwork.arcs[nArcs].lbE = highrise.pumps[a].lbE;
         potentialnetwork.arcs[nArcs].ubE = highrise.pumps[a].ubE;

         (void) SCIPsnprintf(potentialnetwork.arcs[nArcs].name, SCIP_MAXSTRLEN, "Floor%d_%s", v, highrise.pumps[a].name);
         indices[nIndices] = nArcs;
         nArcs++;
         nIndices++;
      }
      indices[nIndices] = -1;
      SCIP_CALL( potnetAddIndicesActiveBound(scip, &potentialnetwork, indices, nIndices, 1) );
   }
   // we also have for each of those a possible bypass
   for( v = 0; v < highrise.nrFloors; ++v )
   {
      arcInitPipe(&potentialnetwork.arcs[nArcs], 1 + 2 * v, 1 + 2 * v + 1, (highrise.nrFloors - v)* highrise.qFloorFail);
      potentialnetwork.arcs[nArcs].passive = FALSE;
      potentialnetwork.arcs[nArcs].emergencyUseOnly = TRUE;
      (void) SCIPsnprintf(potentialnetwork.arcs[nArcs].name, SCIP_MAXSTRLEN, "Bypass_%s__%s", potentialnetwork.vertices[1+2*v].name, potentialnetwork.vertices[1 + 2 * v + 1].name);
      nArcs++;
   }

   // furthermore we have a possible pipe there
   for( v = 0; v < highrise.nrFloors; ++v )
   {
      arcInitPipe(&potentialnetwork.arcs[nArcs], 1 + 2 * v, 1 + 2 * v + 1, (highrise.nrFloors - v)* highrise.qFloor);
      (void) SCIPsnprintf(potentialnetwork.arcs[nArcs].name, SCIP_MAXSTRLEN, "Pipe_%s__%s", potentialnetwork.vertices[1+2*v].name, potentialnetwork.vertices[1 + 2 * v + 1].name);
      nArcs++;
      indices[0] = nArcs-1;
      // this pipe can only be bought if there are no pumps here
      for( a = 0; a < highrise.nrPumptypes; ++a )
      {
         indices[1] = a * highrise.nrFloors + v;
         indices[2] = -1;
         SCIP_CALL( potnetAddIndicesActiveBound(scip, &potentialnetwork, indices, 2, 1) );
      }
      indices[1] = highrise.nrPumptypes * highrise.nrFloors + v;
      SCIP_CALL( potnetAddIndicesActiveBound(scip, &potentialnetwork, indices, 2, 1) );
   }

   // there are arcs from each floor out to the other floor ins above
   for( v = 0; v < highrise.nrFloors; ++v )
   {
      nIndices = 0;
      for( a = v; a < highrise.nrFloors; ++a )
      {
         arcInitPipe(&potentialnetwork.arcs[nArcs], 2 * v, 1 + 2 * a, (highrise.nrFloors - v)* highrise.qFloor);
         (void) SCIPsnprintf(potentialnetwork.arcs[nArcs].name, SCIP_MAXSTRLEN, "%s__%s", potentialnetwork.vertices[2*v].name, potentialnetwork.vertices[2*a + 1].name);
         indices[nIndices] = nArcs;
         nArcs++;
         nIndices++;
      }
      indices[nIndices] = -1;
      SCIP_CALL( potnetAddIndicesActiveBound(scip, &potentialnetwork, indices, nIndices, 1) );
   }

   // we have an upper bound on the total number of built pumps
   nIndices = 0;
   for( a = 0; a < nArcs; ++a )
   {
      // pumps are the arcs which are active and have a positive price, to lazy to check the right indices
      if( !potentialnetwork.arcs[a].passive && potentialnetwork.arcs[a].prize > 0.0 )
      {
         indices[nIndices] = a;
         nIndices++;
      }
   }
   indices[nIndices] = -1;
   SCIP_CALL( potnetAddIndicesActiveBound(scip, &potentialnetwork, indices, nIndices, highrise.maxNrPumps) );

   SCIP_CALL( potnetCreateModel(scip, &potentialnetwork, highrise.kFail) );

   SCIPfreeBufferArray(scip, &indices);
   freeHighrise(scip, &highrise);
   potnetFree(scip, &potentialnetwork);

   *result = SCIP_SUCCESS;
   return SCIP_OKAY;
}





/*
 * reader specific interface methods
 */

/** includes the cip file reader in SCIP */
SCIP_RETCODE SCIPincludeReaderHR(
   SCIP*                 scip                /**< SCIP data structure */
   )
{
   SCIP_READER* reader;

   /* include reader */
   SCIP_CALL( SCIPincludeReaderBasic(scip, &reader, READER_NAME, READER_DESC, READER_EXTENSION, NULL) );

   /* set non fundamental callbacks via setter functions */
   SCIP_CALL( SCIPsetReaderCopy(scip, reader, readerCopyHR) );
   SCIP_CALL( SCIPsetReaderRead(scip, reader, readerReadHR) );

   return SCIP_OKAY;
}
