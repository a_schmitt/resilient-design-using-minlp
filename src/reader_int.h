/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*    This file is part of the program kresilience                           */
/*                                                                           */
/*    Copyright (C) 2020-2021 Andreas Schmitt, Marc Pfetsch                  */
/*                                                                           */
/*                                                                           */
/*    Based on SCIP --- Solving Constraint Integer Programs                  */
/*       SCIP is distributed under the terms of the SCIP Academic Licence,   */
/*       see file COPYING in the SCIP distribution.                          */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**@file   reader_int.h
 * @brief  interdiction file reader
 * @author Andreas Schmitt
 *
 * The int format specifies interdiction problems
 */

/*---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0----+----1----+----2*/

#ifndef __SCIP_READER_INT_H__
#define __SCIP_READER_INT_H__


#include "scip/scip.h"

#ifdef __cplusplus
extern "C" {
#endif

/** includes the int file reader into SCIP */
SCIP_EXPORT
SCIP_RETCODE SCIPincludeReaderInt(
   SCIP*                 scip                /**< SCIP data structure */
   );


/* includes to scip variables and constraints of scip_feas for each possible failure scenario with (exactly) k failures */
SCIP_EXPORT
SCIP_RETCODE SCIPReaderIntAddConstraintsVarForFailureScenarios(
   SCIP*                 scip,               /**< pointer to scip structure */
   int                   nvars,              /**< number of variables which have to be treated */
   SCIP_VAR**            vars_x,             /**< variables in scip */
   SCIP_VAR**            vars_x_feas,        /**< variables in scip_feas */
   SCIP_Bool*            vars_interdictable, /**< which of the variables are interdictable */
   SCIP*                 scip_feas,          /**< SCIP data structure for the subproblem */
   int                   k                   /**< number of possible interdictions */
   );


/* Reformulates the problem if there are packing constraint x_1 + ... + x_n <=
   1, where x_i are interdictable. If so, we include a variable y and constraint
   x_1 + ... + x_n <= y, where y is interdictable and x_i is now
   notinterdictable. */
SCIP_EXPORT
SCIP_RETCODE SCIPreformulateWithPacking(
   SCIP*                 scip,               /**< pointer to scip structure */
   int*                  nvars,              /**< number of variables which have to be treated */
   SCIP_VAR***           vars_x,             /**< variables in scip */
   SCIP_VAR***           vars_x_feas,        /**< variables in scip_feas */
   SCIP_Bool**           vars_interdictable, /**< which of the variables are interdictable */
   SCIP*                 scip_feas           /**< SCIP data structure for the subproblem */
   );

#ifdef __cplusplus
}
#endif

#endif
