/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*    This file is part of the program kresilience                           */
/*                                                                           */
/*    Copyright (C) 2020-2021 Andreas Schmitt, Marc Pfetsch                  */
/*                                                                           */
/*                                                                           */
/*    Based on SCIP --- Solving Constraint Integer Programs                  */
/*       SCIP is distributed under the terms of the SCIP Academic Licence,   */
/*       see file COPYING in the SCIP distribution.                          */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**@file   cmain.c
 * @brief  Main file
 * @author Andreas Schmitt
 */
#include <stdio.h>

#include "scip/scip.h"
#include "scip/scipshell.h"
#include "scip/scipdefplugins.h"

#include "cons_kresilient.h"
#include "reader_int.h"
#include "reader_hr.h"
#include "include_plugins.h"
#include "sepa_2dCubeIndicator.h"


#define DEFAULT_ALLOWSYMMETRY    TRUE

/** creates a SCIP instance with default plugins, evaluates command line parameters, runs SCIP appropriately,
 *  and frees the SCIP instance
 */
static
SCIP_RETCODE runShell(
   int                   argc,               /**< number of shell parameters */
   char**                argv,               /**< array with shell parameters */
   const char*           defaultsetname      /**< name of default settings file */
   )
{
   SCIP* scip = NULL;

   /*********
    * Setup *
    *********/

   printf("SCIP Resilience [GitHash: %s]\n\n", VERSION);

   /* initialize SCIP */
   SCIP_CALL( SCIPcreate(&scip) );

   /* we explicitly enable the use of a debug solution for this main SCIP instance */
   SCIPenableDebugSol(scip);

   /* include readers */
   SCIP_CALL( SCIPincludeReaderInt(scip) );
   SCIP_CALL( SCIPincludeReaderHR(scip) );

   SCIP_CALL( includeDefaultPlugins(scip) );

   SCIP_CALL( SCIPincludeConshdlrKResilient(scip) );

   SCIP_CALL( SCIPincludeSepaCubeIndicator(scip) );

   SCIP_CALL( SCIPaddBoolParam(scip, "allowSymmetryInThirdLevel",
         "Is symmetry for linked variables allowed in the lowest level. Otherwise we remove it",
         NULL, FALSE, DEFAULT_ALLOWSYMMETRY, NULL, NULL) );

   /* Process command line arguments */
   SCIP_CALL( SCIPprocessShellArguments(scip, argc, argv, defaultsetname) );

   /* Deinitialization */
   SCIP_CALL( SCIPfree(&scip) );

   BMScheckEmptyMemory();

   return SCIP_OKAY;
}

int
main(
   int                        argc,
   char**                     argv
   )
{
   SCIP_RETCODE retcode;

   retcode = runShell(argc, argv, "scip.set");
   if( retcode != SCIP_OKAY )
   {
      SCIPprintError(retcode);
      return -1;
   }

   return 0;
}
