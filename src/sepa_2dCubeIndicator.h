/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*    This file is part of the program kresilience                           */
/*                                                                           */
/*    Copyright (C) 2020-2021 Andreas Schmitt, Marc Pfetsch                  */
/*                                                                           */
/*                                                                           */
/*    Based on SCIP --- Solving Constraint Integer Programs                  */
/*       SCIP is distributed under the terms of the SCIP Academic Licence,   */
/*       see file COPYING in the SCIP distribution.                          */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**@file   sepa_2dCubeIndicator.h
 * @brief  assume a 2d cubic function together with indicator structure
 * @author Andreas Schmitt
 */

/*---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0----+----1----+----2*/

#ifndef __SCIP_SEPA_CUBEINDICATOR_H__
#define __SCIP_SEPA_CUBEINDICATOR_H__


#include "scip/scip.h"

#ifdef __cplusplus
extern "C" {
#endif

/** adds variables and the coefficients of cubic polynom to separate to the separator */
extern
SCIP_RETCODE SCIPSepaCubeIndicatorRegister(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_VAR*             varT,               /**< variable t */
   SCIP_VAR*             varX,               /**< variable x */
   SCIP_VAR*             varY,               /**< variable y */
   SCIP_VAR*             varZ,               /**< indicator variable*/
   SCIP_Real             coefXXX,            /**< coefficient of function */
   SCIP_Real             coefXXY,            /**< coefficient of function */
   SCIP_Real             coefXYY,            /**< coefficient of function */
   SCIP_Real             coefYYY,            /**< coefficient of function */
   SCIP_Real             coefXX,             /**< coefficient of function */
   SCIP_Real             coefXY,             /**< coefficient of function */
   SCIP_Real             coefYY,             /**< coefficient of function */
   SCIP_Real             coefX,              /**< coefficient of function */
   SCIP_Real             coefY,              /**< coefficient of function */
   SCIP_Real             coefC               /**< coefficient of function */
   );

/** creates the separator and includes it in SCIP */
extern
SCIP_RETCODE SCIPincludeSepaCubeIndicator(
   SCIP*                 scip                /**< SCIP data structure */
   );

#ifdef __cplusplus
}
#endif

#endif
