/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*    This file is part of the program kresilience                           */
/*                                                                           */
/*    Copyright (C) 2020-2021 Andreas Schmitt, Marc Pfetsch                  */
/*                                                                           */
/*                                                                           */
/*    Based on SCIP --- Solving Constraint Integer Programs                  */
/*       SCIP is distributed under the terms of the SCIP Academic Licence,   */
/*       see file COPYING in the SCIP distribution.                          */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**@file   include_plugins_sdp.c
 * @brief  defines includePlugins with the SCIP-SDP default plugins
 * @author Andreas Schmitt
 */

/*--+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0----+----1----+----2*/


#include "include_plugins.h"
#include "scipsdp/scipsdpdefplugins.h"
#include "heur_maxtruss.h"

SCIP_RETCODE includeDefaultPlugins(
   SCIP*                   scip              /**<pointer to scip */
   )
{
   SCIP_CALL( SCIPSDPincludeDefaultPlugins(scip) );
   SCIP_CALL( SCIPincludeHeurMaxTruss(scip) );

   /* also set specific settings */
   /*  Leads to high objective coefficients, leading to numerical instabilities, e.g. mosek returning infeasible */
   SCIP_CALL( SCIPsetBoolParam(scip, "misc/scaleobj", FALSE) );

   /* mosek has a too coarse tolerance here. This lead to a result of 10^-5, which cut off a 0 solution. Finer
    *tolerances lead to anoter iteration with result 10^-9 */
   SCIP_CALL( SCIPsetRealParam(scip, "relaxing/SDP/sdpsolvergaptol", 1e-6) );

   return SCIP_OKAY;
}
