/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*    This file is part of the program kresilience                           */
/*                                                                           */
/*    Copyright (C) 2020-2021 Andreas Schmitt, Marc Pfetsch                  */
/*                                                                           */
/*                                                                           */
/*    Based on SCIP --- Solving Constraint Integer Programs                  */
/*       SCIP is distributed under the terms of the SCIP Academic Licence,   */
/*       see file COPYING in the SCIP distribution.                          */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**@file   model_potnet.h
 * @brief  functions to create the optimization problem of a potential network
 * @author Andreas Schmitt
 */

/*--+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0----+----1----+----2*/

#ifndef MODEL_POTNET_H
#define MODEL_POTNET_H

#include "scip/scip.h"

/*
 * Data structures
 */

/** arcs data structure */
typedef struct Arc
{
   int                   vIn;                /**< identifier of the input node of the arc (vIn -> vOut) according to the
                                                sorting in the Potnet structure */
   int                   vOut;               /**< identifier of the out node of the arc */
   SCIP_Real             prize;              /**< cost to use this arc */
   SCIP_Real             coefEQQQ;           /**< coefficients of energy function in flow Q and pressure increase P:
                                                E >= coefEQQQ * Q^3 + coefEQQQ * Q^2 * P + coefEQPP * Q * P^2 + coefEPPP *
                                                P^3 + coefEQQ * Q^2 + coefEQP * Q * P + coefEPP * P^2 + coefEQ * Q +
                                                coefEP * P + coefEconst */
   SCIP_Real             coefEQQP;
   SCIP_Real             coefEQPP;
   SCIP_Real             coefEPPP;
   SCIP_Real             coefEQQ;
   SCIP_Real             coefEQP;
   SCIP_Real             coefEPP;
   SCIP_Real             coefEQ;
   SCIP_Real             coefEP;
   SCIP_Real             coefEconst;
   SCIP_Real             coefLbDpQQ;         /**< coefficients of lower bounding function of the pressure difference P
                                                in the flow Q: P >= coefLbDpQQ * Q^2 + coefLbDpQ * Q + coefLbDpconst*/
   SCIP_Real             coefLbDpQ;
   SCIP_Real             coefLbDpconst;
   SCIP_Real             coefUbDpQQ;         /**< coefficients of upper bounding function of the pressure difference P
                                                in the flow Q: P <= coefUbDpQQ * Q^2 + coefUbDpQ * Q + coefUbDpconst*/
   SCIP_Real             coefUbDpQ;
   SCIP_Real             coefUbDpconst;
   SCIP_Real             lbQ;                /**< lower bound on the volume flow over this arc */
   SCIP_Real             ubQ;                /**< upper bound on the volume flow over this arc */
   SCIP_Real             lbP;                /**< lower bound on the pressure difference over this arc */
   SCIP_Real             ubP;                /**< upper bound on the pressure difference over this arc */
   SCIP_Real             lbE;                /**< lower bound on the energy usage of this arc */
   SCIP_Real             ubE;                /**< upper bound on the energy usage of this arc */
   SCIP_Bool             passive;            /**< is this arc passive or active? if passive there is no pressure
                                                difference on this arc + no power consumption */
   SCIP_Bool             emergencyUseOnly;   /**< can this arc be used in the standart model? if Yes then usage only in
                                                the recourse after failure */
   char*                 name;               /**< name of the arc */
} Arc;

/** vertex data structure */
typedef struct Vertex
{
   SCIP_Real*            lbP;                /**< lower bound on the pressure in this vertex for each of the failure
                                                < scenarios */
   SCIP_Real*            ubP;                /**< upper bound on the pressure in this vertex for each of the failure
                                                < scenarios */
   SCIP_Real*            lbQ;                /**< lower bound on the volume flow going out of this vertex for each of the
                                                < failure scenarios */
   SCIP_Real*            ubQ;                /**< upper bound on the volume flow going out of this vertex for each of the
                                                < failure scenarios */
   SCIP_Real             lbPFail;            /**< lower bound on the pressure in this vertex in case of failure */
   SCIP_Real             ubPFail;            /**< upper bound on the pressure in this vertex in case of failure */
   SCIP_Real             lbQFail;            /**< lower bound on the volume flow going out of this vertex in case of
                                                < failure */
   SCIP_Real             ubQFail;            /**< upper bound on the volume flow going out of this vertex in case of
                                                < failure */
   char*                 name;               /**< name of the vertex */
} Vertex;

/** network data structure */
typedef struct Potnet
{
   int                   nScenarios;         /**< nr of load scenarios we consider in this scenario */
   int                   nArcs;              /**< number of arcs in this network */
   int                   nVertices;          /**< number of vertices in this network */
   Arc*                  arcs;               /**< arcs of this network */
   Vertex*               vertices;           /**< vertices of this network */
   SCIP_Real             coefEnergy;         /**< weight to multiply the energy of active arcs in the objective */
   SCIP_Real*            probLoading;        /**< gives for each of the scenarios its probability, which gets multiplied
                                                < to the corresponding energy costs in the objective */
   int                   sizeArcBound;       /**< current allocated size of indicesArcBound */
   int                   nArcBound;          /**< number of currently saved arcBounds in indicesArcBound and ubArcBound */
   int**                 indicesArcBound;    /**< each line gives the indices of arcs which are together in a cardinality
                                                < constraint, an end is indicated by -1 or all arcs are used */
   int*                  ubArcBound;         /**< ub of the cardinality constraint */
} Potnet;


/** creates memory in network for the given number of arcs and vertices and their names and so on */
extern
SCIP_RETCODE potnetInit(
   SCIP*                 scip,               /**< SCIP data structure */
   Potnet*               network,            /**< potentialnetwork structure */
   int                   nScenarios,         /**< number of scenarios */
   int                   nArcs,              /**< number of arcs */
   int                   nVertices           /**< number of vertices */
   );

/** frees all allocated memory in the network, e.g. the vertices/arcs arrays and their names */
extern
void potnetFree(
   SCIP*                 scip,               /**< SCIP data structure */
   Potnet*               network             /**< potentialnetwork structure */
   );


/** initializes all coefficients in an arc, except the nodes which are connected and the upper bound on the flow */
extern
void arcInitPipe(
   Arc*                  arc,                /**< arc data structure */
   int                   vIn,                /**< vertex identifier coming from */
   int                   vOut,               /**< vertex identifier coming to */
   SCIP_Real             ubQ                 /**< upper bound on volume flow */
   );


/** add an cardinality constraint on some given set of arcs such that only a given number of them can be build */
extern
SCIP_RETCODE potnetAddIndicesActiveBound(
   SCIP*                 scip,               /**< SCIP data structure */
   Potnet*               network,            /**< network the bound is added to */
   int*                  indices,            /**< array of indices of the arcs which are bounded */
   int                   nIndices,           /**< number of arcs */
   int                   bound               /**< upper bound */
   );


/** create the resilient design model of the potential network within the given scip */
extern
SCIP_RETCODE potnetCreateModel(
   SCIP*                   scip,             /**< pointer to scip */
   const Potnet*           network,          /**< pointer to network data structure */
   int                     k                 /**< number of failures which may occur */
   );

#endif
