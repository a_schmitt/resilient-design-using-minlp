/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*    This file is part of the program kresilience                           */
/*                                                                           */
/*    Copyright (C) 2020-2021 Andreas Schmitt, Marc Pfetsch                  */
/*                                                                           */
/*                                                                           */
/*    Based on SCIP --- Solving Constraint Integer Programs                  */
/*       SCIP is distributed under the terms of the SCIP Academic Licence,   */
/*       see file COPYING in the SCIP distribution.                          */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**@file   model_potnet.c
 * @brief  functions to create the optimization problem of a potential network
 * @author Andreas Schmitt
 *
 */

/*--+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0----+----1----+----2*/

#include "model_potnet.h"
#include "scip/scipdefplugins.h"

#include "min2dcube.h"
#include "cons_kresilient.h"
#include "sepa_2dCubeIndicator.h"
#include "sepa_1dquadindicator.h"
#include "reader_int.h"

/*
 * Local methods
 */

/** check whether two arcs are equal in some properties (can have different names) */
static
SCIP_Bool ArcIsEqual(
   Arc*                  arc1,               /**< arc data structure */
   Arc*                  arc2                /**< arc data structure */
   )
{  /*lint --e{777}*/
   assert(arc1 != NULL);
   assert(arc2 != NULL);

   return  arc1->vIn == arc2->vIn &&
      arc1->vOut == arc2->vOut &&
      arc1->prize == arc2->prize &&
      arc1->coefEQQQ == arc2->coefEQQQ &&
      arc1->coefEQQP == arc2->coefEQQP &&
      arc1->coefEQPP == arc2->coefEQPP &&
      arc1->coefEPPP == arc2->coefEPPP &&
      arc1->coefEQQ == arc2->coefEQQ &&
      arc1->coefEQP == arc2->coefEQP &&
      arc1->coefEPP == arc2->coefEPP &&
      arc1->coefEQ == arc2->coefEQ &&
      arc1->coefEP == arc2->coefEP &&
      arc1->coefEconst == arc2->coefEconst &&
      arc1->coefLbDpQQ == arc2->coefLbDpQQ &&
      arc1->coefLbDpQ == arc2->coefLbDpQ &&
      arc1->coefLbDpconst == arc2->coefLbDpconst &&
      arc1->coefUbDpQQ == arc2->coefUbDpQQ &&
      arc1->coefUbDpQ == arc2->coefUbDpQ &&
      arc1->coefUbDpconst == arc2->coefUbDpconst &&
      arc1->lbQ == arc2->lbQ &&
      arc1->ubQ == arc2->ubQ &&
      arc1->lbP == arc2->lbP &&
      arc1->ubP == arc2->ubP &&
      arc1->lbE == arc2->lbE &&
      arc1->ubE == arc2->ubE &&
      arc1->passive == arc2->passive;
}

/** computes a lower bound on the possible pressure difference of this arc accoding to the lower bound polynomial in the
    volume flow */
static
SCIP_RETCODE ArcGetLbDp(
   SCIP*                 scip,               /**< SCIP data structure */
   Arc*                  arc,                /**< arc data structure */
   SCIP_Real*            lbDP                /**< resulting lower bound */
   )
{
   assert( arc != NULL );
   assert( lbDP != NULL );

   if( SCIPisInfinity(scip, -arc->lbQ) || SCIPisInfinity(scip, arc->ubQ) )
   {
      if( arc->coefLbDpQQ == 0.0  && arc->coefLbDpQ == 0.0 )
      {
         *lbDP = arc->coefLbDpconst;
         return SCIP_OKAY;
      }
      return SCIP_INVALIDDATA;
   }

   *lbDP = calcMin1dQuad(arc->lbQ, arc->ubQ, arc->coefLbDpQQ, arc->coefLbDpQ, NULL) + arc->coefLbDpconst;

   return SCIP_OKAY;
}

/** computes an upper bound on the possible pressure difference of this arc accoding to the lower bound polynomial in
    the volume flow */
static
SCIP_RETCODE ArcGetUbDp(
   SCIP*                 scip,               /**< SCIP data structure */
   Arc*                  arc,                /**< arc data structure */
   SCIP_Real*            ubDP                /**< resulting upper bound */
   )
{
   assert( arc != NULL );
   assert( ubDP != NULL );

   if( SCIPisInfinity(scip, -arc->lbQ) || SCIPisInfinity(scip, arc->ubQ) )
   {
      if( arc->coefLbDpQQ == 0.0  && arc->coefLbDpQ == 0.0 )
      {
         *ubDP = arc->coefUbDpconst;
         return SCIP_OKAY;
      }
      return SCIP_INVALIDDATA;
   }

   *ubDP = calcMax1dQuad(arc->lbQ, arc->ubQ, arc->coefUbDpQQ, arc->coefUbDpQ, NULL) + arc->coefUbDpconst;

   return SCIP_OKAY;
}


/** adds the constraints e >= a*q^3 + b * q^2 * dP + c*q*dP^2 + d*dP^3 + e * Y
 with coefficients given from the arc, whith additional constraints, such that we have a quadratic*/
static
SCIP_RETCODE addCubicConstraintAsQuadratic(
   SCIP*                 scip,               /**< pointer to scip */
   char*                 subname,
   SCIP_VAR*             var_E,
   SCIP_VAR*             var_Q,
   SCIP_VAR*             var_dP,
   SCIP_VAR*             var_Y,
   Arc*                  arc
   )
{
   SCIP_VAR* var_QQuad;
   SCIP_VAR* var_dPQuad;
   SCIP_CONS* cons;
   char name[SCIP_MAXSTRLEN];

   if( arc->coefEQQQ == 0.0 )
      return SCIP_OKAY;

   (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "%sQuad#%s", subname, SCIPvarGetName(var_Q));
   SCIP_CALL( SCIPcreateVarBasic(scip, &var_QQuad, name, pow(SCIPvarGetLbGlobal(var_Q), 2.0), pow(SCIPvarGetUbGlobal(var_Q), 2.0), 0.0, SCIP_VARTYPE_CONTINUOUS) );
   SCIP_CALL( SCIPaddVar(scip, var_QQuad) );

   (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "%sQuad#%s", subname, SCIPvarGetName(var_dP));
   SCIP_CALL( SCIPcreateVarBasic(scip, &var_dPQuad, name, pow(SCIPvarGetLbGlobal(var_dP), 2.0), pow(SCIPvarGetUbGlobal(var_dP), 2.0), 0.0, SCIP_VARTYPE_CONTINUOUS) );
   SCIP_CALL( SCIPaddVar(scip, var_dPQuad) );

   (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "%sQuadLinkQ#%s", subname, SCIPvarGetName(var_Q));
   SCIP_CALL( SCIPcreateConsBasicQuadratic(scip, &cons, name, 0, NULL, NULL, 0, NULL, NULL, NULL, 0.0, 0.0) );
   SCIP_CALL( SCIPaddLinearVarQuadratic(scip, cons, var_QQuad, -1.0));
   SCIP_CALL( SCIPaddQuadVarQuadratic(scip, cons, var_Q, 0.0, 1.0) );
   SCIP_CALL( SCIPaddCons(scip, cons) );
   SCIP_CALL( SCIPreleaseCons(scip, &cons) );

   (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "%sQuadLinkdP#%s", subname, SCIPvarGetName(var_dP));
   SCIP_CALL( SCIPcreateConsBasicQuadratic(scip, &cons, name, 0, NULL, NULL, 0, NULL, NULL, NULL, 0.0, 0.0) );
   SCIP_CALL( SCIPaddLinearVarQuadratic(scip, cons, var_dPQuad, -1.0));
   SCIP_CALL( SCIPaddQuadVarQuadratic(scip, cons, var_dP, 0.0, 1.0) );
   SCIP_CALL( SCIPaddCons(scip, cons) );
   SCIP_CALL( SCIPreleaseCons(scip, &cons) );

   SCIP_CALL( SCIPcreateConsBasicQuadratic(scip, &cons, name, 0, NULL, NULL, 0, NULL, NULL, NULL, -SCIPinfinity(scip), 0.0) );
   SCIP_CALL( SCIPaddLinearVarQuadratic(scip, cons, var_E, -1.0) );
   SCIP_CALL( SCIPaddBilinTermQuadratic(scip, cons, var_QQuad, var_Q, arc->coefEQQQ) );
   SCIP_CALL( SCIPaddBilinTermQuadratic(scip, cons, var_QQuad, var_dP, arc->coefEQQP) );
   SCIP_CALL( SCIPaddBilinTermQuadratic(scip, cons, var_dP, var_Q, arc->coefEQPP) );
   SCIP_CALL( SCIPaddBilinTermQuadratic(scip, cons, var_dPQuad, var_dP, arc->coefEPPP) );
   SCIP_CALL( SCIPaddLinearVarQuadratic(scip, cons, var_QQuad, arc->coefEQQ) );
   SCIP_CALL( SCIPaddBilinTermQuadratic(scip, cons, var_Q, var_dP, arc->coefEQP) );
   SCIP_CALL( SCIPaddLinearVarQuadratic(scip, cons, var_dPQuad, arc->coefEPP) );
   SCIP_CALL( SCIPaddLinearVarQuadratic(scip, cons, var_Q, arc->coefEQ) );
   SCIP_CALL( SCIPaddLinearVarQuadratic(scip, cons, var_dP, arc->coefEP) );
   SCIP_CALL( SCIPaddLinearVarQuadratic(scip, cons, var_Y, arc->coefEconst) );
   SCIP_CALL( SCIPaddCons(scip, cons) );
   SCIP_CALL( SCIPreleaseCons(scip, &cons) );

   SCIP_CALL( SCIPreleaseVar(scip, &var_QQuad) );
   SCIP_CALL( SCIPreleaseVar(scip, &var_dPQuad) );

   return SCIP_OKAY;
}

/* Creates a model of the network with arc variables vars_X */
static
SCIP_RETCODE potnetCreateModelDefaultXExposed(
   SCIP*                 scip,               /**< pointer to scip */
   SCIP_VAR**            vars_X,             /**< vars_X[i] = 1 iff arc i in network is build */
   const Potnet*         network             /**< pointer to network data structure */
   )
{
   SCIP_VAR*** vars_Y;
   SCIP_VAR*** vars_Q;
   SCIP_VAR*** vars_E;
   SCIP_VAR*** vars_dP;
   SCIP_VAR*** vars_P;
   char name[SCIP_MAXSTRLEN];
   int nArcs;
   int nVertices;
   int nScenarios;
   SCIP_CONS* cons;
   int a;
   int v;
   int s;

   /* todo we include it alreay in the main, since it uses options which are not loaded if we include the sepa this late (when reading a instance)*/
   //   SCIP_CALL( SCIPincludeSepaCubeIndicator(scip) );
   SCIP_CALL( SCIPincludeSepaQuadIndicator(scip) );

   nArcs = network->nArcs;
   nVertices = network->nVertices;
   nScenarios = network->nScenarios;

   // create the variables
   SCIP_CALL( SCIPallocBufferArray(scip, &vars_Y, nArcs) );
   SCIP_CALL( SCIPallocBufferArray(scip, &vars_Q, nArcs) );
   SCIP_CALL( SCIPallocBufferArray(scip, &vars_E, nArcs) );
   SCIP_CALL( SCIPallocBufferArray(scip, &vars_dP, nArcs) );
   SCIP_CALL( SCIPallocBufferArray(scip, &vars_P, nVertices) );

   for( a = 0; a < nArcs; ++a)
   {
      Arc* arc = &network->arcs[a];
      (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "X#%s", arc->name);
      SCIP_CALL( SCIPcreateVarBasic(scip, &vars_X[a], name, 0.0, 1.0, arc->prize, SCIP_VARTYPE_BINARY) );
      SCIP_CALL( SCIPaddVar(scip, vars_X[a]) );
      SCIP_CALL( SCIPaddVarBranchPriority(scip, vars_X[a], 10000000) ); // arbitrary choosen, needs to be tuned

      SCIP_CALL( SCIPallocBufferArray(scip, &vars_Y[a], nScenarios) );
      SCIP_CALL( SCIPallocBufferArray(scip, &vars_Q[a], nScenarios) );
      SCIP_CALL( SCIPallocBufferArray(scip, &vars_E[a], nScenarios) );
      SCIP_CALL( SCIPallocBufferArray(scip, &vars_dP[a], nScenarios) );
      for( s = 0; s < nScenarios; s++ )
      {
         if( arc->passive )
         {
            vars_Y[a][s] = vars_X[a];
         }
         else
         {
            (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "Y#%s#%u", arc->name, s);
            SCIP_CALL( SCIPcreateVarBasic(scip, &vars_Y[a][s], name, 0.0, 1.0, 0.0, SCIP_VARTYPE_BINARY) );
            if( arc->emergencyUseOnly )
            {
               SCIP_CALL( SCIPchgVarUb(scip, vars_Y[a][s], 0.0) );
            }
            SCIP_CALL( SCIPaddVar(scip, vars_Y[a][s]) );
            SCIP_CALL( SCIPaddVarBranchPriority(scip, vars_Y[a][s], 100000) ); // arbitrary choosen, needs to be tuned
         }

         (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "Q#%s#%u", arc->name, s);
         SCIP_CALL( SCIPcreateVarBasic(scip, &vars_Q[a][s], name, MIN(arc->lbQ, 0.0), MAX(arc->ubQ, 0.0), 0.0, SCIP_VARTYPE_CONTINUOUS) );
         SCIP_CALL( SCIPaddVar(scip, vars_Q[a][s]) );

         (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "dP#%s#%u", arc->name, s);
         SCIP_CALL( SCIPcreateVarBasic(scip, &vars_dP[a][s], name, 0.0, arc->ubP, 0.0, SCIP_VARTYPE_CONTINUOUS) );
         SCIP_CALL( SCIPaddVar(scip, vars_dP[a][s]) );

         if( !network->arcs[a].passive )
         {
            (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "E#%s#%u", arc->name, s);
            SCIP_CALL( SCIPcreateVarBasic(scip, &vars_E[a][s], name, 0.0, arc->ubE, network->probLoading[s] * network->coefEnergy, SCIP_VARTYPE_CONTINUOUS) ); // TODO bounds!
            SCIP_CALL( SCIPaddVar(scip, vars_E[a][s]) );
         }
      }
   }
   for( v = 0; v < nVertices; ++v )
   {
      Vertex* vertex = &network->vertices[v];
      SCIP_CALL( SCIPallocBufferArray(scip, &vars_P[v], nScenarios) );
      for( s = 0; s < nScenarios; s++ )
      {
         (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "P#%s#%u", vertex->name, s);
         SCIP_CALL( SCIPcreateVarBasic(scip, &vars_P[v][s], name, MAX(vertex->lbP[s], 0.0), vertex->ubP[s], 0.0, SCIP_VARTYPE_CONTINUOUS) );
         SCIP_CALL( SCIPaddVar(scip, vars_P[v][s]) );
      }
   }

   // Constraints
   for( v = 0; v < nVertices; ++v )
   {
      Vertex* vertex = &network->vertices[v];
      for( s = 0; s < nScenarios; s++ )
      {
         (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "FlowBalance#%u#%s#%u", v, vertex->name, s);
         SCIP_CALL( SCIPcreateConsBasicLinear(scip, &cons, name, 0, NULL, NULL, vertex->lbQ[s], vertex->ubQ[s]) );
         for( a = 0; a < nArcs; ++a)
         {
            if( network->arcs[a].vIn == v )
            {
               SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_Q[a][s], -1.0) );
            }
            else if( network->arcs[a].vOut == v )
            {
               SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_Q[a][s], 1.0) );
            }
         }
         SCIP_CALL( SCIPaddCons(scip, cons) );
         SCIP_CALL( SCIPreleaseCons(scip, &cons) );
      }
   }


   for( a = 0; a < nArcs; ++a)
   {
      Arc* arc = &network->arcs[a];
      for( s = 0; s < nScenarios; s++ )
      {
         SCIP_Real M = SCIPvarGetUbGlobal(vars_P[arc->vOut][s]);
         M -= SCIPvarGetLbGlobal(vars_P[arc->vIn][s]);
         M -= SCIPvarGetLbGlobal(vars_dP[a][s]);

         (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "PotentialBalanceLb#%u#%s#%u", a, arc->name, s);
         SCIP_CALL( SCIPcreateConsBasicLinear(scip, &cons, name, 0, NULL, NULL, -SCIPinfinity(scip), M) );
         SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_P[arc->vOut][s], 1.0) );
         SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_P[arc->vIn][s], -1.0) );
         SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_dP[a][s], -1.0) );
         SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_Y[a][s], M) );
         SCIP_CALL( SCIPaddCons(scip, cons) );
         SCIP_CALL( SCIPreleaseCons(scip, &cons) );

         M = SCIPvarGetLbGlobal(vars_P[arc->vOut][s]);
         M -= SCIPvarGetUbGlobal(vars_P[arc->vIn][s]);
         M -= SCIPvarGetUbGlobal(vars_dP[a][s]);
         (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "PotentialBalanceUb#%u#%s#%u", a, arc->name, s);
         SCIP_CALL( SCIPcreateConsBasicLinear(scip, &cons, name, 0, NULL, NULL, M, SCIPinfinity(scip)) );
         SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_P[arc->vOut][s], 1.0) );
         SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_P[arc->vIn][s], -1.0) );
         SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_dP[a][s], -1.0) );
         SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_Y[a][s], M) );
         SCIP_CALL( SCIPaddCons(scip, cons) );
         SCIP_CALL( SCIPreleaseCons(scip, &cons) );
      }
   }


   for( a = 0; a < nArcs; ++a)
   {
      Arc* arc = &network->arcs[a];
      if( arc->passive )
         continue;
      for( s = 0; s < nScenarios; s++ )
      {
         (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "UsedArc#%u#%s#%u", a, arc->name, s);
         SCIP_CALL( SCIPcreateConsBasicLinear(scip, &cons, name, 0, NULL, NULL, -SCIPinfinity(scip), 0.0) );
         SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_X[a], -1.0) );
         SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_Y[a][s], 1.0) );
         SCIP_CALL( SCIPaddCons(scip, cons) );
         SCIP_CALL( SCIPreleaseCons(scip, &cons) );
      }
   }

   for( a = 0; a < nArcs; ++a)
   {
      Arc* arc = &network->arcs[a];
      for( s = 0; s < nScenarios; s++ )
      {
         SCIP_VAR* vardP;
         SCIP_VAR* varQ;
         SCIP_VAR* varY;
         SCIP_VAR* varE;

         vardP = vars_dP[a][s];
         varQ = vars_Q[a][s];
         varY = vars_Y[a][s];
         varE = vars_E[a][s];

         (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "LbQ#%u#%s#%u", a, arc->name, s);
         SCIP_CALL( SCIPcreateConsBasicLinear(scip, &cons, name, 0, NULL, NULL, -SCIPinfinity(scip), 0.0) );
         SCIP_CALL( SCIPaddCoefLinear(scip, cons, varY, arc->lbQ) );
         SCIP_CALL( SCIPaddCoefLinear(scip, cons, varQ, -1.0) );
         SCIP_CALL( SCIPaddCons(scip, cons) );
         SCIP_CALL( SCIPreleaseCons(scip, &cons) );

         (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "ubQ#%u#%s#%u", a, arc->name, s);
         SCIP_CALL( SCIPcreateConsBasicLinear(scip, &cons, name, 0, NULL, NULL, -SCIPinfinity(scip), 0.0) );
         SCIP_CALL( SCIPaddCoefLinear(scip, cons, varY, -arc->ubQ) );
         SCIP_CALL( SCIPaddCoefLinear(scip, cons, varQ, 1.0) );
         SCIP_CALL( SCIPaddCons(scip, cons) );
         SCIP_CALL( SCIPreleaseCons(scip, &cons) );

         (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "ubP#%u#%s#%u", a, arc->name, s);
         SCIP_CALL( SCIPcreateConsBasicLinear(scip, &cons, name, 0, NULL, NULL, -SCIPinfinity(scip), 0.0) );
         SCIP_CALL( SCIPaddCoefLinear(scip, cons, varY, -arc->ubP) );
         SCIP_CALL( SCIPaddCoefLinear(scip, cons, vardP, 1.0) );
         SCIP_CALL( SCIPaddCons(scip, cons) );
         SCIP_CALL( SCIPreleaseCons(scip, &cons) );

         (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "LbdP#%u#%s#%u", a, arc->name, s);
         SCIP_CALL( SCIPcreateConsBasicQuadratic(scip, &cons, name, 0, NULL, NULL, 0, NULL, NULL, NULL, -SCIPinfinity(scip), 0.0) );
         SCIP_CALL( SCIPaddLinearVarQuadratic(scip, cons, vardP, -1.0));
         SCIP_CALL( SCIPaddLinearVarQuadratic(scip, cons, varY, arc->coefLbDpconst));
         SCIP_CALL( SCIPaddQuadVarQuadratic(scip, cons, varQ, arc->coefLbDpQ, arc->coefLbDpQQ) );
         SCIP_CALL( SCIPaddCons(scip, cons) );
         SCIP_CALL( SCIPreleaseCons(scip, &cons) );

         (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "UbdP#%u#%s#%u", a, arc->name, s);
         SCIP_CALL( SCIPcreateConsBasicQuadratic(scip, &cons, name, 0, NULL, NULL, 0, NULL, NULL, NULL, -SCIPinfinity(scip), 0.0) );
         SCIP_CALL( SCIPaddLinearVarQuadratic(scip, cons, vardP, 1.0));
         SCIP_CALL( SCIPaddLinearVarQuadratic(scip, cons, varY, -arc->coefUbDpconst));
         SCIP_CALL( SCIPaddQuadVarQuadratic(scip, cons, varQ, -arc->coefUbDpQ, -arc->coefUbDpQQ) );
         SCIP_CALL( SCIPaddCons(scip, cons) );
         SCIP_CALL( SCIPreleaseCons(scip, &cons) );

         if( !arc->passive )
         {
            (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "lbE#%u#%s#%u", a, arc->name, s);
            SCIP_CALL( addCubicConstraintAsQuadratic(scip, name, varE, varQ, vardP, varY, arc) );

            SCIP_CALL( SCIPSepaCubeIndicatorRegister(scip, varE, varQ, vardP, varY, arc->coefEQQQ, arc->coefEQQP, arc->coefEQPP, arc->coefEPPP, arc->coefEQQ, arc->coefEQP, arc->coefEPP, arc->coefEQ, arc->coefEP, arc->coefEconst) );
            SCIP_CALL( SCIPSepaQuadIndicatorRegister(scip, varQ, vardP, varY, arc->coefLbDpQQ, arc->coefLbDpQ, arc->coefLbDpconst,arc->coefUbDpQQ, arc->coefUbDpQ, arc->coefUbDpconst) );
         }
      }
   }

   for( v = 0; v < nVertices; ++v )
   {
      Vertex* vertex = &network->vertices[v];
      for( s = 0; s < nScenarios; s++ )
      {
         SCIP_Real ub = SCIPvarGetUbGlobal(vars_P[v][s]);
         SCIP_Real lb = MIN(MAX(0.0, SCIPvarGetLbGlobal(vars_P[v][s])), SCIPvarGetUbGlobal(vars_P[v][s]));
         (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "PressureOnlyIfConnection#%s#%u", vertex->name, s);
         SCIP_CALL( SCIPcreateConsBasicLinear(scip, &cons, name, 0, NULL, NULL, -SCIPinfinity(scip), ub) );
         for( a = 0; a < nArcs; ++a)
         {
            if( network->arcs[a].vIn == v || network->arcs[a].vOut == v )
            {
               SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_Y[a][s], -(ub-lb)) );
            }
         }
         SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_P[v][s], 1.0) );
         SCIP_CALL( SCIPaddCons(scip, cons) );
         SCIP_CALL( SCIPreleaseCons(scip, &cons) );
      }
   }
   for( v = 0; v < network->nArcBound; ++v )
   {
      (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "ArcUb#%d", v);
      SCIP_CALL( SCIPcreateConsBasicLinear(scip, &cons, name, 0, NULL, NULL, -SCIPinfinity(scip), (SCIP_Real) network->ubArcBound[v]) );
      a = 0;
      while( a < network->nArcs && network->indicesArcBound[v][a] >= 0 )
      {
         SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_X[network->indicesArcBound[v][a]], 1.0) );
         a++;
      }
      SCIP_CALL( SCIPaddCons(scip, cons) );
      SCIP_CALL( SCIPreleaseCons(scip, &cons) );
   }

   for(  a = 0; a < nArcs; ++a )
   {
      for( v = a+1; v < nArcs; ++v )
      {
         if( !network->arcs[a].passive && ArcIsEqual(&network->arcs[a], &network->arcs[v]) )
         {
            (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "symbreak#%s__%s", network->arcs[a].name, network->arcs[v].name);
            SCIP_CALL( SCIPcreateConsBasicLinear(scip, &cons, name, 0, NULL, NULL, -SCIPinfinity(scip), 0.0) );
            SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_X[a], -1.0) );
            SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_X[v], 1.0) );
            SCIP_CALL( SCIPaddCons(scip, cons) );
            SCIP_CALL( SCIPreleaseCons(scip, &cons) );

            for( s = 0; s < nScenarios; s++ )
            {
               (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "symbreak#%s__%s__%d", network->arcs[a].name, network->arcs[v].name, s);
               SCIP_CALL( SCIPcreateConsBasicLinear(scip, &cons, name, 0, NULL, NULL, -SCIPinfinity(scip), 0.0) );
               SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_Y[a][s], -1.0) );
               SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_Y[v][s], 1.0) );
               SCIP_CALL( SCIPaddCons(scip, cons) );
               SCIP_CALL( SCIPreleaseCons(scip, &cons) );
            }
            break;
         }
      }
   }


   // release variables
   for( v = nVertices-1; v >= 0; --v )
   {
      for( s = 0; s < nScenarios; s++ )
      {
         SCIP_CALL( SCIPreleaseVar(scip, &vars_P[v][s]) );
      }
      SCIPfreeBufferArray(scip, &vars_P[v]);
   }
   for( a = nArcs-1; a >= 0; --a)
   {
      for( s = nScenarios-1; s >= 0; s-- )
      {
         SCIP_CALL( SCIPreleaseVar(scip, &vars_dP[a][s]) );
         if( !network->arcs[a].passive )
         {
            SCIP_CALL( SCIPreleaseVar(scip, &vars_E[a][s]) );
            SCIP_CALL( SCIPreleaseVar(scip, &vars_Y[a][s]) );
         }
         SCIP_CALL( SCIPreleaseVar(scip, &vars_Q[a][s]) );
      }
      SCIPfreeBufferArray(scip, &vars_dP[a]);
      SCIPfreeBufferArray(scip, &vars_E[a]);
      SCIPfreeBufferArray(scip, &vars_Q[a]);
      SCIPfreeBufferArray(scip, &vars_Y[a]);
   }
   SCIPfreeBufferArray(scip, &vars_P);
   SCIPfreeBufferArray(scip, &vars_dP);
   SCIPfreeBufferArray(scip, &vars_E);
   SCIPfreeBufferArray(scip, &vars_Q);
   SCIPfreeBufferArray(scip, &vars_Y);

   return SCIP_OKAY;
}

/* Creates a model of the network which models the recovery model after a failure occured */
static
SCIP_RETCODE potnetCreateModelFailureXExposed(
   SCIP*                 scip,               /**< pointer to scip */
   SCIP_VAR**            vars_X,             /**< vars_X[i] = 1 iff arc i in network is build */
   const Potnet*         network,            /**< pointer to network data structure */
   char*                 prefix              /**< possible prefix for variable and constraint names */
   )
{
   SCIP_VAR** vars_Y;
   SCIP_VAR** vars_Q;
   SCIP_VAR** vars_E;
   SCIP_VAR** vars_dP;
   SCIP_VAR** vars_P;
   char name[SCIP_MAXSTRLEN];
   int nArcs;
   int nVertices;
   SCIP_CONS* cons;
   int a;
   int v;

   nArcs = network->nArcs;
   nVertices = network->nVertices;

   SCIP_CALL( SCIPincludeSepaCubeIndicator(scip) );
   SCIP_CALL( SCIPincludeSepaQuadIndicator(scip) );

   // create the variables
   SCIP_CALL( SCIPallocBufferArray(scip, &vars_Y, nArcs) );
   SCIP_CALL( SCIPallocBufferArray(scip, &vars_Q, nArcs) );
   SCIP_CALL( SCIPallocBufferArray(scip, &vars_E, nArcs) );
   SCIP_CALL( SCIPallocBufferArray(scip, &vars_dP, nArcs) );
   SCIP_CALL( SCIPallocBufferArray(scip, &vars_P, nVertices) );

   for( a = 0; a < nArcs; ++a)
   {
      Arc* arc = &network->arcs[a];
      SCIP_Real lb;
      SCIP_Real ub;

      (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "%sX#%s", prefix, arc->name);
      SCIP_CALL( SCIPcreateVarBasic(scip, &vars_X[a], name, 0.0, 1.0, 0.0, SCIP_VARTYPE_BINARY) );
      SCIP_CALL( SCIPaddVar(scip, vars_X[a]) );

      SCIP_CALL( ArcGetLbDp(scip, arc, &lb) );
      SCIP_CALL( ArcGetUbDp(scip, arc, &ub) );// TODO bounds depending on scenario and flow!
      (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "%sY#%s", prefix, arc->name);
      SCIP_CALL( SCIPcreateVarBasic(scip, &vars_Y[a], name, 0.0, 1.0, 0.0, SCIP_VARTYPE_BINARY) );
      SCIP_CALL( SCIPaddVar(scip, vars_Y[a]) );

      (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "%sQ#%s", prefix, arc->name);
      SCIP_CALL( SCIPcreateVarBasic(scip, &vars_Q[a], name, MIN(arc->lbQ, 0.0), MAX(arc->ubQ, 0.0), 0.0, SCIP_VARTYPE_CONTINUOUS) );
      SCIP_CALL( SCIPaddVar(scip, vars_Q[a]) );

      lb = MAX(0.0, lb);
      (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "%sdP#%s", prefix, arc->name);
      SCIP_CALL( SCIPcreateVarBasic(scip, &vars_dP[a], name, lb, ub, 0.0, SCIP_VARTYPE_CONTINUOUS) );
      SCIP_CALL( SCIPaddVar(scip, vars_dP[a]) );

      if( !network->arcs[a].passive )
      {
         (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "%sE#%s", prefix, arc->name);
         SCIP_CALL( SCIPcreateVarBasic(scip, &vars_E[a], name, 0.0, arc->ubE, 0.0, SCIP_VARTYPE_CONTINUOUS) );
         SCIP_CALL( SCIPaddVar(scip, vars_E[a]) );
      }
   }
   for( v = 0; v < nVertices; ++v )
   {
      Vertex* vertex = &network->vertices[v];
      (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "%sP#%s", prefix, vertex->name);
      SCIP_CALL( SCIPcreateVarBasic(scip, &vars_P[v], name, MAX(vertex->lbPFail, 0.0), vertex->ubPFail, 0.0, SCIP_VARTYPE_CONTINUOUS) );
      SCIP_CALL( SCIPaddVar(scip, vars_P[v]) );
   }

   // Constraints
   for( v = 0; v < nVertices; ++v )
   {
      Vertex* vertex = &network->vertices[v];
      (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "%sFlowBalance#%u#%s", prefix, v, vertex->name);
      SCIP_CALL( SCIPcreateConsBasicLinear(scip, &cons, name, 0, NULL, NULL, vertex->lbQFail, vertex->ubQFail) );
      for( a = 0; a < nArcs; ++a)
      {
         if( network->arcs[a].vIn == v )
         {
            SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_Q[a], -1.0) );
         }
         else if( network->arcs[a].vOut == v )
         {
            SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_Q[a], 1.0) );
         }
      }
      SCIP_CALL( SCIPaddCons(scip, cons) );
      SCIP_CALL( SCIPreleaseCons(scip, &cons) );
   }


   for( a = 0; a < nArcs; ++a)
   {
      Arc* arc = &network->arcs[a];
      SCIP_Real M = SCIPvarGetUbGlobal(vars_P[arc->vOut]);
      M -= SCIPvarGetLbGlobal(vars_P[arc->vIn]);
      M -= SCIPvarGetLbGlobal(vars_dP[a]);

      (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "%sPotentialBalanceLb#%s", prefix, arc->name);
      SCIP_CALL( SCIPcreateConsBasicLinear(scip, &cons, name, 0, NULL, NULL, -SCIPinfinity(scip), M) );
      SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_P[arc->vOut], 1.0) );
      SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_P[arc->vIn], -1.0) );
      SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_dP[a], -1.0) );
      SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_Y[a], M) );
      SCIP_CALL( SCIPaddCons(scip, cons) );
      SCIP_CALL( SCIPreleaseCons(scip, &cons) );

      M = SCIPvarGetLbGlobal(vars_P[arc->vOut]);
      M -= SCIPvarGetUbGlobal(vars_P[arc->vIn]);
      M -= SCIPvarGetUbGlobal(vars_dP[a]);
      (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "%sPotentialBalanceUb#%s", prefix, arc->name);
      SCIP_CALL( SCIPcreateConsBasicLinear(scip, &cons, name, 0, NULL, NULL, M, SCIPinfinity(scip)) );
      SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_P[arc->vOut], 1.0) );
      SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_P[arc->vIn], -1.0) );
      SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_dP[a], -1.0) );
      SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_Y[a], M) );
      SCIP_CALL( SCIPaddCons(scip, cons) );
      SCIP_CALL( SCIPreleaseCons(scip, &cons) );
   }


   for( a = 0; a < nArcs; ++a)
   {
      Arc* arc = &network->arcs[a];
      (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "%sUsedArc#%s", prefix, arc->name);
      if( arc->passive )
      {
         SCIP_CALL( SCIPcreateConsBasicLinear(scip, &cons, name, 0, NULL, NULL, 0.0, 0.0) );
      }
      else
      {
         SCIP_CALL( SCIPcreateConsBasicLinear(scip, &cons, name, 0, NULL, NULL, -SCIPinfinity(scip), 0.0) );
      }
      SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_X[a], -1.0) );
      SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_Y[a], 1.0) );
      SCIP_CALL( SCIPaddCons(scip, cons) );
      SCIP_CALL( SCIPreleaseCons(scip, &cons) );
   }

   for( a = 0; a < nArcs; ++a)
   {
      SCIP_VAR* vardP;
      SCIP_VAR* varQ;
      SCIP_VAR* varY;
      SCIP_VAR* varE;
      Arc* arc = &network->arcs[a];

      vardP = vars_dP[a];
      varQ = vars_Q[a];
      varY = vars_Y[a];
      varE = vars_E[a];

      (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "%sLbQ#%s", prefix, arc->name);
      SCIP_CALL( SCIPcreateConsBasicLinear(scip, &cons, name, 0, NULL, NULL, -SCIPinfinity(scip), 0.0) );
      SCIP_CALL( SCIPaddCoefLinear(scip, cons, varY, arc->lbQ) );
      SCIP_CALL( SCIPaddCoefLinear(scip, cons, varQ, -1.0) );
      SCIP_CALL( SCIPaddCons(scip, cons) );
      SCIP_CALL( SCIPreleaseCons(scip, &cons) );

      (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "%sUbQ#%s", prefix, arc->name);
      SCIP_CALL( SCIPcreateConsBasicLinear(scip, &cons, name, 0, NULL, NULL, -SCIPinfinity(scip), 0.0) );
      SCIP_CALL( SCIPaddCoefLinear(scip, cons, varY, -arc->ubQ) );
      SCIP_CALL( SCIPaddCoefLinear(scip, cons, varQ, 1.0) );
      SCIP_CALL( SCIPaddCons(scip, cons) );
      SCIP_CALL( SCIPreleaseCons(scip, &cons) );

      (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "%sLbdP#%s", prefix, arc->name);
      SCIP_CALL( SCIPcreateConsBasicQuadratic(scip, &cons, name, 0, NULL, NULL, 0, NULL, NULL, NULL, -SCIPinfinity(scip), 0.0) );
      SCIP_CALL( SCIPaddLinearVarQuadratic(scip, cons, vardP, -1.0));
      SCIP_CALL( SCIPaddLinearVarQuadratic(scip, cons, varY, arc->coefLbDpconst));
      SCIP_CALL( SCIPaddQuadVarQuadratic(scip, cons, varQ, arc->coefLbDpQ, arc->coefLbDpQQ) );
      SCIP_CALL( SCIPaddCons(scip, cons) );
      SCIP_CALL( SCIPreleaseCons(scip, &cons) );

      (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "%sUbdP#%s", prefix, arc->name);
      SCIP_CALL( SCIPcreateConsBasicQuadratic(scip, &cons, name, 0, NULL, NULL, 0, NULL, NULL, NULL, -SCIPinfinity(scip), 0.0) );
      SCIP_CALL( SCIPaddLinearVarQuadratic(scip, cons, vardP, 1.0));
      SCIP_CALL( SCIPaddLinearVarQuadratic(scip, cons, varY, -arc->coefUbDpconst));
      SCIP_CALL( SCIPaddQuadVarQuadratic(scip, cons, varQ, -arc->coefUbDpQ, -arc->coefUbDpQQ) );
      SCIP_CALL( SCIPaddCons(scip, cons) );
      SCIP_CALL( SCIPreleaseCons(scip, &cons) );

      if( !arc->passive )
      {
         (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "%slbE#%s", prefix, arc->name);
         SCIP_CALL( addCubicConstraintAsQuadratic(scip, name, varE, varQ, vardP, varY, arc) );

         SCIP_CALL( SCIPSepaCubeIndicatorRegister(scip, varE, varQ, vardP, varY, arc->coefEQQQ, arc->coefEQQP, arc->coefEQPP, arc->coefEPPP, arc->coefEQQ, arc->coefEQP, arc->coefEPP, arc->coefEQ, arc->coefEP, arc->coefEconst) );
         SCIP_CALL( SCIPSepaQuadIndicatorRegister(scip, varQ, vardP, varY, arc->coefLbDpQQ, arc->coefLbDpQ, arc->coefLbDpconst,arc->coefUbDpQQ, arc->coefUbDpQ, arc->coefUbDpconst) );
      }
   }

   for( v = 0; v < nVertices; ++v )
   {
      Vertex* vertex = &network->vertices[v];
      SCIP_Real ub = SCIPvarGetUbGlobal(vars_P[v]);
      SCIP_Real lb = MIN(MAX(0.0, SCIPvarGetLbGlobal(vars_P[v])), SCIPvarGetUbGlobal(vars_P[v]));
      (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "%sPressureOnlyIfConnection#%s", prefix, vertex->name);
      SCIP_CALL( SCIPcreateConsBasicLinear(scip, &cons, name, 0, NULL, NULL, -SCIPinfinity(scip), ub) );
      for( a = 0; a < nArcs; ++a)
      {
         if( network->arcs[a].vIn == v || network->arcs[a].vOut == v )
         {
            SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_Y[a], -(ub-lb)) );
         }
      }
      SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_P[v], 1.0) );
      SCIP_CALL( SCIPaddCons(scip, cons) );
      SCIP_CALL( SCIPreleaseCons(scip, &cons) );
   }
   for( v = 0; v < network->nArcBound; ++v )
   {
      (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "%sArcUb#%d", prefix, v);
      SCIP_CALL( SCIPcreateConsBasicLinear(scip, &cons, name, 0, NULL, NULL, -SCIPinfinity(scip), (SCIP_Real) network->ubArcBound[v]) );
      a = 0;
      while( a < network->nArcs && network->indicesArcBound[v][a] >= 0 )
      {
         SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_X[network->indicesArcBound[v][a]], 1.0) );
         a++;
      }
      SCIP_CALL( SCIPaddCons(scip, cons) );
      SCIP_CALL( SCIPreleaseCons(scip, &cons) );
   }

   for(  a = 0; a < nArcs; ++a )
   {
      for( v = a+1; v < nArcs; ++v )
      {
         if( !network->arcs[a].passive && ArcIsEqual(&network->arcs[a], &network->arcs[v]) )
         {
            (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "%ssymbreak#%s__%s", prefix, network->arcs[a].name, network->arcs[v].name);
            SCIP_CALL( SCIPcreateConsBasicLinear(scip, &cons, name, 0, NULL, NULL, -SCIPinfinity(scip), 0.0) );
            SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_X[a], -1.0) );
            SCIP_CALL( SCIPaddCoefLinear(scip, cons, vars_X[v], 1.0) );
            SCIP_CALL( SCIPaddCons(scip, cons) );
            SCIP_CALL( SCIPreleaseCons(scip, &cons) );
            break;
         }
      }
   }

   // release variables
   for( v = nVertices-1; v >= 0; --v )
   {
      SCIP_CALL( SCIPreleaseVar(scip, &vars_P[v]) );
   }
   for( a = nArcs-1; a >= 0; --a)
   {
      SCIP_CALL( SCIPreleaseVar(scip, &vars_dP[a]) );
      if( !network->arcs[a].passive )
      {
         SCIP_CALL( SCIPreleaseVar(scip, &vars_E[a]) );
      }
      SCIP_CALL( SCIPreleaseVar(scip, &vars_Q[a]) );
      SCIP_CALL( SCIPreleaseVar(scip, &vars_Y[a]) );
   }
   SCIPfreeBufferArray(scip, &vars_P);
   SCIPfreeBufferArray(scip, &vars_dP);
   SCIPfreeBufferArray(scip, &vars_E);
   SCIPfreeBufferArray(scip, &vars_Q);
   SCIPfreeBufferArray(scip, &vars_Y);

   //SCIPprintOrigProblem(scip, NULL, "cip", FALSE);
   return SCIP_OKAY;
}

/** creates memory in network for the given number of arcs and vertices and their names and so on */
SCIP_RETCODE potnetInit(
   SCIP*                 scip,               /**< SCIP data structure */
   Potnet*               network,            /**< potentialnetwork structure */
   int                   nScenarios,         /**< number of scenarios */
   int                   nArcs,              /**< number of arcs */
   int                   nVertices           /**< number of vertices */
   )
{
   int i;

   assert(scip != NULL);
   assert(network != NULL);
   assert(nScenarios > 0);
   assert(nArcs > 0);
   assert(nVertices > 0);

   network->nScenarios = nScenarios;
   network->nArcs = nArcs;
   network->nVertices = nVertices;
   network->sizeArcBound = 10;
   network->nArcBound = 0;
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &network->indicesArcBound, network->sizeArcBound) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &network->ubArcBound, network->sizeArcBound) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &network->vertices, nVertices) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &network->arcs, nArcs) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &network->probLoading, nScenarios) );

   for( i = 0; i < nArcs; ++i )
   {
      SCIP_ALLOC( BMSallocMemoryArray(&(network->arcs[i].name), SCIP_MAXSTRLEN+1) );
   }
   for( i = 0; i < nVertices; ++i )
   {
      SCIP_ALLOC( BMSallocMemoryArray(&(network->vertices[i].name), SCIP_MAXSTRLEN+1) );
      SCIP_CALL( SCIPallocBlockMemoryArray(scip, &network->vertices[i].lbP, network->nScenarios) );
      SCIP_CALL( SCIPallocBlockMemoryArray(scip, &network->vertices[i].ubP, network->nScenarios) );
      SCIP_CALL( SCIPallocBlockMemoryArray(scip, &network->vertices[i].lbQ, network->nScenarios) );
      SCIP_CALL( SCIPallocBlockMemoryArray(scip, &network->vertices[i].ubQ, network->nScenarios) );
   }

   return SCIP_OKAY;
}

/** frees all allocated memory in the network, e.g. the vertices/arcs arrays and their names */
void potnetFree(
   SCIP*                 scip,               /**< SCIP data structure */
   Potnet*               network             /**< potentialnetwork structure */
   )
{
   int i;

   assert(scip != NULL);
   assert(network != NULL);

   for( i = 0; i < network->nArcBound; ++i )
   {
      SCIPfreeBlockMemoryArray(scip, &network->indicesArcBound[i], network->nArcs);
   }
   SCIPfreeBlockMemoryArray(scip, &network->indicesArcBound, network->sizeArcBound);
   SCIPfreeBlockMemoryArray(scip, &network->ubArcBound, network->sizeArcBound);

   for( i = 0; i < network->nArcs; ++i )
   {
      BMSfreeMemoryArray(&(network->arcs[i].name));
   }
   for( i = 0; i < network->nVertices; ++i )
   {
      BMSfreeMemoryArray(&(network->vertices[i].name));
      SCIPfreeBlockMemoryArray(scip, &network->vertices[i].lbP, network->nScenarios);
      SCIPfreeBlockMemoryArray(scip, &network->vertices[i].ubP, network->nScenarios);
      SCIPfreeBlockMemoryArray(scip, &network->vertices[i].lbQ, network->nScenarios);
      SCIPfreeBlockMemoryArray(scip, &network->vertices[i].ubQ, network->nScenarios);
   }
   SCIPfreeBlockMemoryArray(scip, &network->vertices, network->nVertices);
   SCIPfreeBlockMemoryArray(scip, &network->arcs, network->nArcs);
   SCIPfreeBlockMemoryArray(scip, &network->probLoading, network->nScenarios);
}

/** add an cardinality constraint on some given set of arcs such that only a given number of them can be build */
SCIP_RETCODE potnetAddIndicesActiveBound(
   SCIP*                 scip,               /**< SCIP data structure */
   Potnet*               network,            /**< network the bound is added to */
   int*                  indices,            /**< array of indices of the arcs which are bounded */
   int                   nIndices,           /**< number of arcs */
   int                   bound               /**< upper bound */
   )
{
   int i;

   /* realloc memory if necessary */
   if (network->sizeArcBound == network->nArcBound+1 )
   {
      network->sizeArcBound = SCIPcalcMemGrowSize(scip, network->sizeArcBound+1);
      SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &network->indicesArcBound, network->nArcBound+1, network->sizeArcBound) );
      SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &network->ubArcBound, network->nArcBound+1, network->sizeArcBound) );
   }
   network->ubArcBound[network->nArcBound] = bound;
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &network->indicesArcBound[network->nArcBound], network->nArcs) );
   for( i = 0; i < nIndices; ++i )
   {
      network->indicesArcBound[network->nArcBound][i] = indices[i];
   }
   network->indicesArcBound[network->nArcBound][i] = -1;
   network->nArcBound++;

   return SCIP_OKAY;
}


/** initializes all coefficients in an arc, except the nodes which are connected and the upper bound on the flow */
void arcInitPipe(
   Arc*                  arc,                /**< arc data structure */
   int                   vIn,                /**< vertex identifier coming from */
   int                   vOut,               /**< vertex identifier coming to */
   SCIP_Real             ubQ                 /**< upper bound on volume flow */
   )
{
   assert(arc != NULL);

   arc->vIn = vIn;
   arc->vOut = vOut;
   arc->prize = 0.0;
   arc->coefEQQQ = 0.0;
   arc->coefEQQP = 0.0;
   arc->coefEQPP = 0.0;
   arc->coefEPPP = 0.0;
   arc->coefEQQ = 0.0;
   arc->coefEQP = 0.0;
   arc->coefEPP = 0.0;
   arc->coefEQ = 0.0;
   arc->coefEP = 0.0;
   arc->coefEconst = 0.0;
   arc->coefLbDpQQ = 0.0;
   arc->coefLbDpQ = 0.0;
   arc->coefLbDpconst = 0.0;
   arc->coefUbDpQQ = 0.0;
   arc->coefUbDpQ = 0.0;
   arc->coefUbDpconst = 0.0;
   arc->lbQ = 0.01;
   arc->ubQ = ubQ;
   arc->lbP = 0.0;
   arc->ubP = 0.0;
   arc->lbE = 0.0;
   arc->ubE = 0.0;
   arc->passive = TRUE;
   arc->emergencyUseOnly = FALSE;

   (void) SCIPsnprintf(arc->name, SCIP_MAXSTRLEN, "pipe#%u#%u", vIn, vOut);
}


/** create the resilient design model of the potential network within the given scip */
SCIP_RETCODE potnetCreateModel(
   SCIP*                   scip,             /**< pointer to scip */
   const Potnet*           network,          /**< pointer to network data structure */
   int                     k                 /**< number of failures which may occur */
   )
{
   int a;
   SCIP_VAR** vars_X;    // vars_X[i] = 1 iff component i is build

   // create the variables
   SCIP_CALL( SCIPallocBufferArray(scip, &vars_X, network->nArcs) );

   SCIP_CALL( potnetCreateModelDefaultXExposed( scip, vars_X, network) );

   if( k > 0 )
   {
      SCIP* scipfeas; // feasibility scip, is freed from the Kresilient constrainthandler!
      SCIP_VAR** vars_Xfeas;    // vars_X[i] = 1 iff component i is build
      SCIP_Bool* vars_interdictable;
      char name[SCIP_MAXSTRLEN];
      SCIP_Bool useconskresilient;
      int nvars;

      SCIP_CALL( SCIPallocBufferArray(scip, &vars_Xfeas, network->nArcs) );
      SCIP_CALL( SCIPallocBufferArray(scip, &vars_interdictable, network->nArcs) );
      for( a = 0; a < network->nArcs; ++a )
      {
         vars_interdictable[a] = !network->arcs[a].passive && !network->arcs[a].emergencyUseOnly;
      }

      SCIP_CALL( SCIPcreate(&scipfeas) );
      SCIP_CALL( SCIPcreateProb(scipfeas, "feasibilityProb", NULL, NULL, NULL, NULL, NULL, NULL, NULL) );
      SCIP_CALL( SCIPincludeDefaultPlugins(scipfeas) );
      (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "");
      SCIP_CALL( potnetCreateModelFailureXExposed(scipfeas, vars_Xfeas, network, name) );

      nvars = network->nArcs;
      SCIP_CALL( SCIPgetBoolParam(scip, "reader_int/useconskresilient", &useconskresilient) );
      if( useconskresilient )
      {
         SCIP_CONS* cons;

         nvars = network->nArcs;
         SCIP_CALL( SCIPcreateConsBasicKResilient(scip, &cons, "resilience", nvars, vars_X, vars_Xfeas, vars_interdictable, scipfeas, k) );
         SCIP_CALL( SCIPaddCons(scip, cons) );
         SCIP_CALL( SCIPreleaseCons(scip, &cons) );
      }
      else
      {
         SCIP_CALL( SCIPReaderIntAddConstraintsVarForFailureScenarios(scip, network->nArcs, vars_X, vars_Xfeas, vars_interdictable, scipfeas, k) );
      }

      // release variables
      for( a = nvars-1; a >= 0; --a )
      {
         SCIP_CALL( SCIPreleaseVar(scipfeas, &vars_Xfeas[a]) );
      }

      SCIPfreeBufferArray(scip, &vars_interdictable);
      SCIPfreeBufferArray(scip, &vars_Xfeas);
   }

   // release variables
   for( a = network->nArcs-1; a >= 0; --a )
   {
      SCIP_CALL( SCIPreleaseVar(scip, &vars_X[a]) );
   }

   SCIPfreeBufferArray(scip, &vars_X);
   return SCIP_OKAY;
}
