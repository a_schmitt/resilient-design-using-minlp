/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*    This file is part of the program kresilience                           */
/*                                                                           */
/*    Copyright (C) 2020-2021 Andreas Schmitt, Marc Pfetsch                  */
/*                                                                           */
/*                                                                           */
/*    Based on SCIP --- Solving Constraint Integer Programs                  */
/*       SCIP is distributed under the terms of the SCIP Academic Licence,   */
/*       see file COPYING in the SCIP distribution.                          */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**@file   cons_kresilient.c
 * @brief  constraint handler for resilience constraints
 * @author Andreas Schmitt
 *
 *
 *
 * resilience constraints have the form
 * a^T x + (k+1)*b^T y >= k+1
 * for all a, b suchthat \tilde x^T a + \tilde y^T b >= 1 for all (\tilde x, \tilde y) in some sub problem \tilde X
 *
 * This constraint handler enforces such constraints, where a constrains is given the variables x and y with a variable
 * array and a boolean array which specifies the interdictable variables x. \tilde X is denotes scip_feas in the
 * following.
 *
 * To check for a violated inequality, i.e., coefficients a and b, we solve the subscip scip_int, which models the
 * covering inequalities involving \tilde x and \tilde y using an interdiction constraint.
 *
 */

/*---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0----+----1----+----2*/

#include <assert.h>
#include "cons_kresilient.h"
#include "scip/scipdefplugins.h"
#include "scip/pub_misc.h"
#include "scip/struct_misc.h"
#include <string.h>
#include "cons_interdiction.h"
#include "message_prefix.h"


/* fundamental constraint handler properties */
#define CONSHDLR_NAME          "kresilient"
#define CONSHDLR_DESC          "kresilient constraint handler"
#define CONSHDLR_ENFOPRIORITY      -9999999  /**< priority of the constraint handler for constraint enforcing */
#define CONSHDLR_CHECKPRIORITY     -9999999  /**< priority of the constraint handler for checking feasibility */
#define CONSHDLR_EAGERFREQ          100      /**< frequency for using all instead of only the useful constraints in
                                                separation, propagation and enforcement, -1 for no eager evaluations, 0 for
                                                first only */
#define CONSHDLR_NEEDSCONS         TRUE      /**< should the constraint handler be skipped, if no constraints are
                                                available? */

/* optional constraint handler properties */
#define DEFAULT_RANDSEED            211      /**< default random seed */
#define DEFAULT_RESOLVENODELIMIT      1LL    /**< extra nodes to be checked for resolving scipint */
#define DEFAULT_RESOLVEINTERDICTIONCUTLIMIT   -1LL /**< max number of interdiction cuts which are allowed to be added
                                                      during resolving scipint, afterwards cons_interdiction branched
                                                      (-1 unbounded) */
#define DEFAULT_USERANDOMIMPROVE   FALSE     /**< should random cut improvement strategy be used */
#define DEFAULT_USEEXTENDEDCUTS    FALSE     /**< should extendended formulation be used to enforce resilience? */
#define DEFAULT_COVERINGIMPROVE    0         /**< should a cut improvement which tries reduction based on the covering
                                                rank be used? 0 no, +1 variables with great rank are choosen first, -1
                                                smallest rank is choosen first */


#define DEFAULT_USEREOPTIMIZATION  FALSE     /**< Whether to use reoptimization when solving subscip */
#define DEFAULT_PREFIXVARIABLES    TRUE      /**< Whether to fix variables when solving subscip, if reoptimization is not used */

/** constraint data for kresilient constraints */
struct SCIP_ConsData
{
   int nvars_x;                              /**< number of interdictable x variables */
   int nvars_y;                              /**< number of non-interdictable y variables */
   SCIP_VAR** vars_x;                        /**< interdictable variables in scip */
   SCIP_VAR** vars_y;                        /**< non-interdictable variables in scip */
   SCIP* scip_int;                           /**< separation problem used to seperate resilience cuts for non resilient solutions */
   SCIP_VAR** vars_alpha;                    /**< variables in scip_int corresponding to cut coefficients for the interdictable vars */
   SCIP_VAR** vars_beta;                     /**< variables in scip_int corresponding to cut coefficients for the non-interdictable vars */
   SCIP_CONS* cons_interdiction;             /**< the interdiction constraint we added to scip_int */
   int k;                                    /**< the number of failures to ensure for */
   SCIP_RANDNUMGEN* randnumgen;              /**< random number generator */
   int nenforced;                            /**< number of times a solution was enforced, currently only used to name constraints in the variablescut */
   SCIP* scip_feas;                          /**< scip_feas and variables to enforce resilience using the extended formulation */
   SCIP_VAR** vars_x_feas;                   /**< need to copy them beforehand, since the original is used by scip_int and cons_interdiction during solving in transformed stage (bc. of reoptimization) */
   SCIP_VAR** vars_y_feas;

   int nscipintsolves;                       /**< number of times scipint was optimized */
   SCIP_Real timescipintsolves;              /**< total time for solving scipint */
   SCIP_Longint nnodesscipint;               /**< total nodes occured for solving scipint */
};


/*
 * Local methods
 */

/* given a variabe and an array of variables check by comparing names whether the variable is in the array, return -1 if
   not */
static
int findVariableIndex(
   SCIP_VAR*             var,                /**< variable to search for */
   SCIP_VAR**            vars,               /**< array of variables */
   int                   nvars               /**< size of variable array */
   )
{
   int i;

   assert(var != NULL);
   assert(vars != NULL);

   for( i = 0; i < nvars; ++i )
   {
      if( strcmp(SCIPvarGetName(var), SCIPvarGetName(vars[i])) == 0 )
         return i;
   }

   return -1;
}

/* detects symmetry breaking constraints from a given scip x <= y, where x and y belong to a given array of
   variables. Prepare the corrsponding data of the resilience constraint, such that it knows of the symmetry. */
static
SCIP_RETCODE detectSymmetryBreakingAndAddToScipInt(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONSDATA*        consdata            /**< constraint data */
   )
{
   int i;
   int c;
   SCIP_CONS** conss;
   int nconss;
   int* varsToConsSmaller;
   int* varsToConsGreater;
   int nSyms;
   SCIP* scip_feas;
   char name[SCIP_MAXSTRLEN];

   assert(scip != NULL);
   assert(consdata != NULL);

   nSyms = 0;

   SCIP_CALL( SCIPallocBufferArray(scip, &varsToConsSmaller, consdata->nvars_x) );
   SCIP_CALL( SCIPallocBufferArray(scip, &varsToConsGreater, consdata->nvars_x) );

   for( i = 0; i < consdata->nvars_x; ++i )
   {
      varsToConsSmaller[i] = -1;
      varsToConsGreater[i] = -1;
   }

   scip_feas = consdata->scip_feas;

   assert(scip_feas != NULL);

   nconss = SCIPgetNConss(scip_feas);
   conss = SCIPgetConss(scip_feas);
   assert( conss != NULL );

   /* loop through all constraints of subscip to identify symmetry breaking constraints */
   for( c = 0; c < nconss; ++c )
   {
      SCIP_CONS* cons;

      /* get constraint */
      cons = conss[c];
      assert( cons != NULL );

      if( strcmp(SCIPconshdlrGetName(SCIPconsGetHdlr(cons)), "linear") == 0 && strstr(SCIPconsGetName(cons), "symbreak") )
      {
         SCIP_VAR** vars_lin = SCIPgetVarsLinear(scip_feas, cons);
         SCIP_Real* vals_lin = SCIPgetValsLinear(scip_feas, cons);
         SCIP_Real lhs = SCIPgetLhsLinear(scip_feas, cons);
         SCIP_Real rhs = SCIPgetRhsLinear(scip_feas, cons);
         int indexSmaller;
         int indexGreater;

         if( SCIPgetNVarsLinear(scip_feas, cons) != 2 )
         {
            SCIPwarningMessage(scip, "Number of variables differs from 2 for symmetry breaking constraint %s\n", SCIPconsGetName(cons));
            continue;
         }

         if( !((SCIPisEQ(scip, vals_lin[0], -1.0) && SCIPisEQ(scip, vals_lin[1], 1.0)) || (SCIPisEQ(scip, vals_lin[0], 1.0) && SCIPisEQ(scip, vals_lin[1], -1.0))) || !((SCIPisInfinity(scip, -lhs) && SCIPisZero(scip, rhs)) || (SCIPisZero(scip, lhs) && SCIPisInfinity(scip, rhs))) )
         {
            SCIPwarningMessage(scip, "Dont have format x <= y for symmetry breaking constraint %s\n", SCIPconsGetName(cons));
            continue;
         }

         if( SCIPisZero(scip, lhs) )
         {
            if( vals_lin[0] < 0.0 )
            {
               indexSmaller = findVariableIndex(vars_lin[0], consdata->vars_x_feas, consdata->nvars_x);
               indexGreater = findVariableIndex(vars_lin[1], consdata->vars_x_feas, consdata->nvars_x);
            }
            else
            {
               indexSmaller = findVariableIndex(vars_lin[1], consdata->vars_x_feas, consdata->nvars_x);
               indexGreater = findVariableIndex(vars_lin[0], consdata->vars_x_feas, consdata->nvars_x);
            }
         }
         else
         {
            if( vals_lin[0] < 0.0 )
            {
               indexSmaller = findVariableIndex(vars_lin[1], consdata->vars_x_feas, consdata->nvars_x);
               indexGreater = findVariableIndex(vars_lin[0], consdata->vars_x_feas, consdata->nvars_x);
            }
            else
            {
               indexSmaller = findVariableIndex(vars_lin[0], consdata->vars_x_feas, consdata->nvars_x);
               indexGreater = findVariableIndex(vars_lin[1], consdata->vars_x_feas, consdata->nvars_x);
            }
         }

         if( indexSmaller == -1 || indexGreater == -1 )
            continue;

         if( varsToConsSmaller[indexSmaller] > -1 || varsToConsGreater[indexGreater] > -1)
         {
            SCIPwarningMessage(scip, "Variables already considered in another symmetry breaking constraint. Ignoring %s\n", SCIPconsGetName(cons));
            continue;
         }
         varsToConsSmaller[indexSmaller] = nSyms;
         varsToConsGreater[indexGreater] = nSyms;
         nSyms++;
      }
   }


   /* now loop through all constraints of scip to check that the symmetries are also there. If so, we add it into scip_int */
   nconss = SCIPgetNConss(scip);
   conss = SCIPgetConss(scip);
   assert( conss != NULL );

   for( c = 0; c < nconss; ++c )
   {
      SCIP_CONS* cons;
      SCIP_CONS* newCons;

      /* get constraint */
      cons = conss[c];
      assert( cons != NULL );

      if( strcmp(SCIPconshdlrGetName(SCIPconsGetHdlr(cons)), "linear") == 0 && strstr(SCIPconsGetName(cons), "symbreak") )
      {
         SCIP_VAR** vars_lin = SCIPgetVarsLinear(scip_feas, cons);
         SCIP_Real* vals_lin = SCIPgetValsLinear(scip_feas, cons);
         SCIP_Real lhs = SCIPgetLhsLinear(scip_feas, cons);
         SCIP_Real rhs = SCIPgetRhsLinear(scip_feas, cons);
         int indexSmaller;
         int indexGreater;

         if( SCIPgetNVarsLinear(scip_feas, cons) != 2 )
         {
            SCIPwarningMessage(scip, "Number of variables differs from 2 for symmetry breaking constraint %s\n", SCIPconsGetName(cons));
            continue;
         }

         if( !((SCIPisEQ(scip, vals_lin[0], -1.0) && SCIPisEQ(scip, vals_lin[1], 1.0)) || (SCIPisEQ(scip, vals_lin[0], 1.0) && SCIPisEQ(scip, vals_lin[1], -1.0))) || !((SCIPisInfinity(scip, -lhs) && SCIPisZero(scip, rhs)) || (SCIPisZero(scip, lhs) && SCIPisInfinity(scip, rhs))) )
         {
            SCIPwarningMessage(scip, "Dont have format x <= y for symmetry breaking constraint %s\n", SCIPconsGetName(cons));
            continue;
         }

         if( SCIPisZero(scip, lhs) )
         {
            if( vals_lin[0] < 0.0 )
            {
               indexSmaller = findVariableIndex(vars_lin[0], consdata->vars_x, consdata->nvars_x);
               indexGreater = findVariableIndex(vars_lin[1], consdata->vars_x, consdata->nvars_x);
            }
            else
            {
               indexSmaller = findVariableIndex(vars_lin[1], consdata->vars_x, consdata->nvars_x);
               indexGreater = findVariableIndex(vars_lin[0], consdata->vars_x, consdata->nvars_x);
            }
         }
         else
         {
            if( vals_lin[0] < 0.0 )
            {
               indexSmaller = findVariableIndex(vars_lin[1], consdata->vars_x, consdata->nvars_x);
               indexGreater = findVariableIndex(vars_lin[0], consdata->vars_x, consdata->nvars_x);
            }
            else
            {
               indexSmaller = findVariableIndex(vars_lin[0], consdata->vars_x, consdata->nvars_x);
               indexGreater = findVariableIndex(vars_lin[1], consdata->vars_x, consdata->nvars_x);
            }
         }

         if( indexSmaller == -1 || indexGreater == -1 )
            continue;

         if( varsToConsSmaller[indexSmaller] != varsToConsGreater[indexGreater] )
         {
            SCIPwarningMessage(scip, "Variables of symmetry breaking constraint %s. Are not symmetric in subscip\n", SCIPconsGetName(cons));
            continue;
         }
         if( varsToConsSmaller[indexSmaller] == -1 )
         {
            continue;
         }

         SCIPinfoMessage(scip, NULL, "added symbreaking between %s >= %s.\n", SCIPvarGetName(consdata->vars_alpha[indexSmaller]), SCIPvarGetName(consdata->vars_alpha[indexGreater]));
         (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "SymBreak#%d", varsToConsSmaller[indexSmaller]);
         SCIP_CALL( SCIPcreateConsBasicLinear(consdata->scip_int, &newCons, name, 0, NULL, NULL, -SCIPinfinity(scip), 0.0) );
         SCIP_CALL( SCIPaddCoefLinear(consdata->scip_int, newCons, consdata->vars_alpha[indexSmaller], -1.0) );
         SCIP_CALL( SCIPaddCoefLinear(consdata->scip_int, newCons, consdata->vars_alpha[indexGreater], 1.0) );
         SCIP_CALL( SCIPaddCons(consdata->scip_int, newCons) );
         SCIP_CALL( SCIPreleaseCons(consdata->scip_int, &newCons) );

         varsToConsSmaller[indexSmaller] = -1;
         varsToConsGreater[indexGreater] = -1;
      }
   }

   /* check that all subscip symmetries are also in scip */
   for( i = 0; i < consdata->nvars_x; ++i )
   {
      if( varsToConsSmaller[i] > -1 || varsToConsGreater[i] > -1 )
      {
         SCIPerrorMessage("Variables %s was in a symmetry breaking constriant in scipfeas but not in main scip.\n", SCIPvarGetName(consdata->vars_x_feas[i]));
      }

   }

   SCIPfreeBufferArray(scip, &varsToConsGreater);
   SCIPfreeBufferArray(scip, &varsToConsSmaller);

   return SCIP_OKAY;
}

/* detects symmetry breaking constraints from a given scip x <= y, where x and y belong to a given array
   of variables. Removes these constraints. */
SCIP_RETCODE detectSymmetryBreakingAndDelete(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_VAR**            vars,               /**< vars to which the symmetry breaking constraints belong */
   int                   nvars               /**< number of variables */
   )
{
   int c;
   SCIP_CONS** conss;
   int nconss;
   int nremoved = 0;

   assert(scip != NULL);
   assert(vars != NULL);

   nconss = SCIPgetNConss(scip);
   conss = SCIPgetConss(scip);
   assert( conss != NULL );

   /* loop through all constraints to identify symmetry breaking constraints */
   for( c = 0; c < nconss; ++c )
   {
      SCIP_CONS* cons;

      /* get constraint */
      cons = conss[c];
      assert( cons != NULL );

      if( strcmp(SCIPconshdlrGetName(SCIPconsGetHdlr(cons)), "linear") == 0 && strstr(SCIPconsGetName(cons), "symbreak") )
      {
         SCIP_VAR** vars_lin = SCIPgetVarsLinear(scip, cons);
         SCIP_Real* vals_lin = SCIPgetValsLinear(scip, cons);
         SCIP_Real lhs = SCIPgetLhsLinear(scip, cons);
         SCIP_Real rhs = SCIPgetRhsLinear(scip, cons);

         if( SCIPgetNVarsLinear(scip, cons) != 2 )
         {
            SCIPwarningMessage(scip, "Number of variables differs from 2 for symmetry breaking constraint %s\n", SCIPconsGetName(cons));
            continue;
         }

         if( !((SCIPisEQ(scip, vals_lin[0], -1.0) && SCIPisEQ(scip, vals_lin[1], 1.0)) || (SCIPisEQ(scip, vals_lin[0], 1.0) && SCIPisEQ(scip, vals_lin[1], -1.0))) || !((SCIPisInfinity(scip, -lhs) && SCIPisZero(scip, rhs)) || (SCIPisZero(scip, lhs) && SCIPisInfinity(scip, rhs))) )
         {
            SCIPwarningMessage(scip, "Dont have format x <= y for symmetry breaking constraint %s\n", SCIPconsGetName(cons));
            continue;
         }

         if( findVariableIndex(vars_lin[0], vars, nvars) >= 0 && findVariableIndex(vars_lin[1], vars, nvars) >= 0 )
         {
            nremoved++;
            SCIP_CALL( SCIPdelCons(scip, cons) );
         }
      }
   }

   SCIPinfoMessage(scip, NULL, "Removed %d symbreaking constraints from %s.\n", nremoved, SCIPgetProbName(scip));

   return SCIP_OKAY;
}


/* creates the separation problem.
   min x^t alpha + (k+1) * y^t beta
   s.t. xx^t alpha + yy^t beta >= 1 forall (xx, yy) in scip_feas

   The forall constraint is added as an interdiction constraint.
*/
static
SCIP_RETCODE SCIPIntInit(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONSDATA*        consdata,           /**< constraint data */
   SCIP*                 scip_feas,
   SCIP_VAR**            vars_x_feas,
   SCIP_VAR**            vars_y_feas
   )
{
   int i;
   char name[SCIP_MAXSTRLEN];
   SCIP* scip_int;
   SCIP_VAR** cons_int_vars_x;
   SCIP_VAR** cons_int_vars_y;
   int cons_int_nvars;
   SCIP_Bool usereopt;
   SCIP_Bool usefix;

   assert(scip != NULL);
   assert(consdata != NULL);

   SCIP_CALL( SCIPcreate(&(consdata->scip_int)) );
   scip_int = consdata->scip_int;
   SCIP_CALL( SCIPcreateProbBasic(scip_int, "interdiction") );
   SCIP_CALL( SCIPincludeDefaultPlugins(scip_int) );
   SCIP_CALL( SCIPincludeConshdlrInterdiction(scip_int) );

   SCIP_CALL( SCIPsetMessagehdlrPrefix(scip_int, "\t\tscip_int: ") );
   SCIPsetMessagehdlrQuiet(scip_int, TRUE);

   /* create variables */
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &consdata->vars_alpha, consdata->nvars_x) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &consdata->vars_beta, consdata->nvars_y) );

   for( i = 0; i < consdata->nvars_x; ++i )
   {
      (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "alpha#%d", i);
      SCIP_CALL( SCIPcreateVarBasic(scip_int, &consdata->vars_alpha[i], name, 0.0, 1.0, 0.0, SCIP_VARTYPE_BINARY ) );
      SCIP_CALL( SCIPaddVar(scip_int, consdata->vars_alpha[i]) );
   }

   for( i = 0; i < consdata->nvars_y; ++i )
   {
      (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "beta#%d", i);
      SCIP_CALL( SCIPcreateVarBasic(scip_int, &consdata->vars_beta[i], name, 0.0, 1.0, 0.0, SCIP_VARTYPE_BINARY ) );
      SCIP_CALL( SCIPaddVar(scip_int, consdata->vars_beta[i]) );
   }

   SCIP_CALL( SCIPallocBufferArray(scip, &cons_int_vars_x, 2 * consdata->nvars_x + consdata->nvars_y) );
   SCIP_CALL( SCIPallocBufferArray(scip, &cons_int_vars_y, 2 * consdata->nvars_x + consdata->nvars_y) );

   /* gather variables in an array to add the interdiction constraint */
   cons_int_nvars = 0;
   for( i = 0; i < consdata->nvars_x; ++i )
   {
      cons_int_vars_x[cons_int_nvars] = consdata->vars_alpha[i];
      cons_int_vars_y[cons_int_nvars] = vars_x_feas[i];
      cons_int_nvars++;

   }

   for( i = 0; i < consdata->nvars_y; ++i )
   {
      cons_int_vars_x[cons_int_nvars] = consdata->vars_beta[i];
      cons_int_vars_y[cons_int_nvars] = vars_y_feas[i];
      cons_int_nvars++;
   }



   SCIP_CALL( SCIPgetBoolParam(scip, "cons_kresilient/usereoptimization", &usereopt) );
   SCIP_CALL( SCIPgetBoolParam(scip, "cons_kresilient/usefixing", &usefix) );

   SCIP_CALL( SCIPcreateConsBasicInterdiction(scip_int, &(consdata->cons_interdiction), "interdiction", cons_int_nvars, cons_int_vars_x, cons_int_vars_y, scip_feas, usereopt, usefix) );
   SCIP_CALL( SCIPaddCons(scip_int, (consdata->cons_interdiction)) );

   SCIPfreeBufferArray(scip, &cons_int_vars_y);
   SCIPfreeBufferArray(scip, &cons_int_vars_x);

   return SCIP_OKAY;
}



/* creates constraint data and stores the values */
static
SCIP_RETCODE consdataCreate(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONSDATA**       consdata,           /**< constraint data */
   int                   nvars,              /**< number of variables which have to be treated */
   SCIP_VAR**            vars,               /**< variables in scip */
   SCIP_VAR**            vars_feas,          /**< variables in scip_feas */
   SCIP_Bool*            vars_interdictable, /**< which of the variables are interdictable */
   SCIP*                 scip_feas,          /**< SCIP data structure for the subproblem */
   int                   k                   /**< number of possible interdictions */
   )
{
   int i;
   SCIP_Bool infeasible;
   int nvars_interdictable = 0;
   SCIP_VAR** vars_x_feas;
   SCIP_VAR** vars_y_feas;
   SCIP_Bool valid;
   SCIP_HASHMAP* varmap;
   SCIP_Bool usesymmetry;
   SCIP_Bool useextendedcuts;

   assert(scip != NULL);
   assert(consdata != NULL);
   assert(vars != NULL);
   assert(vars_feas != NULL);
   assert(vars_interdictable != NULL);
   assert(scip_feas != NULL);
   assert(nvars > 0);
   assert(k > 0);

   /* count ninterdictable */
   for( i = 0; i < nvars; i++ )
   {
      assert(vars[i] != NULL);
      assert(vars_feas[i] != NULL);
      if( vars_interdictable[i] )
         nvars_interdictable++;
   }

   /* alloc block memory */
   SCIP_CALL( SCIPallocBlockMemory(scip, consdata) );
   (*consdata)->nvars_x = 0;
   (*consdata)->nvars_y = 0;
   (*consdata)->vars_x = NULL;
   (*consdata)->vars_y = NULL;
   (*consdata)->vars_alpha = NULL;
   (*consdata)->vars_beta = NULL;
   (*consdata)->scip_int = NULL;
   (*consdata)->randnumgen = NULL;
   (*consdata)->k = k;
   (*consdata)->nenforced = 0;
   (*consdata)->scip_feas = NULL;

   (*consdata)->nscipintsolves = 0;
   (*consdata)->timescipintsolves = 0.0;
   (*consdata)->nnodesscipint = 0;

   /* split the variables into vars_x (interdictable) and vars_y (notinterdictable) */
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &(*consdata)->vars_x, nvars_interdictable) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &(*consdata)->vars_y, nvars-nvars_interdictable) );

   SCIP_CALL( SCIPallocBufferArray(scip, &vars_x_feas, nvars_interdictable) );
   SCIP_CALL( SCIPallocBufferArray(scip, &vars_y_feas, nvars-nvars_interdictable) );

   for( i = 0; i < nvars; i++ )
   {
      if( vars_interdictable[i] )
      {
         (*consdata)->vars_x[(*consdata)->nvars_x] = vars[i];
         vars_x_feas[(*consdata)->nvars_x] = vars_feas[i];
         (*consdata)->nvars_x++;
      }
      else
      {
         (*consdata)->vars_y[(*consdata)->nvars_y] = vars[i];
         vars_y_feas[(*consdata)->nvars_y] = vars_feas[i];
         (*consdata)->nvars_y++;
      }
   }

   /* turn all linking variables in scip to binary and capture them */
   for( i = 0; i < nvars; ++i )
   {
      SCIP_CALL( SCIPcaptureVar(scip, vars[i]) );
      SCIP_CALL( SCIPchgVarLb(scip, vars[i], 0.0) );
      SCIP_CALL( SCIPchgVarUb(scip, vars[i], 1.0) );
      if( !SCIPvarIsBinary(vars[i]) )
      {
         SCIP_CALL( SCIPchgVarType(scip, vars[i], SCIP_VARTYPE_BINARY, &infeasible) );
         if( infeasible )
            return SCIP_ERROR;
      }
   }


   /* if we should not use symmetry in scip_feas, we have to remove it there */
   SCIP_CALL( SCIPgetBoolParam(scip, "allowSymmetryInThirdLevel", &usesymmetry) );
   SCIP_CALL( SCIPgetBoolParam(scip, "cons_kresilient/useextendedcuts", &useextendedcuts) );
   if( !usesymmetry || useextendedcuts)
   {
      SCIP_CALL( detectSymmetryBreakingAndDelete(scip_feas, vars_feas, nvars) );
   }


   /* we copy the original scip_feas, so that we dont have the transformed scip_feas from the cons_int in scip_int */
   SCIP_CALL( SCIPcreate(&((*consdata)->scip_feas)) );
   SCIP_CALL( SCIPhashmapCreate(&varmap, SCIPblkmem(scip), SCIPgetNVars(scip_feas)) );
   SCIP_CALL( SCIPcopyOrig(scip_feas, (*consdata)->scip_feas, varmap, NULL, "", FALSE, TRUE, FALSE, &valid) );
   if( !valid )
      return SCIP_ERROR;

   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &(*consdata)->vars_x_feas, (*consdata)->nvars_x) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &(*consdata)->vars_y_feas, nvars-nvars_interdictable) );
   for( i = 0; i < (*consdata)->nvars_x; ++i )
   {
      (*consdata)->vars_x_feas[i] = (SCIP_VAR*) SCIPhashmapGetImage(varmap, vars_x_feas[i]);
      assert( (*consdata)->vars_x_feas[i] != NULL );
      SCIP_CALL( SCIPcaptureVar((*consdata)->scip_feas, (*consdata)->vars_x_feas[i]) );
   }
   for( i = 0; i < (*consdata)->nvars_y; ++i )
   {
      (*consdata)->vars_y_feas[i] = (SCIP_VAR*) SCIPhashmapGetImage(varmap, vars_y_feas[i]);
      assert( (*consdata)->vars_y_feas[i] != NULL );
      SCIP_CALL( SCIPcaptureVar((*consdata)->scip_feas, (*consdata)->vars_y_feas[i]) );
   }
   SCIPhashmapFree(&varmap);

   /* init scip_int */
   SCIP_CALL( SCIPIntInit(scip, *consdata, scip_feas, vars_x_feas, vars_y_feas) );

   SCIPfreeBufferArray(scip, &vars_y_feas);
   SCIPfreeBufferArray(scip, &vars_x_feas);


   if( usesymmetry )
   {
      SCIP_CALL( detectSymmetryBreakingAndAddToScipInt(scip, *consdata) );
   }

   SCIP_CALL( SCIPcreateRandom(scip, &(*consdata)->randnumgen, SCIPinitializeRandomSeed(scip, DEFAULT_RANDSEED), TRUE) );

   return SCIP_OKAY;
}


/* given an interdiction vals_int (for x), add the extended formulation constraints to
   enforce this failure scenario xx_i = x_i for vals_int[i] = 0; xx_i = 0 else; yy_i <= y_i,
   (xx_i, yy_i) in scipfeas.
 */
static
SCIP_RETCODE SCIPaddExtendedVariablesCut(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONS*            cons,               /**< constraint */
   SCIP_Bool*            vals_int,           /**< values of interdiction */
   SCIP_RESULT*          result              /**< result pointer */
   )
{
   int i;
   int c;
   int nconss;
   SCIP_CONS** conss;
   SCIP_CONS* newcons;
   SCIP_VAR* newvar;
   SCIP* scip_feas;
   SCIP_CONSDATA* consdata;
   char name[SCIP_MAXSTRLEN];
   SCIP_HASHMAP* varmap;
   SCIP_HASHMAP* consmap;
   int nvars_scipfeas;
   SCIP_VAR** vars_scipfeas;
   SCIP_Real lb;
   SCIP_Real ub;

   assert(scip != NULL);
   assert(cons != NULL);
   assert(vals_int != NULL);
   assert(result != NULL);

   consdata = SCIPconsGetData(cons);
   assert(consdata != NULL);

   scip_feas = consdata->scip_feas;
   nvars_scipfeas = SCIPgetNVars(scip_feas);
   vars_scipfeas = SCIPgetVars(scip_feas);

   /* create a copy of each scipfeas variable in scip */
   SCIP_CALL( SCIPhashmapCreate(&varmap, SCIPblkmem(scip), nvars_scipfeas) );
   for( i = 0; i < nvars_scipfeas; ++i )
   {
      lb = SCIPvarGetLbGlobal(vars_scipfeas[i]);
      ub = SCIPvarGetUbGlobal(vars_scipfeas[i]);
      (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "Added#%d#%s", consdata->nenforced, SCIPvarGetName(vars_scipfeas[i]));

      SCIP_CALL( SCIPcreateVarBasic(scip, &newvar, name, lb, ub, 0.0, SCIPvarGetType(vars_scipfeas[i])) );
      SCIP_CALL( SCIPhashmapInsert(varmap, vars_scipfeas[i], newvar) );
      SCIP_CALL( SCIPaddVar(scip, newvar) );
      SCIP_CALL( SCIPreleaseVar(scip, &newvar) );
   }

   /* create copies of constraints and add them, use a hashmap since cons_indicator, seems to look for its linear
      cons */
   nconss = SCIPgetNConss(scip_feas);
   conss = SCIPgetConss(scip_feas);
   SCIP_CALL( SCIPhashmapCreate(&consmap, SCIPblkmem(scip), nconss) );
   for( c = 0; c < nconss; ++c )
   {
      SCIP_Bool valid;
      assert( conss[c] != NULL );

      (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "Added#%d#%s", consdata->nenforced, SCIPconsGetName(conss[c]));
      SCIP_CALL( SCIPgetConsCopy(scip_feas, scip, conss[c], &newcons, SCIPconsGetHdlr(conss[c]), varmap, consmap, name,
            SCIPconsIsInitial(conss[c]), SCIPconsIsSeparated(conss[c]), SCIPconsIsEnforced(conss[c]), SCIPconsIsChecked(conss[c]),
            SCIPconsIsPropagated(conss[c]), FALSE, FALSE, SCIPconsIsDynamic(conss[c]),
            SCIPconsIsRemovable(conss[c]), FALSE, TRUE, &valid) );

      if( !valid )
      {
         SCIPerrorMessage("problems in constraint copying %s\n", SCIPconshdlrGetName(SCIPconsGetHdlr(conss[c])));
         return SCIP_ERROR;
      }
      SCIP_CALL( SCIPaddCons(scip, newcons) );
      SCIP_CALL( SCIPreleaseCons(scip, &newcons) );
   }

   /* link vars by xx <= x, if not interdicted. Otherwise set xx == 0 */
   for( i = 0; i < consdata->nvars_x; ++i )
   {
      newvar = (SCIP_VAR*) SCIPhashmapGetImage(varmap, consdata->vars_x_feas[i]);
      assert( newvar != NULL );

      (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "LinkingConstraints#%d#%d", consdata->nenforced, i);

      SCIP_CALL( SCIPcreateConsBasicLinear(scip, &newcons, name, 0, NULL, NULL, -SCIPinfinity(scip), 0.0) );

      SCIP_CALL( SCIPaddCoefLinear(scip, newcons, newvar, 1.0) );

      if( !vals_int[i] )
         SCIP_CALL( SCIPaddCoefLinear(scip, newcons, consdata->vars_x[i], -1.0) );

      SCIP_CALL( SCIPaddCons(scip, newcons) );
      SCIP_CALL( SCIPreleaseCons(scip, &newcons) );
   }

   /* link the non interdictable vars by yy == y */
   for( i = 0; i < consdata->nvars_y; ++i )
   {
      newvar = (SCIP_VAR*) SCIPhashmapGetImage(varmap, consdata->vars_y_feas[i]);
      assert( newvar != NULL );

      (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "LinkingConstraints#%d#%d", consdata->nenforced, i);

      SCIP_CALL( SCIPcreateConsBasicLinear(scip, &newcons, name, 0, NULL, NULL, -SCIPinfinity(scip), 0.0) );
      SCIP_CALL( SCIPaddCoefLinear(scip, newcons, newvar, 1.0) );
      SCIP_CALL( SCIPaddCoefLinear(scip, newcons, consdata->vars_y[i], -1.0) );
      SCIP_CALL( SCIPaddCons(scip, newcons) );
      SCIP_CALL( SCIPreleaseCons(scip, &newcons) );
   }

   SCIPhashmapFree(&consmap);
   SCIPhashmapFree(&varmap);

   consdata->nenforced++;
   *result = SCIP_CONSADDED;

   SCIPrestartSolve(scip); // TODO check, without this gaslib_40 becomes infeasible...

   return SCIP_OKAY;
}


/* given coefficients a, b add the resilience cut a^T x + (k+1) * + (k+1) * b^T y >= k+1 as a linear
   constraint */
static
SCIP_RETCODE SCIPaddConsCut(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONS*            cons,               /**< constraint */
   SCIP_Bool*            vals_alpha_bool,    /**< cut coeficients for x */
   SCIP_Bool*            vals_beta_bool,     /**< cut coeficients for y */
   SCIP_RESULT*          result              /**< result pointer */
   )
{
   int i;
   SCIP_CONS*  consCut;
   SCIP_CONSDATA* consdata;
   char name[SCIP_MAXSTRLEN];

   assert(scip != NULL);
   assert(cons != NULL);
   assert(vals_alpha_bool != NULL);
   assert(vals_beta_bool != NULL);
   assert(result != NULL);

   consdata = SCIPconsGetData(cons);
   assert(consdata != NULL);

   (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "ResilienceCut#%d", consdata->nenforced++);
   SCIP_CALL( SCIPcreateConsBasicLinear(scip, &consCut, name, 0, NULL, NULL, consdata->k+1.0, SCIPinfinity(scip)) );
   for( i = 0; i < consdata->nvars_x; ++i )
   {
      if( vals_alpha_bool[i] )
      {
         SCIP_CALL( SCIPaddCoefLinear(scip, consCut, consdata->vars_x[i], 1.0) );
      }
   }

   for( i = 0; i < consdata->nvars_y; ++i )
   {
      if( vals_beta_bool[i] )
      {
         SCIP_CALL( SCIPaddCoefLinear(scip, consCut, consdata->vars_y[i], consdata->k+1.0) );
      }
   }

   SCIP_CALL( SCIPaddCons(scip, consCut) );
   SCIP_CALL( SCIPreleaseCons(scip, &consCut) );

   *result = SCIP_CONSADDED;

   return SCIP_OKAY;
}

/* given cut coefficients test whether they are feasible in scip_int. If there is not enough timeleft, we set aborted =
   TRUE and return feasible = FALSE */
static
SCIP_RETCODE SCIPtestInterdiction(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONS*            cons,               /**< constraint */
   SCIP_Bool*            vals_alpha_bool,    /**< alpha values of interdiction */
   SCIP_Bool*            vals_beta_bool,     /**< beta values of interdiction */
   SCIP_Bool*            feasible,           /**< true if given interdiction is feasible and testing was not aborted */
   SCIP_Bool*            aborted             /**< true if solving was aborted */
   )
{
   SCIP_CONSDATA* consdata;
   SCIP* scip_int;
   int i;
   SCIP_SOL* testsol;
   SCIP_Real timelimit;

   assert(scip != NULL);
   assert(cons != NULL);
   assert(vals_alpha_bool != NULL);
   assert(vals_beta_bool != NULL);
   assert(feasible != NULL);
   assert(aborted != NULL);

   consdata = SCIPconsGetData(cons);
   assert(consdata != NULL);

   scip_int = consdata->scip_int;

   *aborted = FALSE;

   SCIP_CALL( SCIPcreateOrigSol(scip_int, &testsol, NULL) );

   /* set solution and reset bounds */
   for( i = 0; i < consdata->nvars_x; ++i )
   {
      SCIP_CALL( SCIPchgVarLb(scip_int, consdata->vars_alpha[i], 0.0) );
      SCIP_CALL( SCIPchgVarUb(scip_int, consdata->vars_alpha[i], 1.0) );
      SCIP_CALL( SCIPsetSolVal(scip_int, testsol, consdata->vars_alpha[i], (SCIP_Real) vals_alpha_bool[i]) );
   }

   for( i = 0; i < consdata->nvars_y; ++i )
   {
      SCIP_CALL( SCIPchgVarLb(scip_int, consdata->vars_beta[i], 0.0) );
      SCIP_CALL( SCIPchgVarUb(scip_int, consdata->vars_beta[i], 1.0) );
      SCIP_CALL( SCIPsetSolVal(scip_int, testsol, consdata->vars_beta[i], (SCIP_Real) vals_beta_bool[i]) );
   }

   SCIP_CALL( SCIPgetRealParam(scip, "limits/time", &timelimit) );
   if( !SCIPisInfinity(scip, timelimit) )
      timelimit -= SCIPgetSolvingTime(scip);
   if( timelimit < 1.0 )
   {
      SCIPdebugMsg(scip, "aborted testInterdiction, not enough time left. (%g)\n", timelimit);
      SCIP_CALL( SCIPfreeSol(scip_int, &testsol) );
      *aborted = TRUE;
      return SCIP_OKAY;
   }

   SCIP_CALL( SCIPsetRealParam(scip_int, "limits/time", timelimit) );

   SCIP_CALL( SCIPcheckSolOrig(scip_int, testsol, feasible, FALSE, FALSE) );

   SCIP_CALL( SCIPfreeSol(scip_int, &testsol) );

   SCIP_CALL( SCIPsetRealParam(scip_int, "limits/time", SCIPinfinity(scip_int)) );

   return SCIP_OKAY;
}


/* given coefficients try to turn ones into zeros by testing them one by one. Only coefficients for which the local
   variable x or y is not fixed are considered. The order either begins from the maximum cover rank or the minimum */
static
SCIP_RETCODE SCIPimproveInterdictionCoverRank(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONS*            cons,               /**< constraint */
   SCIP_Bool             decreasing,         /**< start with the maximum cover rank first */
   SCIP_Bool*            vals_alpha_bool,    /**< alpha values of interdiction */
   SCIP_Bool*            vals_beta_bool      /**< beta values of interdiction */
   )
{
   SCIP_CONSDATA* consdata;
   int i;
   int rankingvar;
   SCIP_Bool feasible;
   SCIP_Bool aborted;
   SCIP_Bool* coefToChg; // points at array position in vals_alpha_bool or vals_beta_bool which will change

   assert(scip != NULL);
   assert(cons != NULL);
   assert(vals_alpha_bool != NULL);
   assert(vals_beta_bool != NULL);

   consdata = SCIPconsGetData(cons);
   assert(consdata != NULL);

   for( i = 0; i < consdata->nvars_x + consdata->nvars_y; ++i )
   {
      if( decreasing )
         rankingvar = consInterdictionGetIndexRanking(consdata->cons_interdiction, i);
      else
         rankingvar = consInterdictionGetIndexRanking(consdata->cons_interdiction, consdata->nvars_x + consdata->nvars_y - 1 - i);

      if( rankingvar < consdata->nvars_x )
      {
         coefToChg = &(vals_alpha_bool[rankingvar]);
      }
      else
      {
         coefToChg = &(vals_beta_bool[rankingvar-consdata->nvars_x]);
      }
      if( *coefToChg )
      {
         *coefToChg = FALSE;
         SCIP_CALL( SCIPtestInterdiction(scip, cons, vals_alpha_bool, vals_beta_bool, &feasible, &aborted) );
         if( !feasible || aborted )
            *coefToChg = TRUE;
         if( aborted )
            break;
      }
   }

   return SCIP_OKAY;
}


/* given coefficients try to turn ones into zeros by testing them one by one
   randomly. Only coefficients for which the local variable x or y is not fixed
   to zero are considered. */
static
SCIP_RETCODE SCIPimproveInterdictionRandom(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONS*            cons,               /**< constraint */
   SCIP_Bool*            vals_alpha_bool,    /**< alpha values of interdiction */
   SCIP_Bool*            vals_beta_bool      /**< beta values of interdiction */
   )
{
   SCIP_CONSDATA* consdata;
   int i;
   int* unfixed;
   int nunfixed;
   SCIP_Bool feasible;
   SCIP_Bool aborted;
   SCIP_Bool* coefToChg; // points at array position in vals_alpha_bool or vals_beta_bool which will change

   assert(scip != NULL);
   assert(cons != NULL);
   assert(vals_alpha_bool != NULL);
   assert(vals_beta_bool != NULL);

   consdata = SCIPconsGetData(cons);
   assert(consdata != NULL);

   nunfixed = 0;

   /* save the indice of the one values, i.e., the unfixed */
   SCIP_CALL( SCIPallocBufferArray(scip, &unfixed, 2*consdata->nvars_x + consdata->nvars_y) );
   for( i = 0; i < consdata->nvars_x; ++i )
   {
      if( vals_alpha_bool[i] && SCIPvarGetUbLocal(consdata->vars_alpha[i]) > 0.5 )
      {
         unfixed[nunfixed++] = i;
      }
   }

   for( i = 0; i < consdata->nvars_y; ++i )
   {
      if( vals_beta_bool[i] && SCIPvarGetUbLocal(consdata->vars_beta[i]) > 0.5  )
      {
         unfixed[nunfixed++] = i + consdata->nvars_x;
      }
   }

   /* shuffle indices */
   SCIPrandomPermuteIntArray(consdata->randnumgen, unfixed, 0, nunfixed);

   /* go through the indices and check whether turning it to zero is still a valid interdiction  */
   for( i = 0; i < nunfixed; ++i )
   {
      int j = unfixed[i];

      if( j < consdata->nvars_x )
      {
         coefToChg = &(vals_alpha_bool[j]);
      }
      else
      {
         coefToChg = &(vals_beta_bool[j-consdata->nvars_x]);
      }

      assert(*coefToChg);

      *coefToChg = FALSE;

      SCIP_CALL( SCIPtestInterdiction(scip, cons, vals_alpha_bool, vals_beta_bool, &feasible, &aborted) );
      if( !feasible || aborted )
         *coefToChg = TRUE;
      if( aborted )
         break;
   }

   SCIPfreeBufferArray(scip, &unfixed);

   return SCIP_OKAY;
}


/** given valid interdiction coefficients try to turn ones into zeros by solving scip_int. */
static
SCIP_RETCODE SCIPimproveInterdictionResolve(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONS*            cons,               /**< constraint */
   SCIP_Bool*            vals_alpha_bool,    /**< alpha values of interdiction */
   SCIP_Bool*            vals_beta_bool      /**< beta values of interdiction */
   )
{
   SCIP_CONSDATA* consdata;
   SCIP* scip_int;
   int i;
   SCIP_STATUS scip_int_status;
   SCIP_Longint nodelimit;
   SCIP_Longint interdictioncutlimit;
   SCIP_Real timelimit;

   assert(scip != NULL);
   assert(cons != NULL);
   assert(vals_alpha_bool != NULL);
   assert(vals_beta_bool != NULL);

   consdata = SCIPconsGetData(cons);
   assert(consdata != NULL);

   SCIP_CALL( SCIPgetLongintParam(scip, "cons_kresilient/nodesresolveimprove", &nodelimit) );
   SCIP_CALL( SCIPgetLongintParam(scip, "cons_kresilient/interdictioncutsresolveimprove", &interdictioncutlimit) );
   if( nodelimit == 0 )
      return SCIP_OKAY;

   scip_int = consdata->scip_int;

   /* set bounds for scip_int variables */
   for( i = 0; i < consdata->nvars_x; ++i )
   {
      SCIP_CALL( SCIPchgVarLb(scip_int, consdata->vars_alpha[i], 0.0) );
      SCIP_CALL( SCIPchgVarUb(scip_int, consdata->vars_alpha[i], (SCIP_Real) vals_alpha_bool[i]) );
      SCIP_CALL( SCIPchgVarObj(scip_int, consdata->vars_alpha[i], 1.0) );
   }

   for( i = 0; i < consdata->nvars_y; ++i )
   {
      SCIP_CALL( SCIPchgVarLb(scip_int, consdata->vars_beta[i], 0.0) );
      SCIP_CALL( SCIPchgVarUb(scip_int, consdata->vars_beta[i], (SCIP_Real) vals_beta_bool[i]) );
      SCIP_CALL( SCIPchgVarObj(scip_int, consdata->vars_beta[i], consdata->k+1.0) );
   }

   SCIP_CALL( SCIPgetRealParam(scip, "limits/time", &timelimit) );
   if( !SCIPisInfinity(scip, timelimit) )
      timelimit -= SCIPgetSolvingTime(scip);

   /* only solve if enough time is left */
   if( timelimit <= 1.0 )
   {
      SCIPdebugMsg(scip, "not enough time left for interdiction improvement! (%g)\n", timelimit);
      return SCIP_OKAY;
   }

   if( nodelimit >= 0 )
      SCIP_CALL( SCIPsetLongintParam(scip_int, "limits/nodes", nodelimit) );
   else
      SCIP_CALL( SCIPsetLongintParam(scip_int, "limits/nodes", -1LL) );

   SCIP_CALL( SCIPsetIntParam(scip_int, "limits/solutions", -1) );

   SCIP_CALL( SCIPsetLongintParam(scip_int, "cons_interdiction/cutlimit", interdictioncutlimit) );

   SCIP_CALL( SCIPsetRealParam(scip_int, "limits/time", timelimit) );

   SCIP_CALL( SCIPsolve(scip_int) );

   /* update statistics */
   consdata->nscipintsolves++;
   consdata->timescipintsolves += SCIPgetSolvingTime(scip_int);
   consdata->nnodesscipint += SCIPgetNNodes(scip_int);

   scip_int_status = SCIPgetStatus(scip_int);
   switch( scip_int_status )
   {
   case SCIP_STATUS_USERINTERRUPT:
      SCIPwarningMessage(scip, "User interrupt during scip_int solving. Result may be infeasible.\n");
      SCIP_CALL( SCIPinterruptSolve(scip) );
      SCIP_CALL( SCIPfreeTransform(scip_int) );
      SCIP_CALL( SCIPsetLongintParam(scip_int, "limits/nodes", -1LL) );
      SCIP_CALL( SCIPsetRealParam(scip_int, "limits/time", SCIPinfinity(scip_int)) );
      return SCIP_OKAY;

   case SCIP_STATUS_OPTIMAL:
   case SCIP_STATUS_NODELIMIT:
   case SCIP_STATUS_TIMELIMIT:
      break;
   case SCIP_STATUS_INFEASIBLE:
      SCIPerrorMessage("Improve cut returned infeasible!\n"); /* This is some kind of contradiction, since vals_alpha and vals_beta should have been a feasible solution */
      break;
   default:
      SCIPerrorMessage("scip_int was solved to unsupported status during resolving.\n");
      SCIPsetMessagehdlrQuiet(scip_int, FALSE);
      SCIP_CALL( SCIPfreeTransform(scip_int) );
      SCIP_CALL( SCIPsolve(scip_int) );
      return SCIP_ERROR;
   }

   SCIP_CALL( SCIPsetLongintParam(scip_int, "limits/nodes", -1LL) );
   SCIP_CALL( SCIPsetRealParam(scip_int, "limits/time", SCIPinfinity(scip_int)) );
   SCIP_CALL( SCIPsetLongintParam(scip_int, "cons_interdiction/cutlimit", -1LL) );

   if( SCIPgetNSols(scip_int) >= 1 )
   {
      SCIP_SOL* sol = SCIPgetBestSol(scip_int);

      for( i = 0; i < consdata->nvars_x; ++i )
      {
         vals_alpha_bool[i] = SCIPgetSolVal(scip_int, sol, consdata->vars_alpha[i]) >= 0.5;
      }

      for( i = 0; i < consdata->nvars_y; ++i )
      {
         vals_beta_bool[i] = SCIPgetSolVal(scip_int, sol, consdata->vars_beta[i]) >= 0.5;
      }
   }

   SCIP_CALL( SCIPfreeTransform(scip_int) );

   SCIP_CALL( consInterdictionAddSubscipSolsAsConstraints(scip_int, consdata->cons_interdiction) );

   return SCIP_OKAY;
}

/* given binary x and y, search for interdiction coefficients alpha, beta in scip_int such that x^t alpha +
   (k+1) y^t beta <= k. We optimize this inequality over a modified scip_int, e.g., beta_i
   can be set to 1-y_i and x_i = 0 -> alpha_i = 1, due to the structure of scip_int with only set covering constraints.
 */
static
SCIP_RETCODE SCIPcomputeInterdiction(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONS*            cons,               /**< constraint */
   SCIP_Bool*            vals_x_bool,        /**< x values of solution */
   SCIP_Bool*            vals_y_bool,        /**< x values of solution */
   SCIP_Bool*            vals_alpha_bool,    /**< alpha value of possible found interdiction can be NULL if not needed */
   SCIP_Bool*            vals_beta_bool,     /**< beta value of possible found interdiction can be NULL if not needed */
   SCIP_Bool*            found_interdiction, /**< if an inderdiction was found */
   SCIP_Bool*            aborted             /**< whether the solving of scip_int was aborted, e.g. timelimit */
   )
{
   SCIP_CONSDATA* consdata;
   SCIP* scip_int;
   int i;
   SCIP_STATUS scip_int_status;
   SCIP_Real timelimit;

   assert(scip != NULL);
   assert(cons != NULL);
   assert(vals_x_bool != NULL);
   assert(vals_y_bool != NULL);
   assert(found_interdiction != NULL);
   assert(aborted != NULL);

   consdata = SCIPconsGetData(cons);
   assert(consdata != NULL);

   *found_interdiction = FALSE;
   *aborted = FALSE;

   scip_int = consdata->scip_int;

   /* set bounds for scip_int variables */
   for( i = 0; i < consdata->nvars_x; ++i )
   {
      SCIP_CALL( SCIPchgVarUb(scip_int, consdata->vars_alpha[i], 1.0) );
      if( vals_x_bool[i] )
      {
         SCIP_CALL( SCIPchgVarLb(scip_int, consdata->vars_alpha[i], 0.0) );
         SCIP_CALL( SCIPchgVarObj(scip_int, consdata->vars_alpha[i], 1.0) );
      }
      else
      {
         SCIP_CALL( SCIPchgVarLb(scip_int, consdata->vars_alpha[i], 1.0) );
         SCIP_CALL( SCIPchgVarObj(scip_int, consdata->vars_alpha[i], 0.0) );
      }
   }

   for( i = 0; i < consdata->nvars_y; ++i )
   {
      if( vals_y_bool[i] )
      {
         SCIP_CALL( SCIPchgVarLb(scip_int, consdata->vars_beta[i], 0.0) );
         SCIP_CALL( SCIPchgVarUb(scip_int, consdata->vars_beta[i], 0.0) );
      }
      else
      {
         SCIP_CALL( SCIPchgVarLb(scip_int, consdata->vars_beta[i], 1.0) );
         SCIP_CALL( SCIPchgVarUb(scip_int, consdata->vars_beta[i], 1.0) );
      }
      SCIP_CALL( SCIPchgVarObj(scip_int, consdata->vars_beta[i], 0.0) );
   }

   SCIP_CALL( SCIPgetRealParam(scip, "limits/time", &timelimit) );
   if( !SCIPisInfinity(scip, timelimit) )
      timelimit -= SCIPgetSolvingTime(scip);

   /* only solve if enough time is left */
   if( timelimit <= 1.0 )
   {
      SCIPdebugMsg(scip, "not enough time left! (%g)\n", timelimit);
      *aborted = TRUE;
      return SCIP_OKAY;
   }
   SCIP_CALL( SCIPsetRealParam(scip_int, "limits/time", timelimit) );
   SCIP_CALL( SCIPsetIntParam(scip_int, "limits/solutions", 1) );
   /* to yield a cut, the solution value has to be at most k, objlimit normaly
      allows only solutions with < limit, however with limits/solutions = 1 we
      stop with one solution = limit sometimes. Since the objective is integral, we use k+0.5 */
   SCIP_CALL( SCIPsetObjlimit(scip_int, consdata->k + 0.5) );

   SCIP_CALL( SCIPsolve(scip_int) );

   /* update statistics */
   consdata->nscipintsolves++;
   consdata->timescipintsolves += SCIPgetSolvingTime(scip_int);
   consdata->nnodesscipint += SCIPgetNNodes(scip_int);

   scip_int_status = SCIPgetStatus(scip_int);
   switch( scip_int_status )
   {
   case SCIP_STATUS_USERINTERRUPT:
      SCIPwarningMessage(scip, "User interrupt during scip_int solving. Result may be infeasible.\n");
      SCIP_CALL( SCIPinterruptSolve(scip) );
      SCIP_CALL( SCIPfreeTransform(scip_int) );
      *aborted = TRUE;
      return SCIP_OKAY;

   case SCIP_STATUS_TIMELIMIT:
      *aborted = TRUE;
      SCIP_CALL( SCIPfreeTransform(scip_int) );
      return SCIP_OKAY;
   case SCIP_STATUS_INFEASIBLE:
   case SCIP_STATUS_OPTIMAL:
   case SCIP_STATUS_SOLLIMIT:
      break;
   default:
      SCIPerrorMessage("scip_int was solved to unsupported status.\n");
      return SCIP_ERROR;
   }

   /* check if we found a violated interdiction */
   if( SCIPgetNSols(scip_int) >= 1 && SCIPgetSolOrigObj(scip_int, SCIPgetBestSol(scip_int)) < consdata->k + 0.5 )
   {
      *found_interdiction = TRUE;

      /* copy the interdiction if we should */
      if( vals_alpha_bool != NULL )
      {
         SCIP_SOL* sol = SCIPgetBestSol(scip_int);

         for( i = 0; i < consdata->nvars_x; ++i )
         {
            vals_alpha_bool[i] = SCIPgetSolVal(scip_int, sol, consdata->vars_alpha[i]) >= 0.5;
         }
         for( i = 0; i < consdata->nvars_y; ++i )
         {
            vals_beta_bool[i] = SCIPgetSolVal(scip_int, sol, consdata->vars_beta[i]) >= 0.5;
         }
      }
   }

   /* reset scip_int */
   SCIP_CALL( SCIPfreeTransform(scip_int) );
   SCIP_CALL( SCIPsetObjlimit(scip_int, SCIPinfinity(scip_int)) );
   SCIP_CALL( SCIPsetRealParam(scip_int, "limits/time", 1e+20) );

   /* make separated covering inequalities in scip_int permanent by adding them afterwards */
   SCIP_CALL( consInterdictionAddSubscipSolsAsConstraints(scip_int, consdata->cons_interdiction) );

   return SCIP_OKAY;
}


/** check whether the given constraint is feasible, i.e., there does not exists a successful interdiction.
 *
 * If this is not the case and we inenfolp is true we treat it using a cut or depending on the setting by inclusion of
 * the subscip.
 */
static
SCIP_RETCODE checkFeasibility(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONS*            cons,               /**< constraint */
   SCIP_SOL*             sol,                /**< current solution, or NULL if LP solution should used */
   SCIP_Bool             inenfolp,           /**< whether or not method was called in enfolp */
   SCIP_Bool             printreason,        /**< whether reason for infeasibility should be printed */
   SCIP_RESULT*          result              /**< result pointer */
   )
{
   SCIP_CONSDATA* consdata;
   int i;
   SCIP_Bool* vals_x_bool;
   SCIP_Bool* vals_y_bool;
   SCIP_Bool* vals_alpha_bool = NULL;
   SCIP_Bool* vals_beta_bool = NULL;
   SCIP_Bool infeasible;
   SCIP_Bool aborted;

   assert(scip != NULL);
   assert(cons != NULL);
   assert(result != NULL);

   consdata = SCIPconsGetData(cons);
   assert(consdata != NULL);

   *result = SCIP_FEASIBLE;

   /* get solution values, priority of constraint is negative -> vars_x/vars_y values are binary */
   SCIP_CALL( SCIPallocBufferArray(scip, &vals_x_bool, consdata->nvars_x) );
   for( i = 0; i < consdata->nvars_x; ++i )
   {
      vals_x_bool[i] = SCIPgetSolVal(scip, sol, consdata->vars_x[i]) >= 0.5;
   }

   SCIP_CALL( SCIPallocBufferArray(scip, &vals_y_bool, consdata->nvars_y) );
   for( i = 0; i < consdata->nvars_y; ++i )
   {
      vals_y_bool[i] = SCIPgetSolVal(scip, sol, consdata->vars_y[i]) >= 0.5;
   }

   /* check for a successful interdiction and its coefficients only need interdiction values if we need the cut or want
      to print a reason*/
   if( inenfolp || printreason )
   {
      SCIP_CALL( SCIPallocBufferArray(scip, &vals_alpha_bool, consdata->nvars_x) );
      SCIP_CALL( SCIPallocBufferArray(scip, &vals_beta_bool, consdata->nvars_y) );
   }

   SCIP_CALL( SCIPcomputeInterdiction(scip, cons, vals_x_bool, vals_y_bool, vals_alpha_bool, vals_beta_bool, &infeasible, &aborted) );

   if( aborted )
   {
      if( inenfolp )
      {
         /* as we most likely hit the time limit or an interrupt, we want the main scip to get to a stage to confirm
          * this. In order to terminate correctly, we create a "branching" with only one child node that is a copy of
          * the focusnode
          */
         SCIP_NODE* child;
         SCIP_CALL( SCIPcreateChild(scip, &child, 0.0, SCIPgetLocalTransEstimate(scip)) );
         *result = SCIP_BRANCHED;
      }
      else
      {
         *result = SCIP_INFEASIBLE;
      }
   }
   else if( infeasible && inenfolp )
   {
      SCIP_Bool useextendedcuts;

      SCIP_CALL( SCIPgetBoolParam(scip, "cons_kresilient/useextendedcuts", &useextendedcuts) );
      if( useextendedcuts )
      {
         /* the propagator has currently numerical errors for problem with variables during solving */
         if( SCIPgetParam(scip, "propagating/sdpredcost/forintconts") != NULL )
         {
            SCIP_CALL( SCIPsetBoolParam(scip, "propagating/sdpredcost/forintconts", FALSE) );
         }

         for( i = 0; i < consdata->nvars_x; ++i )
         {
            vals_alpha_bool[i] = vals_x_bool[i] && vals_alpha_bool[i];
         }
         SCIP_CALL( SCIPaddExtendedVariablesCut(scip, cons, vals_alpha_bool, result) );
      }
      else
      {
         SCIP_Bool userandomimprove;
         int usecoverrankimprove;

         SCIP_CALL( SCIPimproveInterdictionResolve(scip, cons, vals_alpha_bool, vals_beta_bool) );

         SCIP_CALL( SCIPgetBoolParam(scip, "cons_kresilient/userandomimprove", &userandomimprove) );

         SCIP_CALL( SCIPgetIntParam(scip, "cons_kresilient/coveringimprove", &usecoverrankimprove) );

         if( usecoverrankimprove != 0 )
         {
            SCIP_Real starttime = SCIPgetSolvingTime(scip);

            SCIP_CALL( SCIPimproveInterdictionCoverRank(scip, cons, usecoverrankimprove > 0, vals_alpha_bool, vals_beta_bool) );

            consdata->timescipintsolves += (SCIPgetSolvingTime(scip)-starttime);
         }

         /* @todo Do not run if no cutlimit? then resolve is optimal */
         if( userandomimprove )
         {
            SCIP_Real starttime = SCIPgetSolvingTime(scip);

            SCIP_CALL( SCIPimproveInterdictionRandom(scip, cons, vals_alpha_bool, vals_beta_bool) );

            consdata->timescipintsolves += (SCIPgetSolvingTime(scip)-starttime);
         }
         SCIP_CALL( SCIPaddConsCut(scip, cons, vals_alpha_bool, vals_beta_bool, result) );
      }
   }
   else if( infeasible )
   {
      *result = SCIP_INFEASIBLE;
   }

   if( printreason )
   {
      if( aborted )
      {
         SCIPinfoMessage(scip, NULL, "%s: can't verify feasibility of solution due to aborted subscip solving; returned branched.\n", SCIPconsGetName(cons));
      }
      else if( infeasible )
      {
         SCIPinfoMessage(scip, NULL, "%s: violated if", SCIPconsGetName(cons));
         for( i = 0; i < consdata->nvars_x; ++i )
         {
            if( vals_x_bool[i] && vals_alpha_bool[i] )
            {
               SCIPinfoMessage(scip, NULL, " %s", SCIPvarGetName(consdata->vars_x[i]));
            }
         }
         SCIPinfoMessage(scip, NULL, " are interdicted.\n");
      }
   }

   if( inenfolp || printreason )
   {
      SCIPfreeBufferArray(scip, &vals_beta_bool);
      SCIPfreeBufferArray(scip, &vals_alpha_bool);
   }
   SCIPfreeBufferArray(scip, &vals_y_bool);
   SCIPfreeBufferArray(scip, &vals_x_bool);

   return SCIP_OKAY;
}

/*
 * Callback methods of constraint handler
 */

/** frees specific constraint data */
static
SCIP_DECL_CONSDELETE(consDeleteKResilient)
{  /*lint --e{715}*/
   int i;
   SCIP* scip_int;
   assert(consdata != NULL);

   scip_int = (*consdata)->scip_int;

   /* release variables */
   for( i = 0; i < (*consdata)->nvars_x; ++i )
   {
      SCIP_CALL( SCIPreleaseVar(scip, &(*consdata)->vars_x[i]) );
      SCIP_CALL( SCIPreleaseVar(scip_int, &(*consdata)->vars_alpha[i]) );
      SCIP_CALL( SCIPreleaseVar((*consdata)->scip_feas, &(*consdata)->vars_x_feas[i]) );
   }

   for( i = 0; i < (*consdata)->nvars_y; ++i )
   {
      SCIP_CALL( SCIPreleaseVar(scip, &(*consdata)->vars_y[i]) );
      SCIP_CALL( SCIPreleaseVar(scip_int, &(*consdata)->vars_beta[i]) );
      SCIP_CALL( SCIPreleaseVar((*consdata)->scip_feas, &(*consdata)->vars_y_feas[i]) );
   }

   SCIPfreeBlockMemoryArray(scip, &(*consdata)->vars_x, (*consdata)->nvars_x);
   SCIPfreeBlockMemoryArray(scip, &(*consdata)->vars_alpha, (*consdata)->nvars_x);
   SCIPfreeBlockMemoryArray(scip, &(*consdata)->vars_y, (*consdata)->nvars_y);
   SCIPfreeBlockMemoryArray(scip, &(*consdata)->vars_beta, (*consdata)->nvars_y);
   SCIPfreeBlockMemoryArray(scip, &(*consdata)->vars_x_feas, (*consdata)->nvars_x);
   SCIPfreeBlockMemoryArray(scip, &(*consdata)->vars_y_feas, (*consdata)->nvars_y);

   SCIP_CALL( SCIPreleaseCons(scip_int, &((*consdata)->cons_interdiction)) );

   SCIPfreeRandom(scip, &((*consdata)->randnumgen));

   SCIP_CALL( SCIPfree(&(*consdata)->scip_feas) );
   /* print a statistic */
   printf("# separated scip_feas solutions %d\n", SCIPgetNConss((*consdata)->scip_int) - 1);
   printf("# scipint solves %d \n", (*consdata)->nscipintsolves);
   printf("Total time scipint solving %f \n", (*consdata)->timescipintsolves);
   printf("Total nodes scipint solving %lld \n", (*consdata)->nnodesscipint);
   SCIP_CALL( SCIPfree(&(*consdata)->scip_int) );
   SCIPfreeBlockMemory(scip, consdata);

   return SCIP_OKAY;
}

/** constraint enforcing method of constraint handler for LP solutions */
static
SCIP_DECL_CONSENFOLP(consEnfolpKResilient)
{  /*lint --e{715}*/
   int c;

   assert( scip != NULL );
   assert( conshdlr != NULL );
   assert( strcmp(SCIPconshdlrGetName(conshdlr), CONSHDLR_NAME) == 0 );
   assert( result != NULL );

   *result = SCIP_FEASIBLE;

   /* loop through constraints */
   for( c = 0; c < nconss && *result == SCIP_FEASIBLE; ++c )
   {
      assert( conss[c] != NULL );

      SCIP_CALL( checkFeasibility(scip, conss[c], NULL, TRUE, FALSE, result) );
   }
   assert(*result == SCIP_FEASIBLE || *result == SCIP_INFEASIBLE || *result == SCIP_CONSADDED || *result == SCIP_BRANCHED);

   return SCIP_OKAY;
}


/** constraint enforcing method of constraint handler for relaxation solutions */
static
SCIP_DECL_CONSENFORELAX(consEnforelaxKResilient)
{  /*lint --e{715}*/
   int c;

   assert( scip != NULL );
   assert( conshdlr != NULL );
   assert( strcmp(SCIPconshdlrGetName(conshdlr), CONSHDLR_NAME) == 0 );
   assert( result != NULL );

   *result = SCIP_FEASIBLE;

   /* loop through constraints */
   for( c = 0; c < nconss && *result == SCIP_FEASIBLE; ++c )
   {
      assert( conss[c] != NULL );

      SCIP_CALL( checkFeasibility(scip, conss[c], sol, TRUE, FALSE, result) );
   }
   assert(*result == SCIP_FEASIBLE || *result == SCIP_INFEASIBLE || *result == SCIP_CONSADDED || *result == SCIP_BRANCHED);

   return SCIP_OKAY;
}


/** constraint enforcing method of constraint handler for pseudo solutions */
static
SCIP_DECL_CONSENFOPS(consEnfopsKResilient)
{  /*lint --e{715}*/
   int c;

   assert( scip != NULL );
   assert( conshdlr != NULL );
   assert( strcmp(SCIPconshdlrGetName(conshdlr), CONSHDLR_NAME) == 0 );
   assert( result != NULL );

   *result = SCIP_FEASIBLE;

   /* loop through constraints */
   for( c = 0; c < nconss && *result == SCIP_FEASIBLE; ++c )
   {
      assert( conss[c] != NULL );

      SCIP_CALL( checkFeasibility(scip, conss[c], NULL, FALSE, FALSE, result) );
   }
   assert(*result == SCIP_FEASIBLE || *result == SCIP_INFEASIBLE);

   /* TODO is there some more intelligent solution than letting SCIP branch? */
   return SCIP_OKAY;
}


/** feasibility check method of constraint handler for integral solutions */
static
SCIP_DECL_CONSCHECK(consCheckKResilient)
{  /*lint --e{715}*/
   int c;

   assert( scip != NULL );
   assert( conshdlr != NULL );
   assert( strcmp(SCIPconshdlrGetName(conshdlr), CONSHDLR_NAME) == 0 );
   assert( result != NULL );

   *result = SCIP_FEASIBLE;

   /* loop through constraints */
   for( c = 0; c < nconss && *result == SCIP_FEASIBLE; ++c )
   {
      assert( conss[c] != NULL );

      SCIP_CALL( checkFeasibility(scip, conss[c], sol, FALSE, printreason, result) );
   }

   assert(*result == SCIP_FEASIBLE || *result == SCIP_INFEASIBLE);

   return SCIP_OKAY;
}

/** variable rounding lock method of constraint handler */
static
SCIP_DECL_CONSLOCK(consLockKResilient)
{  /*lint --e{715}*/
   SCIP_CONSDATA* consdata;
   int i;

   assert(cons != NULL);

   consdata = SCIPconsGetData(cons);
   assert(consdata != NULL);

   /* rounding down a variable makes the problem potentially infeasible */
   /* rounding up is ok, it can make our covering inequalities only more feasible */
   for( i = 0; i < consdata->nvars_x; ++i )
   {
      SCIP_CALL( SCIPaddVarLocksType(scip, consdata->vars_x[i], SCIP_LOCKTYPE_MODEL, nlockspos, nlocksneg) );
   }
   for( i = 0; i < consdata->nvars_y; ++i )
   {
      SCIP_CALL( SCIPaddVarLocksType(scip, consdata->vars_y[i], SCIP_LOCKTYPE_MODEL, nlockspos, nlocksneg) );
   }

   return SCIP_OKAY;
}

/** constraint display method of constraint handler */
static
SCIP_DECL_CONSPRINT(consPrintKResilient)
{  /*lint --e{715}*/
   SCIP_CONSDATA* consdata;
   int i;
   consdata = SCIPconsGetData(cons);

   assert(consdata != NULL);

   for( i = 0; i < consdata->nvars_x; ++i )
      SCIPinfoMessage(scip, file, "<%s> (interdictable) ", SCIPvarGetName(consdata->vars_x[i]));
   for( i = 0; i < consdata->nvars_y; ++i )
      SCIPinfoMessage(scip, file, "<%s> ", SCIPvarGetName(consdata->vars_y[i]));

   return SCIP_OKAY;
}

/*
 * constraint specific interface methods
 */

/** creates the handler for interdiction constraints and includes it in SCIP */
SCIP_RETCODE SCIPincludeConshdlrKResilient(
   SCIP*                 scip                /**< SCIP data structure */
   )
{
   SCIP_CONSHDLRDATA* conshdlrdata = NULL;
   SCIP_CONSHDLR* conshdlr = NULL;

   /* include constraint handler */
   SCIP_CALL( SCIPincludeConshdlrBasic(scip, &conshdlr, CONSHDLR_NAME, CONSHDLR_DESC,
         CONSHDLR_ENFOPRIORITY, CONSHDLR_CHECKPRIORITY, CONSHDLR_EAGERFREQ, CONSHDLR_NEEDSCONS,
         consEnfolpKResilient, consEnfopsKResilient, consCheckKResilient, consLockKResilient,
         conshdlrdata) );
   assert(conshdlr != NULL);

   /* set non-fundamental callbacks via specific setter functions */
   SCIP_CALL( SCIPsetConshdlrDelete(scip, conshdlr, consDeleteKResilient) );
   SCIP_CALL( SCIPsetConshdlrPrint(scip, conshdlr, consPrintKResilient) );
   SCIP_CALL( SCIPsetConshdlrEnforelax(scip, conshdlr, consEnforelaxKResilient) );

   /* add params */
   SCIP_CALL( SCIPaddBoolParam(scip, "cons_kresilient/useextendedcuts",
         "should resilience be enforced by adding extra vars+cons to model the violated failure scenario",
         NULL, FALSE, DEFAULT_USEEXTENDEDCUTS, NULL, NULL) );

   SCIP_CALL( SCIPaddBoolParam(scip, "cons_kresilient/userandomimprove",
         "should the resilient constraint use its random heuristic to improve cuts",
         NULL, FALSE, DEFAULT_USERANDOMIMPROVE, NULL, NULL) );

   SCIP_CALL( SCIPaddLongintParam(scip, "cons_kresilient/interdictioncutsresolveimprove",
         "interdiction cut limit for the cut improvement (-1.0: disabled)",
         NULL, FALSE, DEFAULT_RESOLVEINTERDICTIONCUTLIMIT, -1LL, SCIP_LONGINT_MAX, NULL, NULL) );

   SCIP_CALL( SCIPaddLongintParam(scip, "cons_kresilient/nodesresolveimprove",
         "node limit for the cut improvement (-1.0: disabled)",
         NULL, FALSE, DEFAULT_RESOLVENODELIMIT, -1LL, SCIP_LONGINT_MAX, NULL, NULL) );

   SCIP_CALL( SCIPaddIntParam(scip, "cons_kresilient/coveringimprove",
         "should cut improvement based on covering rank be used (0: disabled, -1: choose small rank first, +1: choose highest rank first)",
         NULL, FALSE, DEFAULT_COVERINGIMPROVE, -1, 1, NULL, NULL) );

   SCIP_CALL( SCIPaddBoolParam(scip, "cons_kresilient/usereoptimization",
         "should reoptimization be used when cons_interdiction solves the scip_feas",
         NULL, FALSE, DEFAULT_USEREOPTIMIZATION, NULL, NULL) );

   SCIP_CALL( SCIPaddBoolParam(scip, "cons_kresilient/usefixing",
         "if reoptimization is not used, should cons_interdiction apply bounds on vals_y",
         NULL, FALSE, DEFAULT_PREFIXVARIABLES, NULL, NULL) );

   return SCIP_OKAY;
}

/** creates and captures a KResilient constraint
 *
 *  @note the constraint gets captured, hence at one point you have to release it using the method SCIPreleaseCons()
 */
SCIP_RETCODE SCIPcreateConsKResilient(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONS**           cons,               /**< pointer to hold the created constraint */
   const char*           name,               /**< name of constraint */
   int                   nvars,              /**< number of variables which have to be treated */
   SCIP_VAR**            vars_x,             /**< variables in scip */
   SCIP_VAR**            vars_x_feas,        /**< variables in scip_feas */
   SCIP_Bool*            vars_interdictable, /**< which of the variables are interdictable */
   SCIP*                 scip_feas,          /**< SCIP data structure for the subproblem */
   int                   k,                  /**< number of possible interdictions */
   SCIP_Bool             initial,            /**< should the LP relaxation of constraint be in the initial LP?
                                              *   Usually set to TRUE. Set to FALSE for 'lazy constraints'. */
   SCIP_Bool             separate,           /**< should the constraint be separated during LP processing?
                                              *   Usually set to TRUE. */
   SCIP_Bool             enforce,            /**< should the constraint be enforced during node processing?
                                              *   TRUE for model constraints, FALSE for additional, redundant constraints. */
   SCIP_Bool             check,              /**< should the constraint be checked for feasibility?
                                              *   TRUE for model constraints, FALSE for additional, redundant constraints. */
   SCIP_Bool             propagate,          /**< should the constraint be propagated during node processing?
                                              *   Usually set to TRUE. */
   SCIP_Bool             local,              /**< is constraint only valid locally?
                                              *   Usually set to FALSE. Has to be set to TRUE, e.g., for branching constraints. */
   SCIP_Bool             modifiable,         /**< is constraint modifiable (subject to column generation)?
                                              *   Usually set to FALSE. In column generation applications, set to TRUE if pricing
                                              *   adds coefficients to this constraint. */
   SCIP_Bool             dynamic,            /**< is constraint subject to aging?
                                              *   Usually set to FALSE. Set to TRUE for own cuts which
                                              *   are separated as constraints. */
   SCIP_Bool             removable,          /**< should the relaxation be removed from the LP due to aging or cleanup?
                                              *   Usually set to FALSE. Set to TRUE for 'lazy constraints' and 'user cuts'. */
   SCIP_Bool             stickingatnode      /**< should the constraint always be kept at the node where it was added, even
                                              *   if it may be moved to a more global node?
                                              *   Usually set to FALSE. Set to TRUE to for constraints that represent node data. */
   )
{
   SCIP_CONSHDLR* conshdlr;
   SCIP_CONSDATA* consdata;

   /* find the interdiction constraint handler */
   conshdlr = SCIPfindConshdlr(scip, CONSHDLR_NAME);
   if( conshdlr == NULL )
   {
      SCIPerrorMessage("constraint handler not found\n");
      return SCIP_PLUGINNOTFOUND;
   }

   assert(scip_feas != NULL);

   /* create constraint data */
   SCIP_CALL( consdataCreate(scip, &consdata, nvars, vars_x, vars_x_feas, vars_interdictable, scip_feas, k) );

   /* create constraint */
   SCIP_CALL( SCIPcreateCons(scip, cons, name, conshdlr, consdata, initial, separate, enforce, check, propagate,
         local, modifiable, dynamic, removable, stickingatnode) );

   return SCIP_OKAY;
}

/** creates and captures a KResilient constraint with all its constraint flags set to their
 *  default values
 *
 *  @note the constraint gets captured, hence at one point you have to release it using the method SCIPreleaseCons()
 */
SCIP_RETCODE SCIPcreateConsBasicKResilient(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONS**           cons,               /**< pointer to hold the created constraint */
   const char*           name,               /**< name of constraint */
   int                   nvars,              /**< number of variables which have to be treated */
   SCIP_VAR**            vars_x,             /**< variables in scip */
   SCIP_VAR**            vars_x_feas,        /**< variables in scip_feas */
   SCIP_Bool*            vars_interdictable, /**< which of the variables are interdictable */
   SCIP*                 scip_feas,          /**< SCIP data structure for the subproblem */
   int                   k                   /**< number of possible interdictions */
   )
{
   SCIP_CALL( SCIPcreateConsKResilient(scip, cons, name, nvars, vars_x, vars_x_feas, vars_interdictable, scip_feas, k,
         TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, TRUE) );

   return SCIP_OKAY;
}
