/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*    This file is part of the program kresilience                           */
/*                                                                           */
/*    Copyright (C) 2020-2021 Andreas Schmitt, Marc Pfetsch                  */
/*                                                                           */
/*                                                                           */
/*    Based on SCIP --- Solving Constraint Integer Programs                  */
/*       SCIP is distributed under the terms of the SCIP Academic Licence,   */
/*       see file COPYING in the SCIP distribution.                          */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**@file   sepa_2dCubeIndicator.c
 * @brief  assume a 2d cubic function together with indicator structure
 * @author Andreas Schmitt
 */

/*---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0----+----1----+----2*/

#include <assert.h>

#include "sepa_2dCubeIndicator.h"
#include "min2dcube.h"


#define SEPA_NAME              "cubeindicator"
#define SEPA_DESC              "cubic function separator"
#define SEPA_PRIORITY               110
#define SEPA_FREQ                     1
#define SEPA_MAXBOUNDDIST           1.0
#define SEPA_USESSUBSCIP          FALSE      /**< does the separator use a secondary SCIP instance? */
#define SEPA_DELAY                FALSE      /**< should separation method be delayed, if other separators found cuts? */

#define SEPA_EPSILON              1.0E-4
#define DEFAULT_USEEXACT          FALSE
#define DEFAULT_USELP             FALSE
#define DEFAULT_USEGRID           FALSE


/*
 * Data structures
 */

/** separator data */
struct SCIP_SepaData
{
   int                   neqs;               /**< number of inequalities/sets we consider */
   int                   sizeeqs;            /**< allocated size of all arrays in this struct */
   SCIP_VAR**            varsT;              /**< variables t */
   SCIP_VAR**            varsX;              /**< variables x */
   SCIP_VAR**            varsY;              /**< variables y */
   SCIP_VAR**            varsZ;              /**< indicator variables */
   SCIP_Real*            coefsXXX;           /**< coefficient of functions */
   SCIP_Real*            coefsXXY;           /**< coefficient of functions */
   SCIP_Real*            coefsXYY;           /**< coefficient of functions */
   SCIP_Real*            coefsYYY;           /**< coefficient of functions */
   SCIP_Real*            coefsXX;            /**< coefficient of functions */
   SCIP_Real*            coefsXY;            /**< coefficient of functions */
   SCIP_Real*            coefsYY;            /**< coefficient of functions */
   SCIP_Real*            coefsX;             /**< coefficient of functions */
   SCIP_Real*            coefsY;             /**< coefficient of functions */
   SCIP_Real*            coefsC;             /**< coefficient of functions */
};

/** separator data */
typedef struct cubic
{
   SCIP_Real             coefsXXX;           /**< coefficient of functions */
   SCIP_Real             coefsXXY;           /**< coefficient of functions */
   SCIP_Real             coefsXYY;           /**< coefficient of functions */
   SCIP_Real             coefsYYY;           /**< coefficient of functions */
   SCIP_Real             coefsXX;            /**< coefficient of functions */
   SCIP_Real             coefsXY;            /**< coefficient of functions */
   SCIP_Real             coefsYY;            /**< coefficient of functions */
   SCIP_Real             coefsX;             /**< coefficient of functions */
   SCIP_Real             coefsY;             /**< coefficient of functions */
   SCIP_Real             coefsC;             /**< coefficient of functions */
} Cubic;

/*
 * Local methods
 */

/* evaluate the cubic for a point */
static
SCIP_Real CubicEval(
   SCIP_Real             x,
   SCIP_Real             y,
   Cubic*                cubic               /**< cubic function struct */
   )
{
   assert(cubic != NULL);

   return cubic->coefsXXX * pow(x, 3.0) + cubic->coefsXXY * pow(x, 2.0) * y + cubic->coefsXYY * x * pow(y, 2.0)
            + cubic->coefsYYY * pow(y, 3.0) + cubic->coefsXX * pow(x, 2.0) + cubic->coefsXY * x * y + cubic->coefsYY * pow(y, 2.0)
            + cubic->coefsX * x + cubic->coefsY * y + cubic->coefsC;
}


/* get the x component of the gradient of given cubic function at a given point */
static
SCIP_Real CubicMaxGradientInX(
   SCIP_Real             lbX,
   SCIP_Real             ubX,
   SCIP_Real             lbY,
   SCIP_Real             ubY,
   Cubic*                cubic               /**< cubic function struct */
   )
{
   return calcMax2dQuad(lbX, ubX, lbY, ubY,
      3.0 * cubic->coefsXXX, 2.0 * cubic->coefsXXY, cubic->coefsXYY,
      2.0 * cubic->coefsXX, cubic->coefsXY, NULL, NULL) + cubic->coefsC;
}

/* get the x component of the gradient of given cubic function at a given point */
static
SCIP_Real CubicMinGradientInX(
   SCIP_Real             lbX,
   SCIP_Real             ubX,
   SCIP_Real             lbY,
   SCIP_Real             ubY,
   Cubic*                cubic               /**< cubic function struct */
   )
{
   return calcMin2dQuad(lbX, ubX, lbY, ubY,
      3.0 * cubic->coefsXXX, 2.0 * cubic->coefsXXY, cubic->coefsXYY,
      2.0 * cubic->coefsXX, cubic->coefsXY, NULL, NULL) + cubic->coefsC;
}
static
SCIP_Real CubicMin2d(
   SCIP_Real             lbX,
   SCIP_Real             ubX,
   SCIP_Real             lbY,
   SCIP_Real             ubY,
   Cubic*                cubic,              /**< cubic function struct */
   SCIP_Real*            optX,
   SCIP_Real*            optY
   )
{
   SCIP_Bool success;
   SCIP_Real temp_x;
   SCIP_Real temp_y;

   assert(cubic != NULL);

   if( optX == NULL )
      optX = &temp_x;
   if( optY == NULL )
      optY = &temp_y;

   return calcMin2dCube(lbX, ubX, lbY, ubY, cubic->coefsXXX, cubic->coefsXXY, cubic->coefsXYY, cubic->coefsYYY,
      cubic->coefsXX, cubic->coefsXY, cubic->coefsYY, cubic->coefsX, cubic->coefsY, optX, optY, &success) + cubic->coefsC;
}

/* solve min_{x in [lbX, ubX] c(x,yy) - alpha*(x-xx) - gamma */
static
SCIP_Real CubicEvalCut1d(
   SCIP_Real             xx,
   SCIP_Real             yy,
   SCIP_Real             lbX,
   SCIP_Real             ubX,
   SCIP_Real             alpha,
   SCIP_Real             gamma,
   Cubic*                cubic,              /**< cubic function struct */
   SCIP_Real*            optX
   )
{
   assert(cubic != NULL);
   assert(optX != NULL);

   (void) calcMin1dCube(lbX, ubX, cubic->coefsXXX, cubic->coefsXXY * yy + cubic->coefsXX, cubic->coefsXYY * pow(yy, 2.0) + cubic->coefsX + cubic->coefsXY * yy - alpha, optX);

   return CubicEval(*optX, yy, cubic) - gamma - alpha * (*optX - xx);
}

/* solve min_{x, y \in [lbX, ubX]x[lbY, ubY]} c(x, y) - alpha*(x-xx) - beta*(y-yy) - gamma */
static
SCIP_Real CubicEvalCut(
   SCIP_Real             xx,
   SCIP_Real             yy,
   SCIP_Real             lbX,
   SCIP_Real             ubX,
   SCIP_Real             lbY,
   SCIP_Real             ubY,
   SCIP_Real             alpha,
   SCIP_Real             beta,
   SCIP_Real             gamma,
   Cubic*                cubic,              /**< cubic function struct */
   SCIP_Real*            optX,
   SCIP_Real*            optY
   )
{
   SCIP_Real result;
   SCIP_Bool success;

   assert(cubic != NULL);

   result = calcMin2dCube(lbX, ubX, lbY, ubY, cubic->coefsXXX, cubic->coefsXXY, cubic->coefsXYY, cubic->coefsYYY, cubic->coefsXX, cubic->coefsXY, cubic->coefsYY, cubic->coefsX - alpha, cubic->coefsY - beta, optX, optY, &success);
   result += cubic->coefsC;
   result += alpha * xx;
   result += beta * yy;
   result -= gamma;

   return result;
}

/* for two points with y1 < yy < y2 check whether we can infer a valid lower or upper bound on alpha from the system
f(x_i, y_i) >= gamma + alpha (x_i-xx) + beta (y_i - yy). We implicitly project beta out using fme returns the bound on alpha, or if there is no coefficient on alpha the lower bound on gamma */
static
SCIP_Real getBoundsAlphaGammaFromTwoPoints(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_Real             x1,
   SCIP_Real             y1,
   SCIP_Real             x2,
   SCIP_Real             y2,
   SCIP_Real             xx,
   SCIP_Real             yy,
   SCIP_Real             gamma,
   Cubic*                cubic,              /**< cubic function struct */
   SCIP_Bool*            lb,                 /**< whether the projection yields an lower bound on alpha */
   SCIP_Bool*            ub                  /**< whether the projection yields an upper bound on alpha */
   )
{
   SCIP_Real coef_alpha;
   SCIP_Real bound;

   assert(cubic != NULL);
   assert(lb != NULL);
   assert(ub != NULL);

   assert(y1 < yy);
   assert(yy < y2);

   coef_alpha = (x1 - xx) * (y2 - yy) + (x2 - xx) * (yy - y1);

   bound = (CubicEval(x1, y1, cubic) - gamma) * (y2 - yy);
   bound += (CubicEval(x2, y2, cubic) - gamma) * (yy - y1);
   if( SCIPisGT(scip, coef_alpha, 0.0) )
   {
      *lb = FALSE;
      *ub = TRUE;
      bound = bound / coef_alpha;
   }
   else if( SCIPisLT(scip, coef_alpha, 0.0) )
   {
      *lb = TRUE;
      *ub = FALSE;
      bound = bound / coef_alpha;
   }
   else
   {
      *lb = TRUE;
      *ub = TRUE;
      bound = CubicEval(x2, y2, cubic) * (y1 - yy) - CubicEval(x1, y1, cubic) * (y2 - yy);
      bound = bound / (y1 - y2);
   }

   return bound;
}

static
SCIP_RETCODE checkAlphaGamma(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_Real             xx,
   SCIP_Real             yy,
   SCIP_Real             lbX,                /**< lower bound of x */
   SCIP_Real             ubX,                /**< upper bound of y */
   SCIP_Real             lbY,                /**< lower bound of y */
   SCIP_Real             ubY,                /**< upper bound of y */
   SCIP_Real             alpha,
   SCIP_Real             gamma,
   Cubic*                cubic,              /**< cubic function struct */
   SCIP_Bool*            lb,                 /**< whether we have a lower bound on alpha */
   SCIP_Bool*            ub,                 /**< whether we have a upper bound on alpha */
   SCIP_Real*            bound
   )
{
   SCIP_Real lbBeta;
   SCIP_Real ubBeta;
   SCIP_Real lbBetaX;
   SCIP_Real lbBetaY;
   SCIP_Real ubBetaX;
   SCIP_Real ubBetaY;
   SCIP_Real x;
   SCIP_Real y;

   assert(cubic != NULL);
   assert(lb != NULL);
   assert(ub != NULL);
   assert(bound != NULL);

   /* check first that alpha is good on the line [x, yy] */
   *lb = FALSE;
   *ub = FALSE;
   if( !SCIPisGE(scip, CubicEvalCut1d(xx, yy, lbX, xx, alpha, gamma, cubic, &x), 0.0) )
   {
      if( x == xx )
         return SCIP_ERROR;

      *bound = (CubicEval(x, yy, cubic) - gamma) / (x - xx);
      *lb = TRUE;
   }

   if( !SCIPisGE(scip, CubicEvalCut1d(xx, yy, xx, ubX, alpha, gamma, cubic, &y), 0.0) )
   {
      if( y == xx )
         return SCIP_ERROR;

      *bound = (CubicEval(y, yy, cubic) - gamma) / (y - xx);
      *ub = TRUE;
   }

   if( *lb && *ub )
   {
      assert(gamma > CubicEval(x, yy, cubic) + ((CubicEval(y, yy, cubic) - CubicEval(x, yy, cubic)) / (y - x)) * (xx - x));
      return CubicEval(x, yy, cubic) + ((CubicEval(y, yy, cubic) - CubicEval(x, yy, cubic)) / (y - x)) * (xx - x);
   }
   else if( *lb || *ub )
   {
      return SCIP_OKAY;
   }

   lbBetaX = lbX;
   lbBetaY = lbY;
   lbBeta = (CubicEval(lbBetaX, lbBetaY, cubic) - alpha * (lbBetaX - xx) - gamma) / (lbBetaY - yy);

   /* check lbBeta is feasible on [yy, ubY]*/
   if( !SCIPisGE(scip, CubicEvalCut(xx, yy, lbX, ubX, yy, ubY, alpha, lbBeta, gamma, cubic, &x, &y), 0.0) )
   {
      *bound = getBoundsAlphaGammaFromTwoPoints(scip, lbBetaX, lbBetaY, x, y, xx, yy, gamma, cubic, lb, ub);

      assert( *lb || *ub );
      return SCIP_OKAY;
   }

   ubBetaX = ubX;
   ubBetaY = ubY;
   ubBeta = (CubicEval(ubBetaX, ubBetaY, cubic) - alpha * (ubBetaX - xx) - gamma) / (ubBetaY - yy);

   /* check ubBeta is feasible on [0, yy]*/
   if( !SCIPisGE(scip, CubicEvalCut(xx, yy, lbX, ubX, lbY, yy, alpha, ubBeta, gamma, cubic, &x, &y), 0.0) )
   {
      *bound = getBoundsAlphaGammaFromTwoPoints(scip, x, y, ubBetaX, ubBetaY, xx, yy, gamma, cubic, lb, ub);

      assert( *lb || *ub );
      return SCIP_OKAY;
   }

   assert(lbBeta <= ubBeta);
   while( !SCIPisZero(scip, lbBeta - ubBeta) && ubBeta >= lbBeta )
   {
      SCIP_Real beta = (lbBeta + ubBeta) / 2.0;

      if( !SCIPisGE(scip, CubicEvalCut(xx, yy, lbX, ubX, lbY, yy, alpha, beta, gamma, cubic, &x, &y), 0.0) )
      {
         if( SCIPisEQ(scip, y, yy) )
         {
            return SCIP_ERROR;
         }
         assert((CubicEval(x, y, cubic) - alpha * (x - xx) - gamma) / (y - yy) > beta);
         lbBeta = (CubicEval(x, y, cubic) - alpha * (x - xx) - gamma) / (y - yy);
         lbBetaX = x;
         lbBetaY = y;
      }
      if( !SCIPisGE(scip, CubicEvalCut(xx, yy, lbX, ubX, yy, ubY, alpha, beta, gamma, cubic, &x, &y), 0.0) )
      {
         if( SCIPisEQ(scip, y, yy) )
         {
            return SCIP_ERROR;
         }
         assert((CubicEval(x, y, cubic) - alpha * (x - xx) - gamma) / (y - yy) < beta);
         ubBeta = (CubicEval(x, y, cubic) - alpha * (x - xx) - gamma) / (y - yy);
         ubBetaX = x;
         ubBetaY = y;

      }
      if( SCIPisGE(scip, CubicEvalCut(xx, yy, lbX, ubX, lbY, ubY, alpha, beta, gamma, cubic, NULL, NULL), 0.0) )
      {
         lbBeta = beta;
         ubBeta = beta;
      }
   }

   if( SCIPisLT(scip, ubBeta, lbBeta) )
   {
      *bound = getBoundsAlphaGammaFromTwoPoints(scip, lbBetaX, lbBetaY, ubBetaX, ubBetaY, xx, yy, gamma, cubic, lb, ub);

      assert( *lb || *ub );

      return SCIP_OKAY;
   }

   *lb = FALSE;
   *ub = FALSE;

   *bound = (ubBeta + lbBeta) / 2.0;

   /* if this bound is not greater equal gamma then something more is wrong */
   if( !SCIPisFeasGE(scip, CubicEvalCut(xx, yy, lbX, ubX, lbY, ubY, alpha, *bound, gamma, cubic, NULL, NULL), 0.0) )
   {
      /* we try to infer something on alpha   */
      printf("now im here\n");
      *bound = getBoundsAlphaGammaFromTwoPoints(scip, lbBetaX, lbBetaY, ubBetaX, ubBetaY, xx, yy, gamma, cubic, lb, ub);
      /* otherwise we are dont know... */
      if( !(*lb || *ub) )
         return SCIP_ERROR;
   }


   return SCIP_OKAY;
}

/* check whether cutX * x + cutY * y + cutC <= f(x, y) for x, y in a box for a given cubic f */
static
SCIP_Bool CubicCutIsFeasible(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_Real             lbX,                /**< lower lound of x */
   SCIP_Real             ubX,                /**< upper bound of y */
   SCIP_Real             lbY,                /**< lower bound of y */
   SCIP_Real             ubY,                /**< upper bound of y */
   SCIP_Real             cutX,               /**< cut coefficient */
   SCIP_Real             cutY,               /**< cut coefficient */
   SCIP_Real             cutC,               /**< cut coefficient */
   Cubic*                cubic               /**< cubic function struct */
   )
{
   SCIP_Bool success;
   SCIP_Real min;
   SCIP_Real optX;
   SCIP_Real optY;

   min = calcMin2dCube(lbX, ubX, lbY, ubY, cubic->coefsXXX, cubic->coefsXXY, cubic->coefsXYY, cubic->coefsYYY, cubic->coefsXX, cubic->coefsXY, cubic->coefsYY, cubic->coefsX - cutX, cubic->coefsY - cutY, &optX, &optY, &success) + cubic->coefsC - cutC;

   return success && SCIPisFeasGE(scip, min, 0.0);
}

/* get the x component of the gradient of given cubic function at a given point */
static
SCIP_Real CubicGradientInX(
   SCIP_Real             x,                  /**< x value of point */
   SCIP_Real             y,                  /**< y value of point */
   Cubic*                cubic               /**< cubic function struct */
   )
{
   return 3.0 * cubic->coefsXXX * x * x + 2.0 * cubic->coefsXXY * x * y + cubic->coefsXYY * y * y + 2.0 * cubic->coefsXX * x + cubic->coefsXY * y + cubic->coefsX;
}

/* get the y component of the gradient of given cubic function at a given point */
static
SCIP_Real CubicGradientInY(
   SCIP_Real             x,                  /**< x value of point */
   SCIP_Real             y,                  /**< y value of point */
   Cubic*                cubic               /**< cubic function struct */
   )
{
   return 3.0 * cubic->coefsYYY * y * y + 2.0 * cubic->coefsXYY * y * x + cubic->coefsXXY * x * x + 2.0 * cubic->coefsYY * y + cubic->coefsXY * x + cubic->coefsY;
}

/* adds the cut varT >= coefX * varX + coefY * varY + coefZ * varZ if efficacious */
static
SCIP_RETCODE tryCut(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_SEPA*            sepa,               /**< separator that computed the cut */
   SCIP_SOL*             sol,                /**< primal solution that should be separated, or NULL for LP solution */
   SCIP_VAR*             varT,               /**< variable in the cut */
   SCIP_VAR*             varX,               /**< variable in the cut */
   SCIP_VAR*             varY,               /**< variable in the cut */
   SCIP_VAR*             varZ,               /**< variable in the cut */
   SCIP_Real             coefX,              /**< cut coefficient */
   SCIP_Real             coefY,              /**< cut coefficient */
   SCIP_Real             coefZ,              /**< cut coefficient */
   SCIP_RESULT*          result              /**< pointer to store the result of the separation call */
   )
{
   SCIP_ROW* row;
   SCIP_Bool infeasible;

   SCIP_CALL( SCIPcreateEmptyRowSepa(scip, &row, sepa, "cutSepa", 0.0, SCIPinfinity(scip), TRUE, FALSE, TRUE) );
   SCIP_CALL( SCIPcacheRowExtensions(scip, row) );

   SCIP_CALL( SCIPaddVarToRow(scip, row, varT, 1.0) );

   SCIP_CALL( SCIPaddVarToRow(scip, row, varX, -coefX) );
   SCIP_CALL( SCIPaddVarToRow(scip, row, varY, -coefY) );
   SCIP_CALL( SCIPaddVarToRow(scip, row, varZ, -coefZ) );

   SCIP_CALL( SCIPflushRowExtensions(scip, row) );

#ifdef SCIP_DEBUG
   SCIP_CALL( SCIPprintRow(scip, row, NULL) );
#endif

   if( SCIPisCutEfficacious(scip, sol, row) )
   {
      SCIP_CALL( SCIPaddRow(scip, row, TRUE, &infeasible) );
      if( infeasible )
         *result = SCIP_CUTOFF;
      else
         *result = SCIP_SEPARATED;
   }

   SCIP_CALL( SCIPreleaseRow(scip, &row) );

   return SCIP_OKAY;
}

/* compute cutX, cutC such that cutX * x + cutC <= c(x,y) for the given y over [lbX, ubX] and cutX * xx + cutC =
   vex[c](xx) */
static
SCIP_RETCODE getConvexUnderestimate1dFixedY(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_Real             xx,                 /**< x value */
   SCIP_Real             lbX,                /**< variable bound */
   SCIP_Real             ubX,                /**< variable bound */
   SCIP_Real             y,                  /**< y value */
   Cubic*                cubic,              /**< cubic function struct */
   SCIP_Real*            cutX,               /**< coefficient of computed cut */
   SCIP_Real*            cutC                /**< coefficient of computed cut */
   )
{
   SCIP_Real cxxx = cubic->coefsXXX;
   SCIP_Real cxx = cubic->coefsXX + cubic->coefsXXY * y;
   SCIP_Real breakpoint;

   assert(scip != NULL);
   assert(cutX != NULL);
   assert(cutC != NULL);

   if( !(SCIPisFeasLE(scip, lbX, xx) && SCIPisFeasLE(scip, xx, ubX)) )
   {
      printf("xx does not belong to interval %f [%f %f]\n", xx, lbX, ubX);
      return SCIP_ERROR;
   }

   if( cubic->coefsXXX > 0.0 )
   {
      /* whole function is convex on [lbX, ubX] */
      if( lbX >= -cxx/(3.0 * cxxx) )
      {
         *cutX = CubicGradientInX(xx, y, cubic);
         *cutC = CubicEval(xx, y, cubic) - *cutX * xx;
      }
      else
      {
         breakpoint = MIN(ubX, -(cxxx * lbX + cxx) / (2.0 * cxxx));
         if( xx <= breakpoint )
         {
            *cutX = CubicEval(breakpoint, y, cubic) - CubicEval(lbX, y, cubic);
            *cutX = *cutX / (breakpoint - lbX);
            *cutC = CubicEval(lbX, y, cubic) - *cutX * lbX;
         }
         else
         {
            *cutX = CubicGradientInX(xx, y, cubic);
            *cutC = CubicEval(xx, y, cubic) - *cutX * xx;
         }
      }
   }
   if( cubic->coefsXXX < 0.0 )
   {
      /* whole function is convex on [lbX, ubX] */
      if( ubX <= -cxx/(3.0 * cxxx) )
      {
         *cutX = CubicGradientInX(xx, y, cubic);
         *cutC = CubicEval(xx, y, cubic) - *cutX * xx;
      }
      else
      {
         breakpoint = MAX(lbX, -(cxxx * ubX + cxx) / (2.0 * cxxx));
         if( xx <= breakpoint )
         {
            *cutX = CubicEval(breakpoint, y, cubic) - CubicEval(ubX, y, cubic);
            *cutX = *cutX / (breakpoint - ubX);
            *cutC = CubicEval(ubX, y, cubic) - *cutX * ubX;
         }
         else
         {
            *cutX = CubicGradientInX(xx, y, cubic);
            *cutC = CubicEval(xx, y, cubic) - *cutX * xx;
         }
      }
   }

   assert(SCIPisFeasGE(scip, CubicEvalCut1d(0.0, y, lbX, ubX, *cutX, *cutC, cubic, &breakpoint), 0.0));

   return SCIP_OKAY;
}

/* compute cut cutX * x + cutY * y + cutC <= c(x,y), which lifts from the lower bound */
static
SCIP_RETCODE getCutFromUb(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_Real             y,                  /**< y value */
   SCIP_Real             lbX,                /**< variable bound */
   SCIP_Real             ubX,                /**< variable bound */
   SCIP_Real             lbY,                /**< variable bound */
   SCIP_Real             ubY,                /**< variable bound */
   Cubic*                cubic,              /**< cubic function struct */
   SCIP_Real*            cutX,               /**< coefficient of computed cut */
   SCIP_Real*            cutY,               /**< coefficient of computed cut */
   SCIP_Real*            cutC                /**< coefficient of computed cut */
   )
{
   SCIP_Real ubCutX;
   SCIP_Real lbCutX;
   SCIP_Real alpha;
   Cubic cubic_switched;

   assert(scip != NULL);
   assert(cubic != NULL);
   assert(cutX != NULL);
   assert(cutY != NULL);
   assert(cutC != NULL);

   if( !(SCIPisFeasLE(scip, lbY, y) && SCIPisFeasLE(scip, y, ubY)) )
   {
      printf("y does not belong to interval %f [%f %f]\n", y, lbY, ubY);
      return SCIP_ERROR;
   }

   cubic_switched.coefsXXX = cubic->coefsYYY;
   cubic_switched.coefsXXY = cubic->coefsXYY;
   cubic_switched.coefsXYY = cubic->coefsXXY;
   cubic_switched.coefsYYY = cubic->coefsXXX;
   cubic_switched.coefsXX = cubic->coefsYY;
   cubic_switched.coefsXY = cubic->coefsXY;
   cubic_switched.coefsYY = cubic->coefsXX;
   cubic_switched.coefsX = cubic->coefsY;
   cubic_switched.coefsY = cubic->coefsX;
   cubic_switched.coefsC = cubic->coefsC;

/* compute cutX, cutC such that cutX * x + cutC <= c(x,y) for the given y over [lbX, ubX] and cutX * xx + cutC =
   vex[c](xx) */
   SCIP_CALL( getConvexUnderestimate1dFixedY(scip, y, lbY, ubY, ubX, &cubic_switched, cutY, cutC) );

   lbCutX = (CubicEval(ubX, y, cubic) - *cutC) / (lbX - ubX);
   ubCutX = CubicMaxGradientInX(lbX, ubX, lbY, ubY, cubic);

   if( SCIPisFeasGE(scip, CubicEvalCut(ubX, 0.0, lbX, ubX, lbY, ubY, lbCutX, *cutY, 0.0, cubic, NULL, NULL), *cutC) )
   {
      ubCutX = lbCutX;
   }
   while( SCIPisGT(scip, ubCutX, lbCutX) )
   {
      alpha = (ubCutX + lbCutX) / 2.0;
      if( SCIPisFeasGE(scip, CubicEvalCut(ubX, 0.0, lbX, ubX, lbY, ubY, alpha, *cutY, 0.0, cubic, NULL, NULL), *cutC) )
      {
         ubCutX = alpha;
      }
      else
      {
         lbCutX = alpha;
      }
   }
   *cutX = ubCutX;
   *cutC = CubicEvalCut(ubX, y, lbX, ubX, lbY, ubY, *cutX, *cutY, 0.0, cubic, NULL, NULL);
   *cutC -= *cutX * ubX;
   *cutC -= *cutY * y;

   return SCIP_OKAY;
}

/* compute cut cutX * x + cutY * y + cutC <= c(x,y), which lifts from the lower bound */
static
SCIP_RETCODE getCutFromLb(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_Real             y,                  /**< y value */
   SCIP_Real             lbX,                /**< variable bound */
   SCIP_Real             ubX,                /**< variable bound */
   SCIP_Real             lbY,                /**< variable bound */
   SCIP_Real             ubY,                /**< variable bound */
   Cubic*                cubic,              /**< cubic function struct */
   SCIP_Real*            cutX,               /**< coefficient of computed cut */
   SCIP_Real*            cutY,               /**< coefficient of computed cut */
   SCIP_Real*            cutC                /**< coefficient of computed cut */
   )
{
   SCIP_Real ubCutX;
   SCIP_Real lbCutX;
   SCIP_Real alpha;
   Cubic cubic_switched;

   assert(scip != NULL);
   assert(cubic != NULL);
   assert(cutX != NULL);
   assert(cutY != NULL);
   assert(cutC != NULL);

   if( !(SCIPisFeasLE(scip, lbY, y) && SCIPisFeasLE(scip, y, ubY)) )
   {
      printf("y does not belong to interval %f [%f %f]\n", y, lbY, ubY);
      return SCIP_ERROR;
   }

   cubic_switched.coefsXXX = cubic->coefsYYY;
   cubic_switched.coefsXXY = cubic->coefsXYY;
   cubic_switched.coefsXYY = cubic->coefsXXY;
   cubic_switched.coefsYYY = cubic->coefsXXX;
   cubic_switched.coefsXX = cubic->coefsYY;
   cubic_switched.coefsXY = cubic->coefsXY;
   cubic_switched.coefsYY = cubic->coefsXX;
   cubic_switched.coefsX = cubic->coefsY;
   cubic_switched.coefsY = cubic->coefsX;
   cubic_switched.coefsC = cubic->coefsC;

/* compute cutX, cutC such that cutX * x + cutC <= c(x,y) for the given y over [lbX, ubX] and cutX * xx + cutC =
   vex[c](xx) */
   SCIP_CALL( getConvexUnderestimate1dFixedY(scip, y, lbY, ubY, lbX, &cubic_switched, cutY, cutC) );

   lbCutX = CubicMinGradientInX(lbX, ubX, lbY, ubY, cubic);
   ubCutX = (CubicEval(ubX, y, cubic) - *cutC) / (ubX - lbX);

   if( SCIPisFeasGE(scip, CubicEvalCut(lbX, 0.0, lbX, ubX, lbY, ubY, ubCutX, *cutY, 0.0, cubic, NULL, NULL), *cutC) )
   {
      lbCutX = ubCutX;
   }
   while( SCIPisGT(scip, ubCutX, lbCutX) )
   {
      alpha = (ubCutX + lbCutX) / 2.0;
      if( SCIPisFeasGE(scip, CubicEvalCut(lbX, 0.0, lbX, ubX, lbY, ubY, alpha, *cutY, 0.0, cubic, NULL, NULL), *cutC) )
      {
         lbCutX = alpha;
      }
      else
      {
         ubCutX = alpha;
      }
   }
   *cutX = lbCutX;
   *cutC = CubicEvalCut(lbX, y, lbX, ubX, lbY, ubY, *cutX, *cutY, 0.0, cubic, NULL, NULL);
   *cutC -= *cutX * lbX;
   *cutC -= *cutY * y;

   return SCIP_OKAY;
}

/** adds the row f(x, y) >= col[0] + col[1] * (x-xx) + col[2] * (y-yy) to the given lpu*/
static
SCIP_RETCODE addLPIRow(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_LPI*             lpi,                /**< lp data structure */
   SCIP_Real             x,                  /**< points */
   SCIP_Real             y,                  /**< points */
   SCIP_Real             xx,                 /**< points */
   SCIP_Real             yy,                 /**< points */
   Cubic*                cubic               /**< cubic function struct */
   )
{
   SCIP_Real lb;
   SCIP_Real ub;
   int nnonz = 3;
   int beg = 0;
   SCIP_Real vals[3] = {1.0, x-xx, y-yy};
   int ind[3] = {0, 1, 2};

   lb = -SCIPinfinity(scip);
   ub = CubicEval(x, y, cubic);

   // lpiAddRows expects the coefficients to be nonzero
   if( SCIPisZero(scip, vals[2]) )
   {
      nnonz = 2;
   }
   if( SCIPisZero(scip, vals[1]) )
   {
      nnonz--;
      vals[1] = vals[2];
      ind[1] = ind[2];
   }

   SCIP_CALL( SCIPlpiAddRows(lpi, 1, &lb, &ub, NULL, nnonz, &beg, ind, vals) );
   //   SCIP_CALL( SCIPlpiWriteLP(lpi, "bla.lp") );
   return SCIP_OKAY;
}

/** tries out gradient cuts */
static
SCIP_RETCODE separateGradientCuts(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_SEPA*            sepa,               /**< separator that computes the cut */
   SCIP_SOL*             sol,                /**< primal solution that should be separated, or NULL for LP solution */
   SCIP_VAR*             varT,               /**< variable in the cut */
   SCIP_VAR*             varX,               /**< variable in the cut */
   SCIP_VAR*             varY,               /**< variable in the cut */
   SCIP_VAR*             varZ,               /**< variable in the cut */
   Cubic*                cubic,              /**< cubic function struct */
   SCIP_Bool*            valid,              /**< whether the gradient cut is valid */
   SCIP_RESULT*          result              /**< pointer to store the result of the separation call */
   )
{
   SCIP_Real cutX;
   SCIP_Real cutY;
   SCIP_Real cutZ;
   SCIP_Real valX;
   SCIP_Real valY;
   SCIP_Real valZ;
   SCIP_Real lbX;
   SCIP_Real ubX;
   SCIP_Real lbY;
   SCIP_Real ubY;

   assert(scip != NULL);
   assert( varT != NULL );
   assert( varX != NULL );
   assert( varY != NULL );
   assert( varZ != NULL );

   valZ = SCIPgetSolVal(scip, sol, varZ);

   if( SCIPisFeasZero(scip, valZ) )
   {
      return SCIP_OKAY;
   }

   valX = SCIPgetSolVal(scip, sol, varX);
   valY = SCIPgetSolVal(scip, sol, varY);

   lbX = SCIPvarGetLbLocal(varX);
   ubX = SCIPvarGetUbLocal(varX);
   lbY = SCIPvarGetLbLocal(varY);
   ubY = SCIPvarGetUbLocal(varY);

   if( !SCIPisFeasEQ(scip, valZ, 1.0) )
   {
      valX = valX / valZ;
      valY = valY / valZ;
   }

   /* Due to the separation of indicator cuts and cuts on the boundary, this should hold */
   assert( SCIPisFeasGT(scip, valX, lbX) && SCIPisFeasLT(scip, valX, ubX) );
   assert( SCIPisFeasGT(scip, valY, lbY) && SCIPisFeasLT(scip, valY, ubY) );

   cutX = CubicGradientInX(valX, valY, cubic);
   cutY = CubicGradientInY(valX, valY, cubic);
   cutZ = CubicEval(valX, valY, cubic);
   cutZ -= cutX * valX;
   cutZ -= cutY * valY;

   if( CubicCutIsFeasible(scip, lbX, ubX, lbY, ubY, cutX, cutY, cutZ, cubic) )
   {
      *valid = TRUE;
      SCIP_CALL( tryCut(scip, sepa, sol, varT, varX, varY, varZ, cutX, cutY, cutZ, result) );
   }

   return SCIP_OKAY;
}

/** exact separation in the interior */
static
SCIP_RETCODE separateInterior(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_SEPA*            sepa,               /**< separator that computes the cut */
   SCIP_SOL*             sol,                /**< primal solution that should be separated, or NULL for LP solution */
   SCIP_VAR*             varT,               /**< variable in the cut */
   SCIP_VAR*             varX,               /**< variable in the cut */
   SCIP_VAR*             varY,               /**< variable in the cut */
   SCIP_VAR*             varZ,               /**< variable in the cut */
   Cubic*                cubic,              /**< cubic function struct */
   SCIP_RESULT*          result              /**< pointer to store the result of the separation call */
   )
{
   SCIP_Real cutX;
   SCIP_Real cutY;
   SCIP_Real cutZ;
   SCIP_Real valX;
   SCIP_Real valY;
   SCIP_Real valZ;
   SCIP_Real lbX;
   SCIP_Real ubX;
   SCIP_Real lbY;
   SCIP_Real ubY;

   Cubic cubic_switched;

   SCIP_Real lbGamma;
   SCIP_Real ubGamma;
   SCIP_Real gamma;
   SCIP_Real lbAlpha;
   SCIP_Real ubAlpha;
   SCIP_Real lbBeta;
   SCIP_Real ubBeta;
   SCIP_Real beta;
   SCIP_Real alpha;
   SCIP_Real alpha_feas;
   SCIP_Real beta_feas;
   int i;

   assert(scip != NULL);
   assert( varT != NULL );
   assert( varX != NULL );
   assert( varY != NULL );
   assert( varZ != NULL );

   valZ = SCIPgetSolVal(scip, sol, varZ);

   if( SCIPisFeasZero(scip, valZ) )
   {
      return SCIP_OKAY;
   }

   valX = SCIPgetSolVal(scip, sol, varX);
   valY = SCIPgetSolVal(scip, sol, varY);

   lbX = SCIPvarGetLbLocal(varX);
   ubX = SCIPvarGetUbLocal(varX);
   lbY = SCIPvarGetLbLocal(varY);
   ubY = SCIPvarGetUbLocal(varY);

   if( SCIPisFeasEQ(scip, lbX, ubX) || SCIPisFeasEQ(scip, lbY, ubY) )
   {
      /*  todo separate the point/1-d line */
      return SCIP_OKAY;
   }

   if( !SCIPisFeasEQ(scip, valZ, 1.0) )
   {
      valX = valX / valZ;
      valY = valY / valZ;
   }

   /* Due to the separation of indicator cuts and cuts on the boundary, this should hold */
   assert( SCIPisFeasGT(scip, valX, lbX) && SCIPisFeasLT(scip, valX, ubX) );
   assert( SCIPisFeasGT(scip, valY, lbY) && SCIPisFeasLT(scip, valY, ubY) );

   cubic_switched.coefsXXX = cubic->coefsYYY;
   cubic_switched.coefsXXY = cubic->coefsXYY;
   cubic_switched.coefsXYY = cubic->coefsXXY;
   cubic_switched.coefsYYY = cubic->coefsXXX;
   cubic_switched.coefsXX = cubic->coefsYY;
   cubic_switched.coefsXY = cubic->coefsXY;
   cubic_switched.coefsYY = cubic->coefsXX;
   cubic_switched.coefsX = cubic->coefsY;
   cubic_switched.coefsY = cubic->coefsX;
   cubic_switched.coefsC = cubic->coefsC;

   ubGamma = CubicEval(valX, valY, cubic);

   cutX = CubicGradientInX(valX, valY, cubic);
   cutY = CubicGradientInY(valX, valY, cubic);
   cutZ = ubGamma;
   cutZ -= cutX * valX;
   cutZ -= cutY * valY;

   if( CubicCutIsFeasible(scip, lbX, ubX, lbY, ubY, cutX, cutY, cutZ, cubic) )
   {
      SCIP_CALL( tryCut(scip, sepa, sol, varT, varX, varY, varZ, cutX, cutY, cutZ, result) );
      return SCIP_OKAY;
   }

   /* initilialize a valid inequality */
   alpha_feas = 0.0;
   beta_feas = 0.0;
   lbGamma = CubicEvalCut(valX, valY, lbX, ubX, lbY, ubY, alpha_feas, beta_feas, 0.0, cubic, NULL, NULL);

   i = 0;
   while( i < 100 && ubGamma - lbGamma >= SEPA_EPSILON )
   {
      cutX = alpha_feas;
      cutY = beta_feas;
      cutZ = lbGamma - alpha_feas * valX - beta_feas * valY;

      if( !SCIPisLE(scip, lbGamma, ubGamma) )
         return SCIP_ERROR;

      if (!CubicCutIsFeasible(scip, lbX, ubX, lbY, ubY, cutX, cutY, cutZ, cubic) )
      {
         return SCIP_ERROR;
      }
      i++;
      gamma = (ubGamma + lbGamma)/2.0;

      lbAlpha = (CubicEval(lbX, valY, cubic) - gamma) / (lbX - valX);
      ubAlpha = (CubicEval(ubX, valY, cubic) - gamma) / (ubX - valX);
      lbBeta = (CubicEval(valX, lbY, cubic) - gamma) / (lbY - valY);
      ubBeta = (CubicEval(valX, ubY, cubic) - gamma) / (ubY - valY);

      while( SCIPisGT(scip, ubAlpha, lbAlpha) && SCIPisGT(scip, ubBeta, lbBeta) )
      {
         if( ubAlpha - lbAlpha > ubBeta - lbBeta )
         {
            SCIP_Real bound;
            SCIP_Bool incAlpha;
            SCIP_Bool decAlpha;

            alpha = (ubAlpha + lbAlpha) / 2.0;

            SCIP_CALL( checkAlphaGamma(scip, valX, valY, lbX, ubX, lbY, ubY, alpha, gamma, cubic, &incAlpha, &decAlpha, &bound) );

            if( incAlpha && decAlpha )
            {
               assert( bound < gamma );
               ubAlpha = 0.0;
               lbAlpha = 1.0;
               gamma = bound;
            }
            else if( incAlpha )
            {
               assert( SCIPisFeasLE(scip, alpha, bound) );
               lbAlpha = bound;
            }
            else if( decAlpha )
            {
               assert( SCIPisFeasGE(scip, alpha, bound) );
               ubAlpha = bound;
            }
            else
            {
               lbAlpha = alpha;
               ubAlpha = alpha;
               lbBeta = bound;
               ubBeta = bound;
            }
         }
         else
         {
            SCIP_Real bound;
            SCIP_Bool incBeta;
            SCIP_Bool decBeta;

            beta = (ubBeta + lbBeta) / 2.0;

            SCIP_CALL( checkAlphaGamma(scip, valY, valX, lbY, ubY, lbX, ubX, beta, gamma, &cubic_switched, &incBeta, &decBeta, &bound) );

            if( incBeta && decBeta )
            {
               assert( bound < gamma );
               ubBeta = 0.0;
               lbBeta = 1.0;
               gamma = bound;
            }
            else if( incBeta )
            {
               assert( SCIPisFeasLE(scip, beta, bound) );
               lbBeta = bound;
            }
            else if( decBeta )
            {
               assert( SCIPisFeasGE(scip, beta, bound) );
               ubBeta = bound;
            }
            else
            {
               lbBeta = beta;
               ubBeta = beta;
               lbAlpha = bound;
               ubAlpha = bound;
            }
         }
      }

      if( lbAlpha > ubAlpha || lbBeta > ubBeta )
      {

         ubGamma = gamma;
         continue;
      }

      alpha = (lbAlpha + ubAlpha) / 2.0;
      beta = (lbBeta + ubBeta) / 2.0;

      lbGamma = CubicEvalCut(valX, valY, lbX, ubX, lbY, ubY, alpha, beta, 0.0, cubic, NULL, NULL);

      assert( SCIPisFeasGE(scip, lbGamma - gamma, 0.0) );
      alpha_feas = alpha;
      beta_feas = beta;
   }

   if( i == 100 )
      printf("used 100 iterations...\n");

   cutX = alpha_feas;
   cutY = beta_feas;
   cutZ = lbGamma - alpha_feas * valX - beta_feas * valY;

   if (!CubicCutIsFeasible(scip, lbX, ubX, lbY, ubY, cutX, cutY, cutZ, cubic) )
   {
      return SCIP_ERROR;
   }

   SCIP_CALL( tryCut(scip, sepa, sol, varT, varX, varY, varZ, cutX, cutY, cutZ, result) );

   return SCIP_OKAY;
}

/** obtains a possible cut by discretization of the search space */
static
SCIP_RETCODE separateInteriorCutGridSearch(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_SEPA*            sepa,               /**< separator that computes the cut */
   SCIP_SOL*             sol,                /**< primal solution that should be separated, or NULL for LP solution */
   SCIP_VAR*             varT,               /**< variable in the cut */
   SCIP_VAR*             varX,               /**< variable in the cut */
   SCIP_VAR*             varY,               /**< variable in the cut */
   SCIP_VAR*             varZ,               /**< variable in the cut */
   Cubic*                cubic,              /**< cubic function struct */
   SCIP_RESULT*          result              /**< pointer to store the result of the separation call */
   )
{
   SCIP_Real valT;
   SCIP_Real valX;
   SCIP_Real valY;
   SCIP_Real valZ;
   SCIP_Real lbT;
   SCIP_Real lbX;
   SCIP_Real ubX;
   SCIP_Real lbY;
   SCIP_Real ubY;
   SCIP_Real lbA;
   SCIP_Real ubA;
   SCIP_Real lbB;
   SCIP_Real ubB;
   SCIP_Real alpha;
   SCIP_Real beta;
   SCIP_Real bestApprox;
   SCIP_Real bestA;
   SCIP_Real bestB;
   SCIP_Real error;
   int i;
   int j;
   int nsteps = 100;

   assert(scip != NULL);

   assert( varT != NULL );
   assert( varX != NULL );
   assert( varY != NULL );
   assert( varZ != NULL );
   assert( result != NULL );

   valZ = SCIPgetSolVal(scip, sol, varZ);

   if( SCIPisFeasZero(scip, valZ) )
   {
      return SCIP_OKAY;
   }

   valX = SCIPgetSolVal(scip, sol, varX);
   valY = SCIPgetSolVal(scip, sol, varY);

   lbX = SCIPvarGetLbLocal(varX);
   ubX = SCIPvarGetUbLocal(varX);
   lbY = SCIPvarGetLbLocal(varY);
   ubY = SCIPvarGetUbLocal(varY);

   valT = SCIPgetSolVal(scip, sol, varT);

   if( !SCIPisFeasEQ(scip, valZ, 1.0) )
   {
      valX = valX / valZ;
      valY = valY / valZ;
   }

   /* Due to the separation of indicator cuts and cuts on the boundary, this should hold */
   assert( SCIPisFeasGT(scip, valX, lbX) && SCIPisFeasLT(scip, valX, ubX) );
   assert( SCIPisFeasGT(scip, valY, lbY) && SCIPisFeasLT(scip, valY, ubY) );

   lbT = MAX(valT / valZ, CubicMin2d(lbX, ubX, lbY, ubY, cubic, NULL, NULL));

   lbA = -(CubicEval(0.0, valY, cubic) - lbT) / valX;
   ubA = (CubicEval(1.0, valY, cubic) - lbT) / (1.0 - valX);
   lbB = -(CubicEval(valX, 0.0, cubic) - lbT) / valY;
   ubB = (CubicEval(valX, 1.0, cubic) - lbT) / (1.0 - valY);

   if( lbA > ubA || lbB > ubB )
      return SCIP_OKAY;

   bestApprox = lbT;
   bestA = 0.0;
   bestB = 0.0;
   for( i = 0; i < nsteps; ++i )
   {
      for( j = 0; j < nsteps; ++j )
      {
         alpha = lbA + i * (ubA - lbA)/(nsteps-1);
         beta = lbB + i * (ubB - lbB)/(nsteps-1);

         error = CubicEvalCut(valX, valY, lbX, ubX, lbY, ubY, alpha, beta, 0.0, cubic, NULL, NULL);

         if( bestApprox < error )
         {
            bestApprox = error;
            bestA = alpha;
            bestB = beta;

         }
      }
   }

   if( bestApprox > lbT )
   {
      bestApprox -= bestA * valX;
      bestApprox -= bestB * valY;

      if (!CubicCutIsFeasible(scip, lbX, ubX, lbY, ubY, bestA, bestB, bestApprox, cubic) )
      {
         printf("cut not feasible\n");
         return SCIP_ERROR;
      }
      SCIP_CALL( tryCut(scip, sepa, sol, varT, varX, varY, varZ, bestA, bestB, bestApprox, result) );
   }

   return SCIP_OKAY;
}

/** tries out to obtain a cut by cutting plane method */
static
SCIP_RETCODE separateInteriorCutLP(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_SEPA*            sepa,               /**< separator that computes the cut */
   SCIP_SOL*             sol,                /**< primal solution that should be separated, or NULL for LP solution */
   SCIP_VAR*             varT,               /**< variable in the cut */
   SCIP_VAR*             varX,               /**< variable in the cut */
   SCIP_VAR*             varY,               /**< variable in the cut */
   SCIP_VAR*             varZ,               /**< variable in the cut */
   Cubic*                cubic,              /**< cubic function struct */
   SCIP_RESULT*          result              /**< pointer to store the result of the separation call */
   )
{
   char* name;
   SCIP_LPI* lpi;
   SCIP_Real obj;
   SCIP_Real lb;
   SCIP_Real ub;
   SCIP_Real valT;
   SCIP_Real valX;
   SCIP_Real valY;
   SCIP_Real valZ;
   SCIP_Real lbX;
   SCIP_Real ubX;
   SCIP_Real lbY;
   SCIP_Real ubY;
   SCIP_Real xopt;
   SCIP_Real yopt;
   SCIP_Bool addedCut;
   SCIP_Bool success;
   SCIP_Real coefsCut[3];
   int i;
   Cubic cubic_switched;
   SCIP_Real lbGamma;
   SCIP_Real alpha_feas = 0.0;
   SCIP_Real beta_feas = 0.0;

   assert(scip != NULL);

   assert( varT != NULL );
   assert( varX != NULL );
   assert( varY != NULL );
   assert( varZ != NULL );
   assert( result != NULL );

   valZ = SCIPgetSolVal(scip, sol, varZ);

   if( SCIPisFeasZero(scip, valZ) )
   {
      return SCIP_OKAY;
   }

   valX = SCIPgetSolVal(scip, sol, varX);
   valY = SCIPgetSolVal(scip, sol, varY);

   lbX = SCIPvarGetLbLocal(varX);
   ubX = SCIPvarGetUbLocal(varX);
   lbY = SCIPvarGetLbLocal(varY);
   ubY = SCIPvarGetUbLocal(varY);

   valT = SCIPgetSolVal(scip, sol, varT);

   lbGamma = CubicEvalCut(valX, valY, lbX, ubX, lbY, ubY, alpha_feas, beta_feas, 0.0, cubic, NULL, NULL);

   if( !SCIPisFeasEQ(scip, valZ, 1.0) )
   {
      valX = valX / valZ;
      valY = valY / valZ;
   }

   /* Due to the separation of indicator cuts and cuts on the boundary, this should hold */
   assert( SCIPisFeasGT(scip, valX, lbX) && SCIPisFeasLT(scip, valX, ubX) );
   assert( SCIPisFeasGT(scip, valY, lbY) && SCIPisFeasLT(scip, valY, ubY) );

   SCIP_CALL( SCIPlpiCreate(&lpi, SCIPgetMessagehdlr(scip), "LP", SCIP_OBJSEN_MAXIMIZE) );

   /* add a dummy row which we later delete. this way we can add each column separately making it easier to name them */
   lb = 1.0;
   SCIP_CALL( SCIPlpiAddRows(lpi, 1, &lb, &lb, NULL, 0, NULL, NULL, NULL) );

   SCIP_CALL( SCIPallocBufferArray(scip, &name, SCIP_MAXSTRLEN) );

   lb = -SCIPinfinity(scip);
   ub = SCIPinfinity(scip);
   obj = 1.0;
   (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "c");
   SCIP_CALL( SCIPlpiAddCols(lpi, 1, &obj, &lb, &ub, &name, 0, NULL, NULL, NULL) );
   obj = 0.0;
   (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "coefX");
   SCIP_CALL( SCIPlpiAddCols(lpi, 1, &obj, &lb, &ub, &name, 0, NULL, NULL, NULL) );
   (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "coefY");
   SCIP_CALL( SCIPlpiAddCols(lpi, 1, &obj, &lb, &ub, &name, 0, NULL, NULL, NULL) );
   SCIP_CALL( SCIPlpiDelRows(lpi, 0, 0) );

   SCIPfreeBufferArray(scip, &name);

   cubic_switched.coefsXXX = cubic->coefsYYY;
   cubic_switched.coefsXXY = cubic->coefsXYY;
   cubic_switched.coefsXYY = cubic->coefsXXY;
   cubic_switched.coefsYYY = cubic->coefsXXX;
   cubic_switched.coefsXX = cubic->coefsYY;
   cubic_switched.coefsXY = cubic->coefsXY;
   cubic_switched.coefsYY = cubic->coefsXX;
   cubic_switched.coefsX = cubic->coefsY;
   cubic_switched.coefsY = cubic->coefsX;
   cubic_switched.coefsC = cubic->coefsC;

   /* add rows which correspond to the corners of the feasible box */
   SCIP_CALL( addLPIRow(scip, lpi, lbX, lbY, valX, valY, cubic) );
   SCIP_CALL( addLPIRow(scip, lpi, lbX, ubY, valX, valY, cubic) );
   SCIP_CALL( addLPIRow(scip, lpi, ubX, lbY, valX, valY, cubic) );
   SCIP_CALL( addLPIRow(scip, lpi, ubX, ubY, valX, valY, cubic) );
   /* also add rows which correspond to the minima of the function over the boundary */
   (void) CubicEvalCut1d(0.0, lbY, lbX, ubX, 0.0, 0.0, cubic, &xopt);
   SCIP_CALL( addLPIRow(scip, lpi, xopt, lbY, valX, valY, cubic) );
   (void) CubicEvalCut1d(0.0, ubY, lbX, ubX, 0.0, 0.0, cubic, &xopt);
   SCIP_CALL( addLPIRow(scip, lpi, xopt, ubY, valX, valY, cubic) );

   (void) CubicEvalCut1d(0.0, lbX, lbY, ubY, 0.0, 0.0, &cubic_switched, &yopt);
   SCIP_CALL( addLPIRow(scip, lpi, lbX, yopt, valX, valY, cubic) );
   (void) CubicEvalCut1d(0.0, ubX, lbY, ubY, 0.0, 0.0, &cubic_switched, &yopt);
   SCIP_CALL( addLPIRow(scip, lpi, ubX, yopt, valX, valY, cubic) );

   addedCut = TRUE;
   success = TRUE;
   i = 0;
   while( addedCut )
   {
      i++;
      //SCIP_CALL( SCIPlpiSolvePrimal(consdata->lpi_sep) );
      //SCIP_CALL( SCIPlpiWriteLP(consdata->lpi_sep, "sepa.lp") );
      SCIP_CALL( SCIPlpiSolveDual(lpi) );
      //SCIP_CALL( SCIPlpiSolveBarrier(consdata->lpi_sep, TRUE) );
      assert( SCIPlpiWasSolved(lpi) );
      if( !SCIPlpiIsPrimalFeasible(lpi) )
      {
         printf("primal infeasible\n");

         SCIP_CALL( SCIPlpiFree(&lpi) );
         return SCIP_OKAY;
      }
      if( !SCIPlpiIsOptimal(lpi) )
      {
         printf("not solved to optimality\n");
         return SCIP_ERROR;
      }
      SCIP_CALL( SCIPlpiGetSol(lpi, NULL, coefsCut, NULL, NULL, NULL) );

      if( SCIPisFeasGE(scip, valT, coefsCut[0]) || i > 100)
      {
         success = FALSE;
         break;
      }

      /* search for a point which is violated by the underestimate */
      if( !SCIPisGE(scip, CubicEvalCut(valX, valY, lbX, ubX, lbY, ubY, coefsCut[1], coefsCut[2], coefsCut[0], cubic, &xopt, &yopt), 0.0) )
      {
         if( CubicEvalCut(valX, valY, lbX, ubX, lbY, ubY, coefsCut[1], coefsCut[2], 0.0, cubic, &xopt, &yopt) > lbGamma )
         {
            lbGamma = CubicEvalCut(valX, valY, lbX, ubX, lbY, ubY, coefsCut[1], coefsCut[2], 0.0, cubic, &xopt, &yopt);
            alpha_feas = coefsCut[1];
            beta_feas = coefsCut[2];
         }
         if( coefsCut[0] - lbGamma < SEPA_EPSILON )
         {
            coefsCut[0] = lbGamma;
            coefsCut[1] = alpha_feas;
            coefsCut[2] = beta_feas;
            success = TRUE;
            addedCut = FALSE;
         }
         else
         {
            SCIP_CALL( addLPIRow(scip, lpi, xopt, yopt, valX, valY, cubic) );
         }
      }
      else
      {
         addedCut = FALSE;
      }

   }
   if( success )
   {
      coefsCut[0] -= coefsCut[1] * valX;
      coefsCut[0] -= coefsCut[2] * valY;

      if( !CubicCutIsFeasible(scip, lbX, ubX, lbY, ubY, coefsCut[1], coefsCut[2], coefsCut[0], cubic) )
      {
         printf("cut not feasible\n");
         return SCIP_ERROR;
      }
      SCIP_CALL( tryCut(scip, sepa, sol, varT, varX, varY, varZ, coefsCut[1], coefsCut[2], coefsCut[0], result) );
   }
   SCIP_CALL( SCIPlpiFree(&lpi) );


   return SCIP_OKAY;
}

/** tries out cuts which lift from the boundary */
static
SCIP_RETCODE separateBorderCuts(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_SEPA*            sepa,               /**< separator that computes the cut */
   SCIP_SOL*             sol,                /**< primal solution that should be separated, or NULL for LP solution */
   SCIP_VAR*             varT,               /**< variable in the cut */
   SCIP_VAR*             varX,               /**< variable in the cut */
   SCIP_VAR*             varY,               /**< variable in the cut */
   SCIP_VAR*             varZ,               /**< variable in the cut */
   Cubic*                cubic,              /**< cubic function struct */
   SCIP_Bool*            valid,              /**< returns if we are on the boundary */
   SCIP_RESULT*          result              /**< pointer to store the result of the separation call */
   )
{
   SCIP_Real cutX;
   SCIP_Real cutY;
   SCIP_Real cutZ;
   SCIP_Real valX;
   SCIP_Real valY;
   SCIP_Real valZ;
   SCIP_Real lbX;
   SCIP_Real ubX;
   SCIP_Real lbY;
   SCIP_Real ubY;
   Cubic cubic_switched;

   assert(scip != NULL);
   assert( varT != NULL );
   assert( varX != NULL );
   assert( varY != NULL );
   assert( varZ != NULL );
   assert( valid != NULL );
   assert( result != NULL );

   *valid = FALSE;

   valZ = SCIPgetSolVal(scip, sol, varZ);

   if( SCIPisFeasZero(scip, valZ) )
   {
      return SCIP_OKAY;
   }

   valX = SCIPgetSolVal(scip, sol, varX);
   valY = SCIPgetSolVal(scip, sol, varY);

   lbX = SCIPvarGetLbLocal(varX);
   ubX = SCIPvarGetUbLocal(varX);
   lbY = SCIPvarGetLbLocal(varY);
   ubY = SCIPvarGetUbLocal(varY);

   if( !SCIPisFeasEQ(scip, valZ, 1.0) )
   {
      valX = valX / valZ;
      valY = valY / valZ;
   }

   /* Due to the separation of indicator cuts, this should hold */
   assert( SCIPisFeasGE(scip, valX, lbX) && SCIPisFeasLE(scip, valX, ubX) );
   assert( SCIPisFeasGE(scip, valY, lbY) && SCIPisFeasLE(scip, valY, ubY) );

   cubic_switched.coefsXXX = cubic->coefsYYY;
   cubic_switched.coefsXXY = cubic->coefsXYY;
   cubic_switched.coefsXYY = cubic->coefsXXY;
   cubic_switched.coefsYYY = cubic->coefsXXX;
   cubic_switched.coefsXX = cubic->coefsYY;
   cubic_switched.coefsXY = cubic->coefsXY;
   cubic_switched.coefsYY = cubic->coefsXX;
   cubic_switched.coefsX = cubic->coefsY;
   cubic_switched.coefsY = cubic->coefsX;
   cubic_switched.coefsC = cubic->coefsC;

   if( SCIPisEQ(scip, valX, lbX) )
   {
      *valid = TRUE;
      SCIP_CALL( getCutFromLb(scip, valY, lbX, ubX, lbY, ubY, cubic, &cutX, &cutY, &cutZ) );

      if( !CubicCutIsFeasible(scip, lbX, ubX, lbY, ubY, cutX, cutY, cutZ, cubic) )
      {
         return SCIP_ERROR;
      }

      SCIP_CALL( tryCut(scip, sepa, sol, varT, varX, varY, varZ, cutX, cutY, cutZ, result) );
      if( *result == SCIP_CUTOFF )
         return SCIP_OKAY;
   }
   if( SCIPisEQ(scip, valX, ubX) )
   {
      *valid = TRUE;
      SCIP_CALL( getCutFromUb(scip, valY, lbX, ubX, lbY, ubY, cubic, &cutX, &cutY, &cutZ) );

      if( !CubicCutIsFeasible(scip, lbX, ubX, lbY, ubY, cutX, cutY, cutZ, cubic) )
      {
         return SCIP_ERROR;
      }

      SCIP_CALL( tryCut(scip, sepa, sol, varT, varX, varY, varZ, cutX, cutY, cutZ, result) );
      if( *result == SCIP_CUTOFF )
         return SCIP_OKAY;
   }
   if( SCIPisEQ(scip, valY, lbY) )
   {
      *valid = TRUE;
      SCIP_CALL( getCutFromLb(scip, valX, lbY, ubY, lbX, ubX, &cubic_switched, &cutY, &cutX, &cutZ) );

      if( !CubicCutIsFeasible(scip, lbX, ubX, lbY, ubY, cutX, cutY, cutZ, cubic) )
      {
         return SCIP_ERROR;
      }

      SCIP_CALL( tryCut(scip, sepa, sol, varT, varX, varY, varZ, cutX, cutY, cutZ, result) );
      if( *result == SCIP_CUTOFF )
         return SCIP_OKAY;
   }
   if( SCIPisEQ(scip, valX, ubX) )
   {
      *valid = TRUE;
      SCIP_CALL( getCutFromUb(scip, valX, lbY, ubY, lbX, ubX, &cubic_switched, &cutY, &cutX, &cutZ) );

      if( !CubicCutIsFeasible(scip, lbX, ubX, lbY, ubY, cutX, cutY, cutZ, cubic) )
      {
         return SCIP_ERROR;
      }

      SCIP_CALL( tryCut(scip, sepa, sol, varT, varX, varY, varZ, cutX, cutY, cutZ, result) );
      if( *result == SCIP_CUTOFF )
         return SCIP_OKAY;
   }

   return SCIP_OKAY;
}

static
SCIP_RETCODE separateIndicatorCuts(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_SEPA*            sepa,               /**< separator that computes the cut */
   SCIP_SOL*             sol,                /**< primal solution that should be separated, or NULL for LP solution */
   SCIP_VAR*             varX,               /**< variable in the cut */
   SCIP_VAR*             varY,               /**< variable in the cut */
   SCIP_VAR*             varZ,               /**< variable in the cut */
   SCIP_Bool*            valid,              /**< whether the gradient cut is valid */
   SCIP_RESULT*          result              /**< pointer to store the result of the separation call */
   )
{
   SCIP_Real valX;
   SCIP_Real valY;
   SCIP_Real valZ;
   SCIP_Real lbX;
   SCIP_Real ubX;
   SCIP_Real lbY;
   SCIP_Real ubY;

   SCIP_ROW* row;
   SCIP_Bool infeasible;

   assert( scip != NULL );
   assert( sepa != NULL );
   assert( varX != NULL );
   assert( varY != NULL );
   assert( varZ != NULL );
   assert( valid != NULL );
   assert( result != NULL );

   valZ = SCIPgetSolVal(scip, sol, varZ);
   valX = SCIPgetSolVal(scip, sol, varX);
   valY = SCIPgetSolVal(scip, sol, varY);

   lbX = SCIPvarGetLbLocal(varX);
   ubX = SCIPvarGetUbLocal(varX);
   lbY = SCIPvarGetLbLocal(varY);
   ubY = SCIPvarGetUbLocal(varY);

   *valid = FALSE;

   if( SCIPisFeasGE(scip, valX, valZ * ubX) )
   {
      *valid = TRUE;
      SCIP_CALL( SCIPcreateEmptyRowSepa(scip, &row, sepa, "cutIndicator", 0.0, SCIPinfinity(scip), TRUE, FALSE, TRUE) );
      SCIP_CALL( SCIPcacheRowExtensions(scip, row) );
      SCIP_CALL( SCIPaddVarToRow(scip, row, varX, -1.0) );
      SCIP_CALL( SCIPaddVarToRow(scip, row, varZ, ubX) );

      SCIP_CALL( SCIPflushRowExtensions(scip, row) );

#ifdef SCIP_DEBUG
      SCIP_CALL( SCIPprintRow(scip, row, NULL) );
#endif

      if( SCIPisCutEfficacious(scip, sol, row) )
      {
         SCIP_CALL( SCIPaddRow(scip, row, TRUE, &infeasible) );
         if( infeasible )
            *result = SCIP_CUTOFF;
         else
            *result = SCIP_SEPARATED;
      }

      SCIP_CALL( SCIPreleaseRow(scip, &row) );

      return SCIP_OKAY;
   }

   if( SCIPisFeasLE(scip, valX, valZ * lbX) )
   {
      *valid = TRUE;
      SCIP_CALL( SCIPcreateEmptyRowSepa(scip, &row, sepa, "cutIndicator", 0.0, SCIPinfinity(scip), TRUE, FALSE, TRUE) );
      SCIP_CALL( SCIPcacheRowExtensions(scip, row) );
      SCIP_CALL( SCIPaddVarToRow(scip, row, varX, 1.0) );
      SCIP_CALL( SCIPaddVarToRow(scip, row, varZ, -lbX) );

      SCIP_CALL( SCIPflushRowExtensions(scip, row) );

#ifdef SCIP_DEBUG
      SCIP_CALL( SCIPprintRow(scip, row, NULL) );
#endif

      if( SCIPisCutEfficacious(scip, sol, row) )
      {
         SCIP_CALL( SCIPaddRow(scip, row, TRUE, &infeasible) );
         if( infeasible )
            *result = SCIP_CUTOFF;
         else
            *result = SCIP_SEPARATED;
      }

      SCIP_CALL( SCIPreleaseRow(scip, &row) );

      return SCIP_OKAY;
   }

   if( SCIPisFeasGE(scip, valY, valZ * ubY) )
   {
      *valid = TRUE;
      SCIP_CALL( SCIPcreateEmptyRowSepa(scip, &row, sepa, "cutIndicator", 0.0, SCIPinfinity(scip), TRUE, FALSE, TRUE) );
      SCIP_CALL( SCIPcacheRowExtensions(scip, row) );
      SCIP_CALL( SCIPaddVarToRow(scip, row, varY, -1.0) );
      SCIP_CALL( SCIPaddVarToRow(scip, row, varZ, ubY) );

      SCIP_CALL( SCIPflushRowExtensions(scip, row) );

#ifdef SCIP_DEBUG
      SCIP_CALL( SCIPprintRow(scip, row, NULL) );
#endif

      if( SCIPisCutEfficacious(scip, sol, row) )
      {
         SCIP_CALL( SCIPaddRow(scip, row, TRUE, &infeasible) );
         if( infeasible )
            *result = SCIP_CUTOFF;
         else
            *result = SCIP_SEPARATED;
      }

      SCIP_CALL( SCIPreleaseRow(scip, &row) );

      return SCIP_OKAY;
   }

   if( SCIPisFeasLE(scip, valY, valZ * lbY) )
   {
      *valid = TRUE;
      SCIP_CALL( SCIPcreateEmptyRowSepa(scip, &row, sepa, "cutIndicator", 0.0, SCIPinfinity(scip), TRUE, FALSE, TRUE) );
      SCIP_CALL( SCIPcacheRowExtensions(scip, row) );
      SCIP_CALL( SCIPaddVarToRow(scip, row, varY, 1.0) );
      SCIP_CALL( SCIPaddVarToRow(scip, row, varZ, -lbY) );

      SCIP_CALL( SCIPflushRowExtensions(scip, row) );

#ifdef SCIP_DEBUG
      SCIP_CALL( SCIPprintRow(scip, row, NULL) );
#endif

      if( SCIPisCutEfficacious(scip, sol, row) )
      {
         SCIP_CALL( SCIPaddRow(scip, row, TRUE, &infeasible) );
         if( infeasible )
            *result = SCIP_CUTOFF;
         else
            *result = SCIP_SEPARATED;
      }

      SCIP_CALL( SCIPreleaseRow(scip, &row) );

      return SCIP_OKAY;
   }

   return SCIP_OKAY;
}

/** check out the different cuts */
static
SCIP_RETCODE separateCut(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_SEPA*            sepa,               /**< separator that computes the cut */
   SCIP_SOL*             sol,                /**< primal solution that should be separated, or NULL for LP solution */
   SCIP_VAR*             varT,               /**< variable in the cut */
   SCIP_VAR*             varX,               /**< variable in the cut */
   SCIP_VAR*             varY,               /**< variable in the cut */
   SCIP_VAR*             varZ,               /**< variable in the cut */
   Cubic*                cubic,              /**< cubic function struct */
   SCIP_RESULT*          result              /**< pointer to store the result of the separation call */
   )
{
   SCIP_Bool cutApplies;
   SCIP_Bool useExact;
   SCIP_Bool useGrid;
   SCIP_Bool useLP;

   /* first check that lbX * z <= x <= ubX * z */
   SCIP_CALL( separateIndicatorCuts(scip, sepa, sol, varX, varY, varZ, &cutApplies, result) );
   if( cutApplies )
      return SCIP_OKAY;

   /* next check whether we are one the boundary */
   SCIP_CALL( separateBorderCuts(scip, sepa, sol, varT, varX, varY, varZ, cubic, &cutApplies, result) );
   if( cutApplies )
      return SCIP_OKAY;

   /* now we check whether the convex envelope is given by the gradient cut */
   SCIP_CALL( separateGradientCuts(scip, sepa, sol, varT, varX, varY, varZ, cubic, &cutApplies, result) );
   if( cutApplies )
      return SCIP_OKAY;

   SCIP_CALL( SCIPgetBoolParam(scip, "sepa_2dCubeInd/useExactConvex", &useExact) );
   SCIP_CALL( SCIPgetBoolParam(scip, "sepa_2dCubeInd/useGridConvex", &useGrid) );
   SCIP_CALL( SCIPgetBoolParam(scip, "sepa_2dCubeInd/useLPConvex", &useLP) );

   if( useExact )
   {
      SCIP_CALL( separateInterior(scip, sepa, sol, varT, varX, varY, varZ, cubic, result) );
      if( *result == SCIP_CUTOFF )
         return SCIP_OKAY;
   }

   if( useGrid )
   {
      SCIP_CALL( separateInteriorCutGridSearch(scip, sepa, sol, varT, varX, varY, varZ, cubic, result) );
      if( *result == SCIP_CUTOFF )
         return SCIP_OKAY;
   }

   if( useLP )
   {
      SCIP_CALL( separateInteriorCutLP(scip, sepa, sol, varT, varX, varY, varZ, cubic, result) );
      if( *result == SCIP_CUTOFF )
         return SCIP_OKAY;
   }


   return SCIP_OKAY;
}

/* separate all saved inequalities in the given separator for the given solution */
static
SCIP_RETCODE separateCuts(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_SEPA*            sepa,               /**< the cut separator itself */
   SCIP_SOL*             sol,                /**< primal solution that should be separated, or NULL for LP solution */
   SCIP_RESULT*          result              /**< pointer to store the result of the separation call */
   )
{  /*lint --e{715}*/
   SCIP_SEPADATA* sepadata;
   int i;

   assert(scip != NULL);
   assert(sepa != NULL);
   assert(*result == SCIP_DIDNOTRUN);

   /* get separator data */
   sepadata = SCIPsepaGetData(sepa);
   assert(sepadata != NULL);

   *result = SCIP_DIDNOTFIND;
   for( i = 0; i < sepadata->neqs; ++i )
   {
      Cubic cubic;

      cubic.coefsXXX = sepadata->coefsXXX[i];
      cubic.coefsXXY = sepadata->coefsXXY[i];
      cubic.coefsXYY = sepadata->coefsXYY[i];
      cubic.coefsYYY = sepadata->coefsYYY[i];
      cubic.coefsXX = sepadata->coefsXX[i];
      cubic.coefsXY = sepadata->coefsXY[i];
      cubic.coefsYY = sepadata->coefsYY[i];
      cubic.coefsX = sepadata->coefsX[i];
      cubic.coefsY = sepadata->coefsY[i];
      cubic.coefsC = sepadata->coefsC[i];

      SCIP_CALL( separateCut(scip, sepa, sol, sepadata->varsT[i], sepadata->varsX[i], sepadata->varsY[i], sepadata->varsZ[i], &cubic, result) );
   }

   return SCIP_OKAY;
}

/** adds variables and the coefficients of cubic polynom to separate to the separator */
SCIP_RETCODE SCIPSepaCubeIndicatorRegister(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_VAR*             varT,               /**< variable t */
   SCIP_VAR*             varX,               /**< variable x */
   SCIP_VAR*             varY,               /**< variable y */
   SCIP_VAR*             varZ,               /**< indicator variable*/
   SCIP_Real             coefXXX,            /**< coefficient of function */
   SCIP_Real             coefXXY,            /**< coefficient of function */
   SCIP_Real             coefXYY,            /**< coefficient of function */
   SCIP_Real             coefYYY,            /**< coefficient of function */
   SCIP_Real             coefXX,             /**< coefficient of function */
   SCIP_Real             coefXY,             /**< coefficient of function */
   SCIP_Real             coefYY,             /**< coefficient of function */
   SCIP_Real             coefX,              /**< coefficient of function */
   SCIP_Real             coefY,              /**< coefficient of function */
   SCIP_Real             coefC               /**< coefficient of function */
   )
{
   SCIP_SEPA* sepa;
   SCIP_SEPADATA* sepadata;

   assert(scip != NULL);

   sepa = SCIPfindSepa(scip, SEPA_NAME);

   if (sepa == NULL)
   {
      printf("no sepa found\n");
      return SCIP_ERROR;
   }

   sepadata = SCIPsepaGetData(sepa);
   if (sepadata == NULL)
   {
      printf("no sepadata found\n");
      return SCIP_ERROR;
   }
   // realloc memory if necessary
   if (sepadata->sizeeqs == sepadata->neqs+1 )
   {
      sepadata->sizeeqs = SCIPcalcMemGrowSize(scip, sepadata->sizeeqs+1);
      SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &sepadata->varsT, sepadata->neqs+1, sepadata->sizeeqs) );
      SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &sepadata->varsX, sepadata->neqs+1, sepadata->sizeeqs) );
      SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &sepadata->varsY, sepadata->neqs+1, sepadata->sizeeqs) );
      SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &sepadata->varsZ, sepadata->neqs+1, sepadata->sizeeqs) );
      SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &sepadata->coefsXXX, sepadata->neqs+1, sepadata->sizeeqs) );
      SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &sepadata->coefsXXY, sepadata->neqs+1, sepadata->sizeeqs) );
      SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &sepadata->coefsXYY, sepadata->neqs+1, sepadata->sizeeqs) );
      SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &sepadata->coefsYYY, sepadata->neqs+1, sepadata->sizeeqs) );
      SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &sepadata->coefsXX, sepadata->neqs+1, sepadata->sizeeqs) );
      SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &sepadata->coefsXY, sepadata->neqs+1, sepadata->sizeeqs) );
      SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &sepadata->coefsYY, sepadata->neqs+1, sepadata->sizeeqs) );
      SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &sepadata->coefsX, sepadata->neqs+1, sepadata->sizeeqs) );
      SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &sepadata->coefsY, sepadata->neqs+1, sepadata->sizeeqs) );
      SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &sepadata->coefsC, sepadata->neqs+1, sepadata->sizeeqs) );
   }

   sepadata->varsT[sepadata->neqs] = varT;
   sepadata->varsX[sepadata->neqs] = varX;
   sepadata->varsY[sepadata->neqs] = varY;
   sepadata->varsZ[sepadata->neqs] = varZ;
   sepadata->coefsXXX[sepadata->neqs] = coefXXX;
   sepadata->coefsXXY[sepadata->neqs] = coefXXY;
   sepadata->coefsXYY[sepadata->neqs] = coefXYY;
   sepadata->coefsYYY[sepadata->neqs] = coefYYY;
   sepadata->coefsXX[sepadata->neqs] = coefXX;
   sepadata->coefsXY[sepadata->neqs] = coefXY;
   sepadata->coefsYY[sepadata->neqs] = coefYY;
   sepadata->coefsX[sepadata->neqs] = coefX;
   sepadata->coefsY[sepadata->neqs] = coefY;
   sepadata->coefsC[sepadata->neqs] = coefC;
   sepadata->neqs++;

   return SCIP_OKAY;
}



/*
 * Callback methods of separator
 */


/** destructor of separator to free user data (called when SCIP is exiting) */
static
SCIP_DECL_SEPAFREE(sepaFreeCubeIndicator)
{  /*lint --e{715}*/

   SCIP_SEPADATA* sepadata;

   sepadata = SCIPsepaGetData(sepa);
   SCIPfreeBlockMemoryArray(scip, &sepadata->varsT, sepadata->sizeeqs);
   SCIPfreeBlockMemoryArray(scip, &sepadata->varsX, sepadata->sizeeqs);
   SCIPfreeBlockMemoryArray(scip, &sepadata->varsY, sepadata->sizeeqs);
   SCIPfreeBlockMemoryArray(scip, &sepadata->varsZ, sepadata->sizeeqs);
   SCIPfreeBlockMemoryArray(scip, &sepadata->coefsXXX, sepadata->sizeeqs);
   SCIPfreeBlockMemoryArray(scip, &sepadata->coefsXXY, sepadata->sizeeqs);
   SCIPfreeBlockMemoryArray(scip, &sepadata->coefsXYY, sepadata->sizeeqs);
   SCIPfreeBlockMemoryArray(scip, &sepadata->coefsYYY, sepadata->sizeeqs);
   SCIPfreeBlockMemoryArray(scip, &sepadata->coefsXX, sepadata->sizeeqs);
   SCIPfreeBlockMemoryArray(scip, &sepadata->coefsXY, sepadata->sizeeqs);
   SCIPfreeBlockMemoryArray(scip, &sepadata->coefsYY, sepadata->sizeeqs);
   SCIPfreeBlockMemoryArray(scip, &sepadata->coefsX, sepadata->sizeeqs);
   SCIPfreeBlockMemoryArray(scip, &sepadata->coefsY, sepadata->sizeeqs);
   SCIPfreeBlockMemoryArray(scip, &sepadata->coefsC, sepadata->sizeeqs);

   SCIPfreeBlockMemory(scip, &sepadata);

   return SCIP_OKAY;
}


/** LP solution separation method of separator */
static
SCIP_DECL_SEPAEXECLP(sepaExeclpCubeIndicator)
{  /*lint --e{715}*/
   *result = SCIP_DIDNOTRUN;

   /* only call separator, if we are not close to terminating */
   if( SCIPisStopped(scip) )
      return SCIP_OKAY;

   /* separate cuts on the LP solution */
   SCIP_CALL( separateCuts(scip, sepa, NULL, result) );

   return SCIP_OKAY;
}


/** arbitrary primal solution separation method of separator */
static
SCIP_DECL_SEPAEXECSOL(sepaExecsolCubeIndicator)
{  /*lint --e{715}*/
   *result = SCIP_DIDNOTRUN;

   /* separate cuts on the given primal solution */
   SCIP_CALL( separateCuts(scip, sepa, sol, result) );

   return SCIP_OKAY;
}
/*
 * separator specific interface methods
 */

/** creates the separator and includes it in SCIP */
SCIP_RETCODE SCIPincludeSepaCubeIndicator(
   SCIP*                 scip                /**< SCIP data structure */
   )
{
   SCIP_SEPADATA* sepadata;
   SCIP_SEPA* sepa;

   /* create separator data */
   sepadata = NULL;

   SCIP_CALL( SCIPallocBlockMemory(scip, &sepadata) );

   assert(sepadata != NULL);

   sepadata->neqs = 0;
   sepadata->sizeeqs = 16;

   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &sepadata->varsT, sepadata->sizeeqs) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &sepadata->varsX, sepadata->sizeeqs) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &sepadata->varsY, sepadata->sizeeqs) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &sepadata->varsZ, sepadata->sizeeqs) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &sepadata->coefsXXX, sepadata->sizeeqs) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &sepadata->coefsXXY, sepadata->sizeeqs) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &sepadata->coefsXYY, sepadata->sizeeqs) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &sepadata->coefsYYY, sepadata->sizeeqs) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &sepadata->coefsXX, sepadata->sizeeqs) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &sepadata->coefsXY, sepadata->sizeeqs) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &sepadata->coefsYY, sepadata->sizeeqs) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &sepadata->coefsX, sepadata->sizeeqs) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &sepadata->coefsY, sepadata->sizeeqs) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &sepadata->coefsC, sepadata->sizeeqs) );

   sepa = NULL;

   /* include separator */
   SCIP_CALL( SCIPincludeSepaBasic(scip, &sepa, SEPA_NAME, SEPA_DESC, SEPA_PRIORITY, SEPA_FREQ, SEPA_MAXBOUNDDIST,
         SEPA_USESSUBSCIP, SEPA_DELAY,
         sepaExeclpCubeIndicator, sepaExecsolCubeIndicator,
         sepadata) );

   assert(sepa != NULL);

   /* set non fundamental callbacks via setter functions */
   SCIP_CALL( SCIPsetSepaFree(scip, sepa, sepaFreeCubeIndicator) );

   SCIP_CALL( SCIPaddBoolParam(scip, "sepa_2dCubeInd/useExactConvex", "Should the exact methods be used to obtain the convex envelope>", NULL, FALSE, DEFAULT_USEEXACT, NULL, NULL) );
   SCIP_CALL( SCIPaddBoolParam(scip, "sepa_2dCubeInd/useLPConvex", "Should the lp method be used to obtain the convex envelope?", NULL, FALSE, DEFAULT_USELP, NULL, NULL) );
   SCIP_CALL( SCIPaddBoolParam(scip, "sepa_2dCubeInd/useGridConvex", "Should the grid method be used to obtain the convex envelope?", NULL, FALSE, DEFAULT_USEGRID, NULL, NULL) );

   return SCIP_OKAY;
}
