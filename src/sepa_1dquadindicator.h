/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*    This file is part of the program kresilience                           */
/*                                                                           */
/*    Copyright (C) 2020-2021 Andreas Schmitt, Marc Pfetsch                  */
/*                                                                           */
/*                                                                           */
/*    Based on SCIP --- Solving Constraint Integer Programs                  */
/*       SCIP is distributed under the terms of the SCIP Academic Licence,   */
/*       see file COPYING in the SCIP distribution.                          */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**@file   sepa_1dquadindicator.h
 * @brief  assume two quadratics to bound a variable from above and below, including a indicator constraint
 * @author Andreas Schmitt
 */

/*---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0----+----1----+----2*/

#ifndef __SCIP_SEPA_QUADINDICATOR_H__
#define __SCIP_SEPA_QUADINDICATOR_H__


#include "scip/scip.h"

#ifdef __cplusplus
extern "C" {
#endif

/** adds variables and polynom coefficients to separate to the separator  */
extern
SCIP_RETCODE SCIPSepaQuadIndicatorRegister(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_VAR*             varX,               /**< variable x */
   SCIP_VAR*             varY,               /**< variable x */
   SCIP_VAR*             varZ,               /**< indicator variable*/
   SCIP_Real             coefXXlb,           /**< coefficient of lower bound function */
   SCIP_Real             coefXlb,            /**< coefficient of lower bound function */
   SCIP_Real             coefClb,            /**< coefficient of lower bound function */
   SCIP_Real             coefXXub,           /**< coefficient of upper bound function */
   SCIP_Real             coefXub,            /**< coefficient of upper bound function */
   SCIP_Real             coefCub             /**< coefficient of upper bound function */
   );

/** creates the separator and includes it in SCIP */
extern
SCIP_RETCODE SCIPincludeSepaQuadIndicator(
   SCIP*                 scip                /**< SCIP data structure */
   );

#ifdef __cplusplus
}
#endif

#endif
