/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*    This file is part of the program kresilience                           */
/*                                                                           */
/*    Copyright (C) 2020-2021 Andreas Schmitt, Marc Pfetsch                  */
/*                                                                           */
/*                                                                           */
/*    Based on SCIP --- Solving Constraint Integer Programs                  */
/*       SCIP is distributed under the terms of the SCIP Academic Licence,   */
/*       see file COPYING in the SCIP distribution.                          */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**@file   cons_interdiction.h
 * @brief  constraint handler for interdiction constraints of the form y^t x >= 1 forall y feasible for a subscip
 * @author Andreas Schmitt
 */

/*---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0----+----1----+----2*/

#ifndef __SCIP_CONS_INTERDICTION_H__
#define __SCIP_CONS_INTERDICTION_H__


#include "scip/scip.h"

#ifdef __cplusplus
extern "C" {
#endif

/** creates the handler for xyz constraints and includes it in SCIP
 *
 * @ingroup ConshdlrIncludes
 * */
SCIP_EXPORT
SCIP_RETCODE SCIPincludeConshdlrInterdiction(
   SCIP*                 scip                /**< SCIP data structure */
   );

/** Returns for a given cons and a given rank the variable which has this covering rank.
 */
SCIP_EXPORT
int consInterdictionGetIndexRanking(
   SCIP_CONS*            cons,               /**< interdiction constraint */
   int                   i                   /**< rank we query for */
   );

/** Gets an interdicttion constraint and the scip it belongs. For the constraint we check for the saved solutions vals_y
    and add them as new constraints x^t y >= 1 to scip. Sets the counter for the saved solutions to 0 afterwards.
 */
SCIP_EXPORT
SCIP_RETCODE consInterdictionAddSubscipSolsAsConstraints(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONS*            cons                /**< interdiction constraint */
   );

/**@addtogroup CONSHDLRS
 *
 * @{
 *
 * @name Xyz Constraints
 *
 * @{
 */

/** creates and captures a constraint
 *
 *  @note the constraint gets captured, hence at one point you have to release it using the method SCIPreleaseCons()
 */
SCIP_EXPORT
SCIP_RETCODE SCIPcreateConsInterdiction(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONS**           cons,               /**< pointer to hold the created constraint */
   const char*           name,               /**< name of constraint */
   int                   nvars,              /**< number of variables which have to be treated */
   SCIP_VAR**            vars_x,             /**< variables in scip */
   SCIP_VAR**            vars_y,             /**< variables in subscip */
   SCIP*                 subscip,            /**< SCIP data structure for the subproblem */
   SCIP_Bool             usereopt,           /**< should reoptimization be used to solve the subscip */
   SCIP_Bool             usefix,             /**< if reoptimization is not used, should variable fixing be used */
   SCIP_Bool             initial,            /**< should the LP relaxation of constraint be in the initial LP?
                                              *   Usually set to TRUE. Set to FALSE for 'lazy constraints'. */
   SCIP_Bool             separate,           /**< should the constraint be separated during LP processing?
                                              *   Usually set to TRUE. */
   SCIP_Bool             enforce,            /**< should the constraint be enforced during node processing?
                                              *   TRUE for model constraints, FALSE for additional, redundant constraints. */
   SCIP_Bool             check,              /**< should the constraint be checked for feasibility?
                                              *   TRUE for model constraints, FALSE for additional, redundant constraints. */
   SCIP_Bool             propagate,          /**< should the constraint be propagated during node processing?
                                              *   Usually set to TRUE. */
   SCIP_Bool             local,              /**< is constraint only valid locally?
                                              *   Usually set to FALSE. Has to be set to TRUE, e.g., for branching constraints. */
   SCIP_Bool             modifiable,         /**< is constraint modifiable (subject to column generation)?
                                              *   Usually set to FALSE. In column generation applications, set to TRUE if pricing
                                              *   adds coefficients to this constraint. */
   SCIP_Bool             dynamic,            /**< is constraint subject to aging?
                                              *   Usually set to FALSE. Set to TRUE for own cuts which
                                              *   are separated as constraints. */
   SCIP_Bool             removable,          /**< should the relaxation be removed from the LP due to aging or cleanup?
                                              *   Usually set to FALSE. Set to TRUE for 'lazy constraints' and 'user cuts'. */
   SCIP_Bool             stickingatnode      /**< should the constraint always be kept at the node where it was added, even
                                              *   if it may be moved to a more global node?
                                              *   Usually set to FALSE. Set to TRUE to for constraints that represent node data. */
   );

/** creates and captures a constraint with all its constraint flags set to their
 *  default values
 *
 *  @note the constraint gets captured, hence at one point you have to release it using the method SCIPreleaseCons()
 */
SCIP_EXPORT
SCIP_RETCODE SCIPcreateConsBasicInterdiction(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONS**           cons,               /**< pointer to hold the created constraint */
   const char*           name,               /**< name of constraint */
   int                   nvars,              /**< number of variables which have to be treated */
   SCIP_VAR**            vars_x,             /**< variables in scip */
   SCIP_VAR**            vars_y,             /**< variables in subscip */
   SCIP*                 subscip,            /**< SCIP data structure for the subproblem */
   SCIP_Bool             usereopt,           /**< should reoptimization be used to solve the subscip */
   SCIP_Bool             usefix              /**< if reoptimization is not used, should variable fixing be used */
   );

/* @} */

/* @} */

#ifdef __cplusplus
}
#endif

#endif
