/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*    This file is part of the program kresilience                           */
/*                                                                           */
/*    Copyright (C) 2020-2021 Andreas Schmitt, Marc Pfetsch                  */
/*                                                                           */
/*                                                                           */
/*    Based on SCIP --- Solving Constraint Integer Programs                  */
/*       SCIP is distributed under the terms of the SCIP Academic Licence,   */
/*       see file COPYING in the SCIP distribution.                          */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**@file   min2dcube.c
 * @brief  provides functions to minimize a cubic polynomial in up to two variables over a box
 * @author Andreas Schmitt
 *
 * compile with flags: -lgsl
*/

/*--+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0----+----1----+----2*/

#include "min2dcube.h"
#include <math.h>
#include <assert.h>
#include <stdio.h>
#include <gsl/gsl_poly.h>

/* compute the value of the quadratic polynomial in two dimensions
   cxx * x^2 + cxy * x * y + cyy * y^2 + cx * x + cy * y */
static
SCIP_Real f_quad(
   SCIP_Real             x,
   SCIP_Real             y,
   SCIP_Real             cxx,
   SCIP_Real             cxy,
   SCIP_Real             cyy,
   SCIP_Real             cx,
   SCIP_Real             cy
   )
{
   return cxx * pow(x, 2.0) + cxy * x * y + cyy * pow(y, 2.0)
            + cx * x + cy * y;
}

/* compute the value of the cubic polynomial in two dimensions
   cxxx * x^3 + cxxy * x^2 * y + cxyy * x * y^2 + cyyy * y^3 + cxx * x^2 + cxy * x * y + cyy * y^2 + cx * x + cy * y */
static
SCIP_Real f_cube(
   SCIP_Real             x,
   SCIP_Real             y,
   SCIP_Real             cxxx,
   SCIP_Real             cxxy,
   SCIP_Real             cxyy,
   SCIP_Real             cyyy,
   SCIP_Real             cxx,
   SCIP_Real             cxy,
   SCIP_Real             cyy,
   SCIP_Real             cx,
   SCIP_Real             cy
   )
{
   return cxxx * pow(x, 3.0) + cxxy * pow(x, 2.0) * y + cxyy * x * pow(y, 2.0)
            + cyyy * pow(y, 3.0) + cxx * pow(x, 2.0) + cxy * x * y + cyy * pow(y, 2.0)
            + cx * x + cy * y;
}

/* save the coefficients of a one dimensional quartic polynomial into the vector cz: g(z) = cz[0] * z^0 + ... + cz[4] *
   z^4. The real roots of g(z) corrspond to the y components of stationary points to the cubic polynomial in two dimensions
   cxxx * x^3 + cxxy * x^2 * y + cxyy * x * y^2 + cyyy * y^3 + cxx * x^2 + cxy * x * y + cyy * y^2 + cx * x + cy * y */
static
void getCoefStatPointPolynomialInY(
   SCIP_Real             cxxx,
   SCIP_Real             cxxy,
   SCIP_Real             cxyy,
   SCIP_Real             cyyy,
   SCIP_Real             cxx,
   SCIP_Real             cxy,
   SCIP_Real             cyy,
   SCIP_Real             cx,
   SCIP_Real             cy,
   SCIP_Real*            cz
   )
{
   cz[0] = (double) pow(cx * cxxy, 2.0) - 2.0*cx*cxx*cxxy*cxy - 6.0*cx*cxxx*cxxy*cy + 3.0*cx*cxxx*pow(cxy, 2.0) + 4.0*pow(cxx, 2.0)*cxxy*cy - 6.0*cxx*cxxx*cxy*cy + 9.0*pow(cxxx*cy, 2.0);
   cz[1] = (double)-4.0*cx*cxx*cxxy*cxyy - 12.0*cx*cxxx*cxxy*cyy + 12.0*cx*cxxx*cxy*cxyy + 8.0*pow(cxx, 2.0)*cxxy*cyy - 12.0*cxx*cxxx*cxy*cyy - 12.0*cxx*cxxx*cxyy*cy + 8.0*cxx*pow(cxxy,2.0)*cy - 2.0*cxx*cxxy*pow(cxy, 2.0) + 36.0*pow(cxxx, 2.0)*cy*cyy - 12.0*cxxx*cxxy*cxy*cy + 3.0*cxxx*pow(cxy, 3.0);
   cz[2] = (double) -18.0*cx*cxxx*cxxy*cyyy + 12.0*cx*cxxx*pow(cxyy, 2.0) - 2.0*cx*pow(cxxy,2.0)*cxyy + 12.0*pow(cxx, 2.0)*cxxy*cyyy - 18.0*cxx*cxxx*cxy*cyyy - 24.0*cxx*cxxx*cxyy*cyy + 16.0*cxx*pow(cxxy,2.0)*cyy - 6.0*cxx*cxxy*cxy*cxyy + 54.0*pow(cxxx, 2.0)*cy*cyyy + 36.0*pow(cxxx*cyy, 2.0) - 24.0*cxxx*cxxy*cxy*cyy - 18.0*cxxx*cxxy*cxyy*cy + 15.0*cxxx*pow(cxy,2.0)*cxyy + 4.0*pow(cxxy,3.0)*cy - pow(cxxy*cxy,2.0);
   cz[3] = (double) -36.0*cxx*cxxx*cxyy*cyyy + 24.0*cxx*pow(cxxy,2.0)*cyyy - 4.0*cxx*cxxy*pow(cxyy,2.0) + 108.0*pow(cxxx,2.0)*cyy*cyyy - 36.0*cxxx*cxxy*cxy*cyyy - 36.0*cxxx*cxxy*cxyy*cyy + 24.0*cxxx*cxy*pow(cxyy,2.0) + 8.0*pow(cxxy,3.0)*cyy - 4.0*pow(cxxy,2.0)*cxy*cxyy;
   cz[4] = (double) 81.0*pow(cxxx*cyyy, 2.0) - 54.0*cxxx*cxxy*cxyy*cyyy + 12.0*cxxx*pow(cxyy, 3.0) + 12.0*pow(cxxy, 3.0)*cyyy - 3.0*pow(cxxy*cxyy,2.0);
}

/* compute the minimum of the quadratic polynomial cxx * x^2 + cx * x over the intervall [lbx, ubx]. For optx != NULL
   save an optimal point */
SCIP_Real calcMin1dQuad(
   SCIP_Real             lbx,
   SCIP_Real             ubx,
   SCIP_Real             cxx,
   SCIP_Real             cx,
   SCIP_Real*            optx
   )
{

   SCIP_Real result;
   SCIP_Real x = 0;
   SCIP_Real optval;

   assert(lbx <= ubx);

   // get objective value on boundary
   result = cxx * pow(lbx, 2.0) + cx * lbx;
   if( optx != NULL )
   {
      *optx = lbx;
   }
   optval = cxx * pow(ubx, 2.0) + cx * ubx;
   if( optval < result )
   {
      result = optval;
      if( optx != NULL )
      {
         *optx = ubx;
      }
   }

   // test whether we have a quadratic and test its stationary point
   if( cxx != 0.0 )
   {
      x = - cx / ( 2.0 * cxx );
      optval = cxx * pow(x, 2.0) + cx * x;
      if( lbx <= x && x <= ubx && optval < result )
      {
         result = optval;
         if( optx != NULL )
         {
            *optx = x;
         }
      }
   }

   return result;
}

/* compute the maximum of the quadratic polynomial cxx * x^2 + cx * x over the intervall [lbx, ubx]. For optx != NULL
   save an optimal point */
SCIP_Real calcMax1dQuad(
   SCIP_Real             lbx,
   SCIP_Real             ubx,
   SCIP_Real             cxx,
   SCIP_Real             cx,
   SCIP_Real*            optx
   )
{
   return -calcMin1dQuad(lbx, ubx, -cxx, -cx, optx);
}

/* compute the minimum of the cubic polynomial cxxx * x^3 + cxx * x^2 + cx * x over the intervall [lbx, ubx]. For optx
   != NULL save an optimal point */
SCIP_Real calcMin1dCube(
   SCIP_Real             lbx,
   SCIP_Real             ubx,
   SCIP_Real             cxxx,
   SCIP_Real             cxx,
   SCIP_Real             cx,
   SCIP_Real*            optx
   )
{
   SCIP_Real result;
   SCIP_Real x = 0;
   SCIP_Real optval;

   assert(lbx <= ubx);

   // get objective value on boundary
   result = cxxx * pow(lbx, 3.0) + cxx * pow(lbx, 2.0) + cx * lbx;
   if( optx != NULL )
   {
      *optx = lbx;
   }
   optval = cxxx * pow(ubx, 3.0) + cxx * pow(ubx, 2.0) + cx * ubx;
   if( optval < result )
   {
      result = optval;
      if( optx != NULL )
      {
         *optx = ubx;
      }
   }
   // if the function is concave we are ok
   if( MAX(3.0 * cxxx * lbx, 3.0 * cxxx * ubx) + cxx <= 0.0 )
   {
      return result;
   }

   // check if we even have a cubic and whether stationary points exist
   if( cxxx != 0.0 && pow(cxx, 2.0) - 3.0 * cxxx * cx >= 0)
   {
      x = (-cxx + pow(pow(cxx, 2.0) - 3.0 * cxxx * cx, 0.5)) / ( 3.0 * cxxx );
      optval = cxxx * pow(x, 3.0) + cxx * pow(x, 2.0) + cx * x;
      if( lbx <= x && x <= ubx && optval < result )
      {
         result = optval;
         if( optx != NULL )
         {
            *optx = x;
         }
      }

      x = (-cxx - pow(pow(cxx, 2.0) - 3.0 * cxxx * cx, 0.5)) / ( 3.0 * cxxx );
      optval = cxxx * pow(x, 3.0) + cxx * pow(x, 2.0) + cx * x;
      if( lbx <= x && x <= ubx && optval < result )
      {
         result = optval;
         if( optx != NULL )
         {
            *optx = x;
         }
      }
   }
   // otherwise test whether we have a quadratic and test its stationary point
   else if( cxxx == 0.0 && cxx != 0.0 )
   {
      x = - cx / ( 2.0 * cxx );
      optval = cxxx * pow(x, 3.0) + cxx * pow(x, 2.0) + cx * x;
      if( lbx <= x && x <= ubx && optval < result )
      {
         result = optval;
         if( optx != NULL )
         {
            *optx = x;
         }
      }
   }

   return result;
}

/* compute the maximum of the cubic polynomial cxxx * x^3 + cxx * x^2 + cx * x over the intervall [lbx, ubx]. For optx
   != NULL save an optimal point */
SCIP_Real calcMax1dCube(
   SCIP_Real             lbx,
   SCIP_Real             ubx,
   SCIP_Real             cxxx,
   SCIP_Real             cxx,
   SCIP_Real             cx,
   SCIP_Real*            optx
   )
{
   return -calcMin1dCube(lbx, ubx, -cxxx, -cxx, -cx, optx);
}


/* compute the minimum of the quadratic polynomial in two dimensions cxx * x^2 + cxy * x * y + cyy * y^2 + cx * x + cx *
   y over the box [lbx, ubx]x[lby, uby]. For optx and opty != NULL save an optimal point */
SCIP_Real calcMin2dQuad(
   SCIP_Real             lbx,
   SCIP_Real             ubx,
   SCIP_Real             lby,
   SCIP_Real             uby,
   SCIP_Real             cxx,
   SCIP_Real             cxy,
   SCIP_Real             cyy,
   SCIP_Real             cx,
   SCIP_Real             cy,
   SCIP_Real*            optx,
   SCIP_Real*            opty
   )
{
   SCIP_Real statx;
   SCIP_Real staty;
   SCIP_Real result;
   SCIP_Real xx;
   SCIP_Real yy;
   SCIP_Real optval;

   assert( lbx <= ubx );
   assert( lby <= uby );

   // calc minima on bounday points
   result = calcMin1dQuad(lbx, ubx, cxx, cx + lby * cxy, &xx) + cyy * pow(lby, 2.0) + cy * lby;
   if( optx != NULL && opty != NULL )
   {
      *optx = xx;
      *opty = lby;
   }

   optval = calcMin1dQuad(lbx, ubx, cxx, cx + uby * cxy, &xx) + cyy * pow(uby, 2.0) + cy * uby;
   if( optval < result )
   {
      result = optval;
      if( optx != NULL && opty != NULL )
      {
         *optx = xx;
         *opty = uby;
      }
   }

   optval = calcMin1dQuad(lby, uby, cyy, cy + lbx * cxy, &yy) + cxx * pow(lbx, 2.0) + cx * lbx;
   if( optval < result )
   {
      result = optval;
      if( optx != NULL && opty != NULL )
      {
         *optx = lbx;
         *opty = yy;
      }
   }

   optval = calcMin1dQuad(lby, uby, cyy, cy + ubx * cxy, &yy) + cxx * pow(ubx, 2.0) + cx * ubx;
   if( optval < result )
   {
      result = optval;
      if( optx != NULL && opty != NULL )
      {
         *optx = ubx;
         *opty = yy;
      }
   }
   // try out the only possible stat point
   if( 4.0 * cxx * cyy - pow(cxy, 2.0) > 0.0 )
   {
      statx = (cxy * cy - 2 * cx * cyy ) / ( 4.0 * cxx * cyy - pow(cxy, 2.0) );
      staty = (cxy * cx - 2 * cy * cyy ) / ( 4.0 * cxx * cyy - pow(cxy, 2.0) );
      optval = f_quad(statx, staty, cxx, cxy, cyy, cx, cy);
      if( lbx <= 0.0 && ubx >= 0.0 && lby <= 0.0 && uby >= 0.0 && optval < result )
      {
         result = optval;
         if( optx != NULL && opty != NULL )
         {
            *optx = statx;
            *opty = staty;
         }
      }
   }

   return result;
}

/* compute the maximum of the quadratic polynomial in two dimensions cxx * x^2 + cxy * x * y + cyy * y^2 + cx * x + cy *
   y over the box [lbx, ubx]x[lby, uby]. For optx and opty != NULL save an optimal point */
SCIP_Real calcMax2dQuad(
   SCIP_Real             lbx,
   SCIP_Real             ubx,
   SCIP_Real             lby,
   SCIP_Real             uby,
   SCIP_Real             cxx,
   SCIP_Real             cxy,
   SCIP_Real             cyy,
   SCIP_Real             cx,
   SCIP_Real             cy,
   SCIP_Real*            optx,
   SCIP_Real*            opty
   )
{
   return -calcMin2dQuad(lbx, ubx, lby, uby, -cxx, -cxy, -cyy, -cx, -cy, optx, opty);
}

/* compute the minimum of the cubic polynomial in two dimensions
   cxxx * x^3 + cxxy * x^2 * y + cxyy * x * y^2 + cyyy * y^3 + cxx * x^2 + cxy * x * y + cyy * y^2 + cx * x + cy * y
   over the box [lbx, ubx]x[lby, uby]. For optx and opty != NULL save an optimal point */
SCIP_Real calcMin2dCube(
   SCIP_Real             lbx,
   SCIP_Real             ubx,
   SCIP_Real             lby,
   SCIP_Real             uby,
   SCIP_Real             cxxx,
   SCIP_Real             cxxy,
   SCIP_Real             cxyy,
   SCIP_Real             cyyy,
   SCIP_Real             cxx,
   SCIP_Real             cxy,
   SCIP_Real             cyy,
   SCIP_Real             cx,
   SCIP_Real             cy,
   SCIP_Real*            optx,
   SCIP_Real*            opty,
   SCIP_Bool*            success
   )
{
   SCIP_Real result;
   double cz[5]; // coefficients of quadric polynomials used in the derivation of the possible stationary points
   gsl_poly_complex_workspace* w;
   double y[8];
   double x[8];
   double xx;
   double yy;
   SCIP_Real optval;
   int failure;
   SCIP_Real locOptx;
   SCIP_Real locOpty;

   assert( success != NULL );

   *success = FALSE;

   // calc minima on bounday points
   result = calcMin1dCube(lbx, ubx, cxxx, cxx + lby * cxxy, cx + pow(lby, 2.0) * cxyy + lby * cxy, &xx) + cyyy * pow(lby, 3.0) + cyy * pow(lby, 2.0) + cy * lby;
   locOptx = xx;
   locOpty = lby;


   optval = calcMin1dCube(lbx, ubx, cxxx, cxx + uby * cxxy, cx + pow(uby, 2.0) * cxyy + uby * cxy, &xx) + cyyy * pow(uby, 3.0) + cyy * pow(uby, 2.0) + cy * uby;
   if( optval < result )
   {
      result = optval;
      locOptx = xx;
      locOpty = uby;
   }


   optval = calcMin1dCube(lby, uby, cyyy, cyy + lbx * cxyy, cy + pow(lbx, 2.0) * cxxy + lbx * cxy, &yy) + cxxx * pow(lbx, 3.0) + cxx * pow(lbx, 2.0) + cx * lbx;
   if( optval < result )
   {
      result = optval;
      locOptx = lbx;
      locOpty = yy;
   }

   optval = calcMin1dCube(lby, uby, cyyy, cyy + ubx * cxyy, cy + pow(ubx, 2.0) * cxxy + ubx * cxy, &yy) + cxxx * pow(ubx, 3.0) + cxx * pow(ubx, 2.0) + cx * ubx;
   if( optval < result )
   {
      result = optval;
      locOptx = ubx;
      locOpty = yy;
   }


   // get possible stationary points by obtaining the roots of two quartic polynomials

   getCoefStatPointPolynomialInY(cxxx, cxxy, cxyy, cyyy, cxx, cxy, cyy, cx, cy, cz);

   w = gsl_poly_complex_workspace_alloc(5uL);
   failure = gsl_poly_complex_solve (cz, 5uL, w, y);
   if( failure )
   {
      *success = FALSE;
      return -1.0;
   }
   gsl_poly_complex_workspace_free(w);

   getCoefStatPointPolynomialInY(cyyy, cxyy, cxxy, cxxx, cyy, cxy, cxx, cy, cx, cz);

   w = gsl_poly_complex_workspace_alloc(5uL);
   failure = gsl_poly_complex_solve(cz, 5uL, w, x);
   if( failure )
   {
      *success = FALSE;
      return -1.0;
   }
   gsl_poly_complex_workspace_free(w);

   // try each combination of points which are not complex and belong to the feasible box
   for( int i = 0; i < 4; i++ )
   {
      xx =  x[2*i];

      if( !(x[2*i+1] == 0.0 && lbx <= xx && xx <= ubx) )
         continue;

      for( int j = 0; j < 4; j++ )
      {
         yy =  y[2*i];

         if( !(y[2*i+1] == 0.0 && lby <= yy && yy <= uby) )
            continue;

         optval = f_cube(xx, yy, cxxx, cxxy, cxyy, cyyy, cxx, cxy, cyy, cx, cy);
         if( optval < result )
         {
            result = optval;
            locOptx = xx;
            locOpty = yy;
         }
      }
   }
   *success = TRUE;

   if( optx != NULL )
      *optx = locOptx;
   if( opty != NULL )
      *opty = locOpty;

   return result;
}
