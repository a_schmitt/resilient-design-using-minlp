/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*    This file is part of the program kresilience                           */
/*                                                                           */
/*    Copyright (C) 2020-2021 Andreas Schmitt, Marc Pfetsch                  */
/*                                                                           */
/*                                                                           */
/*    Based on SCIP --- Solving Constraint Integer Programs                  */
/*       SCIP is distributed under the terms of the SCIP Academic Licence,   */
/*       see file COPYING in the SCIP distribution.                          */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**@file   message_prefix.h
 * @brief  prefix message handler
 * @author Andreas Schmitt
 */

/*---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0----+----1----+----2*/

#ifndef __SCIP_MESSAGE_PREFIX_H__
#define __SCIP_MESSAGE_PREFIX_H__

#include "scip/type_message.h"
#include "scip/scip.h"

#ifdef __cplusplus
extern "C" {
#endif


SCIP_EXPORT
/** Create Prefix message handler directly in scip->messagehdlr. Frees previous message handler. */
SCIP_RETCODE SCIPsetMessagehdlrPrefix(
   SCIP*                 scip,               /**< SCIP data structure */
   const char*           prefix              /**< prefix which is put before each new line */
   );

#ifdef __cplusplus
}
#endif

#endif
