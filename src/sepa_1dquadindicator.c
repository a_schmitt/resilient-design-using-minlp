/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*    This file is part of the program kresilience                           */
/*                                                                           */
/*    Copyright (C) 2020-2021 Andreas Schmitt, Marc Pfetsch                  */
/*                                                                           */
/*                                                                           */
/*    Based on SCIP --- Solving Constraint Integer Programs                  */
/*       SCIP is distributed under the terms of the SCIP Academic Licence,   */
/*       see file COPYING in the SCIP distribution.                          */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**@file   sepa_1dquadindicator.c
 * @brief  assume two quadratics to bound a variable from above and below, including a indicator constraint
 * @author Andreas Schmitt
 *
 * This separator assumes the set structure
 *     XXlb * xx + Xlb * x + Clb * z <= y <= XXub * xx + Xub * x + Cub * z
 *     x * (1 - z) = 0
 *     z in {0, 1}
 * and separates perspective cuts.
 */

/*---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0----+----1----+----2*/

#include <assert.h>

#include "sepa_1dquadindicator.h"


#define SEPA_NAME              "quadindicator"
#define SEPA_DESC              "quadratic indicator separator"
#define SEPA_PRIORITY               150
#define SEPA_FREQ                    -1
#define SEPA_MAXBOUNDDIST           1.0
#define SEPA_USESSUBSCIP          FALSE      /**< does the separator use a secondary SCIP instance? */
#define SEPA_DELAY                FALSE      /**< should separation method be delayed, if other separators found cuts? */


/*
 * Data structures
 */

/** separator data */
struct SCIP_SepaData
{
   int                   neqs;               /**< number of inequalities/sets we consider */
   int                   sizeeqs;            /**< allocated size of all arrays in this struct */
   SCIP_VAR**            varsX;              /**< x variables */
   SCIP_VAR**            varsY;              /**< y variables */
   SCIP_VAR**            varsZ;              /**< indicator variables */
   SCIP_Real*            coefsXXlb;          /**< coefficient of lower bound functions */
   SCIP_Real*            coefsXlb;           /**< coefficient of lower bound functions */
   SCIP_Real*            coefsClb;           /**< coefficient of lower bound functions */
   SCIP_Real*            coefsXXub;          /**< coefficient of upper bound functions */
   SCIP_Real*            coefsXub;           /**< coefficient of upper bound functions */
   SCIP_Real*            coefsCub;           /**< coefficient of upper bound functions */
};

/** adds variables and polynom coefficients to separate to the separator  */
SCIP_RETCODE SCIPSepaQuadIndicatorRegister(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_VAR*             varX,               /**< variable x */
   SCIP_VAR*             varY,               /**< variable x */
   SCIP_VAR*             varZ,               /**< indicator variable*/
   SCIP_Real             coefXXlb,           /**< coefficient of lower bound function */
   SCIP_Real             coefXlb,            /**< coefficient of lower bound function */
   SCIP_Real             coefClb,            /**< coefficient of lower bound function */
   SCIP_Real             coefXXub,           /**< coefficient of upper bound function */
   SCIP_Real             coefXub,            /**< coefficient of upper bound function */
   SCIP_Real             coefCub             /**< coefficient of upper bound function */
   )
{
   SCIP_SEPA* sepa;
   SCIP_SEPADATA* sepadata;

   assert(scip != NULL);

   sepa = SCIPfindSepa(scip, SEPA_NAME);

   if (sepa == NULL)
   {
      printf("no sepa found\n");
      return SCIP_ERROR;
   }

   sepadata = SCIPsepaGetData(sepa);
   if (sepadata == NULL)
   {
      printf("no sepadata found\n");
      return SCIP_ERROR;
   }
   /* realloc memory if necessary */
   if (sepadata->sizeeqs == sepadata->neqs+1 )
   {
      sepadata->sizeeqs = SCIPcalcMemGrowSize(scip, sepadata->sizeeqs+1);
      SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &sepadata->varsX, sepadata->neqs+1, sepadata->sizeeqs) );
      SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &sepadata->varsY, sepadata->neqs+1, sepadata->sizeeqs) );
      SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &sepadata->varsZ, sepadata->neqs+1, sepadata->sizeeqs) );
      SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &sepadata->coefsXXlb, sepadata->neqs+1, sepadata->sizeeqs) );
      SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &sepadata->coefsXlb, sepadata->neqs+1, sepadata->sizeeqs) );
      SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &sepadata->coefsClb, sepadata->neqs+1, sepadata->sizeeqs) );
      SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &sepadata->coefsXXub, sepadata->neqs+1, sepadata->sizeeqs) );
      SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &sepadata->coefsXub, sepadata->neqs+1, sepadata->sizeeqs) );
      SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &sepadata->coefsCub, sepadata->neqs+1, sepadata->sizeeqs) );
   }

   sepadata->varsX[sepadata->neqs] = varX;
   sepadata->varsY[sepadata->neqs] = varY;
   sepadata->varsZ[sepadata->neqs] = varZ;
   sepadata->coefsXXub[sepadata->neqs] = coefXXub;
   sepadata->coefsXub[sepadata->neqs] = coefXub;
   sepadata->coefsCub[sepadata->neqs] = coefCub;
   sepadata->coefsXXlb[sepadata->neqs] = coefXXlb;
   sepadata->coefsXlb[sepadata->neqs] = coefXlb;
   sepadata->coefsClb[sepadata->neqs] = coefClb;
   sepadata->neqs++;

   return SCIP_OKAY;
}

/** check wheter we can separate set f(x) >= y for z = 1 but x = y = 0 for z = 0 */
static
SCIP_RETCODE separateLbCut(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_SEPA*            sepa,
   SCIP_SOL*             sol,                /**< primal solution that should be separated, or NULL for LP solution */
   SCIP_VAR*             varX,
   SCIP_VAR*             varY,
   SCIP_VAR*             varZ,
   SCIP_Real             coefXX,
   SCIP_Real             coefX,
   SCIP_Real             coefC,
   SCIP_RESULT*          result              /**< pointer to store the result of the separation call */
   )
{
   SCIP_Real cutX;
   SCIP_Real cutZ;
   SCIP_Real valX;
   SCIP_Real valZ;
   SCIP_Real lbX;
   SCIP_Real ubX;
   SCIP_Bool local;
   SCIP_ROW* row;
   SCIP_Bool infeasible;

   assert(scip != NULL);
   assert( varX != NULL );
   assert( varY != NULL );
   assert( varZ != NULL );

   valZ = SCIPgetSolVal(scip, sol, varZ);

   if( SCIPisFeasZero(scip, valZ) )
   {
      return SCIP_OKAY;
   }

   valX = SCIPgetSolVal(scip, sol, varX);

   lbX = SCIPvarGetLbLocal(varX);
   ubX = SCIPvarGetUbLocal(varX);

   if( SCIPisFeasEQ(scip, lbX, ubX) )
   {
      return SCIP_OKAY;
   }

   if( !SCIPisFeasEQ(scip, valZ, 1.0) )
   {
      valX = valX / valZ;
   }

   if( coefXX >= 0 )
   {
      cutX = 2 * coefXX * valX + coefX;
      cutZ = coefXX * pow(valX, 2.0) + coefX * valX + coefC - cutX * valX;
      local = FALSE;
   }
   else
   {
      cutX = (coefXX * pow(ubX, 2.0) + coefX * ubX - coefXX * pow(lbX, 2.0) - coefX * lbX) / (ubX - lbX);
      cutZ = coefXX * pow(ubX, 2.0) - coefX * ubX - cutX * lbX;
      local = TRUE;
   }

   SCIP_CALL( SCIPcreateEmptyRowSepa(scip, &row, sepa, "lbcutSepa", 0.0, SCIPinfinity(scip), local, FALSE, TRUE) );
   SCIP_CALL( SCIPcacheRowExtensions(scip, row) );

   SCIP_CALL( SCIPaddVarToRow(scip, row, varY, 1.0) );

   SCIP_CALL( SCIPaddVarToRow(scip, row, varX, -cutX) );
   SCIP_CALL( SCIPaddVarToRow(scip, row, varZ, -cutZ) );

   SCIP_CALL( SCIPflushRowExtensions(scip, row) );

   if( SCIPisCutEfficacious(scip, sol, row) )
   {
#ifdef SCIP_DEBUG
   SCIP_CALL( SCIPprintRow(scip, row, NULL) );
#endif
      SCIP_CALL( SCIPaddRow(scip, row, FALSE, &infeasible) );
      if( infeasible )
         *result = SCIP_CUTOFF;
      else
         *result = SCIP_SEPARATED;
   }

   SCIP_CALL( SCIPreleaseRow(scip, &row) );

   return SCIP_OKAY;
}

/** check wheter we can separate f(x) <= y for z = 1 but x = y = 0 for z = 0 */
static
SCIP_RETCODE separateUbCut(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_SEPA*            sepa,
   SCIP_SOL*             sol,                /**< primal solution that should be separated, or NULL for LP solution */
   SCIP_VAR*             varX,
   SCIP_VAR*             varY,
   SCIP_VAR*             varZ,
   SCIP_Real             coefXX,
   SCIP_Real             coefX,
   SCIP_Real             coefC,
   SCIP_RESULT*          result              /**< pointer to store the result of the separation call */
   )
{
   SCIP_Real cutX;
   SCIP_Real cutZ;
   SCIP_Real valX;
   SCIP_Real valZ;
   SCIP_Real lbX;
   SCIP_Real ubX;
   SCIP_Bool local;
   SCIP_ROW* row;
   SCIP_Bool infeasible;

   assert(scip != NULL);
   assert( varX != NULL );
   assert( varY != NULL );
   assert( varZ != NULL );

   valZ = SCIPgetSolVal(scip, sol, varZ);

   if( SCIPisFeasZero(scip, valZ) )
   {
      return SCIP_OKAY;
   }

   valX = SCIPgetSolVal(scip, sol, varX);

   lbX = SCIPvarGetLbLocal(varX);
   ubX = SCIPvarGetUbLocal(varX);

   if( SCIPisFeasEQ(scip, lbX, ubX) )
   {
      return SCIP_OKAY;
   }

   if( !SCIPisFeasEQ(scip, valZ, 1.0) )
   {
      valX = valX / valZ;
   }

   if( coefXX <= 0 )
   {
      cutX = 2 * coefXX * valX + coefX;
      cutZ = coefXX * pow(valX, 2.0) + coefX * valX + coefC - cutX * valX;
      local = FALSE;
   }
   else
   {
      cutX = (coefXX * pow(ubX, 2.0) + coefX * ubX - coefXX * pow(lbX, 2.0) - coefX * lbX) / (ubX - lbX);
      cutZ = coefXX * pow(ubX, 2.0) - coefX * ubX - cutX * lbX;
      local = TRUE;
   }

   SCIP_CALL( SCIPcreateEmptyRowSepa(scip, &row, sepa, "ubcutSepa", 0.0, SCIPinfinity(scip), local, FALSE, TRUE) );
   SCIP_CALL( SCIPcacheRowExtensions(scip, row) );

   SCIP_CALL( SCIPaddVarToRow(scip, row, varY, -1.0) );

   SCIP_CALL( SCIPaddVarToRow(scip, row, varX, cutX) );
   SCIP_CALL( SCIPaddVarToRow(scip, row, varZ, cutZ) );

   SCIP_CALL( SCIPflushRowExtensions(scip, row) );

   if( SCIPisCutEfficacious(scip, sol, row) )
   {
#ifdef SCIP_DEBUG
   SCIP_CALL( SCIPprintRow(scip, row, NULL) );
#endif
      SCIP_CALL( SCIPaddRow(scip, row, FALSE, &infeasible) );
      if( infeasible )
         *result = SCIP_CUTOFF;
      else
         *result = SCIP_SEPARATED;
   }

   SCIP_CALL( SCIPreleaseRow(scip, &row) );

   return SCIP_OKAY;
}


/** searches though the saved inequalities and checks whether we can fidn a violated cut */
static
SCIP_RETCODE separateCuts(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_SEPA*            sepa,               /**< the cut separator itself */
   SCIP_SOL*             sol,                /**< primal solution that should be separated, or NULL for LP solution */
   SCIP_RESULT*          result              /**< pointer to store the result of the separation call */
   )
{  /*lint --e{715}*/
   SCIP_SEPADATA* sepadata;
   int i;

   assert(scip != NULL);
   assert(sepa != NULL);
   assert(*result == SCIP_DIDNOTRUN);

   /* get separator data */
   sepadata = SCIPsepaGetData(sepa);
   assert(sepadata != NULL);

   *result = SCIP_DIDNOTFIND;
   for( i = 0; i < sepadata->neqs; ++i )
   {
      SCIP_CALL( separateUbCut(scip, sepa, sol, sepadata->varsX[i], sepadata->varsY[i], sepadata->varsZ[i], sepadata->coefsXXub[i], sepadata->coefsXub[i], sepadata->coefsCub[i], result) );
      if( *result == SCIP_CUTOFF )
         return SCIP_OKAY;

      SCIP_CALL( separateLbCut(scip, sepa, sol, sepadata->varsX[i], sepadata->varsY[i], sepadata->varsZ[i], sepadata->coefsXXlb[i], sepadata->coefsXlb[i], sepadata->coefsClb[i], result) );
      if( *result == SCIP_CUTOFF )
         return SCIP_OKAY;

   }

   return SCIP_OKAY;
}



/*
 * Callback methods of separator
 */

/** destructor of separator to free user data (called when SCIP is exiting) */
static
SCIP_DECL_SEPAFREE(sepaFreeVexiter)
{  /*lint --e{715}*/

   SCIP_SEPADATA* sepadata;

   sepadata = SCIPsepaGetData(sepa);
   SCIPfreeBlockMemoryArray(scip, &sepadata->varsX, sepadata->sizeeqs);
   SCIPfreeBlockMemoryArray(scip, &sepadata->varsY, sepadata->sizeeqs);
   SCIPfreeBlockMemoryArray(scip, &sepadata->varsZ, sepadata->sizeeqs);
   SCIPfreeBlockMemoryArray(scip, &sepadata->coefsXXlb, sepadata->sizeeqs);
   SCIPfreeBlockMemoryArray(scip, &sepadata->coefsXlb, sepadata->sizeeqs);
   SCIPfreeBlockMemoryArray(scip, &sepadata->coefsClb, sepadata->sizeeqs);
   SCIPfreeBlockMemoryArray(scip, &sepadata->coefsXXub, sepadata->sizeeqs);
   SCIPfreeBlockMemoryArray(scip, &sepadata->coefsXub, sepadata->sizeeqs);
   SCIPfreeBlockMemoryArray(scip, &sepadata->coefsCub, sepadata->sizeeqs);

   SCIPfreeBlockMemory(scip, &sepadata);

   return SCIP_OKAY;
}

/** LP solution separation method of separator */
static
SCIP_DECL_SEPAEXECLP(sepaExeclpVexiter)
{  /*lint --e{715}*/
   *result = SCIP_DIDNOTRUN;

   /* only call separator, if we are not close to terminating */
   if( SCIPisStopped(scip) )
      return SCIP_OKAY;

   /* separate cuts on the LP solution */
   SCIP_CALL( separateCuts(scip, sepa, NULL, result) );

   return SCIP_OKAY;
}


/** arbitrary primal solution separation method of separator */
static
SCIP_DECL_SEPAEXECSOL(sepaExecsolVexiter)
{  /*lint --e{715}*/
   *result = SCIP_DIDNOTRUN;

   /* separate cuts on the given primal solution */
   SCIP_CALL( separateCuts(scip, sepa, sol, result) );

   return SCIP_OKAY;
}


/*
 * separator specific interface methods
 */

/** creates the separator and includes it in SCIP */
SCIP_RETCODE SCIPincludeSepaQuadIndicator(
   SCIP*                 scip                /**< SCIP data structure */
   )
{
   SCIP_SEPADATA* sepadata;
   SCIP_SEPA* sepa;

   /* create separator data */
   sepadata = NULL;

   SCIP_CALL( SCIPallocBlockMemory(scip, &sepadata) );

   assert(sepadata != NULL);

   sepadata->sizeeqs = 16;
   sepadata->neqs = 0;

   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &sepadata->varsX, sepadata->sizeeqs) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &sepadata->varsY, sepadata->sizeeqs) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &sepadata->varsZ, sepadata->sizeeqs) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &sepadata->coefsXXlb, sepadata->sizeeqs) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &sepadata->coefsXlb, sepadata->sizeeqs) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &sepadata->coefsClb, sepadata->sizeeqs) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &sepadata->coefsXXub, sepadata->sizeeqs) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &sepadata->coefsXub, sepadata->sizeeqs) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &sepadata->coefsCub, sepadata->sizeeqs) );

   sepa = NULL;

   /* include separator */
   SCIP_CALL( SCIPincludeSepaBasic(scip, &sepa, SEPA_NAME, SEPA_DESC, SEPA_PRIORITY, SEPA_FREQ, SEPA_MAXBOUNDDIST,
         SEPA_USESSUBSCIP, SEPA_DELAY,
         sepaExeclpVexiter, sepaExecsolVexiter,
         sepadata) );

   assert(sepa != NULL);

   /* set non fundamental callbacks via setter functions */
   SCIP_CALL( SCIPsetSepaFree(scip, sepa, sepaFreeVexiter) );

   return SCIP_OKAY;
}
