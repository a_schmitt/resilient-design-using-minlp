/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*    This file is part of the program kresilience                           */
/*                                                                           */
/*    Copyright (C) 2020-2021 Andreas Schmitt, Marc Pfetsch                  */
/*                                                                           */
/*                                                                           */
/*    Based on SCIP --- Solving Constraint Integer Programs                  */
/*       SCIP is distributed under the terms of the SCIP Academic Licence,   */
/*       see file COPYING in the SCIP distribution.                          */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**@file   reader_int.c
 * @brief  interdiction file reader
 * @author Andreas Schmitt
 *
 * The int format specifies interdiction problems.
 */

/*---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0----+----1----+----2*/

#define _GNU_SOURCE
#include <string.h>
#include <ctype.h>

#include "scip/scip.h"

#include <scip/cons_linear.h>

#include "scip/var.h"

#include "scip/struct_scip.h"

#include "reader_int.h"
#include "cons_kresilient.h"
#include "heur_maxtruss.h"
#include "include_plugins.h"

#define READER_NAME             "intreader"
#define READER_DESC             "file reader for int (interdiction) format"
#define READER_EXTENSION        "int"
#define DEFAULT_USECONSRESILIENT TRUE

/*
 * Local methods for reading/parsing
 */

/*  FailureScenarios: structure to  save failure scenarios*/
typedef struct FailureScenarios{
   int**                 failures;
   int                   nFailures;
   int                   sizeFailures;
   int                   k;
} FailureScenarios;

static
SCIP_RETCODE FailureInit(
   SCIP*                 scip,               /**< pointer to scip */
   FailureScenarios*     failures,
   int                   k
   )
{

   failures->nFailures = 0;
   failures->sizeFailures = 10;
   SCIP_CALL( SCIPallocBufferArray(scip, &failures->failures, failures->sizeFailures) );
   failures->k = k;

   return SCIP_OKAY;
}

static
SCIP_RETCODE FailureAddFailures(
   SCIP*                 scip,               /**< pointer to scip */
   FailureScenarios*     failures,
   int*                  fail
   )
{
   assert( fail!= NULL );
   // realloc memory if necessary
   if (failures->sizeFailures <= failures->nFailures+1 )
   {
      failures->sizeFailures = SCIPcalcMemGrowSize(scip, failures->sizeFailures+1);;
      SCIP_CALL( SCIPreallocBufferArray(scip, &failures->failures, failures->sizeFailures) );
   }
   SCIP_CALL( SCIPduplicateBufferArray(scip, &(failures->failures[failures->nFailures]), fail, failures->k) );
   failures->nFailures++;

   return SCIP_OKAY;
}
static
void FailureExit(
   SCIP*                 scip,               /**< pointer to scip */
   FailureScenarios*     failures
   )
{

   for(int i = 0; i < failures->nFailures; ++i )
   {
      SCIPfreeBufferArray(scip, &(failures->failures[i]));
   }
   SCIPfreeBufferArray(scip, &failures->failures);

}

/*  Code to generate all failure scenarios */
static
SCIP_RETCODE process(
   SCIP*                 scip,               /**< pointer to scip */
   int*                  subset,
   FailureScenarios*     failures
   )
{
   assert(subset != NULL);
   SCIP_CALL( FailureAddFailures(scip, failures, subset) );
   return SCIP_OKAY;
}

static
SCIP_RETCODE processLargerSubsets(
   SCIP*                 scip,
   int*                  set,
   int                   sizeset,
   int*                  subset,
   int                   subsetSize,
   int                   nextIndex,
   int                   k,
   FailureScenarios*     failures
   )
{
   assert(subset != NULL);
    if (subsetSize == k) {
       SCIP_CALL( process(scip, subset, failures) );
    } else {
        for (int j = nextIndex; j < sizeset; j++) {
            subset[subsetSize] = set[j];
            SCIP_CALL( processLargerSubsets(scip, set, sizeset, subset, subsetSize + 1, j + 1, k, failures) );
        }
    }
    return SCIP_OKAY;
}

static
SCIP_RETCODE processSubsets(
   SCIP*                 scip,
   int*                  set,
   int                   sizeset,
   int*                  subset,
   int                   sizesubset,
   FailureScenarios*     failures
   )
{
   SCIP_CALL( processLargerSubsets(scip, set, sizeset, subset, 0, 0, sizesubset, failures) );
   return SCIP_OKAY;
}

/* goes through the constraints of scip_feas and checks whether some interdictable vars_x_feas are together in a packing
constraint. This is return usint the hashmap varsToPacking, which maps each interdictable vars_x_feas to a number, which
represents a packing. Note, that if there is no packing for a variable, we add it as a unique packing of size 1. Also
note, that we only add one packing per constraint.
*/
static
SCIP_RETCODE SCIPidentifyPackingVars(
   SCIP*                 scip,               /**< pointer to scip structure */
   int                   nvars,              /**< number of variables which have to be treated */
   SCIP_VAR**            vars_x_feas,        /**< variables in scip_feas */
   SCIP_Bool*            vars_interdictable, /**< which of the variables are interdictable */
   SCIP*                 scip_feas,          /**< SCIP data structure for the subproblem */
   SCIP_HASHMAP*         varsToPacking,      /**< vars->int hashmap to store for packing for each variable */
   int*                  npackings           /**< number of found packings, i.e., number of unique images in
                                                  varsToPacking */
   )
{
   int i;
   int c;
   SCIP_HASHSET* interdictableVars;
   SCIP_CONS** conss;
   int nconss;
   SCIP_VAR** varsPack;
   SCIP_CONSHDLR* conshdlr;

   assert(scip != NULL);

   *npackings = 0;

   /* Create hashmap for the interdictable variables. */
   SCIP_CALL( SCIPhashsetCreate(&interdictableVars, SCIPblkmem(scip), nvars) );

   for( i = 0; i < nvars; ++i )
   {
      if( vars_interdictable[i] )
      {
         SCIP_CALL( SCIPhashsetInsert(interdictableVars, SCIPblkmem(scip), (void*) vars_x_feas[i]) );
      }
   }

   nconss = SCIPgetNConss(scip_feas);
   conss = SCIPgetConss(scip_feas);
   assert( conss != NULL );

   /* loop through all constraints to identify setpacking constraints */
   for( c = 0; c < nconss && SCIPhashsetGetNElements(interdictableVars) > 1; ++c )
   {
      const char* conshdlrname;
      SCIP_CONS* cons;
      SCIP_CONS* cons_upg = NULL;

      /* get constraint */
      cons = conss[c];
      assert( cons != NULL );

      /* get constraint handler */
      conshdlr = SCIPconsGetHdlr(cons);
      assert( conshdlr != NULL );

      conshdlrname = SCIPconshdlrGetName(conshdlr);
      assert( conshdlrname != NULL );
      if ( strcmp(conshdlrname, "linear") == 0 )
      {
         SCIP_CALL( SCIPupgradeConsLinear(scip_feas, cons, &cons_upg) );
         cons = cons_upg;
      }
      if( cons != NULL )
      {
         conshdlr = SCIPconsGetHdlr(cons);
         assert( conshdlr != NULL );

         conshdlrname = SCIPconshdlrGetName(conshdlr);
         assert( conshdlrname != NULL );

         /* check type of constraint */
         if( strcmp(conshdlrname, "setppc") == 0 && SCIPgetTypeSetppc(scip, cons) == SCIP_SETPPCTYPE_PACKING && SCIPgetNVarsSetppc(scip, cons) > 1)
         {
            int nInterdictableInPacking = 0;
            int iLastAdded = 0;
            varsPack = SCIPgetVarsSetppc(scip, cons);
            for( i = 0; i < SCIPgetNVarsSetppc(scip, cons); ++i )
            {
               if( SCIPhashsetExists(interdictableVars, (void*) varsPack[i]) )
               {
                  nInterdictableInPacking++;
                  iLastAdded = i;
                  SCIP_CALL( SCIPhashmapInsertInt(varsToPacking, (void*) varsPack[i], *npackings) );
                  SCIP_CALL( SCIPhashsetRemove(interdictableVars, (void*) varsPack[i]) );

               }
            }
            if( nInterdictableInPacking > 1 )
            {
               (*npackings)++;
            }
            else if( nInterdictableInPacking == 1 )
            {
               SCIP_CALL( SCIPhashmapRemove(varsToPacking, varsPack[iLastAdded]) );
               SCIP_CALL( SCIPhashsetInsert(interdictableVars, SCIPblkmem(scip), (void*) varsPack[i]) );
            }
         }
      }

      if( cons_upg != NULL )
      {
         SCIP_CALL( SCIPreleaseCons(scip_feas, &cons_upg) );
      }
   }

   for( i = 0; i < nvars && SCIPhashsetGetNElements(interdictableVars) > 0; ++i )
   {
      if( SCIPhashsetExists(interdictableVars, (void*) vars_x_feas[i]) )
      {
         SCIP_CALL( SCIPhashmapInsertInt(varsToPacking, (void*) vars_x_feas[i], *npackings) );
         SCIP_CALL( SCIPhashsetRemove(interdictableVars, (void*) vars_x_feas[i]) );
         (*npackings)++;
      }
   }

   assert( SCIPhashsetGetNElements(interdictableVars) == 0 );
   for( i = 0; i < nvars; ++i )
   {
      if( vars_interdictable[i] )
      {

         printf("%s \t%d\n", SCIPvarGetName(vars_x_feas[i]), SCIPhashmapGetImageInt(varsToPacking, (void*) vars_x_feas[i]) );
      }
   }

   SCIPhashsetFree(&interdictableVars, SCIPblkmem(scip));

   return SCIP_OKAY;
}


/* includes to scip variables and constraints of scip_feas for each possible failure scenario with k failures */
SCIP_RETCODE SCIPReaderIntAddConstraintsVarForFailureScenarios(
   SCIP*                 scip,               /**< pointer to scip structure */
   int                   nvars,              /**< number of variables which have to be treated */
   SCIP_VAR**            vars_x,             /**< variables in scip */
   SCIP_VAR**            vars_x_feas,        /**< variables in scip_feas */
   SCIP_Bool*            vars_interdictable, /**< which of the variables are interdictable */
   SCIP*                 scip_feas,          /**< SCIP data structure for the subproblem */
   int                   k                   /**< number of possible interdictions */
   )
{
   FailureScenarios scenarios;
   int* indicesFailures;
   int* subset;
   SCIP_CONS** conss;
   SCIP_CONS* newcons;
   SCIP_VAR* newvar;
   int nconss;
   int i;
   int j;
   int l;
   int c;
   SCIP_HASHMAP* varmap;
   SCIP_HASHMAP* consmap;
   SCIP_Bool valid;
   int nvars_scipfeas;
   SCIP_VAR** vars_scipfeas;
   SCIP_Real lb;
   SCIP_Real ub;
   char name[SCIP_MAXSTRLEN];
   SCIP_HASHMAP* varsToPacking;
   int npackings;
   SCIP_Real time;
   SCIP_Real memory;


   /* remove symmetry between linking variables from scip_feas, they could make the lowest level to weak in comparison
      to the failure scenarios */
   SCIP_CALL( detectSymmetryBreakingAndDelete(scip_feas, vars_x_feas, nvars) );

   /* identify setpacking structure. E.g. for a truss we dont need to add a failure scenario for each bar diameter, it
      suffices to add it for the bar itself and just set each corresponding bar diameter to 0. */
   SCIP_CALL( SCIPhashmapCreate(&varsToPacking, SCIPblkmem(scip), nvars) );
   SCIP_CALL( SCIPidentifyPackingVars(scip, nvars, vars_x_feas, vars_interdictable, scip_feas, varsToPacking, &npackings) );

   SCIP_CALL( SCIPallocBufferArray(scip, &subset, k) );
   SCIP_CALL( SCIPallocBufferArray(scip, &indicesFailures, npackings) );

   for( i = 0; i < npackings; ++i )
   {
      indicesFailures[i] = i;
   }

   SCIP_CALL( FailureInit(scip, &scenarios, k) );
   SCIP_CALL( processSubsets(scip, indicesFailures, npackings, subset, k, &scenarios) );

   nvars_scipfeas = SCIPgetNVars(scip_feas);
   vars_scipfeas = SCIPgetVars(scip_feas);
   SCIP_CALL( SCIPhashmapCreate(&varmap, SCIPblkmem(scip), nvars_scipfeas) );
   nconss = SCIPgetNConss(scip_feas);
   conss = SCIPgetConss(scip_feas);
   SCIP_CALL( SCIPhashmapCreate(&consmap, SCIPblkmem(scip), nconss) );

   time = SCIPgetTotalTime(scip);
   memory = SCIPgetMemUsed(scip)/1048576.0;
   /* go through each failure scenario and add constraints+vars */
   for( i = 0; i < scenarios.nFailures; ++i )
   {
      for( j = 0; j < nvars_scipfeas; ++j )
      {
         lb = SCIPvarGetLbGlobal(vars_scipfeas[j]);
         ub = SCIPvarGetUbGlobal(vars_scipfeas[j]);
         (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "Added#%d#%s", i, SCIPvarGetName(vars_scipfeas[j]));

         SCIP_CALL( SCIPcreateVarBasic(scip, &newvar, name, lb, ub, 0.0, SCIPvarGetType(vars_scipfeas[j])) );
         SCIP_CALL( SCIPhashmapInsert(varmap, vars_scipfeas[j], newvar) );
         SCIP_CALL( SCIPaddVar(scip, newvar) );
         SCIP_CALL( SCIPreleaseVar(scip, &newvar) );
      }
      /* create copies of constraints and add them, use a hashmap since
         cons_indicator, seems to look for its linear cons */
      for (c = 0; c < nconss; ++c)
      {
            assert( conss[c] != NULL );

            (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "Added#%d#%s", i, SCIPconsGetName(conss[c]));
            SCIP_CALL( SCIPgetConsCopy(scip_feas, scip, conss[c], &newcons, SCIPconsGetHdlr(conss[c]), varmap, consmap, name,
                                       SCIPconsIsInitial(conss[c]), SCIPconsIsSeparated(conss[c]), SCIPconsIsEnforced(conss[c]), SCIPconsIsChecked(conss[c]),
                                       SCIPconsIsPropagated(conss[c]), FALSE, FALSE, SCIPconsIsDynamic(conss[c]),
                                       SCIPconsIsRemovable(conss[c]), FALSE, TRUE, &valid) );
            if( !valid )
            {
               printf("problems in constraint copying %s\n", SCIPconshdlrGetName(SCIPconsGetHdlr(conss[c])));
               return SCIP_ERROR;
            }
            SCIP_CALL( SCIPaddCons(scip, newcons) );
            SCIP_CALL( SCIPreleaseCons(scip, &newcons) );
      }

      /* link vars by xx <= x ( or xx == x ) if not interdicted, otherwise xx == 0*/
      for( j = 0; j < nvars; ++j )
      {
         SCIP_Bool interdicted = FALSE;
         newvar = (SCIP_VAR*) SCIPhashmapGetImage(varmap, vars_x_feas[j]);
         assert( newvar != NULL );

         /*  check whether this var is interdicted */
         for( l = 0; l < k; ++l )
         {
            if( scenarios.failures[i][l] == SCIPhashmapGetImageInt(varsToPacking, vars_x_feas[j]) )
            {
               interdicted = TRUE;
            }
         }

         if( interdicted )
         {
            SCIP_CALL( SCIPchgVarLb(scip, newvar, 0.0) );
            SCIP_CALL( SCIPchgVarUb(scip, newvar, 0.0) );
         }
         else
         {
            (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "LinkingConstraints#%d#%d", i, j);

            SCIP_CALL( SCIPcreateConsBasicLinear(scip, &newcons, name, 0, NULL, NULL, -SCIPinfinity(scip), 0.0) );
            SCIP_CALL( SCIPaddCoefLinear(scip, newcons, newvar, 1.0) );
            SCIP_CALL( SCIPaddCoefLinear(scip, newcons, vars_x[j], -1.0) );
            SCIP_CALL( SCIPaddCons(scip, newcons) );
            SCIP_CALL( SCIPreleaseCons(scip, &newcons) );
         }
      }
      SCIP_CALL( SCIPhashmapRemoveAll(consmap) );
      SCIP_CALL( SCIPhashmapRemoveAll(varmap) );

      if( i == 0 )
      {
         SCIP_Real timelimit;
         SCIP_Real memlimit;
         time = (SCIPgetTotalTime(scip) - time) * scenarios.nFailures + time;
         memory = (SCIPgetMemUsed(scip)/1048576.0 - memory) * scenarios.nFailures + memory;

         SCIP_CALL( SCIPgetRealParam(scip, "limits/time", &timelimit) );
         SCIP_CALL( SCIPgetRealParam(scip, "limits/memory", &memlimit) );
         printf("estimated time and memory to construct the whole scenario formulation, estimation: %fs, %f MB\n", time, memory);
         if( time > timelimit || memory > memlimit )
         {
            printf("not sufficient time or memory to construct the whole scenario formulation\n");
            return SCIP_NOMEMORY;
         }


      }

   }

   SCIPhashmapFree(&consmap);
   SCIPhashmapFree(&varmap);
   SCIPhashmapFree(&varsToPacking);
   FailureExit(scip, &scenarios);
   SCIPfreeBufferArray(scip, &subset);
   SCIPfreeBufferArray(scip, &indicesFailures);

   return SCIP_OKAY;
}



/* Reformulates the problem if there are packing constraint x_1 + ... + x_n <=
   1, where x_i are interdictable. If so, we include a variable y and constraint
   x_1 + ... + x_n <= y, where y is interdictable and x_i is now
   notinterdictable. */
SCIP_RETCODE SCIPreformulateWithPacking(
   SCIP*                 scip,               /**< pointer to scip structure */
   int*                  nvars,              /**< number of variables which have to be treated */
   SCIP_VAR***           vars_x,             /**< variables in scip */
   SCIP_VAR***           vars_x_feas,        /**< variables in scip_feas */
   SCIP_Bool**           vars_interdictable, /**< which of the variables are interdictable */
   SCIP*                 scip_feas           /**< SCIP data structure for the subproblem */
   )
{
   SCIP_CONS* newcons;
   SCIP_CONS* newcons_feas;
   int i;
   int j;
   char name[SCIP_MAXSTRLEN];
   SCIP_HASHMAP* varsToPacking;
   int npackings;
   int naddedpackings;

   /* identify setpacking structure */
   SCIP_CALL( SCIPhashmapCreate(&varsToPacking, SCIPblkmem(scip), *nvars) );
   SCIP_CALL( SCIPidentifyPackingVars(scip, *nvars, *vars_x_feas, *vars_interdictable, scip_feas, varsToPacking, &npackings) );

   if( npackings <= 0 )
   {
      SCIPhashmapFree(&varsToPacking);
      return SCIP_OKAY;
   }

   SCIP_CALL( SCIPreallocBufferArray(scip, vars_x, *nvars+npackings) );
   SCIP_CALL( SCIPreallocBufferArray(scip, vars_x_feas, *nvars+npackings) );
   SCIP_CALL( SCIPreallocBufferArray(scip, vars_interdictable, *nvars+npackings) );

   naddedpackings = 0;
   for( i = 0; i < npackings; ++i )
   {
      int sizepacking = 0;

      for( j = 0; j < *nvars; ++j )
      {
         if( i == SCIPhashmapGetImageInt(varsToPacking, (*vars_x_feas)[j]) )
         {
            sizepacking++;
         }
      }
      if( sizepacking <= 1 )
         continue;

      naddedpackings++;
      (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "Packing#%d", i);
      SCIP_CALL( SCIPcreateVarBasic(scip, &((*vars_x)[*nvars+i]), name, 0.0, 1.0, 0.0, SCIP_VARTYPE_BINARY ) );
      SCIP_CALL( SCIPaddVar(scip, (*vars_x)[*nvars+i]) );

      (void) SCIPsnprintf(name, SCIP_MAXSTRLEN, "Packing#%d", i);
      SCIP_CALL( SCIPcreateVarBasic(scip_feas, &((*vars_x_feas)[*nvars+i]), name, 0.0, 1.0, 0.0, SCIP_VARTYPE_BINARY ) );
      SCIP_CALL( SCIPaddVar(scip_feas, (*vars_x_feas)[*nvars+i]) );

      SCIP_CALL( SCIPcreateConsBasicLinear(scip, &newcons, name, 0, NULL, NULL, 0.0, 0.0) );
      SCIP_CALL( SCIPaddCoefLinear(scip, newcons, (*vars_x)[*nvars+i], -1.0) );

      SCIP_CALL( SCIPcreateConsBasicLinear(scip_feas, &newcons_feas, name, 0, NULL, NULL, 0.0, 0.0) );
      SCIP_CALL( SCIPaddCoefLinear(scip_feas, newcons_feas, (*vars_x_feas)[*nvars+i], -1.0) );
      (*vars_interdictable)[*nvars+i] = TRUE;
      for( j = 0; j < *nvars; ++j )
      {
         if( i == SCIPhashmapGetImageInt(varsToPacking, (*vars_x_feas)[j]) )
         {
            (*vars_interdictable)[j] = FALSE;
            SCIP_CALL( SCIPaddCoefLinear(scip, newcons, (*vars_x)[j], 1.0) );
            SCIP_CALL( SCIPaddCoefLinear(scip_feas, newcons_feas, (*vars_x_feas)[j], 1.0) );
         }
      }

      SCIP_CALL( SCIPaddCons(scip_feas, newcons_feas) );
      SCIP_CALL( SCIPreleaseCons(scip_feas, &newcons_feas) );

      SCIP_CALL( SCIPaddCons(scip, newcons) );
      SCIP_CALL( SCIPreleaseCons(scip, &newcons) );
   }

   *nvars += naddedpackings;

   SCIP_CALL( SCIPreallocBufferArray(scip, vars_x, *nvars) );
   SCIP_CALL( SCIPreallocBufferArray(scip, vars_x_feas, *nvars) );
   SCIP_CALL( SCIPreallocBufferArray(scip, vars_interdictable, *nvars) );

   SCIPhashmapFree(&varsToPacking);

   return SCIP_OKAY;
}

/** if the last char is a \n remove it */
static
void trimNewline(
   char* buffer
   )
{
   size_t len = strlen(buffer);
   if (len > 0 && buffer[len - 1] == '\n')
      buffer[--len] = '\0';
}


/** reads the int file and create the interdiction problem */
static
SCIP_RETCODE readProblem(
   SCIP*                 scip,               /**< pointer to scip structure */
   const char*           filename            /**< file of the network data */
   )
{
   SCIP_FILE* file;
   char buffer[SCIP_MAXSTRLEN];
   char* saveptr;
   char* token;
   char* path;
   char* pathorig;
   int nvars  = -1;
   int nvarsread = 0;
   int k = -1;
   SCIP* scipfeas = NULL; /* pointer to scip which defines feasible solutions to scip */
   SCIP_Bool mainprobread = FALSE;
   SCIP_Bool feasprobread = FALSE;
   SCIP_VAR** vars_main = NULL;
   SCIP_VAR** vars_feas = NULL;
   SCIP_Bool* vars_interdictable = NULL; /**< which of the variables are interdictable */
   SCIP_Bool useconskresilient;
   int i;


   assert(scip != NULL);

   if( NULL == (file = SCIPfopen(filename, "r")) )
   {
      SCIPerrorMessage("cannot open file <%s> for reading\n", filename);
      SCIPprintSysError(filename);
      return SCIP_NOFILE;
   }
   /* construct basename to read subproblems */
   SCIP_CALL( SCIPduplicateBufferArray(scip, &pathorig, filename, strlen(filename)+1) );
   SCIPsplitFilename(pathorig, &path, NULL, NULL, NULL);

   while( SCIPfgets(buffer, (int) sizeof(buffer), file) )
   {
      token = SCIPstrtok(buffer, " ", &saveptr);
      assert(token != NULL);

      if( strlen(token) >= 2 && token[0] == '/' && token[1] == '/' )
      {
      }
      else if( strcmp(token, "Problem") == 0 )
      {
         trimNewline(saveptr);

         SCIPinfoMessage(scip, NULL, "read main problem \"%s\" in directory \"%s\".\n", saveptr, path);

         SCIPsetMessagehdlrQuiet(scip, TRUE);
         if( path != NULL )
         {
            char fullpath[SCIP_MAXSTRLEN];
            (void) SCIPsnprintf(fullpath, SCIP_MAXSTRLEN, "%s/%s", path, saveptr);
            SCIP_CALL( SCIPreadProb(scip, fullpath, NULL) );
         }
         else
         {
            SCIP_CALL( SCIPreadProb(scip, saveptr, NULL) );
         }
         SCIP_CALL( heurMaxTrussIdentifyAndSetHeurdata(scip) );
         SCIPsetMessagehdlrQuiet(scip, FALSE);

         mainprobread = TRUE;
      }
      else if( strcmp(token, "Feasibility_Problem") == 0 )
      {
         trimNewline(saveptr);
         SCIP_CALL( SCIPcreate(&scipfeas) );
         SCIP_CALL( includeDefaultPlugins(scipfeas) );
         SCIPinfoMessage(scip, NULL, "read feasibility problem \"%s\" in directory \"%s\".\n", saveptr, path);

         SCIPsetMessagehdlrQuiet(scipfeas, TRUE);
         if( path != NULL )
         {
            char fullpath[SCIP_MAXSTRLEN];
            (void) SCIPsnprintf(fullpath, SCIP_MAXSTRLEN, "%s/%s", path, saveptr);
            SCIP_CALL( SCIPreadProb(scipfeas, fullpath, NULL) );
         }
         else
         {
            SCIP_CALL( SCIPreadProb(scipfeas, saveptr, NULL) );
         }
         SCIP_CALL( SCIPsetProbName(scipfeas, "scipfeas") );
         SCIP_CALL( heurMaxTrussIdentifyAndSetHeurdata(scipfeas) );
         SCIPsetMessagehdlrQuiet(scipfeas, FALSE);

         feasprobread = TRUE;
      }
      else if( strcmp(token, "Linking_Variables") == 0 )
      {
         if( !SCIPstrToIntValue(saveptr, &nvars, &saveptr) )
         {
            printf("Could not read number of interdiction variables \n");
            return SCIP_READERROR;
         }

         if( nvars > 0 )
         {
            SCIP_CALL( SCIPallocBufferArray(scip, &vars_main, nvars) );
            SCIP_CALL( SCIPallocBufferArray(scip, &vars_feas, nvars) );
            SCIP_CALL( SCIPallocBufferArray(scip, &vars_interdictable, nvars) );
         }
      }
      else if( strcmp(token, "K") == 0 )
      {
         if( !SCIPstrToIntValue(saveptr, &k, &saveptr) )
         {
            printf("Could not read number of possible interdictions k \n");
            return SCIP_READERROR;
         }
      }
      else if( !mainprobread || !feasprobread || nvars <= 0 || k <= -1 )
      {
         SCIPinfoMessage(scip, NULL, "Problem or Feasibility_Problem or Linking_Variables or K not set but got input:\n %s\n", token);
         return SCIP_READERROR;
      }
      else if (strcmp(token, "interdictableVar")  == 0 || strcmp(token, "noninterdictableVar") == 0)
      {
         SCIP_Bool infeasible;
         if( nvarsread >= nvars )
         {
            SCIPinfoMessage(scip, NULL, "Found to many variables in file\n");
            return SCIP_READERROR;
         }
         vars_interdictable[nvarsread] = strcmp(token, "interdictableVar") == 0;
         trimNewline(saveptr);
         vars_main[nvarsread] = SCIPfindVar(scip, saveptr);
         vars_feas[nvarsread] = SCIPfindVar(scipfeas, saveptr);

         if( vars_main[nvarsread] == NULL )
         {
            SCIPinfoMessage(scip, NULL, "Did not find variable in main problem:\n %s\n", saveptr);
            return SCIP_ERROR;
         }
         if( vars_feas[nvarsread] == NULL )
         {
            SCIPinfoMessage(scip, NULL, "Did not find variable in feasibility problem:\n %s\n", saveptr);
            return SCIP_ERROR;
         }

         // change linking variables to binary, ignore infeasibility
         SCIP_CALL( SCIPchgVarType(scip, vars_main[nvarsread], SCIP_VARTYPE_BINARY, &infeasible) );
         SCIP_CALL( SCIPchgVarUb(scip, vars_main[nvarsread], 1.0) );
         SCIP_CALL( SCIPchgVarLb(scip, vars_main[nvarsread], 0.0) );
         SCIP_CALL( SCIPchgVarType(scipfeas, vars_feas[nvarsread], SCIP_VARTYPE_BINARY, &infeasible) );
         SCIP_CALL( SCIPchgVarUb(scipfeas, vars_feas[nvarsread], 1.0) );
         SCIP_CALL( SCIPchgVarLb(scipfeas, vars_feas[nvarsread], 0.0) );

         nvarsread++;
      }
   }

   SCIPfclose(file);

   SCIPfreeBufferArray(scip, &pathorig);


   if( !mainprobread || !feasprobread || nvars < 0 || k <= -1 )
   {
      SCIPinfoMessage(scip, NULL, "Problem or Feasibility_Problem or Linking_Variables or K not set at end of file\n");
      return SCIP_READERROR;
   }

   /* if the file does not specify the number of linking variables, we assume
      that every integer variable is binary and all binary variables are
      interdictable */
   if( nvars == 0 )
   {
      SCIP_VAR** varsfeas;
      SCIP_Bool infeasible;
      nvars = SCIPgetNBinVars(scipfeas) + SCIPgetNIntVars(scipfeas);
      varsfeas = SCIPgetVars(scipfeas);

      SCIP_CALL( SCIPallocBufferArray(scip, &vars_main, nvars) );
      SCIP_CALL( SCIPallocBufferArray(scip, &vars_feas, nvars) );
      SCIP_CALL( SCIPallocBufferArray(scip, &vars_interdictable, nvars) );

      for( i = 0; i < nvars; ++i )
      {
         vars_interdictable[i] = TRUE;
      }
      for( i = 0; i < SCIPgetNVars(scipfeas); ++i )
      {
         if( SCIPvarIsBinary(varsfeas[i]) || SCIPvarIsIntegral(varsfeas[i]) )
         {
            vars_feas[nvarsread] = varsfeas[i];
            vars_main[nvarsread] = SCIPfindVar(scip, SCIPvarGetName(vars_feas[nvarsread]));
            if( vars_main[nvarsread] == NULL )
            {
               printf("cant find variable %s in main scip\n", SCIPvarGetName(vars_feas[nvarsread]));
            }
            nvarsread++;
         }
      }

      for( i = 0; i < nvarsread; ++i )
      {
         SCIP_CALL( SCIPchgVarUb(scip, vars_main[i], 1.0) );
         SCIP_CALL( SCIPchgVarLb(scip, vars_main[i], 0.0) );
         SCIP_CALL( SCIPchgVarType(scip, vars_main[i], SCIP_VARTYPE_BINARY, &infeasible) );
         if( infeasible )
            return SCIP_ERROR;

         SCIP_CALL( SCIPchgVarUb(scipfeas, vars_feas[i], 1.0) );
         SCIP_CALL( SCIPchgVarLb(scipfeas, vars_feas[i], 0.0) );
         SCIP_CALL( SCIPchgVarType(scipfeas, vars_feas[i], SCIP_VARTYPE_BINARY, &infeasible) );
         if( infeasible )
            return SCIP_ERROR;
      }

      /* the possible conversion from integer to binary variables makes it possible, that we find more structure */
      SCIP_CALL( heurMaxTrussIdentifyAndSetHeurdata(scip) );
   }

   if( nvarsread != nvars )
   {
      SCIPinfoMessage(scip, NULL, "Did only read %d of %d variables\n", nvarsread, nvars);
      return SCIP_READERROR;
   }

   /* capture vars, some functions create more vars, which must be freed later on, This way we dont need to remember which where added */
   for( i = 0; i < nvars; ++i )
   {
      SCIP_CALL( SCIPcaptureVar(scip, vars_main[i]) );
      SCIP_CALL( SCIPcaptureVar(scipfeas, vars_feas[i]) );
   }

   SCIP_CALL( SCIPgetBoolParam(scip, "reader_int/useconskresilient", &useconskresilient) );
   // create the interdiction constraints if necessary otherwise delete feasibility problem
   if( k > 0 && useconskresilient )
   {
      SCIP_CONS* cons;
      SCIP_CALL( SCIPcreateConsBasicKResilient(scip, &cons, "resilienceConstraint", nvars, vars_main, vars_feas, vars_interdictable, scipfeas, k) );

      SCIP_CALL( SCIPaddCons(scip, cons) );
      SCIP_CALL( SCIPreleaseCons(scip, &cons) );
   }
   else if( k > 0 )
   {
      SCIP_CALL( SCIPReaderIntAddConstraintsVarForFailureScenarios(scip, nvars, vars_main, vars_feas, vars_interdictable, scipfeas, k) );
   }

   SCIPfreeBufferArray(scip, &vars_interdictable);

   for( i = 0; i < nvars; ++i )
   {
      SCIP_CALL( SCIPreleaseVar(scip, &vars_main[i]) );
      SCIP_CALL( SCIPreleaseVar(scipfeas, &vars_feas[i]) );
   }

   /* free scipfeas if not used, otherwise it will be freed by cons_interdiction, created by cons_kresilient */
   if( k == 0 || !useconskresilient )
   {
      SCIP_CALL( SCIPfree(&scipfeas) );
   }

   SCIPfreeBufferArray(scip, &vars_feas);
   SCIPfreeBufferArray(scip, &vars_main);

   return SCIP_OKAY;
}



/*
 * Callback methods of reader
 */

/** copy method for reader plugins (called when SCIP copies plugins) */
static
SCIP_DECL_READERCOPY(readerCopyInt)
{  /*lint --e{715}*/
   assert(scip != NULL);
   assert(reader != NULL);
   assert(strcmp(SCIPreaderGetName(reader), READER_NAME) == 0);

   /* call inclusion method of reader */
   SCIP_CALL( SCIPincludeReaderInt(scip) );

   return SCIP_OKAY;
}


/** problem reading method of reader */
static
SCIP_DECL_READERREAD(readerReadInt)
{  /*lint --e{715}*/

   assert(reader != NULL);
   assert(strcmp(SCIPreaderGetName(reader), READER_NAME) == 0);
   assert(result != NULL);

   *result = SCIP_DIDNOTRUN;

   SCIP_CALL( readProblem(scip, filename) );

   *result = SCIP_SUCCESS;
   return SCIP_OKAY;
}


/*
 * reader specific interface methods
 */

/** includes the int file reader in SCIP */
SCIP_RETCODE SCIPincludeReaderInt(
   SCIP*                 scip                /**< SCIP data structure */
   )
{
   SCIP_READER* reader;

   /* include reader */
   SCIP_CALL( SCIPincludeReaderBasic(scip, &reader, READER_NAME, READER_DESC, READER_EXTENSION, NULL) );

   /* set non fundamental callbacks via setter functions */
   SCIP_CALL( SCIPsetReaderCopy(scip, reader, readerCopyInt) );
   SCIP_CALL( SCIPsetReaderRead(scip, reader, readerReadInt) );

   /* add params */
   SCIP_CALL( SCIPaddBoolParam(scip, "reader_int/useconskresilient", "should the resilient constraint handler be used. Otherwise scip_feas is added for each failure scenario", NULL, FALSE, DEFAULT_USECONSRESILIENT, NULL, NULL) );

   return SCIP_OKAY;
}
