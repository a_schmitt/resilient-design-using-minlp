/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*    This file is part of the program kresilience                           */
/*                                                                           */
/*    Copyright (C) 2020-2021 Andreas Schmitt, Marc Pfetsch                  */
/*                                                                           */
/*                                                                           */
/*    Based on SCIP --- Solving Constraint Integer Programs                  */
/*       SCIP is distributed under the terms of the SCIP Academic Licence,   */
/*       see file COPYING in the SCIP distribution.                          */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**@file   heur_maxtruss.c
 * @brief  checks whether the truss with all the greatest diameter bars is feasible
 * @author Andreas Schmitt
 *
 * This heuristic assume the structure of a truss design problem with buckling constraints. It tries to identify binary
 * variables corresponding to differen bar diameters, where we assume that variables corresponding to the same bar but
 * for different diameters are connected within a covering constraint. Within a b&c node it then tries to find a
 * solution by solving the relaxation, which fixes each bar to its greatest diameter. If the relaxation solution still
 * contains fractional values, we round the greatest up and resolve. (These fractional variables correspond to actuators
 * in use case). The heuristic also tries to find indicator constraints and propagates these after fixing variables,
 * since they are usually not enforced within the relaxation.
 */

/*---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0----+----1----+----2*/

#include <assert.h>
#include <string.h>

#include "heur_maxtruss.h"
#include "scip/cons_indicator.h"
#include "scip/cons_setppc.h"
#include "scip/scipdefplugins.h"
#include "scipsdp/relax_sdp.h"

#define HEUR_NAME             "maxTruss"
#define HEUR_DESC             "includes the truss whith all bars set to biggest diameter as solution"
#define HEUR_DISPCHAR         'T'
#define HEUR_PRIORITY         -10000
#define HEUR_FREQ             10
#define HEUR_FREQOFS          0
#define HEUR_MAXDEPTH         -1
#define HEUR_TIMING           SCIP_HEURTIMING_AFTERNODE
#define HEUR_USESSUBSCIP      FALSE  /* does the heuristic use a secondary SCIP instance? */

/* locally defined heuristic data */
struct SCIP_HeurData
{
   int                   nbars;              /**< number of bars */
   int                   nareas;             /**< number of diameters */
   SCIP_VAR***           vars_bars;          /**< [i][j] bar i with area j, sorted by increasing objective*/
   int                   nind;               /**< number of indicator constraints */
   SCIP_VAR**            vars_indslack;      /**< slack variable of indicator constraint */
   SCIP_VAR**            vars_indbin;        /**< indicator variable of indicator constraint */
   SCIP_SOL*             sol;                /**< working solution */
};

/** Frees the variable arrays in HeurData, if they were allocated. */
static
SCIP_RETCODE heurMaxTrussFree(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_HEUR*            heur                /**< heuristic data structure */
)
{
   SCIP_HEURDATA* heurdata;
   int i;

   assert(heur != NULL);
   assert(strcmp(SCIPheurGetName(heur), HEUR_NAME) == 0);
   assert(scip != NULL);

   /* free heuristic data */
   heurdata = SCIPheurGetData(heur);
   assert(heurdata != NULL);

   if( heurdata->nind > 0 )
   {
      SCIPfreeBlockMemoryArray(scip, &heurdata->vars_indbin, heurdata->nind);
      SCIPfreeBlockMemoryArray(scip, &heurdata->vars_indslack, heurdata->nind);
   }

   if( heurdata->nbars > 0 )
   {
      for( i = 0; i < heurdata->nbars; ++i )
      {
         SCIPfreeBlockMemoryArray(scip, &heurdata->vars_bars[i], heurdata->nareas);
      }
      SCIPfreeBlockMemoryArray(scip, &heurdata->vars_bars, heurdata->nbars);
   }

   heurdata->nbars = -1;
   heurdata->nind = -1;

   return SCIP_OKAY;
}

/** Checks the problem for structure corresponding to a truss and identifies
    variables for bars, and their interrelationsships as well as slack
    variables.

    This method needs to be called, in order for the heuristic to run.
  */
SCIP_RETCODE heurMaxTrussIdentifyAndSetHeurdata(
   SCIP*                 scip                /**< SCIP data structure */
)
{
   SCIP_HEUR* heur;
   SCIP_HEURDATA* heurdata;
   SCIP_CONSHDLR* conshdlr;
   SCIP_CONS** conss;
   int nconss;
   int nvars;
   int c;
   int i;

   /* do not run if heuristic is not found */
   heur = SCIPfindHeur(scip, HEUR_NAME);
   if( heur == NULL )
      return SCIP_OKAY;

   /* get data */
   heurdata = SCIPheurGetData(heur);
   assert(heurdata != NULL);

   if( heurdata->nbars > 0 || heurdata->nind > 0 )
   {
      SCIP_CALL( heurMaxTrussFree(scip, heur) );
      printf("%s already called identify Heurdata, reset data.\n", HEUR_NAME);
   }

   heurdata->nbars = 0;
   heurdata->nind = 0;
   heurdata->nareas = -1;

   nvars = SCIPgetNVars(scip);

   /* allocate possibly to much, reallocate later */
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &heurdata->vars_bars, nvars) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &heurdata->vars_indslack, nvars) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &heurdata->vars_indbin, nvars) );

   nconss = SCIPgetNConss(scip);
   conss = SCIPgetConss(scip);
   assert( conss != NULL );

   /* loop through all constraints to get setpacking constraints and indicator constraints */
   for (c = 0; c < nconss; ++c)
   {
      const char* conshdlrname;
      SCIP_CONS* cons;
      SCIP_CONS* cons_upg = NULL;

      /* get constraint */
      cons = conss[c];
      assert( cons != NULL );

      /* get constraint handler */
      conshdlr = SCIPconsGetHdlr(cons);
      assert( conshdlr != NULL );

      conshdlrname = SCIPconshdlrGetName(conshdlr);
      assert( conshdlrname != NULL );
      if ( strcmp(conshdlrname, "linear") == 0 )
      {
         SCIP_CALL( SCIPupgradeConsLinear(scip, cons, &cons_upg) );
         cons = cons_upg;
      }

      if( cons != NULL )
      {
         conshdlr = SCIPconsGetHdlr(cons);
         assert( conshdlr != NULL );

         conshdlrname = SCIPconshdlrGetName(conshdlr);
         assert( conshdlrname != NULL );

         /* check type of constraint */
         if( strcmp(conshdlrname, "setppc") == 0 && SCIPgetTypeSetppc(scip, cons) == SCIP_SETPPCTYPE_PACKING && SCIPgetNVarsSetppc(scip, cons) > 1)
         {
            if( heurdata->nareas >= 0 && heurdata->nareas != SCIPgetNVarsSetppc(scip, cons) )
            {
               printf("found setpacking constraints, with different numbers of variables. Not sure whether we have bar variables. Turning heuristic off\n");
               heurdata->nareas = 0;
               heurdata->nbars = 0;

               // TODO free stuff
               return SCIP_OKAY;
            }
            heurdata->nareas = SCIPgetNVarsSetppc(scip, cons);

            SCIP_CALL( SCIPduplicateBlockMemoryArray(scip, &heurdata->vars_bars[heurdata->nbars], SCIPgetVarsSetppc(scip, cons), heurdata->nareas) );

            /* check vars are sorted with increasing objective TODO sort them otherwise */
            for( i = 0; i < heurdata->nareas-1; ++i )
            {
               if( SCIPvarGetObj(heurdata->vars_bars[heurdata->nbars][i]) >= SCIPvarGetObj(heurdata->vars_bars[heurdata->nbars][i+1]) )
               {
                  printf("set packing variables are not sorted increasing. TODO implement sorting\n");
                  return SCIP_ERROR;
               }
            }
            heurdata->nbars++;
         }
         else if ( strcmp(conshdlrname, "indicator") == 0 )
         {
            heurdata->vars_indbin[heurdata->nind] = SCIPgetBinaryVarIndicator(cons);
            heurdata->vars_indslack[heurdata->nind] = SCIPgetSlackVarIndicator(cons);
            heurdata->nind++;
         }
      }

      if( cons_upg != NULL )
      {
         SCIP_CALL( SCIPreleaseCons(scip, &cons_upg) );
      }
   }

   /* maybe we only have one diameter, then we can identify the bar variables from the slack constraint */
   if( heurdata->nind > 0 && heurdata->nbars == 0 )
   {
      heurdata->nbars = heurdata->nind;
      heurdata->nareas = 1;
      for( i = 0; i < heurdata->nbars; ++i )
      {
         SCIP_CALL( SCIPallocBlockMemoryArray(scip, &heurdata->vars_bars[i], 1) );
         heurdata->vars_bars[i][0] = heurdata->vars_indbin[i];
      }
   }

   /* reallocate/free memory */
   if( heurdata->nind == 0 )
   {
      SCIPfreeBlockMemoryArray(scip, &heurdata->vars_indbin, nvars);
      SCIPfreeBlockMemoryArray(scip, &heurdata->vars_indslack, nvars);
   }
   else
   {
      SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &(heurdata->vars_indbin), nvars, heurdata->nind) );
      SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &(heurdata->vars_indslack), nvars, heurdata->nind) );
   }

   if( heurdata->nbars == 0 )
   {
      SCIPfreeBlockMemoryArray(scip, &heurdata->vars_bars, nvars);
   }
   else
   {
      SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &(heurdata->vars_bars), nvars, heurdata->nbars) );
   }

   return SCIP_OKAY;
}

/** destructor of primal heuristic to free user data (called when SCIP is exiting) */
static
SCIP_DECL_HEURFREE(heurFreeMaxTruss)
{  /*lint --e{715}*/
   SCIP_HEURDATA* heurdata;

   assert(heur != NULL);
   assert(strcmp(SCIPheurGetName(heur), HEUR_NAME) == 0);
   assert(scip != NULL);

   heurdata = SCIPheurGetData(heur);
   assert(heurdata != NULL);

   SCIP_CALL( heurMaxTrussFree(scip, heur) );

   SCIPfreeMemory(scip, &heurdata);

   SCIPheurSetData(heur, NULL);
   return SCIP_OKAY;
}


/** execution method of primal heuristic */
static
SCIP_DECL_HEUREXEC(heurExecMaxTruss)
{  /*lint --e{715}*/
   SCIP_HEURDATA* heurdata;
   int i;
   int j;
   SCIP_Bool cutoff;
   SCIP_RELAX* relaxsdp;

   assert(heur != NULL);
   assert(strcmp(SCIPheurGetName(heur), HEUR_NAME) == 0);

   heurdata = SCIPheurGetData(heur);
   assert(heurdata != NULL);

   /* exit if no variables are available */
   if( heurdata->nbars <= 0 && heurdata->nind <= -1 )
   {
      *result = SCIP_DIDNOTRUN;
      return SCIP_OKAY;
   }

   *result = SCIP_DELAYED;
   if ( nodeinfeasible )
   {
      return SCIP_OKAY;
   }
   if( SCIPinProbing(scip) )
   {
      return SCIP_OKAY;
   }

   *result = SCIP_DIDNOTFIND;

   relaxsdp = SCIPfindRelax(scip, "SDP");

   /* start diving, so we can set all bars to their highest diameter */
   SCIP_CALL( SCIPstartProbing(scip) );

   /* loop through all bars */
   for( i = 0; i < heurdata->nbars; ++i )
   {
      SCIP_Bool tight = FALSE;
      int thickestFree = -1;

      /* check whether the setpacking constraint is already tight */
      for( j = 0; j < heurdata->nareas; ++j )
      {
         if( SCIPvarGetLbLocal(heurdata->vars_bars[i][j]) > 0.5 )
         {
            tight = TRUE;
         }
         else if( SCIPvarGetUbLocal(heurdata->vars_bars[i][j]) > 0.5 )
         {
            thickestFree = j;
         }
      }

      if( !tight && thickestFree >= 0 )
      {
         /* if not, set the thickest bar to one  */
         SCIP_CALL( SCIPfixVarProbing(scip, heurdata->vars_bars[i][thickestFree], 1.0) );
      }
   }

   /* enforce the slack constraint in the relaxation */
   for( i = 0; i < heurdata->nind; ++i )
   {
      if( SCIPvarGetLbLocal(heurdata->vars_indbin[i]) > 0.5 && SCIPisLE(scip, SCIPvarGetLbLocal(heurdata->vars_indslack[i]), 0.0) && SCIPisGT(scip, SCIPvarGetUbLocal(heurdata->vars_indslack[i]), 0.0) )
      {
         SCIP_CALL( SCIPfixVarProbing(scip, heurdata->vars_indslack[i], 0.0) );
      }
   }
   PROPAGATION:
   SCIP_CALL( SCIPpropagateProbing(scip, 0, &cutoff, NULL) );

   if( !cutoff )
   {
      /* solve the diving SDP */
      SCIP_CALL( SCIPsolveProbingRelax(scip, &cutoff) );

      if( SCIPrelaxSdpSolvedProbing(relaxsdp) && SCIPrelaxSdpIsFeasible(relaxsdp) )
      {
         SCIP_Bool success;

         /* create solution from diving SDP */
         SCIP_CALL( SCIPlinkRelaxSol(scip, heurdata->sol) );

         /* we only check improving solutions */
         if( SCIPgetSolTransObj(scip, heurdata->sol) < SCIPgetCutoffbound(scip) )
         {
            /* check integrality */
            SCIP_Real maxVal = -SCIPinfinity(scip);
            int indMaxVal = -1;
            SCIP_VAR** vars = SCIPgetVars(scip);
            SCIP_Real val;
            for( i = 0; i < SCIPgetNBinVars(scip) + SCIPgetNIntVars(scip); ++i )
            {
               val = SCIPgetSolVal(scip, heurdata->sol, vars[i]);

               if( !SCIPisIntegral(scip, val) && val >= maxVal )
               {
                  maxVal = val;
                  indMaxVal = i;
               }
            }
            if( indMaxVal >= 0 )
            {
               /* if we have violated integrality we round the greatest value up and fix it */
               SCIP_CALL( SCIPfixVarProbing(scip, vars[indMaxVal], SCIPceil(scip, maxVal)) );
               goto PROPAGATION;
            }
            /* try to add solution to SCIP */
            SCIP_CALL( SCIPtrySol(scip, heurdata->sol, FALSE, FALSE, TRUE, TRUE, FALSE, &success) );

            /* check, if solution was feasible and good enough */
            if( success )
            {
               *result = SCIP_FOUNDSOL;
            }
         }
      }
   }

   SCIP_CALL( SCIPendProbing(scip) );

   return SCIP_OKAY;
}


/** initialization method of primal heuristic (called after problem was transformed) */
static
SCIP_DECL_HEURINIT(heurInitMaxTruss)
{  /*lint --e{715}*/
   SCIP_HEURDATA* heurdata;

   assert(heur != NULL);
   assert(strcmp(SCIPheurGetName(heur), HEUR_NAME) == 0);

   /* get heuristic data */
   heurdata = SCIPheurGetData(heur);
   assert(heurdata != NULL);

   /* create working solution */
   SCIP_CALL( SCIPcreateSol(scip, &heurdata->sol, heur) );

   return SCIP_OKAY;
}


/** deinitialization method of primal heuristic (called before transformed problem is freed) */
static
SCIP_DECL_HEUREXIT(heurExitMaxTruss)
{  /*lint --e{715}*/
   SCIP_HEURDATA* heurdata;

   assert(heur != NULL);
   assert(strcmp(SCIPheurGetName(heur), HEUR_NAME) == 0);

   /* get heuristic data */
   heurdata = SCIPheurGetData(heur);
   assert(heurdata != NULL);

   /* free working solution */
   SCIP_CALL( SCIPfreeSol(scip, &heurdata->sol) );

   return SCIP_OKAY;
}


/*
 * heuristic specific interface methods
 */

/** creates the heuristic and includes it in SCIP */
SCIP_RETCODE SCIPincludeHeurMaxTruss(
   SCIP*                 scip                /**< SCIP data structure */
   )
{
   SCIP_HEUR* heur;
   SCIP_HEURDATA* heurdata;


   heur = SCIPfindHeur(scip, HEUR_NAME);
   if( heur != NULL )
   {
      SCIP_CALL( heurMaxTrussFree(scip, heur) );
      printf("%s already included in SCIP, reset data\n", HEUR_NAME);
      return SCIP_OKAY;
   }

   SCIP_CALL( SCIPallocMemory(scip, &heurdata) );

   heurdata->nbars = -1;
   heurdata->nind = -1;

   /* include primal heuristic */
   SCIP_CALL( SCIPincludeHeurBasic(scip, &heur,
         HEUR_NAME, HEUR_DESC, HEUR_DISPCHAR, HEUR_PRIORITY, HEUR_FREQ, HEUR_FREQOFS,
         HEUR_MAXDEPTH, HEUR_TIMING, HEUR_USESSUBSCIP, heurExecMaxTruss, heurdata) );

   assert( heur != NULL );

   SCIP_CALL( SCIPsetHeurFree(scip, heur, heurFreeMaxTruss) );
   SCIP_CALL( SCIPsetHeurInit(scip, heur, heurInitMaxTruss) );
   SCIP_CALL( SCIPsetHeurExit(scip, heur, heurExitMaxTruss) );

   return SCIP_OKAY;
}
