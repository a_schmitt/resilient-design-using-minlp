/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*    This file is part of the program kresilience                           */
/*                                                                           */
/*    Copyright (C) 2020-2021 Andreas Schmitt, Marc Pfetsch                  */
/*                                                                           */
/*                                                                           */
/*    Based on SCIP --- Solving Constraint Integer Programs                  */
/*       SCIP is distributed under the terms of the SCIP Academic Licence,   */
/*       see file COPYING in the SCIP distribution.                          */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**@file   include_plugins_scip.c
 * @brief  defines includeDefaultPlugins with the vanilla SCIP default plugins
 * @author Andreas Schmitt
 */

/*--+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0----+----1----+----2*/


#include "include_plugins.h"
#include "scip/scipdefplugins.h"

SCIP_RETCODE includeDefaultPlugins(
   SCIP*                   scip              /**<pointer to scip */
   )
{
   return SCIPincludeDefaultPlugins(scip);
}
