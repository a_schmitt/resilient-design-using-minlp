/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*    This file is part of the program kresilience                           */
/*                                                                           */
/*    Copyright (C) 2020-2021 Andreas Schmitt, Marc Pfetsch                  */
/*                                                                           */
/*                                                                           */
/*    Based on SCIP --- Solving Constraint Integer Programs                  */
/*       SCIP is distributed under the terms of the SCIP Academic Licence,   */
/*       see file COPYING in the SCIP distribution.                          */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**@file   min2dcube.h
 * @brief  provides functions to minimize a cubic polynomial in up to two variables over a box
 * @author Andreas Schmitt
 *
 * compile with flags: -lgsl
 */

/*--+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0----+----1----+----2*/

#ifndef __min2dcube_H__
#define __min2dcube_H__

#include "scip/scip.h"
#include <gsl/gsl_poly.h>

extern
/* compute the minimum of the quadratic polynomial cxx * x^2 + cx * x over the intervall [lbx, ubx]. For optx != NULL
   save an optimal point */
SCIP_Real calcMin1dQuad(
   SCIP_Real             lbx,
   SCIP_Real             ubx,
   SCIP_Real             cxx,
   SCIP_Real             cx,
   SCIP_Real*            optx
   );

extern
/* compute the maximum of the quadratic polynomial cxx * x^2 + cx * x over the intervall [lbx, ubx]. For optx != NULL
   save an optimal point */
SCIP_Real calcMax1dQuad(
   SCIP_Real             lbx,
   SCIP_Real             ubx,
   SCIP_Real             cxx,
   SCIP_Real             cx,
   SCIP_Real*            optx
   );

extern
/* compute the minimum of the cubic polynomial cxxx * x^3 + cxx * x^2 + cx * x over the intervall [lbx, ubx]. For optx
   != NULL save an optimal point */
SCIP_Real calcMin1dCube(
   SCIP_Real             lbx,
   SCIP_Real             ubx,
   SCIP_Real             cxxx,
   SCIP_Real             cxx,
   SCIP_Real             cx,
   SCIP_Real*            optx
   );

extern
/* compute the maximum of the cubic polynomial cxxx * x^3 + cxx * x^2 + cx * x over the intervall [lbx, ubx]. For optx
   != NULL save an optimal point */
SCIP_Real calcMax1dCube(
   SCIP_Real             lbx,
   SCIP_Real             ubx,
   SCIP_Real             cxxx,
   SCIP_Real             cxx,
   SCIP_Real             cx,
   SCIP_Real*            optx
   );

extern
/* compute the minimum of the quadratic polynomial in two dimensions cxx * x^2 + cxy * x * y + cyy * y^2 + cx * x + cy *
   y over the box [lbx, ubx]x[lby, uby]. For optx and opty != NULL save an optimal point */
SCIP_Real calcMin2dQuad(
   SCIP_Real             lbx,
   SCIP_Real             ubx,
   SCIP_Real             lby,
   SCIP_Real             uby,
   SCIP_Real             cxx,
   SCIP_Real             cxy,
   SCIP_Real             cyy,
   SCIP_Real             cx,
   SCIP_Real             cy,
   SCIP_Real*            optx,
   SCIP_Real*            opty
   );

extern
/* compute the maximum of the quadratic polynomial in two dimensions cxx * x^2 + cxy * x * y + cyy * y^2 + cx * x + cy *
   y over the box [lbx, ubx]x[lby, uby]. For optx and opty != NULL save an optimal point */
SCIP_Real calcMax2dQuad(
   SCIP_Real             lbx,
   SCIP_Real             ubx,
   SCIP_Real             lby,
   SCIP_Real             uby,
   SCIP_Real             cxx,
   SCIP_Real             cxy,
   SCIP_Real             cyy,
   SCIP_Real             cx,
   SCIP_Real             cy,
   SCIP_Real*            optx,
   SCIP_Real*            opty
   );

extern
/* compute the minimum of the cubic polynomial in two dimensions
   cxxx * x^3 + cxxy * x^2 * y + cxyy * x * y^2 + cyyy * y^3 + cxx * x^2 + cxy * x * y + cyy * y^2 + cx * x + cy * y
   over the box [lbx, ubx]x[lby, uby]. For optx and opty != NULL save an optimal point */
SCIP_Real calcMin2dCube(
   SCIP_Real             lbx,
   SCIP_Real             ubx,
   SCIP_Real             lby,
   SCIP_Real             uby,
   SCIP_Real             cxxx,
   SCIP_Real             cxxy,
   SCIP_Real             cxyy,
   SCIP_Real             cyyy,
   SCIP_Real             cxx,
   SCIP_Real             cxy,
   SCIP_Real             cyy,
   SCIP_Real             cx,
   SCIP_Real             cy,
   SCIP_Real*            optx,
   SCIP_Real*            opty,
   SCIP_Bool*            success
   );

#endif
