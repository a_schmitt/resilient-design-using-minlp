/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*    This file is part of the program kresilience                           */
/*                                                                           */
/*    Copyright (C) 2020-2021 Andreas Schmitt, Marc Pfetsch                  */
/*                                                                           */
/*                                                                           */
/*    Based on SCIP --- Solving Constraint Integer Programs                  */
/*       SCIP is distributed under the terms of the SCIP Academic Licence,   */
/*       see file COPYING in the SCIP distribution.                          */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**@file   message_prefix.c
 * @brief  prefix message handler
 * @author Andreas Schmitt
 */

/*---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0----+----1----+----2*/

#include "message_prefix.h"
#include "nlpi/nlpi.h"
#include "scip/debug.h"
#include "scip/struct_scip.h"
#include "scip/struct_set.h"


struct SCIP_MessagehdlrData
{
   char*                 prefix;             /**< prefix which is put before each new line */
};

/*
 * Local methods
 */

/** prints a message to the given file stream and writes the same messate to the log file */
static
void logMessage(
   FILE*                 file,               /**< file stream to print message into */
   const char*           msg                 /**< message to print (or NULL to flush) */
   )
{
   if ( msg != NULL )
      fputs(msg, file);
   fflush(file);
}

/*
 * Callback methods of message handler
 */

/** warning message print method of message handler */
static
SCIP_DECL_MESSAGEWARNING(messageWarningPrefix)
{  /*lint --e{715}*/
   SCIP_MESSAGEHDLRDATA* data;

   data = SCIPmessagehdlrGetData(messagehdlr);

   if ( msg != NULL && msg[0] != '\0' && msg[0] != '\n' )
   {
      fputs("WARNING: ", file);
      logMessage(file, data->prefix);
   }

   logMessage(file, msg);
}

/** dialog message print method of message handler */
static
SCIP_DECL_MESSAGEDIALOG(messageDialogPrefix)
{  /*lint --e{715}*/
   logMessage(file, msg);
}

/** info message print method of message handler */
static
SCIP_DECL_MESSAGEINFO(messageInfoPrefix)
{  /*lint --e{715}*/
   SCIP_MESSAGEHDLRDATA* data;

   data = SCIPmessagehdlrGetData(messagehdlr);

   if ( msg != NULL && msg[0] != '\0' && msg[0] != '\n' )
      logMessage(file, data->prefix);
   logMessage(file, msg);
}

/** destructor of message handler to free message handler data */
static
SCIP_DECL_MESSAGEHDLRFREE(messageFreePrefix)
{  /*lint --e{715}*/
   SCIP_MESSAGEHDLRDATA* data;

   data = SCIPmessagehdlrGetData(messagehdlr);
   BMSfreeMemoryArrayNull(&(data->prefix));
   BMSfreeMemorySize(&data);

   return SCIP_OKAY;
}

/** Create Prefix message handler. To free the message handler use SCIPmessagehdlrRelease(). */
static
SCIP_RETCODE SCIPcreateMessagehdlrPrefix(
   SCIP_MESSAGEHDLR**    messagehdlr,        /**< pointer to store message handler */
   SCIP_Bool             bufferedoutput,     /**< should the output be buffered up to the next newline? */
   const char*           filename,           /**< name of log file, or NULL (stdout) */
   const char*           prefix,             /**< prefix which is put before each new line */
   SCIP_Bool             quiet               /**< should screen messages be suppressed? */
   )
{
   SCIP_MESSAGEHDLRDATA* data;

   SCIP_ALLOC( BMSallocMemorySize(&data, sizeof(SCIP_MESSAGEHDLRDATA)) );
   SCIP_ALLOC( BMSallocMemoryArray(&(data->prefix), SCIP_MAXSTRLEN) ); /*lint !e506*/

   data->prefix[0] = '\0';
   (void) SCIPsnprintf(data->prefix, SCIP_MAXSTRLEN, "%s", prefix);

   /* create message handler */
   SCIP_CALL( SCIPmessagehdlrCreate(messagehdlr, bufferedoutput, filename, quiet,
         messageWarningPrefix, messageDialogPrefix, messageInfoPrefix,
         messageFreePrefix, data) );

   return SCIP_OKAY;
}

/** Create Prefix message handler directly in scip->messagehdlr. Frees previous message handler. */
SCIP_RETCODE SCIPsetMessagehdlrPrefix(
   SCIP*                 scip,               /**< SCIP data structure */
   const char*           prefix              /**< prefix which is put before each new line */
   )
{
   int i;

   SCIP_CALL( SCIPcheckStage(scip, "SCIPsetMessagehdlr", TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE) );

   assert(scip != NULL);
   assert(scip->set != NULL);
   assert(scip->set->nlpis != NULL || scip->set->nnlpis == 0);

   SCIP_CALL( SCIPmessagehdlrRelease(&scip->messagehdlr) );
   assert(scip->messagehdlr == NULL);

   SCIP_CALL( SCIPcreateMessagehdlrPrefix(&scip->messagehdlr, TRUE, NULL, prefix, FALSE) );

   /* update message handler in NLP solver interfaces */
   for( i = 0; i < scip->set->nnlpis; ++i )
   {
      assert(scip->set->nlpis[i] != NULL);

      SCIP_CALL( SCIPnlpiSetMessageHdlr(scip->set->nlpis[i], scip->messagehdlr) );
   }

   return SCIP_OKAY;
}
