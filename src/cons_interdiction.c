/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*    This file is part of the program kresilience                           */
/*                                                                           */
/*    Copyright (C) 2020-2021 Andreas Schmitt, Marc Pfetsch                  */
/*                                                                           */
/*                                                                           */
/*    Based on SCIP --- Solving Constraint Integer Programs                  */
/*       SCIP is distributed under the terms of the SCIP Academic Licence,   */
/*       see file COPYING in the SCIP distribution.                          */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**@file   cons_interdiction.c
 * @brief  constraint handler for interdiction constraints of the form y^t x >= 1 forall y feasible for a subscip
 * @author Andreas Schmitt
 */

/*---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0----+----1----+----2*/

#include <assert.h>
#include "cons_interdiction.h"
#include "scip/scipdefplugins.h"
#include <string.h>
#include "message_prefix.h"


/* fundamental constraint handler properties */
#define CONSHDLR_NAME          "interdiction"
#define CONSHDLR_DESC          "interdiction constraint handler"
#define CONSHDLR_ENFOPRIORITY      -9999999 /**< priority of the constraint handler for constraint enforcing */
#define CONSHDLR_CHECKPRIORITY     -9999999/**< priority of the constraint handler for checking feasibility */
#define CONSHDLR_EAGERFREQ          100 /**< frequency for using all instead of only the useful constraints in separation,
                                         *   propagation and enforcement, -1 for no eager evaluations, 0 for first only */
#define CONSHDLR_NEEDSCONS         TRUE /**< should the constraint handler be skipped, if no constraints are available? */

/* optional constraint handler properties */
#define CONSHDLR_SEPAPRIORITY         10000 /**< priority of the constraint handler for separation */
#define CONSHDLR_SEPAFREQ             1     /**< frequency for separating cuts; zero means to separate only in the root node */
#define CONSHDLR_DELAYSEPA            TRUE  /**< should separation method be delayed, if other separators found cuts? */

#define DEFAULT_RESOLVENODELIMIT   0LL /**< extra nodes to be checked for resolving scipint */
#define DEFAULT_CUTLIMIT          -1LL /**< max number of cuts which are allowed to add counted from the last transform, otherwise we branch (-1 unbounded) */

/** constraint data for interdiction constraints */
struct SCIP_ConsData
{
   int nvars;                                /**< number of variables in vars_x and vars_y */
   SCIP_VAR** vars_x;                        /**< variables in scip */
   SCIP_VAR** vars_y;                        /**< variables in subscip, which are interdicted */
   SCIP* subscip;                            /**< subscip modeling the system, which has to be interdicted */

   SCIP_Bool** subscipsols;                  /**< found solutions of subscip are saved here. Each subscipsol[i] gives values
                                                corresponding to the variables vars_y. */
   int nsubscipsols;                         /**< number of solutions saved */
   int sizesubscipsols;                      /**< current allocation size of subscipsols */
   SCIP_Bool* subscipsoladded;               /**< marks when a solution was added to the problem by
                                                consInterdictionAddSubscipSolsAsConstraints(...) */
   SCIP_Bool** scipsols;                     /**< found solutions of scip which are feasible for this constraint. Each
                                                scipsol[i] gives values corresponding to the variables vars_x. */
   int nscipsols;                            /**< number of solutions saved */
   int sizescipsols;                         /**< current allocation size of scipsols */

   int ncutsadded;                           /**< number of cuts added to the problem since the last problem transformation */

   SCIP_Bool usereopt;                       /**< should reoptimization be used to solve the subscip */
   SCIP_Bool usefix;                         /**< if reoptimization is not used, should variable fixing be used */

   int* ncovers;                             /**< counting array, which gives the number of separated solutions of scip_feas which contain element i */
   int* coverrankingtovar;                   /**< mirrors ranking onto the variables according to the number of solutions covered, ranking is decreasing! */
   int* vartocoverranking;                   /**< mirrors variable index in vars_x onto the cover ranking */

   int nsubscipsolves;                       /**< number of times subscip was optimized */
   SCIP_Real timesubscipsolves;              /**< total time for solving subscip */
   SCIP_Longint nnodessubscip;               /**< total nodes occured for solving subscip */
};


/** Adds a solution of scip given by vals_x to the solution storage of the constraint. Checks if there exists already
    a domination and replaces or does not add accordingly.
 */
static
SCIP_RETCODE saveScipSol(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONS*            cons,               /**< interdiction constraint */
   SCIP_Real*            vals_x              /**< values of scip solution */
   )
{
   int i;
   int j;
   SCIP_CONSDATA* consdata;

   assert(scip != NULL);
   assert(cons != NULL);
   assert(vals_x != NULL);

   consdata = SCIPconsGetData(cons);
   assert(consdata != NULL);

   /* we check first, whether we have a new solution, this is only the case if there exists no already saved one which
      is smaller */
   for( i = 0; i < consdata->nscipsols; ++i )
   {
      /* x dominates y if x <= y */
      SCIP_Bool solDominatesStorage = TRUE;
      SCIP_Bool storageDominatesSol = TRUE;
      for( j = 0; j < consdata->nvars && (solDominatesStorage || storageDominatesSol); ++j )
      {
         if( vals_x[j] <= 0.5 && consdata->scipsols[i][j] )
            storageDominatesSol = FALSE;
         else if( vals_x[j] >= 0.5 && !consdata->scipsols[i][j] )
            solDominatesStorage = FALSE;
      }
      /* there is already a better or the same solution stored */
      if( storageDominatesSol )
         return SCIP_OKAY;
      /* vals_x is better then the current solution, replace it */
      if( solDominatesStorage )
         break;
   }

   if( i >= consdata->nscipsols )
   {
      /* reallocate scipsols if necessary */
      if( consdata->nscipsols >= consdata->sizescipsols)
      {
         consdata->sizescipsols = SCIPcalcMemGrowSize(scip, consdata->sizescipsols+1);
         SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &consdata->scipsols, consdata->nscipsols, consdata->sizescipsols) );
      }
      SCIP_CALL( SCIPallocBlockMemoryArray(scip, &consdata->scipsols[i], consdata->nvars) );
      consdata->nscipsols++;
   }

   /* copy solution */
   for( j = 0; j < consdata->nvars; ++j )
   {
      consdata->scipsols[i][j] = vals_x[j] > 0.5;
   }

   return SCIP_OKAY;
}


/** Given a vector vals_x check whether there exists a saved and checked solution which is smaller. Then vals_x is also
    feasible.
 */
static
SCIP_RETCODE findScipSolSmaller(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONS*            cons,               /**< interdiction constraint */
   SCIP_Real*            vals_x,             /**< values of interdiction */
   SCIP_Bool*            found               /**< true if there exists a subscip sol response */
   )
{
   int i;
   int j;
   SCIP_CONSDATA* consdata;

   assert(scip != NULL);
   assert(cons != NULL);
   assert(vals_x != NULL);
   assert(found != NULL);

   consdata = SCIPconsGetData(cons);
   assert(consdata != NULL);

   *found = FALSE;

   for( i = 0; i < consdata->nscipsols; ++i )
   {
      for( j = 0; j < consdata->nvars && (vals_x[j] >= 0.5 || !consdata->scipsols[i][j] ); ++j );
      if( j == consdata->nvars )
      {
         *found = TRUE;
         return SCIP_OKAY;
      }
   }

   return SCIP_OKAY;
}



/** Adds a solution of subscip given by vals_y to the solution storage of the constraint. Checks if there exists already
    a domination and replaces or does not add accordingly.
 */
static
SCIP_RETCODE saveSubscipSol(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONS*            cons,               /**< interdiction constraint */
   SCIP_Real*            vals_y              /**< values of subscip solution */
   )
{
   int i;
   int j;
   SCIP_CONSDATA* consdata;

   assert(scip != NULL);
   assert(cons != NULL);
   assert(vals_y != NULL);

   consdata = SCIPconsGetData(cons);
   assert(consdata != NULL);

   /* we check first, whether we have a new solution */
   for( i = 0; i < consdata->nsubscipsols; ++i )
   {
      SCIP_Bool solDominatesStorage = TRUE;
      SCIP_Bool storageDominatesSol = TRUE;
      for( j = 0; j < consdata->nvars && (solDominatesStorage || storageDominatesSol); ++j )
      {
         if( vals_y[j] <= 0.5 && consdata->subscipsols[i][j] )
            storageDominatesSol = FALSE;
         else if( vals_y[j] >= 0.5 && !consdata->subscipsols[i][j] )
            solDominatesStorage = FALSE;
      }
      if( storageDominatesSol )
         return SCIP_OKAY;
      if( solDominatesStorage )
         break;
   }

   if( i >= consdata->nsubscipsols )
   {
      /* reallocate subscipsols if necessary */
      if( consdata->nsubscipsols >= consdata->sizesubscipsols)
      {
         consdata->sizesubscipsols = SCIPcalcMemGrowSize(scip, consdata->sizesubscipsols+1);
         SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &consdata->subscipsols, consdata->nsubscipsols, consdata->sizesubscipsols) );
         SCIP_CALL( SCIPreallocBlockMemoryArray(scip, &consdata->subscipsoladded, consdata->nsubscipsols, consdata->sizesubscipsols) );
      }
      SCIP_CALL( SCIPallocBlockMemoryArray(scip, &consdata->subscipsols[i], consdata->nvars) );
      consdata->nsubscipsols++;
   }

   /* copy solution */
   for( j = 0; j < consdata->nvars; ++j )
   {
      consdata->subscipsols[i][j] = vals_y[j] > 0.5;
   }
   consdata->subscipsoladded[i] = FALSE;

   return SCIP_OKAY;
}

/** Given a vector vals_x check whether there exists a saved solution which is a valid response, i.e., vals_x\T vals_y =
    0. If found and pointer != NULL, we return it in vals_y.
 */
static
SCIP_RETCODE findSubscipSolResponse(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONS*            cons,               /**< interdiction constraint */
   SCIP_Real*            vals_x,             /**< values of interdiction */
   SCIP_Real*            vals_y,             /**< values of subscip solution, can be NULL if not interested in */
   SCIP_Bool*            found               /**< true if there exists a subscip sol response */
   )
{
   int i;
   int j;
   SCIP_CONSDATA* consdata;

   assert(scip != NULL);
   assert(cons != NULL);
   assert(vals_x != NULL);
   assert(found != NULL);

   consdata = SCIPconsGetData(cons);
   assert(consdata != NULL);

   *found = FALSE;

   for( i = 0; i < consdata->nsubscipsols; ++i )
   {
      for( j = 0; j < consdata->nvars && (vals_x[j] <= 0.5 || !consdata->subscipsols[i][j] ); ++j );
      if( j == consdata->nvars )
      {
         *found = TRUE;
         if( vals_y != NULL )
         {
            for( j = 0; j < consdata->nvars; ++j )
               vals_y[j] = consdata->subscipsols[i][j] ? 1.0 : 0.0;
         }
      }
   }

   return SCIP_OKAY;
}


/** Returns for a given cons and a given rank the variable which has this covering rank.
 */
int consInterdictionGetIndexRanking(
   SCIP_CONS*            cons,               /**< interdiction constraint */
   int                   i                   /**< rank we query for */
   )
{
   SCIP_CONSDATA* consdata;

   assert(cons != NULL);
   consdata = SCIPconsGetData(cons);
   assert(consdata != NULL);

   return consdata->coverrankingtovar[i];
}


/** Update the cover rank of the variables according to a new solution.
 */
static
SCIP_RETCODE updateCoverRanking(
   SCIP_CONS*            cons,               /**< interdiction constraint */
   SCIP_Real*            vals_y              /**< values of subscip solution */
   )
{
   int i;
   int j;
   int varrankingi;
   SCIP_CONSDATA* consdata;

   assert(cons != NULL);
   assert(vals_y != NULL);

   consdata = SCIPconsGetData(cons);
   assert(consdata != NULL);

   for( i = 0; i < consdata->nvars; ++i )
   {
      varrankingi = consdata->coverrankingtovar[i];

      if( vals_y[varrankingi] > 0.5 )
      {
         consdata->ncovers[varrankingi]++;
         for( j = i; j > 0 &&  consdata->ncovers[varrankingi] > consdata->ncovers[consdata->coverrankingtovar[j-1]]; --j);
         if( j < i )
         {
            consdata->vartocoverranking[varrankingi] = j;
            consdata->vartocoverranking[consdata->coverrankingtovar[j]] = i;

            consdata->coverrankingtovar[i] = consdata->coverrankingtovar[j];
            consdata->coverrankingtovar[j] = varrankingi;
         }
      }
   }

   return SCIP_OKAY;
}


/** Gets an interdicttion constraint and the scip it belongs. For the constraint we check for the saved solutions vals_y
    and add them as new constraints x^t y >= 1 to scip. Sets the counter for the saved solutions to 0 afterwards.
 */
SCIP_RETCODE consInterdictionAddSubscipSolsAsConstraints(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONS*            cons                /**< interdiction constraint */
   )
{
   SCIP_CONS* consCut;
   int i;
   int j;
   SCIP_CONSDATA* consdata;

   assert(scip != NULL);
   assert(cons != NULL);
   assert(SCIPconsGetHdlr(cons) != NULL);
   assert(SCIPconshdlrGetName(SCIPconsGetHdlr(cons)) != NULL);
   assert(strcmp(SCIPconshdlrGetName(SCIPconsGetHdlr(cons)), CONSHDLR_NAME) == 0);

   consdata = SCIPconsGetData(cons);
   assert(consdata != NULL);
   for( i = 0; i < consdata->nsubscipsols; ++i )
   {
      if( !consdata->subscipsoladded[i] )
      {
         SCIP_CALL( SCIPcreateConsBasicSetcover(scip, &consCut, "cut", 0, NULL) );
         for( j = 0; j < consdata->nvars; ++j )
         {
            if( consdata->subscipsols[i][j] )
            {
               SCIP_CALL( SCIPaddCoefSetppc(scip, consCut, consdata->vars_x[j]) );
            }
         }
         SCIP_CALL( SCIPaddCons(scip, consCut) );
         SCIP_CALL( SCIPreleaseCons(scip, &consCut) );
         consdata->subscipsoladded[i] = TRUE;
      }
   }

   return SCIP_OKAY;
}


/*
 * Local methods
 */

/* creates constraintdata and stores the values */
static
SCIP_RETCODE consdataCreate(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONSDATA**       consdata,           /**< constraint data */
   int                   nvars,              /**< number of variables which have to be treated */
   SCIP_VAR**            vars_x,             /**< variables in scip */
   SCIP_VAR**            vars_y,             /**< variables in subscip */
   SCIP*                 subscip,            /**< SCIP data structure for the subproblem */
   SCIP_Bool             usereopt,           /**< should reoptimization be used to solve the subscip */
   SCIP_Bool             usefix              /**< if reoptimization is not used, should variable fixing be used */
   )
{
   int i;
   SCIP_Bool infeasible;

   assert(scip != NULL);
   assert(consdata != NULL);
   assert(nvars > 0);
   assert(vars_x != NULL);
   assert(vars_y != NULL);
   assert(subscip != NULL);

   for( i = 0; i < nvars; i++ )
   {
      assert(vars_x[i] != NULL);
      assert(vars_y[i] != NULL);
   }

   /* alloc block memory */
   SCIP_CALL( SCIPallocBlockMemory(scip, consdata) );

   SCIP_CALL( SCIPduplicateBlockMemoryArray(scip, &(*consdata)->vars_x, vars_x, nvars) );
   SCIP_CALL( SCIPduplicateBlockMemoryArray(scip, &(*consdata)->vars_y, vars_y, nvars) );

   (*consdata)->nvars = nvars;
   (*consdata)->subscip = subscip;
   (*consdata)->nsubscipsolves = 0;
   (*consdata)->timesubscipsolves = 0.0;
   (*consdata)->nnodessubscip = 0;
   (*consdata)->usereopt = usereopt;
   (*consdata)->usefix = usefix;

   SCIP_CALL( SCIPsetObjsense(subscip, SCIP_OBJSENSE_MINIMIZE) );
   for( i = 0; i < nvars; ++i )
   {
      SCIP_CALL( SCIPchgVarObj(subscip, (*consdata)->vars_y[i], 1.0) );
   }

   /* turn all linking variables to binary and capture them */
   for( i = 0; i < nvars; ++i )
   {
      SCIP_CALL( SCIPcaptureVar(scip, vars_x[i]) );
      SCIP_CALL( SCIPcaptureVar(subscip, vars_y[i]) );

      SCIP_CALL( SCIPchgVarUb(scip, vars_x[i], 1.0) );
      SCIP_CALL( SCIPchgVarLb(scip, vars_x[i], 0.0) );
      if( !SCIPvarIsBinary(vars_x[i]) )
      {
         SCIP_CALL( SCIPchgVarType(scip, vars_x[i], SCIP_VARTYPE_BINARY, &infeasible) );
         if( infeasible )
            return SCIP_ERROR;

      }

      SCIP_CALL( SCIPchgVarUb(subscip, vars_y[i], 1.0) );
      SCIP_CALL( SCIPchgVarLb(subscip, vars_y[i], 0.0) );
      if( !SCIPvarIsBinary(vars_y[i]) )
      {
         SCIP_CALL( SCIPchgVarType(subscip, vars_y[i], SCIP_VARTYPE_BINARY, &infeasible) );
         if( infeasible )
            return SCIP_ERROR;

      }
   }

   SCIP_CALL( SCIPsetMessagehdlrPrefix(subscip, "\t\t\t\t\tsubscip: ") );
   SCIPsetMessagehdlrQuiet(subscip, TRUE);

   if( usereopt )
      SCIP_CALL( SCIPenableReoptimization(subscip, TRUE) );

   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &(*consdata)->ncovers, nvars) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &(*consdata)->coverrankingtovar, nvars) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &(*consdata)->vartocoverranking, nvars) );

   for( i = 0; i < nvars; ++i )
   {

      (*consdata)->ncovers[i] = 0;
      (*consdata)->coverrankingtovar[i] = i;
      (*consdata)->vartocoverranking[i] = i;
   }

   (*consdata)->nsubscipsols = 0;
   (*consdata)->sizesubscipsols = 16;

   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &(*consdata)->subscipsols, (*consdata)->sizesubscipsols) );
   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &(*consdata)->subscipsoladded, (*consdata)->sizesubscipsols) );

   (*consdata)->nscipsols = 0;
   (*consdata)->sizescipsols = 16;

   SCIP_CALL( SCIPallocBlockMemoryArray(scip, &(*consdata)->scipsols, (*consdata)->sizescipsols) );

   return SCIP_OKAY;
}

/* given a integral vector y add the set covering constraint sum y\T x >= 1.0 as
   an lp row
*/
static
SCIP_RETCODE addRowCut(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONS*            cons,               /**< constraint */
   SCIP_Real*            vals_y,             /**< values of cover */
   SCIP_SOL*             sol,                /**< current solution, or NULL if LP solution should used */
   SCIP_RESULT*          result              /**< result pointer */
   )
{
   int i;
   SCIP_ROW*  row;
   SCIP_CONSDATA* consdata;
   SCIP_Bool infeasible;

   assert(scip != NULL);
   assert(cons != NULL);
   assert(vals_y != NULL);
   assert(result != NULL);

   consdata = SCIPconsGetData(cons);
   assert(consdata != NULL);

   //  	local = False, modifiable = False, removable = True
   SCIP_CALL( SCIPcreateEmptyRowCons(scip, &row, cons, "coverCut", 1.0, SCIPinfinity(scip), FALSE, FALSE, TRUE) );
   SCIP_CALL( SCIPcacheRowExtensions(scip, row) );

   for( i = 0; i < consdata->nvars; ++i )
   {
      if( !SCIPisZero(scip, vals_y[i]) )
      {
         SCIP_CALL( SCIPaddVarToRow(scip, row, consdata->vars_x[i], vals_y[i]) );
      }
   }

   SCIP_CALL( SCIPflushRowExtensions(scip, row) );

#ifdef SCIP_DEBUG
   SCIP_CALL( SCIPprintRow(scip, row, NULL) );
#endif

   if( SCIPisCutEfficacious(scip, sol, row) )
   {
      SCIP_CALL( SCIPaddRow(scip, row, FALSE, &infeasible) );
      if( infeasible )
         *result = SCIP_CUTOFF;
      else
         *result = SCIP_SEPARATED;
   }
   SCIP_CALL( SCIPreleaseRow(scip, &row) );

   return SCIP_OKAY;
}

/* given a integral vector y add the set covering constraint y\T x >= 1.0 as as
   a cons_setppc constraint
*/
static
SCIP_RETCODE addConsCut(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONS*            cons,               /**< constraint */
   SCIP_Real*            vals_y              /**< values of interdiction */
   )
{
   int i;
   SCIP_CONS*  consCut;
   SCIP_CONSDATA* consdata;

   assert(scip != NULL);
   assert(cons != NULL);
   assert(vals_y != NULL);

   consdata = SCIPconsGetData(cons);
   assert(consdata != NULL);

   SCIP_CALL( SCIPcreateConsBasicSetcover(scip, &consCut, "cut", 0, NULL) );
   for( i = 0; i < consdata->nvars; ++i )
   {
      if( vals_y[i] > 0.5 )
      {
         SCIP_CALL( SCIPaddCoefSetppc(scip, consCut, consdata->vars_x[i]) );
      }
   }
   SCIP_CALL( SCIPaddCons(scip, consCut) );
   SCIP_CALL( SCIPreleaseCons(scip, &consCut) );

   return SCIP_OKAY;
}


/* given a feasible solution y* to subscip, we try to reduce the number of ones
   in it. We solve (y*)^T y + (n+1) (1-y*)^T y over subscip, with an objlimit of n+0.5.
   This way only solutsion with y with y <= y* will be accepted as solution candidates.
*/
static
SCIP_RETCODE SCIPimproveCutResolve(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONS*            cons,               /**< constraint */
   SCIP_Real*            vals_y
   )
{
   SCIP_CONSDATA* consdata;
   SCIP* subscip;
   int i;
   SCIP_STATUS subscip_status;
   SCIP_Real* coefs;
   SCIP_Longint nodelimit;
   SCIP_Real timelimit;

   assert(scip != NULL);
   assert(cons != NULL);
   assert(vals_y != NULL);

   consdata = SCIPconsGetData(cons);
   assert(consdata != NULL);

   subscip = consdata->subscip;

   /* check node and timelimit */
   SCIP_CALL( SCIPgetLongintParam(scip, "cons_interdiction/nodesresolveimprove", &nodelimit) );
   if( nodelimit == 0 )
      return SCIP_OKAY;

   SCIP_CALL( SCIPgetRealParam(scip, "limits/time", &timelimit) );
   if( !SCIPisInfinity(scip, timelimit) )
      timelimit -= SCIPgetSolvingTime(scip);
   if( timelimit < 1.0 )
   {
      SCIPdebugMsg(scip, "aborted improveCutResolve, not enough time left. (%g)\n", timelimit);
      return SCIP_OKAY;
   }


   /* set objective or fix bounds in subscip */
   if( consdata->usereopt )
   {
      /* together with the objlimit below, we accept only new solutions yy with yy <= y^old */
      SCIP_CALL( SCIPallocBufferArray(scip, &coefs, consdata->nvars) );
      for( i = 0; i < consdata->nvars; ++i )
      {
         coefs[i] = vals_y[i] > 0.5 ? 1.0 : consdata->nvars+1.0;
      }
      SCIP_CALL( SCIPchgReoptObjective(subscip, SCIP_OBJSENSE_MINIMIZE, consdata->vars_y, coefs, consdata->nvars) );
      SCIPfreeBuffer(scip, &coefs);
   }
   else
   {
      if( consdata->usefix )
      {
         /* only accept new solutions yy with yy <= vals_y */
         for( i = 0; i < consdata->nvars; ++i )
         {
            SCIP_CALL( SCIPchgVarLb(subscip, consdata->vars_y[i], 0.0) );
            SCIP_CALL( SCIPchgVarUb(subscip, consdata->vars_y[i], vals_y[i] > 0.5 ? 1.0 : 0.0) );
            SCIP_CALL( SCIPchgVarObj(subscip, consdata->vars_y[i], 1.0) );
         }
      }
      else
      {
         /* together with the objlimit below, we accept only new solutions yy with yy <= y^old */
         for( i = 0; i < consdata->nvars; ++i )
         {
            SCIP_CALL( SCIPchgVarObj(subscip, consdata->vars_y[i], vals_y[i] > 0.5 ? 1.0 : consdata->nvars+1.0) );
         }
      }
   }
   /* set limits */
   if( consdata->usereopt || !consdata->usefix )
      SCIP_CALL( SCIPsetObjlimit(subscip, consdata->nvars+0.5) );

   SCIP_CALL( SCIPsetRealParam(subscip, "limits/time", timelimit) );
   if( nodelimit >= 0 )
   {
      SCIP_CALL( SCIPsetLongintParam(subscip, "limits/nodes", nodelimit) );
   }
   SCIP_CALL( SCIPsetIntParam(subscip, "limits/solutions", -1) );

   SCIP_CALL( SCIPsolve(subscip) );

   /* update statistics */
   consdata->nsubscipsolves++;
   consdata->timesubscipsolves += SCIPgetSolvingTime(subscip);
   consdata->nnodessubscip += SCIPgetNNodes(subscip);

   subscip_status = SCIPgetStatus(subscip);
   switch( subscip_status )
   {
   case SCIP_STATUS_USERINTERRUPT:
      SCIPwarningMessage(scip, "User interrupt during subscip solving in improve cut resolve. Result may be infeasible.\n");
      SCIP_CALL( SCIPinterruptSolve(scip) );
      return SCIP_OKAY;
   case SCIP_STATUS_OPTIMAL:
   case SCIP_STATUS_NODELIMIT:
   case SCIP_STATUS_TIMELIMIT:
      break;
   case SCIP_STATUS_INFEASIBLE:
      SCIPerrorMessage("Improve cut returned infeasible!\n"); /* This is some kind of contradiction, since vals_y should have been a feasible solution */
      break;
   default:
      SCIPerrorMessage("subscip was solved to unsupported status during resolving.\n");
      return SCIP_ERROR;
   }

   /* a solution should be at least as good as the current one */
   if( SCIPgetNSols(subscip) >= 1 )
   {
      SCIP_CALL( SCIPgetSolVals(subscip, SCIPgetBestSol(subscip), consdata->nvars, consdata->vars_y, vals_y) );
   }

   if( consdata->usereopt )
   {
      SCIP_CALL( SCIPfreeReoptSolve(subscip) );
   }
   else
   {
      SCIP_CALL( SCIPfreeTransform(subscip) );
   }

   /* reset limits */
   SCIP_CALL( SCIPsetLongintParam(subscip, "limits/nodes", -1LL) );
   SCIP_CALL( SCIPsetRealParam(subscip, "limits/time", SCIPinfinity(subscip)) );
   SCIP_CALL( SCIPsetObjlimit(subscip, SCIPinfinity(subscip)) );

   return SCIP_OKAY;
}

/* given binary x search for a response y in subscip such that y \le 1 - x. This can be done either by optimizing over
   subscip with the objective values of y changed or the bounds of y changed.
 */
static
SCIP_RETCODE SCIPcomputeResponse(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONS*            cons,               /**< constraint */
   SCIP_Real*            vals_x,             /**< x values of solution */
   SCIP_Real*            vals_y,             /**< y values of possible found response, can be NULL if not interested in */
   SCIP_Bool*            found_response,     /**< if a response was found */
   SCIP_Bool*            aborted             /**< whether the solving of subscip was aborted, e.g. timelimit */
   )
{
   SCIP_CONSDATA* consdata;
   int i;
   SCIP* subscip;
   SCIP_STATUS subscip_status;
   SCIP_Real timelimit;

   assert(scip != NULL);
   assert(cons != NULL);
   assert(vals_x != NULL);
   assert(found_response != NULL);
   assert(aborted != NULL);

   *found_response = FALSE;
   *aborted = FALSE;

   /* check timelimit */
   SCIP_CALL( SCIPgetRealParam(scip, "limits/time", &timelimit) );
   if( !SCIPisInfinity(scip, timelimit) )
      timelimit -= SCIPgetSolvingTime(scip);
   if( timelimit <= 1.0 )
   {
      SCIPdebugMsg(scip, "not enough time left! (%g)\n", timelimit);
      *aborted = TRUE;
      return SCIP_OKAY;
   }

   /* search through the already computed solutions */
   SCIP_CALL( findSubscipSolResponse(scip, cons, vals_x, vals_y, found_response) );
   if( *found_response )
      return SCIP_OKAY;

   consdata = SCIPconsGetData(cons);
   assert(consdata != NULL);
   subscip = consdata->subscip;
   assert(subscip != NULL);

   /* adapt objective or variable bounds*/
   if( consdata->usereopt )
   {
      /* set objective such that x^t y is minimized */
      for( i = 0; i < consdata->nvars; ++i )
      {
         vals_x[i] = vals_x[i] >= 0.5 ? 1.0 : 0.0;
      }
      SCIP_CALL( SCIPchgReoptObjective(subscip, SCIP_OBJSENSE_MINIMIZE, consdata->vars_y, vals_x, consdata->nvars) );
   }
   else if( consdata->usefix )
   {
      /* directly set bounds on y such that y <= 1-x, i.e., x^t y <= 0 */
      for( i = 0; i < consdata->nvars; ++i )
      {
         SCIP_CALL( SCIPchgVarLb(subscip, consdata->vars_y[i], 0.0) );
         SCIP_CALL( SCIPchgVarUb(subscip, consdata->vars_y[i], vals_x[i] > 0.5 ? 0.0 : 1.0) );
         SCIP_CALL( SCIPchgVarObj(subscip, consdata->vars_y[i], 1.0) );
      }
   }
   else
   {
      /* set objective such that x^t y is minimized */
      for( i = 0; i < consdata->nvars; ++i )
      {
         SCIP_CALL( SCIPchgVarObj(subscip, consdata->vars_y[i], vals_x[i] >= 0.5 ? 1.0 : 0.0) );
      }
   }

   /* set limits */
   SCIP_CALL( SCIPsetRealParam(subscip, "limits/time", timelimit) );
   SCIP_CALL( SCIPsetIntParam(subscip, "limits/solutions", 1) );
   if( consdata->usereopt || !consdata->usefix )
      SCIP_CALL( SCIPsetObjlimit(subscip, 0.5) ); // TODO would a smaller objvalue make sense? Too small and feasible solutions were cut off...

   SCIP_CALL( SCIPsolve(subscip) );

   /* update statistics */
   consdata->nsubscipsolves++;
   consdata->timesubscipsolves += SCIPgetSolvingTime(subscip);
   consdata->nnodessubscip += SCIPgetNNodes(subscip);

   subscip_status = SCIPgetStatus(subscip);
   switch( subscip_status )
   {
   case SCIP_STATUS_USERINTERRUPT:
      SCIPwarningMessage(scip, "User interrupt during subscip solving in check feasibility. Result may be infeasible.\n");
      SCIP_CALL( SCIPinterruptSolve(scip) );
      /* fall through */
   case SCIP_STATUS_TIMELIMIT:
      /* we cant verify feasibility thus we return aborted */
      *aborted = TRUE;
      break;
   case SCIP_STATUS_INFEASIBLE:
   case SCIP_STATUS_OPTIMAL:
   case SCIP_STATUS_SOLLIMIT:
      break;
   default:
      printf("subscip was solved to unsupported status.\n");
      return SCIP_ERROR;
   }

   /* check whether we found a solution leading to a violated constraint */
   if( SCIPgetNSols(subscip) >= 1 && (SCIPgetSolOrigObj(subscip, SCIPgetBestSol(subscip)) <= 0.5 || (!consdata->usereopt && consdata->usefix)) )
   {
      *found_response = TRUE;

      if( vals_y != NULL )
      {
         SCIP_CALL( SCIPgetSolVals(subscip, SCIPgetBestSol(subscip), consdata->nvars, consdata->vars_y, vals_y) );
      }
   }

   /* free subscip */
   if( consdata->usereopt )
      SCIP_CALL( SCIPfreeReoptSolve(subscip) );
   else
      SCIP_CALL( SCIPfreeTransform(subscip) );

   return SCIP_OKAY;
}


/* check whether the given constraint is feasible, i.e., for a given binary
 * solution x does there exists y in subscip with y <= 1-x or equivalently
 * has the optimal solution to min x\T y s.t. y in subscip a value greater zero.
 *
 * If this is the case and we are inenfolp we treat it using a cut until a possible cutlimit is reached. Afterwards we
 * either return INFEASIBLE to branch or if all our binary variables are fixed CUTOFF.
 *
 * If the computation is aborted, e.g. timelimit or userinterrupt, we return *result = INFEASIBLE or BRANCHED.
 */
static
SCIP_RETCODE checkFeasibility(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONS*            cons,               /**< constraint */
   SCIP_SOL*             sol,                /**< current solution, or NULL if LP solution should used */
   SCIP_Bool             inenfolp,           /**< whether or not method was called in enfolp */
   SCIP_Bool             printreason,        /**< whether reason for infeasibility should be printed */
   SCIP_RESULT*          result              /**< result pointer */
   )
{
   SCIP_CONSDATA* consdata;
   int i;
   SCIP_Real* vals_x;
   SCIP_Real* vals_y = NULL;
   SCIP_Bool infeasible;
   SCIP_Bool feasible;
   SCIP_Bool aborted = FALSE;
   SCIP_STATUS status;

   assert(scip != NULL);
   assert(cons != NULL);
   assert(result != NULL);

   *result = SCIP_FEASIBLE;

   /* if scip is in solved stage, we dont check the given solution again, since we assume, that the solution was already
      checked by cons_interdiction. We also don't check, when the status of scip is nodelimit, which happens when
      cons_kresilient resolves scip_int. This saves time for projects involving cons_kresilient. Here a large number of
      problems are solved, such that this situation occurs often. The hopeful correct rational here is, that correctness
      of the solution depends on binary values which should be equal for solutions of the transformed and original
      problem. */
   status = SCIPgetStatus(scip);
   if( (SCIPgetStage(scip) == SCIP_STAGE_SOLVED || status == SCIP_STATUS_NODELIMIT) && !inenfolp )
      return SCIP_OKAY;

   consdata = SCIPconsGetData(cons);
   assert(consdata != NULL);

   SCIP_CALL( SCIPallocBufferArray(scip, &vals_x, consdata->nvars) );
   SCIP_CALL( SCIPgetSolVals(scip, sol, consdata->nvars, consdata->vars_x, vals_x) );

   SCIP_CALL( SCIPallocBufferArray(scip, &vals_y, consdata->nvars) );

   SCIP_CALL( findScipSolSmaller(scip, cons, vals_x, &feasible) );
   if( feasible )
   {
      infeasible = FALSE;
   }
   else
   {
      SCIP_CALL( SCIPcomputeResponse(scip, cons, vals_x, vals_y, &infeasible, &aborted) );
      if( !aborted && !infeasible )
      {
         SCIP_CALL( saveScipSol(scip, cons, vals_x) );
      }
   }

   if( aborted )
   {
      if( inenfolp )
      {
         /* as we most likely hit the time limit or an interrupt, we want the main scip to get to a stage to confirm
          * this. In order to terminate correctly, we create a "branching" with only one child node that is a copy of
          * the focusnode.
          */
         SCIP_NODE* child;
         SCIP_CALL( SCIPcreateChild(scip, &child, 0.0, SCIPgetLocalTransEstimate(scip)) );
         *result = SCIP_BRANCHED;
      }
      else
      {
         *result = SCIP_INFEASIBLE;
      }
   }
   else if( infeasible )
   {
      *result = SCIP_INFEASIBLE;

      if( inenfolp )
      {
         SCIP_Longint cutlimit;


         SCIP_CALL( SCIPgetLongintParam(scip, "cons_interdiction/cutlimit", &cutlimit) );
         /* add cut if the cut limit is not exceeded */
         if( inenfolp && (cutlimit == -1 || consdata->ncutsadded < cutlimit) )
         {
            SCIP_CALL( SCIPimproveCutResolve(scip, cons, vals_y) );

            SCIP_CALL( addConsCut(scip, cons, vals_y) );

            *result = SCIP_CONSADDED;

            consdata->ncutsadded++;
         }
         /* if the cut limit is exceeded and one of the variables is unfixed we just return infeasible and let scip branch, if all are fixed, we return cutoff */
         else
         {
            for( i = 0; i < consdata->nvars && (SCIPvarGetLbLocal(consdata->vars_x[i]) > 0.5 || SCIPvarGetUbLocal(consdata->vars_x[i]) < 0.5); ++i );
            if( i == consdata->nvars )
               *result = SCIP_CUTOFF;
         }
      }

      SCIP_CALL( saveSubscipSol(scip, cons, vals_y) );
      SCIP_CALL( updateCoverRanking(cons, vals_y) );

   }

   if( printreason )
   {
      if( aborted )
      {
         SCIPinfoMessage(scip, NULL, "%s: can't verify feasibility of solution due to aborted subscip solving.\n", SCIPconsGetName(cons));
      }
      else if( infeasible )
      {
         SCIPinfoMessage(scip, NULL, "%s: violated by subscip solution y", SCIPconsGetName(cons));
         for( i = 0; i < consdata->nvars; ++i )
         {
            if( vals_y[i] > 0.5 )
            {
               SCIPinfoMessage(scip, NULL, " %s", SCIPvarGetName(consdata->vars_y[i]));
            }
         }
         SCIPinfoMessage(scip, NULL, ".\n");
      }
   }

   SCIPfreeBuffer(scip, &vals_y);
   SCIPfreeBuffer(scip, &vals_x);

   return SCIP_OKAY;
}


/* checks for the current solution x whether there exists a y in subscip
   suchthat x\T y < 1. If this is the case we add the cut x\T y >= 1.
 */
static
SCIP_RETCODE separateCoverInequality(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONS*            cons,               /**< constraints to process */
   SCIP_SOL*             sol,                /**< solution to separate (NULL for the LP solution) */
   SCIP_RESULT*          result              /**< pointer to store the result (should be initialized) */
   )
{
   SCIP_CONSDATA* consdata;
   int i;
   SCIP* subscip;
   SCIP_STATUS subscip_status;
   SCIP_Real* vals_x;
   SCIP_Real timelimit;
   SCIP_Longint cutlimit;

   assert(scip != NULL);
   assert(cons != NULL);
   assert(result != NULL);

   consdata = SCIPconsGetData(cons);
   assert(consdata != NULL);

   subscip = consdata->subscip;

   /* check timelimit */
   SCIP_CALL( SCIPgetRealParam(scip, "limits/time", &timelimit) );
   if( !SCIPisInfinity(scip, timelimit) )
      timelimit -= SCIPgetSolvingTime(scip);
   if( timelimit <= 1.0 )
   {
      SCIPdebugMsg(scip, "aborted separatedCoverInequality, not enough time left. (%g)\n", timelimit);
      return SCIP_OKAY;
   }

   /* check cut limit */
   SCIP_CALL( SCIPgetLongintParam(scip, "cons_interdiction/cutlimit", &cutlimit) );
   if( cutlimit >= 0 && (cutlimit - consdata->ncutsadded) < 0 )
      return SCIP_OKAY;

   /* adapt objective */
   if( consdata->usereopt )
   {
      SCIP_CALL( SCIPallocBufferArray(scip, &vals_x, consdata->nvars) );
      for( i = 0; i < consdata->nvars; ++i )
      {
         vals_x[i] = SCIPgetSolVal(scip, sol, consdata->vars_x[i]);
      }
      SCIP_CALL( SCIPchgReoptObjective(subscip, SCIP_OBJSENSE_MINIMIZE, consdata->vars_y, vals_x, consdata->nvars) );
      SCIPfreeBuffer(scip, &vals_x);
   }
   else
   {
      for( i = 0; i < consdata->nvars; ++i )
      {
         SCIP_CALL( SCIPchgVarObj(subscip, consdata->vars_y[i], SCIPgetSolVal(scip, sol, consdata->vars_x[i])) );
         if( consdata->usefix )
         {
            SCIP_CALL( SCIPchgVarLb(subscip, consdata->vars_y[i], 0.0) );
            SCIP_CALL( SCIPchgVarUb(subscip, consdata->vars_y[i], 1.0) );
         }
      }
   }

   /* set limits */
   SCIP_CALL( SCIPsetRealParam(subscip, "limits/time", timelimit) );
   SCIP_CALL( SCIPsetObjlimit(subscip, 0.99) ); // TODO would a smaller objvalue make sense? Leads to possible stronger cuts.
   SCIP_CALL( SCIPsetIntParam(subscip, "limits/solutions", 1) );

   SCIP_CALL( SCIPsolve(subscip) );

   /* update statistics */
   consdata->nsubscipsolves++;
   consdata->timesubscipsolves += SCIPgetSolvingTime(subscip);
   consdata->nnodessubscip += SCIPgetNNodes(subscip);

   subscip_status = SCIPgetStatus(subscip);
   switch( subscip_status )
   {
   case SCIP_STATUS_USERINTERRUPT:
      SCIPwarningMessage(scip, "User interrupt during subscip solving. Result may be infeasible.\n");
      SCIP_CALL( SCIPinterruptSolve(scip) );
      break;
   case SCIP_STATUS_TIMELIMIT:
   case SCIP_STATUS_INFEASIBLE:
   case SCIP_STATUS_OPTIMAL:
   case SCIP_STATUS_SOLLIMIT:
      break;
   default:
      SCIPerrorMessage("subscip was solved to unsupported status.\n");
      return SCIP_ERROR;
   }

   /* add a cut if we found one */
   if( SCIPgetNSols(subscip) >= 1 && SCIPisLT(scip, SCIPgetSolOrigObj(subscip, SCIPgetBestSol(subscip)), 0.99) )
   {
      SCIP_Real* vals_y;

      SCIP_CALL( SCIPallocBufferArray(scip, &vals_y, consdata->nvars) );
      SCIP_CALL( SCIPgetSolVals(subscip, SCIPgetBestSol(subscip), consdata->nvars, consdata->vars_y, vals_y) );

      /* call, since improve cut needs a clean problem */
      if( consdata->usereopt )
         SCIP_CALL( SCIPfreeReoptSolve(subscip) );
      else
         SCIP_CALL( SCIPfreeTransform(subscip) );

      SCIP_CALL( SCIPimproveCutResolve(scip, cons, vals_y) );

      SCIP_CALL( saveSubscipSol(scip, cons, vals_y) );
      SCIP_CALL( updateCoverRanking(cons, vals_y) );
      SCIP_CALL( addRowCut(scip, cons, vals_y, sol, result) );

      SCIPfreeBufferArray(scip, &vals_y);

      consdata->ncutsadded++;
   }

   /* free subscip */
   if( consdata->usereopt )
      SCIP_CALL( SCIPfreeReoptSolve(subscip) );
   else
      SCIP_CALL( SCIPfreeTransform(subscip) );

   return SCIP_OKAY;
}

/* go through the cons_interdiction constraints and separate x\T y >= 1 for y in subscip.
 */
static
SCIP_RETCODE separateConstraints(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONSHDLR*        conshdlr,           /**< pump constraint handler */
   SCIP_CONS*const*      conss,              /**< constraints to process */
   int                   nconss,             /**< number of constraints */
   SCIP_SOL*             sol,                /**< solution to separate (NULL for the LP solution) */
   SCIP_RESULT*          result              /**< pointer to store the result (should be initialized) */
   )
{
   int c;

   assert( scip != NULL );
   assert( conshdlr != NULL );
   assert( strcmp(SCIPconshdlrGetName(conshdlr), CONSHDLR_NAME) == 0 );
   assert( result != NULL );

   *result = SCIP_DIDNOTFIND;

   /* loop through constraints */
   for (c = 0; c < nconss && *result != SCIP_CUTOFF; c++)
   {
      assert( conss[c] != NULL );

      SCIP_CALL( separateCoverInequality(scip, conss[c], sol, result) );
   }

   assert(*result == SCIP_DIDNOTFIND || *result == SCIP_SEPARATED || *result == SCIP_CUTOFF);

   return SCIP_OKAY;
}


/*
 * Callback methods of constraint handler
 */

/** frees specific constraint data */
static
SCIP_DECL_CONSDELETE(consDeleteInterdiction)
{  /*lint --e{715}*/
   int i;
   assert(consdata != NULL);

   for( i = 0; i < (*consdata)->nvars; ++i )
   {
      SCIP_CALL( SCIPreleaseVar(scip, &(*consdata)->vars_x[i]) );
      SCIP_CALL( SCIPreleaseVar((*consdata)->subscip, &(*consdata)->vars_y[i]) );
   }

   SCIPfreeBlockMemoryArray(scip, &(*consdata)->ncovers, (*consdata)->nvars);
   SCIPfreeBlockMemoryArray(scip, &(*consdata)->coverrankingtovar, (*consdata)->nvars);
   SCIPfreeBlockMemoryArray(scip, &(*consdata)->vartocoverranking, (*consdata)->nvars);

   SCIPfreeBlockMemoryArray(scip, &(*consdata)->vars_x, (*consdata)->nvars);
   SCIPfreeBlockMemoryArray(scip, &(*consdata)->vars_y, (*consdata)->nvars);

   for( i = 0; i < (*consdata)->nsubscipsols; ++i )
   {
      SCIPfreeBlockMemoryArray(scip, &(*consdata)->subscipsols[i], (*consdata)->nvars);
   }
   SCIPfreeBlockMemoryArray(scip, &(*consdata)->subscipsols, (*consdata)->sizesubscipsols);
   SCIPfreeBlockMemoryArray(scip, &(*consdata)->subscipsoladded, (*consdata)->sizesubscipsols);

   for( i = 0; i < (*consdata)->nscipsols; ++i )
   {
      SCIPfreeBlockMemoryArray(scip, &(*consdata)->scipsols[i], (*consdata)->nvars);
   }
   SCIPfreeBlockMemoryArray(scip, &(*consdata)->scipsols, (*consdata)->sizescipsols);

   /* print a statistic */
   printf("# subscip solves %d \n", (*consdata)->nsubscipsolves);
   printf("Total time subscip solving %f \n", (*consdata)->timesubscipsolves);
   printf("Total nodes subscip solving %lld \n", (*consdata)->nnodessubscip);

   SCIP_CALL( SCIPfree(&(*consdata)->subscip) );

   SCIPfreeBlockMemory(scip, consdata);

   return SCIP_OKAY;
}


/** separation method of constraint handler for LP solutions */
static
SCIP_DECL_CONSSEPALP(consSepalpInterdiction)
{  /*lint --e{715}*/
   assert( scip != NULL );
   assert( result != NULL );

   SCIPdebugMsg(scip, "Separation of constraint handler <%s> for LP solution.\n", SCIPconshdlrGetName(conshdlr));

   *result = SCIP_DIDNOTFIND;

   /* separate constraints */
   SCIP_CALL( separateConstraints(scip, conshdlr, conss, nconss, NULL, result) );

   assert(*result == SCIP_DIDNOTFIND || *result == SCIP_SEPARATED || *result == SCIP_CUTOFF);

   return SCIP_OKAY;
}


/** separation method of constraint handler for arbitrary primal solutions */
static
SCIP_DECL_CONSSEPASOL(consSepasolInterdiction)
{  /*lint --e{715}*/
   assert( scip != NULL );
   assert( result != NULL );

   SCIPdebugMsg(scip, "Separation of pump constraint handler <%s> for primal solution.\n", SCIPconshdlrGetName(conshdlr));

   *result = SCIP_DIDNOTFIND;

   /* separate constraints */
   SCIP_CALL( separateConstraints(scip, conshdlr, conss, nconss, sol, result) );

   assert(*result == SCIP_DIDNOTFIND || *result == SCIP_SEPARATED || *result == SCIP_CUTOFF);

   return SCIP_OKAY;
}


/** constraint enforcing method of constraint handler for LP solutions */
static
SCIP_DECL_CONSENFOLP(consEnfolpInterdiction)
{  /*lint --e{715}*/
   int c;

   assert( scip != NULL );
   assert( conshdlr != NULL );
   assert( strcmp(SCIPconshdlrGetName(conshdlr), CONSHDLR_NAME) == 0 );
   assert( result != NULL );

   *result = SCIP_FEASIBLE;

   /* loop through constraints */
   for( c = 0; c < nconss && *result == SCIP_FEASIBLE; ++c)
   {
      assert( conss[c] != NULL );

      SCIP_CALL( checkFeasibility(scip, conss[c], NULL, TRUE, FALSE, result) );
   }
   assert(*result == SCIP_FEASIBLE || *result == SCIP_CONSADDED || *result == SCIP_INFEASIBLE || *result == SCIP_CUTOFF || *result == SCIP_BRANCHED);

   return SCIP_OKAY;
}


/** constraint enforcing method of constraint handler for relaxation solutions */
static
SCIP_DECL_CONSENFORELAX(consEnforelaxInterdiction)
{  /*lint --e{715}*/
   int c;

   assert( scip != NULL );
   assert( conshdlr != NULL );
   assert( strcmp(SCIPconshdlrGetName(conshdlr), CONSHDLR_NAME) == 0 );
   assert( result != NULL );

   *result = SCIP_FEASIBLE;

   /* loop through constraints */
   for( c = 0; c < nconss && *result == SCIP_FEASIBLE; ++c )
   {
      assert( conss[c] != NULL );

      SCIP_CALL( checkFeasibility(scip, conss[c], sol, TRUE, FALSE, result) );
   }
   assert(*result == SCIP_FEASIBLE || *result == SCIP_CONSADDED || *result == SCIP_INFEASIBLE || *result == SCIP_CUTOFF || *result == SCIP_BRANCHED);

   return SCIP_OKAY;
}


/** constraint enforcing method of constraint handler for pseudo solutions */
static
SCIP_DECL_CONSENFOPS(consEnfopsInterdiction)
{  /*lint --e{715}*/
   int c;

   assert( scip != NULL );
   assert( conshdlr != NULL );
   assert( strcmp(SCIPconshdlrGetName(conshdlr), CONSHDLR_NAME) == 0 );
   assert( result != NULL );

   *result = SCIP_FEASIBLE;

   /* loop through constraints */
   for( c = 0; c < nconss && *result == SCIP_FEASIBLE; ++c )
   {
      assert( conss[c] != NULL );

      SCIP_CALL( checkFeasibility(scip, conss[c], NULL, TRUE, FALSE, result) );
   }
   assert(*result == SCIP_FEASIBLE || *result == SCIP_CONSADDED || *result == SCIP_INFEASIBLE);

   return SCIP_OKAY;
}


/** feasibility check method of constraint handler for integral solutions */
static
SCIP_DECL_CONSCHECK(consCheckInterdiction)
{  /*lint --e{715}*/
   int c;

   assert( scip != NULL );
   assert( conshdlr != NULL );
   assert( strcmp(SCIPconshdlrGetName(conshdlr), CONSHDLR_NAME) == 0 );
   assert( result != NULL );

   *result = SCIP_FEASIBLE;

   /* loop through constraints */
   for( c = 0; c < nconss && *result == SCIP_FEASIBLE; ++c )
   {
      assert( conss[c] != NULL );

      SCIP_CALL( checkFeasibility(scip, conss[c], sol, FALSE, printreason, result) );
   }
   assert(*result == SCIP_FEASIBLE || *result == SCIP_INFEASIBLE);

   return SCIP_OKAY;
}

/** variable rounding lock method of constraint handler */
static
SCIP_DECL_CONSLOCK(consLockInterdiction)
{  /*lint --e{715}*/
   SCIP_CONSDATA* consdata;
   int i;

   consdata = SCIPconsGetData(cons);
   assert(consdata != NULL);

   /* rounding down a variable makes the problem potentially infeasible */
   /* rounding up is ok, it can make our covering inequalities only more feasible */
   for( i = 0; i < consdata->nvars; ++i )
   {
      SCIP_CALL( SCIPaddVarLocksType(scip, consdata->vars_x[i], SCIP_LOCKTYPE_MODEL, nlockspos, nlocksneg) );
   }

   return SCIP_OKAY;
}

/** constraint display method of constraint handler */
static
SCIP_DECL_CONSPRINT(consPrintInterdiction)
{  /*lint --e{715}*/
   SCIP_CONSDATA* consdata;
   int i;

   consdata = SCIPconsGetData(cons);
   assert(consdata != NULL);

   SCIPinfoMessage(scip, file, "Linking Variables: ");
   for( i = 0; i < consdata->nvars; ++i )
      SCIPinfoMessage(scip, file, "<%s> interdicts <%s> ", SCIPvarGetName(consdata->vars_x[i]), SCIPvarGetName(consdata->vars_y[i]));

   return SCIP_OKAY;
}


/** constraint init method of constraint handler */
static
SCIP_DECL_CONSINIT(consInitInterdiction)
{  /*lint --e{715}*/
   SCIP_CONSDATA* consdata;
   int i;

   for( i = 0; i < nconss; ++i )
   {
      consdata = SCIPconsGetData(conss[i]);
      assert(consdata != NULL);
      consdata->ncutsadded = 0;
   }

   return SCIP_OKAY;
}

/*
 * constraint specific interface methods
 */

/** creates the handler for interdiction constraints and includes it in SCIP */
SCIP_RETCODE SCIPincludeConshdlrInterdiction(
   SCIP*                 scip                /**< SCIP data structure */
   )
{
   SCIP_CONSHDLRDATA* conshdlrdata = NULL;
   SCIP_CONSHDLR* conshdlr = NULL;

   /* include constraint handler */
   SCIP_CALL( SCIPincludeConshdlrBasic(scip, &conshdlr, CONSHDLR_NAME, CONSHDLR_DESC,
         CONSHDLR_ENFOPRIORITY, CONSHDLR_CHECKPRIORITY, CONSHDLR_EAGERFREQ, CONSHDLR_NEEDSCONS,
         consEnfolpInterdiction, consEnfopsInterdiction, consCheckInterdiction, consLockInterdiction,
         conshdlrdata) );
   assert(conshdlr != NULL);

   /* set non-fundamental callbacks via specific setter functions */
   SCIP_CALL( SCIPsetConshdlrDelete(scip, conshdlr, consDeleteInterdiction) );
   SCIP_CALL( SCIPsetConshdlrPrint(scip, conshdlr, consPrintInterdiction) );
   SCIP_CALL( SCIPsetConshdlrSepa(scip, conshdlr, consSepalpInterdiction, consSepasolInterdiction, CONSHDLR_SEPAFREQ, CONSHDLR_SEPAPRIORITY, CONSHDLR_DELAYSEPA) );
   SCIP_CALL( SCIPsetConshdlrEnforelax(scip, conshdlr, consEnforelaxInterdiction) );
   SCIP_CALL( SCIPsetConshdlrInit(scip, conshdlr, consInitInterdiction) );

   /* add params */
   SCIP_CALL( SCIPaddLongintParam(scip, "cons_interdiction/cutlimit",
         "limit for adding cuts, afterwards we branch (-1.0: disabled)",
         NULL, FALSE, DEFAULT_CUTLIMIT, -1LL, SCIP_LONGINT_MAX, NULL, NULL) );

   SCIP_CALL( SCIPaddLongintParam(scip, "cons_interdiction/nodesresolveimprove",
         "node limit for the cut improvement (-1.0: disabled)",
         NULL, FALSE, DEFAULT_RESOLVENODELIMIT, -1LL, SCIP_LONGINT_MAX, NULL, NULL) );

   return SCIP_OKAY;
}

/** creates and captures a constraint
 *
 *  @note the constraint gets captured, hence at one point you have to release it using the method SCIPreleaseCons()
 */
SCIP_RETCODE SCIPcreateConsInterdiction(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONS**           cons,               /**< pointer to hold the created constraint */
   const char*           name,               /**< name of constraint */
   int                   nvars,              /**< number of variables which have to be treated */
   SCIP_VAR**            vars_x,             /**< variables in scip */
   SCIP_VAR**            vars_y,             /**< variables in subscip */
   SCIP*                 subscip,            /**< SCIP data structure for the subproblem */
   SCIP_Bool             usereopt,           /**< should reoptimization be used to solve the subscip */
   SCIP_Bool             usefix,             /**< if reoptimization is not used, should variable fixing be used */
   SCIP_Bool             initial,            /**< should the LP relaxation of constraint be in the initial LP?
                                              *   Usually set to TRUE. Set to FALSE for 'lazy constraints'. */
   SCIP_Bool             separate,           /**< should the constraint be separated during LP processing?
                                              *   Usually set to TRUE. */
   SCIP_Bool             enforce,            /**< should the constraint be enforced during node processing?
                                              *   TRUE for model constraints, FALSE for additional, redundant constraints. */
   SCIP_Bool             check,              /**< should the constraint be checked for feasibility?
                                              *   TRUE for model constraints, FALSE for additional, redundant constraints. */
   SCIP_Bool             propagate,          /**< should the constraint be propagated during node processing?
                                              *   Usually set to TRUE. */
   SCIP_Bool             local,              /**< is constraint only valid locally?
                                              *   Usually set to FALSE. Has to be set to TRUE, e.g., for branching constraints. */
   SCIP_Bool             modifiable,         /**< is constraint modifiable (subject to column generation)?
                                              *   Usually set to FALSE. In column generation applications, set to TRUE if pricing
                                              *   adds coefficients to this constraint. */
   SCIP_Bool             dynamic,            /**< is constraint subject to aging?
                                              *   Usually set to FALSE. Set to TRUE for own cuts which
                                              *   are separated as constraints. */
   SCIP_Bool             removable,          /**< should the relaxation be removed from the LP due to aging or cleanup?
                                              *   Usually set to FALSE. Set to TRUE for 'lazy constraints' and 'user cuts'. */
   SCIP_Bool             stickingatnode      /**< should the constraint always be kept at the node where it was added, even
                                              *   if it may be moved to a more global node?
                                              *   Usually set to FALSE. Set to TRUE to for constraints that represent node data. */
   )
{
   SCIP_CONSHDLR* conshdlr;
   SCIP_CONSDATA* consdata;

   /* find the interdiction constraint handler */
   conshdlr = SCIPfindConshdlr(scip, CONSHDLR_NAME);
   if( conshdlr == NULL )
   {
      SCIPerrorMessage("constraint handler not found\n");
      return SCIP_PLUGINNOTFOUND;
   }

   assert(scip != NULL);
   assert(cons != NULL);
   assert(name != NULL);
   assert(nvars > 0);
   assert(vars_x != NULL);
   assert(subscip != NULL);

   /* create constraint data */
   SCIP_CALL( consdataCreate(scip, &consdata, nvars, vars_x, vars_y, subscip, usereopt, usefix) );

   /* create constraint */
   SCIP_CALL( SCIPcreateCons(scip, cons, name, conshdlr, consdata, initial, separate, enforce, check, propagate,
         local, modifiable, dynamic, removable, stickingatnode) );

   return SCIP_OKAY;
}

/** creates and captures a constraint with all its constraint flags set to their
 *  default values
 *
 *  @note the constraint gets captured, hence at one point you have to release it using the method SCIPreleaseCons()
 */
SCIP_RETCODE SCIPcreateConsBasicInterdiction(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_CONS**           cons,               /**< pointer to hold the created constraint */
   const char*           name,               /**< name of constraint */
   int                   nvars,              /**< number of variables which have to be treated */
   SCIP_VAR**            vars_x,             /**< variables in scip */
   SCIP_VAR**            vars_y,             /**< variables in subscip */
   SCIP*                 subscip,            /**< SCIP data structure for the subproblem */
   SCIP_Bool             usereopt,           /**< should reoptimization be used to solve the subscip */
   SCIP_Bool             usefix              /**< if reoptimization is not used, should variable fixing be used */
   )
{
   SCIP_CALL( SCIPcreateConsInterdiction(scip, cons, name, nvars, vars_x, vars_y, subscip, usereopt, usefix, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, TRUE) );

   return SCIP_OKAY;
}
