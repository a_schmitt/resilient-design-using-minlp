/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*    This file is part of the program kresilience                           */
/*                                                                           */
/*    Copyright (C) 2020-2021 Andreas Schmitt, Marc Pfetsch                  */
/*                                                                           */
/*                                                                           */
/*    Based on SCIP --- Solving Constraint Integer Programs                  */
/*       SCIP is distributed under the terms of the SCIP Academic Licence,   */
/*       see file COPYING in the SCIP distribution.                          */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**@file   heur_maxtruss.h
 * @brief  checks whether the truss with all the greatest diameter bars is feasible
 * @author Andreas Schmitt
 */

/*---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0----+----1----+----2*/

#ifndef __SCIP_HEUR_MAXTTRUSS_H__
#define __SCIP_HEUR_MAXTTRUSS_H__


#include "scip/scip.h"
#include "scip/cons_setppc.h"

#ifdef __cplusplus
extern "C" {
#endif


/** Checks the problem for structure corresponding to a truss and identifies
    variables for bars, and their interrelationsships as well as slack
    variables.

    This method needs to be called, in order for the heuristic to run.
  */
SCIP_EXPORT
SCIP_RETCODE heurMaxTrussIdentifyAndSetHeurdata(
   SCIP*                 scip                /**< SCIP data structure */
   );

/** creates the heuristic and includes it in SCIP */
SCIP_EXPORT
SCIP_RETCODE SCIPincludeHeurMaxTruss(
   SCIP*                 scip                /**< SCIP data structure */
   );

#ifdef __cplusplus
}
#endif

#endif
